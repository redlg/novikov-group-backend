<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->call(DeliveryOrderSeeder::class);
        $this->call(CompilationsSeeder::class);
        $this->call(EventTagSeeder::class);
    }
}
