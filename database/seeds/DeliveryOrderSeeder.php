<?php

use App\Models\Address;
use App\Models\Device;
use App\Models\Guest;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DeliveryOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    /**
	     * Fill the DB for the API testing
	     */
	    $guest = new Guest();
	    $guest->phone = '999999';
	    $guest->email = '999999@gmail.com';
	    $guest->first_name = 'Mobile user';
	    $guest->save();

	    $address = new Address();
	    $address->addressable_id = $guest->id;
	    $address->addressable_type = Guest::class;
	    $address->text_ru = 'Мобильная 100Z';
	    $address->city = 'Москва';
	    $address->street = 'Мобильная';
	    $address->building = '10Z';
	    $address->apartment = 100;
	    $address->floor = 10;
		$address->save();

	    $device = new Device();
	    $device->guest_id = $guest->id;
	    $device->device_id = '21da2-fda2x-fas2da-f2t5g-1sasgg';
	    $device->save();

	    $in_process_id = DB::table('delivery_orders')->insertGetId([
		    'guest_id'                   => $guest->id,
		    'address_id'                 => $address->id,
		    'restaurant_id'              => 5,
		    'delivery_payment_method_id' => 1,
		    'delivery_method_id'         => 1,
		    'delivered_at'               => Carbon::now()->addMinutes(90),
		    'number_of_persons'          => 2,
		    'sum'                        => 4000,
		    'created_at'                 => Carbon::now(),
		    'updated_at'                 => Carbon::now(),
	    ]);

	    $completed_id = DB::table('delivery_orders')->insertGetId([
		    'guest_id'                   => $guest->id,
		    'address_id'                 => $address->id,
		    'restaurant_id'              => 8,
		    'delivery_payment_method_id' => 1,
		    'delivery_method_id'         => 1,
		    'delivered_at'               => Carbon::now()->addMinutes(90),
		    'completed_at'               => Carbon::now()->subMinutes(75),
		    'number_of_persons'          => 2,
		    'sum'                        => 4000,
		    'created_at'                 => Carbon::now(),
		    'updated_at'                 => Carbon::now(),
	    ]);

	    $canceled_id = DB::table('delivery_orders')->insertGetId([
		    'guest_id'                   => $guest->id,
		    'address_id'                 => $address->id,
		    'restaurant_id'              => 5,
		    'delivery_payment_method_id' => 1,
		    'delivery_method_id'         => 1,
		    'delivered_at'               => Carbon::now()->addMinutes(90),
		    'canceled_at'                => Carbon::now()->addMinutes(15),
		    'number_of_persons'          => 2,
		    'sum'                        => 4000,
		    'created_at'                 => Carbon::now(),
		    'updated_at'                 => Carbon::now(),
	    ]);

	    DB::table('delivery_dish_delivery_order')->insert([[
		    'delivery_dish_id'  => 3,
		    'delivery_order_id' => $in_process_id,
		    'count'             => 1,
	    ], [
		    'delivery_dish_id'  => 5,
		    'delivery_order_id' => $in_process_id,
		    'count'             => 1,
	    ], [
		    'delivery_dish_id'  => 7,
		    'delivery_order_id' => $in_process_id,
		    'count'             => 3,
	    ], [
		    'delivery_dish_id'  => 3,
		    'delivery_order_id' => $completed_id,
		    'count'             => 1,
	    ], [
		    'delivery_dish_id'  => 5,
		    'delivery_order_id' => $completed_id,
		    'count'             => 1,
	    ], [
		    'delivery_dish_id'  => 7,
		    'delivery_order_id' => $completed_id,
		    'count'             => 3,
	    ], [
		    'delivery_dish_id'  => 3,
		    'delivery_order_id' => $canceled_id,
		    'count'             => 1,
	    ], [
		    'delivery_dish_id'  => 5,
		    'delivery_order_id' => $canceled_id,
		    'count'             => 1,
	    ], [
		    'delivery_dish_id'  => 7,
		    'delivery_order_id' => $canceled_id,
		    'count'             => 3,
	    ]]);
    }
}
