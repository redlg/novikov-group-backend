<?php

use App\Models\DeliveryDish;
use App\Models\Restaurant;
use Illuminate\Database\Seeder;

class CompilationsSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('tags')->insert([[
			'name_ru' => 'Рекомендуем',
			'name_en' => 'Recommend',
		], [
			'name_ru' => 'Мясной',
			'name_en' => 'Meat',
		], [
			'name_ru' => 'Рыбный',
			'name_en' => 'Fish',
		], [
			'name_ru' => 'Бургер',
			'name_en' => 'Burger',
		], [
			'name_ru' => 'Сладости',
			'name_en' => 'Sweets',
		], [
			'name_ru' => 'Азия',
			'name_en' => 'Asia',
		], [
			'name_ru' => 'Новинка',
			'name_en' => 'New',
		]]);

		$sirena = Restaurant::whereExternalId('16')->first();
		$kolbasniy = Restaurant::whereExternalId('238')->first();
		$bar_grill = Restaurant::whereExternalId('233')->first();
		$chips = Restaurant::whereExternalId('22')->first();
		$brisket = Restaurant::whereExternalId('199')->first();

		DB::table('taggables')->insert([[
			'tag_id'        => 1,
			'taggable_id'   => $sirena->id,
			'taggable_type' => Restaurant::class,
		], [
			'tag_id'        => 1,
			'taggable_id'   => $chips->id,
			'taggable_type' => Restaurant::class,
		], [
			'tag_id'        => 1,
			'taggable_id'   => $brisket->id,
			'taggable_type' => Restaurant::class,
		], [
			'tag_id'        => 2,
			'taggable_id'   => $bar_grill->id,
			'taggable_type' => Restaurant::class,
		], [
			'tag_id'        => 2,
			'taggable_id'   => $kolbasniy->id,
			'taggable_type' => Restaurant::class,
		], [
			'tag_id'        => 2,
			'taggable_id'   => $sirena->id,
			'taggable_type' => Restaurant::class,
		], [
			'tag_id'        => 3,
			'taggable_id'   => $sirena->id,
			'taggable_type' => Restaurant::class,
		]]);

		DB::table('restaurants_compilations')->insert([[
			'name' => 'По совету Новикова',
		], [
			'name' => 'Не для вегетарианцев',
		]]);

		DB::table('restaurants_compilation_tag')->insert([[
			'restaurants_compilation_id' => 1,
			'tag_id'                    => 1,
		], [
			'restaurants_compilation_id' => 2,
			'tag_id'                    => 2,
		], [
			'restaurants_compilation_id' => 2,
			'tag_id'                    => 3,
		]]);

		$china = Restaurant::whereExternalId('19')->with(['delivery_categories'])->first();
		$china_dishes = DeliveryDish::whereIn('delivery_category_id', $china->delivery_categories->pluck('id'))->get();
		$sirena_dishes = DeliveryDish::whereIn('delivery_category_id', $sirena->delivery_categories->pluck('id'))->get();
		$yoko = Restaurant::whereExternalId('66')->with(['delivery_categories'])->first();
		$yoko_dishes = DeliveryDish::whereIn('delivery_category_id', $yoko->delivery_categories->pluck('id'))->get();
		$kavkaz = Restaurant::whereExternalId('36')->with(['delivery_categories'])->first();
		$kavkaz_dishes = DeliveryDish::whereIn('delivery_category_id', $kavkaz->delivery_categories->pluck('id'))->get();
		$market = Restaurant::whereExternalId('43')->with(['delivery_categories'])->first();
		$market_dishes = DeliveryDish::whereIn('delivery_category_id', $market->delivery_categories->pluck('id'))->get();


		DB::table('taggables')->insert([[
			'tag_id'        => 7,
			'taggable_id'   => $sirena_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $sirena_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $sirena_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $sirena_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 5,
			'taggable_id'   => $sirena_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 5,
			'taggable_id'   => $sirena_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $china_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $china_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $china_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $china_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $china_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $yoko_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $yoko_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 6,
			'taggable_id'   => $yoko_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $yoko_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $yoko_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $kavkaz_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $kavkaz_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 7,
			'taggable_id'   => $kavkaz_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 4,
			'taggable_id'   => $market_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		], [
			'tag_id'        => 4,
			'taggable_id'   => $market_dishes->random()->id,
			'taggable_type' => DeliveryDish::class,
		]]);

		DB::table('delivery_dishes_compilations')->insert([[
			'restaurant_id' => $yoko->id,
			'name'          => 'По совету шеф-повара',
		], [
			'restaurant_id' => $sirena->id,
			'name'          => 'На десерт',
		], [
			'restaurant_id' => $sirena->id,
			'name'          => 'Новинки осени',
		], [
			'restaurant_id' => $china->id,
			'name'          => 'Пикантные блюда',
		], [
			'restaurant_id' => $market->id,
			'name'          => 'Фаст-фуд',
		], [
			'restaurant_id' => $kavkaz->id,
			'name'          => 'Самые продаваемые',
		]]);

		DB::table('delivery_dishes_compilation_tag')->insert([[
			'delivery_dishes_compilation_id' => 4,
			'tag_id'                       => 6,
		], [
			'delivery_dishes_compilation_id' => 3,
			'tag_id'                       => 7,
		], [
			'delivery_dishes_compilation_id' => 2,
			'tag_id'                       => 5,
		], [
			'delivery_dishes_compilation_id' => 1,
			'tag_id'                       => 6,
		], [
			'delivery_dishes_compilation_id' => 1,
			'tag_id'                       => 7,
		], [
			'delivery_dishes_compilation_id' => 6,
			'tag_id'                       => 7,
		], [
			'delivery_dishes_compilation_id' => 5,
			'tag_id'                       => 4,
		]]);
	}
}