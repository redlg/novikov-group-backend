<?php

use Illuminate\Database\Seeder;
use App\Models\Event;

class EventTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('tags')->insert([[
		    'name_ru' => 'Для детей',
		    'name_en' => 'For kids',
	    ], [
		    'name_ru' => 'Подарок',
		    'name_en' => 'Gift',
	    ], [
		    'name_ru' => 'Знаменитость',
		    'name_en' => 'Celebrity',
	    ], [
		    'name_ru' => 'Новинки',
		    'name_en' => 'New',
	    ]]);

	    DB::table('taggables')->insert([[
		    'tag_id'        => 8,
		    'taggable_id'   => 1,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 9,
		    'taggable_id'   => 1,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 10,
		    'taggable_id'   => 1,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 11,
		    'taggable_id'   => 2,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 11,
		    'taggable_id'   => 3,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 11,
		    'taggable_id'   => 4,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 8,
		    'taggable_id'   => 4,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 8,
		    'taggable_id'   => 5,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 9,
		    'taggable_id'   => 6,
		    'taggable_type' => Event::class,
	    ], [
		    'tag_id'        => 10,
		    'taggable_id'   => 7,
		    'taggable_type' => Event::class,
	    ]]);
    }
}
