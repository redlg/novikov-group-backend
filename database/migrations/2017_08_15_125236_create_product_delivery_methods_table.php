<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDeliveryMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_delivery_methods', function (Blueprint $table) {
            $table->increments('id');
			$table->string('delivery_group')->nullable();
			$table->string('name');
			$table->float('price')->default(0);
			$table->boolean('show_address')->default(false);
			$table->string('external_id')->nullable();
			$table->string('temp_id', 10)->nullable();

	        $table->index('external_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_delivery_methods');
    }
}
