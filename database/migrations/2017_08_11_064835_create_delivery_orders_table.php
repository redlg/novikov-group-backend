<?php

use App\Models\DeliveryOrder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('guest_id');
            $table->unsignedInteger('address_id')->nullable(); //самовывоз
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('delivery_payment_method_id');
            $table->unsignedInteger('delivery_method_id');
            $table->unsignedInteger('discount_card_id')->nullable();
            $table->unsignedInteger('certificate_id')->nullable();
            $table->dateTimeTz('delivered_at')->nullable();
            $table->dateTimeTz('canceled_at')->nullable();
            $table->dateTimeTz('completed_at')->nullable();
            $table->integer('number_of_persons');
            $table->float('sum');
            $table->text('comment')->nullable();
            $table->string('external_id')->nullable();

            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('delivery_payment_method_id')->references('id')->on('delivery_payment_methods');
            $table->foreign('delivery_method_id')->references('id')->on('delivery_methods');
            $table->foreign('discount_card_id')->references('id')->on('discount_cards');
            $table->foreign('certificate_id')->references('id')->on('certificates');

            $table->index('guest_id');
            $table->index('restaurant_id');
            $table->index('external_id');

            $table->softDeletes();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_orders');
    }
}
