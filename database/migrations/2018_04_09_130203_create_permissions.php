<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;

class CreatePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    $permissions = [
		    Permission::USERS_EDIT          => 'Редактирование пользователей',
		    Permission::RESTAURANTS_EDIT    => 'Редактирование ресторанов и связанных данных',
		    Permission::RESTAURANTS_CREATE  => 'Создание и удаление ресторанов',
		    Permission::PARTNERS_EDIT       => 'Редактирование партнеров',
		    Permission::PROJECTS_EDIT       => 'Редактирование проектов',
		    Permission::EVENTS_EDIT         => 'Редактирование событий',
		    Permission::TAGS_EDIT           => 'Редактирование тегов',
		    Permission::CARDS_EDIT          => 'Редактирование карт',
		    Permission::CARDS_EXPORT        => 'Экспорт отчета по картам',
		    Permission::GUESTS_EDIT         => 'Редактирование гостей',
		    Permission::GUESTS_EXPORT       => 'Экпорт отчета по гостям',
		    Permission::OPERATIONS_READ     => 'Просмотр операций и транзакций',
		    Permission::OPERATIONS_EXPORT   => 'Экспорт отчета по операциям или транзакциям',
		    Permission::LEGAL_ENTITIES_EDIT => 'Редактирование юридических лиц',
		    Permission::COMPILATIONS_EDIT   => 'Редактирование подборок',
		    Permission::PRODUCTS_EDIT       => 'Редактирование заказов на билеты и карты',
		    Permission::IMAGES_EDIT         => 'Редактирование изображений',
		    Permission::ADDRESSES_EDIT      => 'Редактирование адресов',
	    ];

	    $created_permissions = [];
	    foreach($permissions as $name => $desc) {
		    $permission = Permission::forceCreate([
			    'name'         => $name,
			    'display_name' => $desc,
		    ]);
		    $created_permissions[$permission->name] = $permission->id;
	    }

	    //добавляем админа и прикрепляем к нему роль и всевозможные права
        $admin = new User();
	    $admin->name = User::ADMIN_NAME;
	    $admin->email = 'info@novikov.ru';
	    $admin->password = Hash::make('Administrator');
	    $admin->save();
	    $admin_role = Role::whereName(Role::ADMIN)->first();
	    $admin_role->attachPermissions(array_values($created_permissions));
	    $admin->attachRole($admin_role);

	    //добавляем определенные права менеджеру ресторанов
	    $manager_permissions = [
	    	$created_permissions[Permission::RESTAURANTS_EDIT],
	    	$created_permissions[Permission::OPERATIONS_READ],
	    	$created_permissions[Permission::GUESTS_EDIT],
	    	$created_permissions[Permission::IMAGES_EDIT],
	    	$created_permissions[Permission::ADDRESSES_EDIT],
	    	$created_permissions[Permission::TAGS_EDIT],
	    ];
	    Role::whereName(Role::RESTAURANT_MANAGER)->first()->attachPermissions($manager_permissions);

	    //добавляем определенные права маркетологу
	    $marketer_permissions = [
		    $created_permissions[Permission::GUESTS_EDIT],
		    $created_permissions[Permission::GUESTS_EXPORT],
		    $created_permissions[Permission::EVENTS_EDIT],
		    $created_permissions[Permission::PARTNERS_EDIT],
		    $created_permissions[Permission::PROJECTS_EDIT],
		    $created_permissions[Permission::IMAGES_EDIT],
		    $created_permissions[Permission::ADDRESSES_EDIT],
		    $created_permissions[Permission::TAGS_EDIT],
	    ];
	    Role::whereName(Role::MARKETER)->first()->attachPermissions($marketer_permissions);

	    //добавляем определенные права бухгалеру/финансисту
	    $accountant_permissions = [
		    $created_permissions[Permission::OPERATIONS_READ],
		    $created_permissions[Permission::OPERATIONS_EXPORT],
		    $created_permissions[Permission::CARDS_EDIT],
		    $created_permissions[Permission::CARDS_EXPORT],
	    ];
	    Role::whereName(Role::ACCOUNTANT)->first()->attachPermissions($accountant_permissions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
