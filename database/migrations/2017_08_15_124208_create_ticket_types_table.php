<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_types', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->string('name');
            $table->float('price')->default(0);
            $table->boolean('not_fix_price')->default(false);
            $table->integer('in_stock');
            $table->integer('available');
            $table->string('external_id')->nullable();
            $table->string('temp_id', 10)->nullable();

            $table->foreign('event_id')->references('id')->on('events');

            $table->index('external_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_types');
    }
}
