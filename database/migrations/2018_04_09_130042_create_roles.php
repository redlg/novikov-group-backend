<?php

use App\Models\Role;
use Illuminate\Database\Migrations\Migration;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    $roles = [
		    Role::ADMIN              => 'Администратор',
		    Role::RESTAURANT_MANAGER => 'Менеджер ресторана',
		    Role::MARKETER           => 'Маркетолог',
		    Role::ACCOUNTANT         => 'Бухгалтер/Финансист',
	    ];

	    foreach($roles as $name => $desc) {
		    Role::forceCreate([
			    'name'         => $name,
			    'display_name' => $desc,
		    ]);
	    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
