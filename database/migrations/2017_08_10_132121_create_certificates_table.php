<?php

use App\Models\Abstracts\Card;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('card_type_id');
            $table->unsignedInteger('issuer_id')->nullable();
            $table->unsignedInteger('contractor_id')->nullable();
            $table->unsignedInteger('guest_id')->nullable();
            $table->unsignedInteger('restaurant_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->string('number')->unique();
            $table->float('balance');
            $table->dateTimeTz('started_at')->nullable();
            $table->dateTimeTz('ended_at')->nullable();
            $table->dateTimeTz('blocked_at')->nullable();
            $table->string('status')->default(Card::NOT_ACTIVATED);
	        $table->string('external_id')->nullable();

            $table->foreign('card_type_id')->references('id')->on('certificate_types');
            $table->foreign('issuer_id')->references('id')->on('legal_entities');
            $table->foreign('contractor_id')->references('id')->on('legal_entities');
            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('user_id')->references('id')->on('users');

	        $table->index('external_id');
	        $table->index('number');

            $table->softDeletes();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
