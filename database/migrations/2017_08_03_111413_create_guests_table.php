<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone'); //todo здесь должен быть unique, но в их текущей базе куча пользователей, у которых записана пустая строка
            $table->string('email')->nullable(); //todo здесь должен быть unique, но в их текущей базе куча пользователей, у которых записана пустая строка
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->tinyInteger('sex')->default(0);
            $table->date('birthday_at')->nullable();
            $table->string('code')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
