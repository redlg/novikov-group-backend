<?php

use App\Models\Transaction;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('operation_type_id');
            $table->unsignedInteger('restaurant_id');
            $table->unsignedInteger('payment_method_id');
            $table->unsignedInteger('card_id')->nullable();
            $table->string('card_type')->nullable();
	        $table->unsignedInteger('guest_id')->nullable();
            $table->unsignedInteger('operation_id')->nullable();
            $table->string('card_input_method')->nullable();
            $table->float('sum')->default(0);
            $table->float('discount_sum')->default(0);
            $table->string('check_number');
            $table->string('cashbox_number');

            $table->foreign('operation_type_id')->references('id')->on('operation_types');
            $table->foreign('restaurant_id')->references('id')->on('restaurants');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
	        $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('operation_id')->references('id')->on('operations');

	        $table->index('operation_id');
	        $table->index('check_number');

            $table->softDeletes();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
