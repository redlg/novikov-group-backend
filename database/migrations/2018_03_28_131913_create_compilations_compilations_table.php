<?php

use App\Models\CompilationsCompilation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompilationsCompilationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compilations_compilations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('screen_type');

            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('compilations_compilations')->insert([
	        'name'        => 'Подборка для экрана «Доставка»',
	        'screen_type' => CompilationsCompilation::SCREEN_TYPE_DELIVERY_ID,
	        'created_at'  => Carbon\Carbon::now(),
	        'updated_at'  => Carbon\Carbon::now(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compilations_compilations');
    }
}
