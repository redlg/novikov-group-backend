<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('legal_entity_id');
            $table->unsignedInteger('logo_id');
            $table->unsignedInteger('address_id');
            $table->unsignedInteger('chef_id')->nullable();
            $table->string('name_ru');
            $table->string('name_en');
            $table->string('phone')->nullable();
            $table->string('phone_string')->nullable();
            $table->timeTz('work_time_weekdays_from')->nullable();
            $table->timeTz('work_time_weekdays_to')->nullable();
            $table->timeTz('work_time_friday_from')->nullable();
            $table->timeTz('work_time_friday_to')->nullable();
            $table->timeTz('work_time_saturday_from')->nullable();
            $table->timeTz('work_time_saturday_to')->nullable();
            $table->timeTz('work_time_sunday_from')->nullable();
            $table->timeTz('work_time_sunday_to')->nullable();
            $table->string('work_time_string')->nullable();
            $table->boolean('last_people')->default(0);
            $table->text('description_ru');
            $table->text('description_en')->nullable();
	        $table->text('announce_ru')->nullable();
	        $table->text('announce_en')->nullable();
            $table->boolean('parking')->nullable();
            $table->boolean('delivery')->default(0);
            $table->string('delivery_phone')->nullable();
            $table->integer('min_order_check')->default(0);
            $table->integer('average_check')->default(0);
            $table->string('average_check_string')->nullable();
            $table->integer('number_of_seats')->nullable();
	        $table->string('language_hash', 50)->nullable();
	        $table->string('external_id')->nullable();
            $table->string('resto_id')->nullable();
            $table->string('iiko_id')->nullable();
            $table->string('rkeeper_id')->unique()->nullable();
	        $table->string('temp_id', 10)->nullable();

            $table->foreign('legal_entity_id')->references('id')->on('legal_entities');
            $table->foreign('logo_id')->references('id')->on('images');
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('chef_id')->references('id')->on('chefs');

            $table->index('external_id');
	        $table->index('resto_id');
	        $table->index('iiko_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
