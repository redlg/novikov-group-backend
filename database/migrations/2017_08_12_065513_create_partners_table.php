<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('legal_entity_id')->nullable();
            $table->unsignedInteger('logo_id');
            $table->string('name_ru');
            $table->string('name_en');
            $table->text('description_ru')->nullable();
            $table->text('description_en')->nullable();
	        $table->string('link')->nullable();
            $table->string('phone')->nullable();
            $table->float('discount')->default(0);
            $table->boolean('general')->default(false);
	        $table->string('external_id')->nullable();
	        $table->string('temp_id')->nullable();

            $table->foreign('legal_entity_id')->references('id')->on('legal_entities');
            $table->foreign('logo_id')->references('id')->on('images');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
