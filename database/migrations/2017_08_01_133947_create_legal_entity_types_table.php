<?php

use App\Models\LegalEntityType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalEntityTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_entity_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('legal_entity_types')->insert([[
	        'name'       => LegalEntityType::ISSUER,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => LegalEntityType::COUNTERPARTY,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => LegalEntityType::RESTAURANT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => LegalEntityType::PARTNER,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_entity_types');
    }
}
