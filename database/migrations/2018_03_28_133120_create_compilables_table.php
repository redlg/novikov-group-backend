<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompilablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compilables', function (Blueprint $table) {
	        $table->increments('id');
	        $table->unsignedInteger('compilations_compilation_id');
	        $table->unsignedInteger('compilable_id');
	        $table->string('compilable_type');
	        $table->integer('sort')->default(1000);
	        $table->boolean('geo')->default(false);

	        $table->foreign('compilations_compilation_id')->references('id')->on('compilations_compilations')->onDelete('cascade');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compilables');
    }
}
