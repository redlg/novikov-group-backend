<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('menu_dishes', function (Blueprint $table) {
		    $table->increments('id');
		    $table->unsignedInteger('menu_category_id');
		    $table->text('name_ru'); //в этом поле могу быть очень длинные тексты
		    $table->text('name_en');
		    $table->string('price'); //есть цены типа "500/250"
		    $table->string('external_id')->nullable();
		    $table->string('temp_id', 10)->nullable();

		    $table->foreign('menu_category_id')->references('id')->on('menu_categories')->onDelete('cascade');

		    $table->index('external_id');

		    $table->softDeletes();
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_dishes');
    }
}
