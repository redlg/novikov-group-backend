<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryDishDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_dish_device', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedInteger('delivery_dish_id');
	        $table->unsignedInteger('device_id');

	        $table->foreign('delivery_dish_id')->references('id')->on('delivery_dishes')->onDelete('cascade');
	        $table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_dish_device');
    }
}
