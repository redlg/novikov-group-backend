<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('guest_id');
            $table->unsignedInteger('restaurant_id');
            $table->dateTimeTz('reserved_at');
            $table->integer('number_of_persons');
            $table->text('comment')->nullable();

            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('restaurant_id')->references('id')->on('guests');

	        $table->index('restaurant_id');

            $table->softDeletes();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
