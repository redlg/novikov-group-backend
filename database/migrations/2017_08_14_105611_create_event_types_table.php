<?php

use App\Models\EventType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru');
            $table->string('name_en');

            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('event_types')->insert([[
	        'name_ru'    => EventType::CONCERT,
	        'name_en'    => 'Concert',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name_ru'    => EventType::COURSE,
	        'name_en'    => 'Course',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name_ru'    => EventType::PARTY,
	        'name_en'    => 'Party',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name_ru'    => EventType::CONTEST,
	        'name_en'    => 'Contest',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_types');
    }
}
