<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryDishesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_dishes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('delivery_category_id');
            $table->unsignedInteger('image_id')->nullable();
            $table->string('name_ru');
            $table->string('name_en');
            $table->float('price');
            $table->text('description_ru')->nullable();
            $table->text('description_en')->nullable();
            $table->text('composition_ru')->nullable();
            $table->text('composition_en')->nullable();
            $table->string('weight')->nullable();
            $table->string('volume')->nullable();
            $table->string('external_id')->nullable();
	        $table->string('temp_id', 10)->nullable();

            $table->foreign('delivery_category_id')->references('id')->on('delivery_categories')->onDelete('cascade');
            $table->foreign('image_id')->references('id')->on('images');

            $table->index('external_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_dishes');
    }
}
