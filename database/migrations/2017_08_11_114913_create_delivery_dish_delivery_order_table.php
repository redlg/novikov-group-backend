<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryDishDeliveryOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_dish_delivery_order', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedInteger('delivery_dish_id');
	        $table->unsignedInteger('delivery_order_id');
            $table->integer('count')->default('1');

	        $table->foreign('delivery_dish_id')->references('id')->on('delivery_dishes'); //if we removed dish then keep the relation
	        $table->foreign('delivery_order_id')->references('id')->on('delivery_orders')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_dish_delivery_order');
    }
}
