<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalEntitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_entities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('legal_entity_type_id');
            $table->string('inn');
            $table->string('kpp');
            $table->string('name');
            $table->string('legal_address');
            $table->string('actual_address');
            $table->string('brand')->nullable();
            $table->string('bank')->nullable();
            $table->string('payment_account');
            $table->string('correspondent_account');
            $table->string('bik');
            $table->string('ogrn');
            $table->string('general_manager');
            $table->string('phone');
            $table->string('email');
            $table->string('contact_person');

            $table->foreign('legal_entity_type_id')->references('id')->on('legal_entity_types');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_entities');
    }
}
