<?php

use App\Models\Revision;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('value')->default(0);
            $table->dateTime('date')->nullable();

            $table->index('name');

            $table->timestamps();
        });

        DB::table('revisions')->insert([[
	        'name' => Revision::CERTIFICATES,
	        'date'       => new Carbon('2000-01-01 00:00:01'), //some very old date
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name' => Revision::DISCOUNT_CARDS,
	        'date'       => new Carbon('2000-01-01 00:00:01'),
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name' => Revision::NOVIKOV_RESTAURANTS_RU,
	        'date'       => new Carbon('2000-01-01 00:00:01'),
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name' => Revision::NOVIKOV_RESTAURANTS_EN,
	        'date'       => new Carbon('2000-01-01 00:00:01'),
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
           'name' => Revision::NOVIKOV_DELIVERY_DISHES,
           'date'       => new Carbon('2000-01-01 00:00:01'),
           'created_at' => Carbon::now(),
           'updated_at' => Carbon::now(),
        ], [
	        'name' => Revision::NOVIKOV_EVENTS,
	        'date'       => new Carbon('2000-01-01 00:00:01'),
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name' => Revision::NOVIKOV_PROJECTS,
	        'date'       => new Carbon('2000-01-01 00:00:01'),
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revisions');
    }
}
