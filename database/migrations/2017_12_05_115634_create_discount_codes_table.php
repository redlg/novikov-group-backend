<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('device_id');
            $table->unsignedInteger('discount_card_type_id');
            $table->unsignedInteger('discount_card_id');
            $table->string('code', 8)->unique();
            $table->dateTime('expiration_at');

            $table->foreign('device_id')->references('id')->on('devices')->onDelete('cascade');
            $table->foreign('discount_card_type_id')->references('id')->on('discount_card_types')->onDelete('cascade');
            $table->foreign('discount_card_id')->references('id')->on('discount_cards')->onDelete('cascade');

            $table->index('code');
            $table->index('expiration_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_codes');
    }
}
