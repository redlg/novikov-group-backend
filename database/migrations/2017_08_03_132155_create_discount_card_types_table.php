<?php

use App\Models\DiscountCardType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountCardTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount_card_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('discount')->nullable();
            $table->integer('life_time')->default(365);
            $table->string('external_id')->nullable();
            $table->string('rkeeper_id')->unique()->nullable();

            $table->index('external_id');
            $table->index('rkeeper_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_card_types');
    }
}
