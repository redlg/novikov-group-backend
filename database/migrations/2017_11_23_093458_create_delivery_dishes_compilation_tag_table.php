<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryDishesCompilationTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_dishes_compilation_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('delivery_dishes_compilation_id');
            $table->unsignedInteger('tag_id');

            $table->foreign('delivery_dishes_compilation_id')->references('id')->on('delivery_dishes_compilations');
            $table->foreign('tag_id')->references('id')->on('tags');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_dishes_compilation_tag');
    }
}
