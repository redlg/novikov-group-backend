<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsCompilationTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants_compilation_tag', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedInteger('restaurants_compilation_id');
	        $table->unsignedInteger('tag_id');

	        $table->foreign('restaurants_compilation_id')->references('id')->on('restaurants_compilations')->onDelete('cascade');
	        $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants_compilation_tag');
    }
}
