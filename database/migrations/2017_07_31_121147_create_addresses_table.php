<?php

use App\Models\Address;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('addressable_id')->nullable();
            $table->string('addressable_type')->nullable();
	        $table->string('text_ru')->nullable();
	        $table->string('text_en')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->default(Address::DEFAULT_CITY);
            $table->string('timezone')->nullable();
            $table->string('street');
            $table->string('building');
            $table->string('block')->nullable();
            $table->string('apartment')->nullable();
            $table->string('floor')->nullable();
            $table->string('intercom')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('main')->default(false);
            $table->string('partner_name')->nullable();
            $table->float('partner_discount')->default(0);
	        $table->string('lng', 25)->nullable();
	        $table->string('lat', 25)->nullable();
	        $table->string('external_id')->nullable();
	        $table->string('temp_id')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
