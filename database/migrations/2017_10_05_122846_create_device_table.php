<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('guest_id')->nullable();
            $table->string('device_id')->unique();
            $table->string('token')->unique();
            $table->string('phone')->nullable();
            $table->string('code', 6)->nullable();
            $table->dateTime('code_valid_until')->nullable();

            $table->foreign('guest_id')->references('id')->on('guests')->onDelete('set null');

            $table->index('device_id');
            $table->index('token');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devices');
    }
}
