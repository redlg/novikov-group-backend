<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('card_type_id');
            $table->string('card_type_type');
            $table->unsignedInteger('guest_id');
            $table->unsignedInteger('product_delivery_method_id');
            $table->unsignedInteger('address_id')->nullable();
            $table->text('comment')->nullable();

            $table->foreign('guest_id')->references('id')->on('guests');
            $table->foreign('product_delivery_method_id')->references('id')->on('product_delivery_methods');
            $table->foreign('address_id')->references('id')->on('addresses');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_orders');
    }
}
