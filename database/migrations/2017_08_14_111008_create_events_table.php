<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_type_id');
            $table->unsignedInteger('logo_id');
            $table->string('name_ru');
            $table->string('name_en')->nullable();
	        $table->text('announce_ru');
	        $table->text('announce_en')->nullable();
            $table->text('description_ru');
            $table->text('description_en')->nullable();
            $table->string('phone')->nullable();
            $table->dateTimeTz('started_at');
            $table->dateTimeTz('ended_at')->nullable();
            $table->boolean('repeat')->default(0);
	        $table->string('external_id')->nullable();
	        $table->string('temp_id', 10)->nullable();

            $table->foreign('event_type_id')->references('id')->on('event_types');
            $table->foreign('logo_id')->references('id')->on('images');

	        $table->index('external_id');

            $table->softDeletes();
            $table->timestampsTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
