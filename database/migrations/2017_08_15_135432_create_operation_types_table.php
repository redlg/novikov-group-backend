<?php

use App\Models\OperationType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operation_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type')->default(OperationType::EVENT);

            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('operation_types')->insert([[
	        'name'       => OperationType::TRANSFER_TO_THE_RESTAURANT,
	        'type'       => OperationType::EVENT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::ACTIVATION,
	        'type'       => OperationType::EVENT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::BLOCKING_EXPIRED,
	        'type'       => OperationType::EVENT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::BLOCKING_MANUALLY,
	        'type'       => OperationType::EVENT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::BLOCKING_SERVICE,
	        'type'       => OperationType::EVENT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::UNBLOCKING,
	        'type'       => OperationType::EVENT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::ROLLING,
	        'type'       => OperationType::EVENT,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::DISCOUNT,
	        'type'       => OperationType::TRANSACTION,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::DISCOUNT_CANCEL,
	        'type'       => OperationType::TRANSACTION,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::PAYMENT,
	        'type'       => OperationType::TRANSACTION,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ], [
	        'name'       => OperationType::PAYMENT_CANCEL,
	        'type'       => OperationType::TRANSACTION,
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
        ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operation_types');
    }
}
