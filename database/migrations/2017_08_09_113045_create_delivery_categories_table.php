<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru');
            $table->string('name_en');
	        $table->unsignedInteger('delivery_category_id')->nullable();
	        $table->unsignedInteger('restaurant_id');
	        $table->string('external_id')->nullable();
	        $table->string('temp_id', 10)->nullable();

	        $table->foreign('delivery_category_id')->references('id')->on('delivery_categories');
	        $table->foreign('restaurant_id')->references('id')->on('restaurants');

	        $table->index('external_id');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_categories');
    }
}
