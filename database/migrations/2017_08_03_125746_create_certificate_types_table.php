<?php

use App\Models\CertificateType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificate_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('sum');
            $table->integer('life_time')->default(365);
            $table->boolean('reusable')->default(0);
	        $table->string('external_id')->nullable();
	        $table->string('rkeeper_id')->unique()->nullable();

	        $table->index('external_id');
	        $table->index('rkeeper_id');

            $table->softDeletes();
            $table->timestamps();
        });

	    DB::table('certificate_types')->insert([[
		    'name'       => CertificateType::LITE,
		    'sum'        => 3000,
		    'life_time'  => 365,
		    'reusable'   => false,
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ], [
		    'name'       => CertificateType::STANDARD,
		    'sum'        => 5000,
		    'life_time'  => 365,
		    'reusable'   => false,
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ], [
		    'name'       => CertificateType::CLASSIC,
		    'sum'        => 10000,
		    'life_time'  => 365,
		    'reusable'   => false,
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ], [
		    'name'       => CertificateType::PLATINUM,
		    'sum'        => 15000,
		    'life_time'  => 365,
		    'reusable'   => false,
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ], [
		    'name'       => CertificateType::EXCLUSIVE,
		    'sum'        => -1,
		    'life_time'  => 365,
		    'reusable'   => true,
		    'created_at' => Carbon::now(),
		    'updated_at' => Carbon::now(),
	    ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate_types');
    }
}
