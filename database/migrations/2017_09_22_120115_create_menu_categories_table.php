<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name_ru');
			$table->string('name_en');
			$table->unsignedInteger('menu_category_id')->nullable();
			$table->unsignedInteger('restaurant_id');
			$table->string('external_id')->nullable();
			$table->string('temp_id', 10)->nullable();

			$table->foreign('menu_category_id')->references('id')->on('menu_categories')->onDelete('cascade');
			$table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('cascade');

			$table->index('external_id');

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('menu_categories');
	}
}
