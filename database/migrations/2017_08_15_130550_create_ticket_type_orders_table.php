<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTypeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_type_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ticket_type_id');
            $table->unsignedInteger('product_delivery_method_id');
            $table->unsignedInteger('guest_id')->nullable();
            $table->string('server_url')->nullable();
            $table->float('cost');
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->string('customer_email')->nullable();
            $table->integer('order_id');
            $table->integer('invoice_id');
            $table->boolean('paid')->default(false);

            $table->foreign('ticket_type_id')->references('id')->on('ticket_types');
            $table->foreign('product_delivery_method_id')->references('id')->on('product_delivery_methods');
            $table->foreign('guest_id')->references('id')->on('guests');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_type_orders');
    }
}
