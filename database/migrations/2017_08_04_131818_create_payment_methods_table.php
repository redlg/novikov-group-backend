<?php

use App\Models\PaymentMethod;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_ru');
            $table->string('name_en');
            $table->boolean('common')->default(true);

            $table->softDeletes();
            $table->timestamps();
        });

        DB::table('payment_methods')->insert([[
	        'name_ru'    => PaymentMethod::CASH,
	        'name_en'    => 'Cash',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
	        'common'     => true,
        ], [
	        'name_ru'    => PaymentMethod::CARD,
	        'name_en'    => 'Card',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
	        'common'     => true,
        ], [
	        'name_ru'    => PaymentMethod::DISCOUNT_CARD,
	        'name_en'    => 'Discount card',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
	        'common'     => true,
        ], [
	        'name_ru'    => PaymentMethod::DISCOUNT_CARD_VIA_APP,
	        'name_en'    => 'Discount card via app',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
	        'common'     => true,
        ], [
	        'name_ru'    => PaymentMethod::CERTIFICATE,
	        'name_en'    => 'Certificate',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
	        'common'     => true,
        ], [
	        'name_ru'    => PaymentMethod::CERTIFICATE_VIA_APP,
	        'name_en'    => 'Certificate via app',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
	        'common'     => true,
        ], [
	        'name_ru'    => PaymentMethod::EXPENSES,
	        'name_en'    => 'Expenses',
	        'created_at' => Carbon::now(),
	        'updated_at' => Carbon::now(),
	        'common'     => false,
        ]]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_methods');
    }
}
