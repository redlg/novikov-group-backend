<?php

namespace App\Jobs;

use App\Http\Controllers\CRM\TransactionController;
use App\Models\Abstracts\Card;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\Restaurant;
use App\Models\Transaction;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ExportTransactions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request_array;
    protected $filename;

	/**
	 * Create a new job instance.
	 *
	 * @param array $request_array
	 * @param string  $filename
	 */
    public function __construct($request_array, $filename)
    {
        $this->request_array = $request_array;
        $this->filename = $filename;
    }

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
    public function handle()
    {
    	$request = new Request($this->request_array);
    	$data = (new TransactionController())->index($request, true);
	    Excel::create($this->filename, function($excel) use($data) {
		    $excel->sheet('Транзакции', function($sheet) use($data) {
			    //header row
			    $sheet->appendRow([
				    'ID',
				    'Дата',
				    'Тип транзакции',
				    'Метод ввода',
				    'Номер чека',
				    'Номер кассы',
				    'Ресторан',
				    'Номер операции',
				    'Метод оплаты',
				    'Карта',
				    'Сумма транзакции',
				    'Скидка',
			    ]);
			    $restaurants = Restaurant::all()->mapWithKeys(function($item) {
				    return [$item['id'] => $item['name']];
			    });
			    //body rows
			    foreach ($data as $transaction) {
				    /** @var Transaction $transaction */
				    if ($transaction->card_type == Certificate::class) {
				    	$card_type = Card::RELATION_CERTIFICATE_NAME;
				    } elseif ($transaction->card_type == DiscountCard::class) {
				    	$card_type = Card::RELATION_DISCOUNT_CARD_NAME;
				    } else {
				    	$card_type = null;
				    }
				    $sheet->appendRow([
					    'id'                => $transaction->id,
					    'date'              => $transaction->created_at,
					    'transaction_type'  => $transaction->type->name,
					    'card_input_method' => $transaction->card_input_method,
					    'check_number'      => $transaction->check_number,
					    'cashbox_number'    => $transaction->cashbox_number,
					    'restaurant'        => $restaurants->get($transaction->restaurant_id),
					    'operation'         => $transaction->operation_id,
					    'payment_method'    => $transaction->payment_method->name_ru,
					    'card'              => $card_type,
					    'sum'               => $transaction->sum,
					    'discount_sum'      => $transaction->discount_sum,
				    ]);
			    }
		    });
	    })->store('xls');
    }
}
