<?php
namespace App\Jobs;

ini_set('max_execution_time', 3600);

use App\Models\DeliveryCategory;
use App\Models\DeliveryDish;
use App\Models\DeliveryEmail;
use App\Models\DeliveryMethod;
use App\Models\DeliveryPaymentMethod;
use App\Models\Partner;
use App\Models\Project;
use App\Models\Revision;
use App\Models\TicketType;
use App\Requests\NovikovRequester;
use App\Models\Address;
use App\Models\Chef;
use App\Models\Event;
use App\Models\EventType;
use App\Models\Image;
use App\Models\Kitchen;
use App\Models\LegalEntity;
use App\Models\LegalEntityType;
use App\Models\MenuCategory;
use App\Models\MenuDish;
use App\Models\PaymentMethod;
use App\Models\Restaurant;
use App\Models\ProductDeliveryMethod;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Storage;

class ImportFromNovikovgroup implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $temp_id;
	protected $restaurant_logo_resolution;
    protected $restaurant_photo_resolution;
    protected $partner_logo_resolution;
    protected $chef_logo_resolution;
    protected $event_logo_resolution;
    protected $delivery_dish_image_resolution;
    protected $project_logo_resolution;
    protected $restaurant_ru_revision;
    protected $restaurant_ru_revision_value;
    protected $restaurant_en_revision;
    protected $restaurant_en_revision_value;
    protected $dishes_revision;
    protected $dishes_revision_value;
	protected $event_revision;
	protected $event_revision_value;
	protected $project_revision;
	protected $project_revision_value;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        $this->temp_id = str_random(10);
        $this->restaurant_logo_resolution = array(config('crm.restaurant_logo_width'), config('crm.restaurant_logo_height'));
        $this->restaurant_photo_resolution = array(config('crm.restaurant_image_width'), config('crm.restaurant_image_height'));
        $this->partner_logo_resolution = array(config('crm.partner_logo_width'), config('crm.partner_logo_height'));
        $this->chef_logo_resolution = array(config('crm.chef_logo_width'), config('crm.chef_logo_height'));
        $this->event_logo_resolution = array(config('crm.event_logo_width'), config('crm.event_logo_height'));
        $this->delivery_dish_image_resolution = array(config('crm.delivery_dish_image_width'), config('crm.delivery_dish_image_height'));
        $this->project_logo_resolution = array(config('crm.project_logo_width'), config('crm.project_logo_height'));
	    $this->restaurant_ru_revision = Revision::whereName(Revision::NOVIKOV_RESTAURANTS_RU)->first();
	    $this->restaurant_ru_revision_value = $this->restaurant_ru_revision->value;
	    $this->restaurant_en_revision = Revision::whereName(Revision::NOVIKOV_RESTAURANTS_EN)->first();
	    $this->restaurant_en_revision_value = $this->restaurant_en_revision->value;
	    $this->dishes_revision = Revision::whereName(Revision::NOVIKOV_DELIVERY_DISHES)->first();
	    $this->dishes_revision_value = $this->dishes_revision->value;
	    $this->event_revision = Revision::whereName(Revision::NOVIKOV_EVENTS)->first();
	    $this->event_revision_value = $this->event_revision->value;
	    $this->project_revision = Revision::whereName(Revision::NOVIKOV_PROJECTS)->first();
	    $this->project_revision_value = $this->project_revision->value;
    }

    /**
     * Execute the import.
     *
     * @return void
     */
    public function handle()
    {
	    $this->importProductDeliveryMethods();
	    $this->importDeliveryMethods();
	    $this->importDeliveryPaymentMethods();
	    echo "Импорт способов доставки и оплаты завершен " . date("Y-m-d H:i:s")  . "\n";
	    $this->deactivateProductDeliveryMethods();
	    $this->deactivateDeliveryMethods();
	    $this->deactivateDeliveryPaymentMethods();
	    echo "Деактивация неиспользуемых способов доставки и оплаты завершена " . date("Y-m-d H:i:s")  . "\n\n";

		$this->importRuRestaurants();
	    echo "Импорт ресторанов и меню завершен " . date("Y-m-d H:i:s")  . "\n";
	    $this->importEnRestaurants();
	    echo "Импорт полей на английском для ресторанов завершен " . date("Y-m-d H:i:s")  . "\n";
	    $this->deactivateUnusedRestaurants();
	    echo "Деактивация неиспользуемых ресторанов завершена " . date("Y-m-d H:i:s")  . "\n";
	    $this->deactivateRestaurantsMenu();
	    echo "Деактивация неиспользуемых меню ресторанов завершена " . date("Y-m-d H:i:s")  . "\n";
	    $this->importDeliveryEmails();
	    echo "Импорт почт для доставки ресторанов завершен " . date("Y-m-d H:i:s")  . "\n";
        $this->deactivateDeliveryEmails();
        echo "Деактивация неиспользуемых почт доставки ресторанов завершена " . date("Y-m-d H:i:s")  . "\n";
	    echo "Синхронизация ресторанов завершена " . date("Y-m-d H:i:s") . "\n\n";

	    $this->importEvents();
	    echo "Импорт событий и билетов завершен " . date("Y-m-d H:i:s")  . "\n";
	    $this->deactivateUnusedEvents();
	    $this->deactivateUnusedTicketTypes();
	    echo "Деактивация неиспользуемых событий и билетов завершена " . date("Y-m-d H:i:s")  . "\n";
	    echo "Синхронизация событий завершена " . date("Y-m-d H:i:s")  . "\n\n";

	    $this->importPartners();
	    echo "Импорт партнеров завершен " . date("Y-m-d H:i:s")  . "\n";
	    $this->importProjects();
	    echo "Импорт проектов завершен " . date("Y-m-d H:i:s")  . "\n";
	    $this->deactivateUnusedPartners();
	    $this->deactivateUnusedProjects();
	    echo "Деактивация неиспользуемых партнеров и проектов завершена " . date("Y-m-d H:i:s")  . "\n";
	    echo "Синхронизация партнеров и проектов завершена " . date("Y-m-d H:i:s")  . "\n\n";
    }

	/**
	 * Create restaurants with logo, address, legal entity, chef, photos, payment methods, kitchens, menu and delivery menu
	 *
	 * @return void
	 */
	private function importRuRestaurants()
    {
	    $offset = 0;
        $localRestaurants = Restaurant::whereNotNull('external_id')
	        ->get()
	        ->mapWithKeys(function($restaurant) {
        	    return [$restaurant['external_id'] => $restaurant];
            });
        $requester = new NovikovRequester();
        while($restaurants = $requester->getRestaurantList($offset)) {
            foreach($restaurants as $restaurant) {
	            if ($localRestaurants->has($restaurant['ID'])) {
		            $localRestaurant = $localRestaurants->get($restaurant['ID']);
		            //если ресторан не обновлялся, то обновим ему temp_id и сразу начнем обновлять меню
		            if ($restaurant['LastUpdated'] <= $this->restaurant_ru_revision_value) {
			            $localRestaurant->temp_id = $this->temp_id;
			            $localRestaurant->save();
			            $this->importRestaurantMenu($localRestaurant);
                        $this->importRestaurantDeliveryMenu($localRestaurant);
			            continue;
		            }
	            } else {
		            $localRestaurant = new Restaurant();
		            $localRestaurant->external_id = $restaurant['ID'];
	            }
	            if ($this->restaurant_ru_revision->value < $restaurant['LastUpdated'] ) {
		            $this->restaurant_ru_revision->value = $restaurant['LastUpdated'];
	            }
	            //get detail restaurant
                $restaurant = $requester->getRestaurant($restaurant['ID']);

                $localRestaurant->temp_id = $this->temp_id;
	            $localRestaurant->name_ru = $restaurant['Name'];
	            $localRestaurant->name_en = transliterate($restaurant['Name']);
                $localRestaurant->phone_string = $restaurant['Phones'];
                $localRestaurant->work_time_string = $restaurant['Work_time'];
                $localRestaurant->last_people = $restaurant['LastPeople'];
                $localRestaurant->average_check_string = $restaurant['Bill'];
                $localRestaurant->number_of_seats = intval($restaurant['SitPlaces']);
                $localRestaurant->description_ru = $restaurant['Concept'];
                $localRestaurant->parking = $restaurant['Parking'];
                $localRestaurant->delivery = $restaurant['Delivery'];
                $localRestaurant->delivery_phone = isset($restaurant['DeliveryPhone']) ? $restaurant['DeliveryPhone'] : null;
                $localRestaurant->language_hash = $restaurant['LanguageHash'];
                $localRestaurant->min_order_check = isset($restaurant['DeliveryMinSum']) ? $restaurant['DeliveryMinSum'] : 0;

                //если у ресторана есть лого, но оно сменилось
	            if ($localRestaurant->logo
		            && $localRestaurant->logo->external_id != $restaurant['Images']['Logo']
		            && $restaurant['Images']['Logo'] != '')
	            {
	            	$old_logo_id = $localRestaurant->logo->id;
		            Storage::delete($localRestaurant->logo->path);
		            $logo = $this->saveLogo($restaurant['Images']['Logo'], $this->restaurant_logo_resolution);
		            $localRestaurant->logo_id = $logo->id;
	            } elseif (!$localRestaurant->logo || ($localRestaurant->logo && !Storage::exists($localRestaurant->logo->path))) {
		            $logo = $this->saveLogo($restaurant['Images']['Logo'], $this->restaurant_logo_resolution);
					if (!$logo) {
						$logo = new Image();
						$logo->type = Image::LOGO;
						$logo->path = 'images/' . $restaurant['ID'] . '_' . time();
						$logo->name = $restaurant['ID'] . '_' . time();
						$logo->confirmed = true;
						$logo->external_id = Image::TEMPORARY_ID;
						$logo->save();
					}
		            $localRestaurant->logo_id = $logo->id;
	            }

	            if (!$localRestaurant->address) {
		            $address = new Address();
	            } else {
	            	$address = $localRestaurant->address;
	            }
	            $address->addressable_type = Restaurant::class;
	            $address->text_ru = $restaurant['Addres']['Text'];
	            $address->country = $restaurant['AddressBlock']['Country'];
	            $address->city = $restaurant['AddressBlock']['City'] ?: $restaurant['City'];
	            $address->street = $restaurant['AddressBlock']['Street'];
	            $address->building = $restaurant['AddressBlock']['House'];
	            $address->timezone = $restaurant['AddressBlock']['Timezone'];
	            $address->lng = $restaurant['Addres']['Lng'];
	            $address->lat = $restaurant['Addres']['Lat'];
	            $address->save();
	            $localRestaurant->address_id = $address->id;

	            if (!$localRestaurant->legal_entity) {
		            $legal_entity = new LegalEntity();
		            $legal_entity->legal_entity_type_id = LegalEntityType::whereName(LegalEntityType::RESTAURANT)->first()->id;
	            } else {
		            $legal_entity = $localRestaurant->legal_entity;
	            }
	            $legal_entity->inn = $restaurant['LegalEntity']['INN'];
	            $legal_entity->kpp = $restaurant['LegalEntity']['KPP'];
	            $legal_entity->name = empty($restaurant['LegalEntity']['Name']) ? $restaurant['Name'] : $restaurant['LegalEntity']['Name'];
	            $legal_entity->legal_address = $restaurant['LegalEntity']['UL_Address'];
	            $legal_entity->actual_address = $restaurant['LegalEntity']['Address'];
	            $legal_entity->payment_account = $restaurant['LegalEntity']['Billing'];
	            $legal_entity->correspondent_account = $restaurant['LegalEntity']['Correspondent'];
	            $legal_entity->bik = $restaurant['LegalEntity']['BIK'];
	            $legal_entity->ogrn = $restaurant['LegalEntity']['OGRN'];
	            $legal_entity->general_manager = $restaurant['LegalEntity']['Director'];
	            $legal_entity->phone = $restaurant['LegalEntity']['Phone'];
	            $legal_entity->email = $restaurant['LegalEntity']['Email'];
	            $legal_entity->contact_person = $restaurant['LegalEntity']['Contact'];
	            $legal_entity->bank = $restaurant['LegalEntity']['Bank'];
	            $legal_entity->brand = $restaurant['LegalEntity']['Brand'];
	            $legal_entity->save();
	            $localRestaurant->legal_entity_id = $legal_entity->id;

	            if (!empty($restaurant['Chef']['Name'])) {
		            if (!$localRestaurant->chef) {
			            $chef = new Chef();
		            } else {
			            $chef = $localRestaurant->chef;
		            }
		            $chefNames = explode(' ', $restaurant['Chef']['Name'], 2);
		            $chef->first_name_ru = $chefNames[0];
		            $chef->last_name_ru = isset($chefNames[1]) ? $chefNames[1] : '';
		            $chef->first_name_en = $chefNames[0];
		            $chef->last_name_en = isset($chefNames[1]) ? $chefNames[1] : '';
		            $chef->description_ru = $restaurant['Chef']['Biography'];
		            $chef->description_en = $restaurant['Chef']['Biography'];
		            if (!empty($restaurant['Chef']['Photo'])) {
			            $chefLogo = $this->saveLogo($restaurant['Chef']['Photo'], $this->chef_logo_resolution);
			            if ($chefLogo) {
			                $chef->image_id = $chefLogo->id;
			            }
		            }
		            $chef->save();

		            $localRestaurant->chef_id = $chef->id;
	            }

	            $localRestaurant->save();
	            if (isset($old_logo_id)) {
		            Image::whereId($old_logo_id)->delete();
	            }

	            $restaurant_images = $localRestaurant->images;
	            $images = $this->savePhotos($restaurant['Images']['Gallery'], $restaurant_images, $this->restaurant_photo_resolution);
	            $localRestaurant->images()->sync($images);

	            $kitchens = $this->getKitchens($restaurant['Kithen']);
                $localRestaurant->kitchens()->syncWithoutDetaching($kitchens);

                $payment_methods = $this->getPaymentMethods($restaurant['BillType']);
                $localRestaurant->payment_methods()->syncWithoutDetaching($payment_methods);

                $this->importRestaurantMenu($localRestaurant);
                $this->importRestaurantDeliveryMenu($localRestaurant);
            }
            $offset += count($restaurants);
        }
        $this->restaurant_ru_revision->save();
        $this->dishes_revision->save();
    }

    /**
     * @param Restaurant $localRestaurant
     */
    private function importRestaurantMenu(Restaurant $localRestaurant)
    {
        $requester = new NovikovRequester();
        $menu = $requester->getRestaurantMenu($localRestaurant->external_id);
        if (false !== $menu) {
            foreach ($menu as $category) {
                $this->importMenuCategory($category, $localRestaurant->id);
            }
        }
    }

    /**
     * @param Restaurant $localRestaurant
     */
    private function importRestaurantDeliveryMenu(Restaurant $localRestaurant)
    {
        $requester = new NovikovRequester();
        $deliveryCategories = $requester->getDeliveryCategories($localRestaurant->external_id);
        if (false !== $deliveryCategories) {
            $localDeliveryCategories = DeliveryCategory::whereRestaurantId($localRestaurant->id)
                ->whereNotNull('external_id')
                ->get()
                ->mapWithKeys(function($category) {
                    return [$category['external_id'] => $category];
                });
            foreach ($deliveryCategories as $category) {
                $deliveryCategory = $this->importDeliveryCategory($category, $localRestaurant->id, $localDeliveryCategories);
                $deliveryDishes = $requester->getDeliveryDishes($deliveryCategory->external_id);
                if (false !== $deliveryDishes) {
                    $localDeliveryDishes = DeliveryDish::whereDeliveryCategoryId($deliveryCategory->id)
                        ->whereNotNull('external_id')
                        ->get()
                        ->mapWithKeys(function($dish) {
                            return [$dish['external_id'] => $dish];
                        });
                    $this->importDeliveryItems($deliveryDishes, $deliveryCategory->id, $localDeliveryDishes);
                }
            }
        }
    }

	/**
	 * Attach delivery emails to restaurants
	 *
	 * @return void
	 */
	private function importDeliveryEmails()
    {
        $requester = new NovikovRequester();
        $localRestaurants = Restaurant::whereNotNull('external_id')
            ->get()
            ->mapWithKeys(function($restaurant) {
                return [$restaurant['external_id'] => $restaurant];
            });
        $localEmails = DeliveryEmail::whereNotNull('external_id')
            ->get()
            ->mapWithKeys(function ($email) {
                return [$email['external_id'] => $email];
            });
        $emails = $requester->getRestaurantDeliveryEmails();

        foreach ($emails as $email) {
            if ($localEmails->has($email['ID'])) {
                $localEmail = $localEmails->get($email['ID']);
            } else {
                $localEmail = new DeliveryEmail();
                $localEmail->external_id = $email['ID'];
            }

            $restaurant = $localRestaurants->get($email['Restaurant_ID']);

            if ($restaurant) {
                $localEmail->temp_id = $this->temp_id;
                $localEmail->restaurant_id = $restaurant->id;
                $localEmail->name = $email['Email'];
                $localEmail->save();
            }
        }
    }

	/**
	 * Append english fields for restaurants
	 *
	 * @return void
	 */
	private function importEnRestaurants()
	{
		$offset = 0;
		$localRestaurants = Restaurant::whereNotNull('language_hash')->get()
			->mapWithKeys(function($restaurant) {
				return [$restaurant['language_hash'] => $restaurant];
			});
		$requester = new NovikovRequester();
		$requester->setLang(NovikovRequester::LANG_EN);
		while($restaurants = $requester->getRestaurantList($offset)) {
			foreach ($restaurants as $restaurant) {
				if ($restaurant['LastUpdated'] <= $this->restaurant_en_revision_value) {
					continue;
				}
				$restaurant = $requester->getRestaurant($restaurant['ID']);

				if ($localRestaurants->has($restaurant['LanguageHash'])) {
					$localRestaurant = $localRestaurants->get($restaurant['LanguageHash']);
				} else {
					continue; //если нет связи с рестораном "на русском"
				}
				if ($this->restaurant_en_revision->value < $restaurant['LastUpdated'] ) {
					$this->restaurant_en_revision->value = $restaurant['LastUpdated'];
				}

				$localRestaurant->name_en = $restaurant['Name'];
				$localRestaurant->description_en = $restaurant['Concept'];

				$address = $localRestaurant->address;
				$address->text_en = $restaurant['Addres']['Text'];
				$address->save();

				$localRestaurant->save();
			}
			$offset += count($restaurants);
		}
		$this->restaurant_en_revision->save();
	}

	/**
	 * @param string $kitchens
	 *
	 * @return Kitchen[]|Collection
	 */
    private function getKitchens($kitchens)
    {
        static $_kitchens = null;
        if ($_kitchens === null) {
	        $_kitchens = Kitchen::all()->mapWithKeys(function($kitchen) {
		        return [ mb_strtolower($kitchen->name_ru) => $kitchen ];
	        });
        }
        $result = [];
        $names = collect(explode(',', $kitchens))->map(function($name) {
        	return trim($name);
        });
	    foreach ($names as $name) {
		    $lower_name = mb_strtolower($name);
		    if ($lower_name != '') {
			    if (!$_kitchens->has($lower_name)) {
				    $kitchen = new Kitchen([
					    'name_ru' => $name,
					    'name_en' => transliterate($name),
				    ]);
				    $kitchen->save();
				    $_kitchens->put($lower_name, $kitchen);
			    }
			    $result[] = $_kitchens->get($lower_name)->id;
		    }
	    }

        return $result;
    }

    /**
     * @param string $methods
     *
     * @return PaymentMethod[]|Collection
     */
    private function getPaymentMethods($methods)
    {
        static $_methods = null;
	    if ($_methods === null) {
		    $_methods = PaymentMethod::all()->mapWithKeys(function($method) {
			    return [ mb_strtolower($method->name_ru) => $method ];
		    });
	    }
        $result = [];
        $names = collect(explode(',', $methods))->map(function($name) {
        	return trim($name);
        });
        foreach ($names as $name) {
            $lower_name = mb_strtolower($name);
            if ($lower_name != '') {
	            if (!$_methods->has($lower_name)) {
		            $method = new PaymentMethod([
						'name_ru' => $name,
						'name_en' => $name,
						'common'  => false,
		            ]);
		            $method->save();
		            $_methods->put($lower_name, $method);
	            }
	            $result[] = $_methods->get($lower_name)->id;
            }
        }

        return $result;
    }

	/**
	 * @param Collection  $localAddresses
	 * @param array       $partnerAddresses
	 * @param integer     $partnerId
	 */
	private function syncAddresses($localAddresses, $partnerAddresses, $partnerId)
	{
		foreach($partnerAddresses as $address) {
			if ($localAddresses->has($address['Addres']['Text'])) {
				$localAddress = $localAddresses->get($address['Addres']['Text']);
			} else {
				$localAddress = new Address();
				$localAddress->external_id = 'imported';
			}
			$localAddress->addressable_id = $partnerId;
			$localAddress->addressable_type = Partner::class;
			$localAddress->text_ru = $address['Addres']['Text'];
			$localAddress->city = '';
			$localAddress->street = '';
			$localAddress->building = '';
			$localAddress->partner_name = $address['Name'];
			$localAddress->partner_discount = (integer)$address['Discount'];
			$localAddress->lat = $address['Addres']['Lat'];
			$localAddress->lng = $address['Addres']['Lng'];
			$localAddress->temp_id = $this->temp_id;
			$localAddress->save();
		}
	}

	/**
	 * @param string $Logo
	 * @param array  $resolution
	 *
	 * @return Image|bool
	 */
    private function saveLogo($Logo, $resolution)
    {
    	$request = new NovikovRequester();
    	$image = $request->downloadImage($Logo, $resolution);
	    $extension = $this->getExtension($image);

	    if ($extension) {
	    	$newImage = $this->createImage($image, $Logo, $extension, Image::LOGO);

	    	return $newImage;
	    }

	    return false;
    }

	/**
	 * @param array      $Gallery
	 * @param collection $images
	 * @param array      $resolution
	 *
	 * @return array
	 */
	private function savePhotos($Gallery, $images, $resolution)
    {
    	$result = [];
    	$external_ids = $images->where('external_id', '!==', null)->pluck('external_id');
    	$Gallery = collect($Gallery);
    	$to_delete = $external_ids->diff($Gallery)->toArray(); //изображения из базы, которых нет в импорте
    	$to_add = $Gallery->diff($external_ids)->toArray(); //новые изображения из импорта
    	$intersect = $Gallery->intersect($external_ids)->toArray(); //пересечение изображений из базы и импорта
    	if (!empty($to_delete)) {
		    foreach ($to_delete as $image_external_id) {
		    	$image_to_delete = $images->where('external_id', $image_external_id)->first();
		    	if ($image_to_delete) {
                    Storage::delete($image_to_delete->path);
                    Image::whereExternalId($image_external_id)->delete();
                }
		    }
	    }
	    if (!empty($to_add)) {
		    foreach ($to_add as $image_id) {
			    $request = new NovikovRequester();
			    $image = $request->downloadImage($image_id, $resolution);
			    $ext = $this->getExtension($image);

			    if ($ext) {
				    $newImage = $this->createImage($image, $image_id, $ext, Image::PHOTO);
				    $result[] = $newImage->id;
			    }
		    }
	    }
	    if (!empty($intersect)) {
    		$intersect_ids = Image::whereIn('external_id', $intersect)->get(['id'])->pluck('id')->toArray();
    		$result = array_merge($result, $intersect_ids);
	    }

        return $result;
    }

	/**
	 * Return extension of image file or false
	 *
	 * @param resource $image
	 *
	 * @return bool|string
	 */
	private function getExtension($image)
    {
	    $finfo = new \finfo(FILEINFO_MIME_TYPE);
	    $mime = $finfo->buffer($image);
	    list($basetype, $type) = explode('/', $mime);
	    if ($basetype !== 'image') {
		    return false;
	    }
        switch ($type) {
            case 'jpeg':
                return 'jpg';
            case 'svg+xml':
                return 'svg';
        }

        return $type;
    }

	/**
	 * @param resource $image
	 * @param string   $external_id
	 * @param string   $extension
	 * @param string   $type
	 *
	 * @return Image
	 */
	private function createImage($image, $external_id, $extension, $type)
    {
    	$name = "$external_id.$extension";
	    Storage::put("images/$name", $image);

	    $newImage = new Image();
	    $newImage->type = $type;
	    $newImage->path = "images/$name";
	    $newImage->name = $name;
	    $newImage->confirmed = true;
	    $newImage->external_id = $external_id;
	    $newImage->save();

	    return $newImage;
    }

	/**
	 * Create delivery methods for tickets
	 *
	 * @return void
	 */
	private function importProductDeliveryMethods()
	{
		$requester = new NovikovRequester();
		$localMethods = ProductDeliveryMethod::whereNotNull('external_id')->get()->mapWithKeys(function($method) {
			return [$method['external_id'] => $method];
		});
		$methods = $requester->getProductDeliveryMethods();
		foreach ($methods as $method) {
			if ($localMethods->has($method['ID'])) {
				$localMethod = $localMethods->get($method['ID']);
			} else {
				$localMethod = new ProductDeliveryMethod();
				$localMethod->external_id = $method['ID'];
			}

			$localMethod->temp_id = $this->temp_id;
			$localMethod->delivery_group = $method['Delivery_Group'];
			$localMethod->name = $method['Name'];
			$localMethod->price = $method['Price'];
			$localMethod->show_address = $method['Show_Addres'];
			$localMethod->save();
		}
	}

	/**
	 * Create delivery methods for restaurants and orders
	 *
	 * @return void
	 */
	private function importDeliveryMethods()
	{
		$requester = new NovikovRequester();
		$localMethods = DeliveryMethod::whereNotNull('external_id')->get()->mapWithKeys(function($method) {
			return [$method['external_id'] => $method];
		});
		$methods = $requester->getDeliveryMethods();
		foreach ($methods as $method) {
			if ($localMethods->has($method['ID'])) {
				$localMethod = $localMethods->get($method['ID']);
			} else {
				$localMethod = new DeliveryMethod();
				$localMethod->external_id = $method['ID'];
			}

			$localMethod->temp_id = $this->temp_id;
			$localMethod->name = $method['Name'];
			$localMethod->save();
		}
	}

	/**
	 * Create delivery payment methods for delivery orders and restaurants
	 *
	 * @return void
	 */
	private function importDeliveryPaymentMethods()
	{
		$requester = new NovikovRequester();
		$localMethods = DeliveryPaymentMethod::whereNotNull('external_id')->get()->mapWithKeys(function($method) {
			return [$method['external_id'] => $method];
		});
		$methods = $requester->getDeliveryPaymentMethods();
		foreach ($methods as $method) {
			if ($localMethods->has($method['ID'])) {
				$localMethod = $localMethods->get($method['ID']);
			} else {
				$localMethod = new DeliveryPaymentMethod();
				$localMethod->external_id = $method['ID'];
			}

			$localMethod->temp_id = $this->temp_id;
			$localMethod->name = $method['Name'];
			$localMethod->save();
		}
	}

	/**
	 * Create events with logo and relations with restaurants and ticket types
	 *
	 * @return void
	 */
	private function importEvents()
	{
		$offset = 0;
		$requester = new NovikovRequester();
		$localEvents = Event::whereNotNull('external_id')->get()->mapWithKeys(function($event) {
			return [$event['external_id'] => $event];
		});
		$localTicketTypes = TicketType::whereNotNull('external_id')->get()->mapWithKeys(function($ticket_type) {
			return [$ticket_type['external_id'] => $ticket_type];
		});
		while($events = $requester->getEventList($offset)) {
			foreach ($events as $event) {
				if ($localEvents->has($event['ID'])) {
					$localEvent = $localEvents->get($event['ID']);
					//если событие не обновлялось, то обновим ему только temp_id
					if ($event['LastUpdated'] <= $this->event_revision_value) {
						$localEvent->temp_id = $this->temp_id;
						$localEvent->save();
						continue;
					}
				} else {
					$localEvent = new Event();
					$localEvent->external_id = $event['ID'];
				}
				if ($this->event_revision->value < $event['LastUpdated'] ) {
					$this->event_revision->value = $event['LastUpdated'];
				}
				//get detail event
				$event = $requester->getEvent($event['ID']);

				$localEvent->temp_id = $this->temp_id;
				$localEvent->name_ru = $event['Name'];
				$localEvent->name_en = $event['Name'];
				$localEvent->started_at = Carbon::createFromTimestamp($event['StartDate']);
				$localEvent->ended_at = !empty($event['EndDate']) ? Carbon::createFromTimestamp($event['EndDate']) : null;
				$localEvent->description_ru = $event['Text'];
				$localEvent->announce_ru = $event['Anounce'];

				//если у события есть лого, но оно сменилось
				if ($localEvent->logo && $localEvent->logo->external_id != $event['Image']) {
					$old_logo_id = $localEvent->logo->id;
					Storage::delete($localEvent->logo->path);
					$logo = $this->saveLogo($event['Image'], $this->event_logo_resolution);
					$localEvent->logo_id = $logo->id;
				} elseif (!$localEvent->logo || ($localEvent->logo && !Storage::exists($localEvent->logo->path))) {
					$logo = $this->saveLogo($event['Image'], $this->event_logo_resolution);
					if ($logo) {
						$localEvent->logo_id = $logo->id;
					} else {
						continue;
					}
				}

				$event_type = $this->getEventType($event['Tab']);
				$localEvent->event_type_id = $event_type->id;

				$localEvent->save();
				if (isset($old_logo_id)) {
					Image::whereId($old_logo_id)->delete();
				}

				if (!empty($event['Restaurants_ID'])) {
					$restaurants = $this->getRestaurants($event['Restaurants_ID']);
					$localEvent->restaurants()->syncWithoutDetaching($restaurants);
				}

				if (is_array($event['BuyProduct'])) {
					$external_id = $event['BuyProduct']['ID'];
					if ($localTicketTypes->has($external_id)) {
						$ticketType = $localTicketTypes->get($external_id);
					} else {
						$ticketType = new TicketType();
						$ticketType->external_id = $external_id;
					}

					$ticketType->temp_id = $this->temp_id;
					$ticketType->event_id = $localEvent->id;
					$ticketType->name = $event['BuyProduct']['Name'];
					$ticketType->price = $event['BuyProduct']['Price'];
					$ticketType->not_fix_price = $event['BuyProduct']['Not_fix_price'];
					$ticketType->in_stock = $event['BuyProduct']['In_stock'];
					$ticketType->available = $event['BuyProduct']['Available'];
					$ticketType->save();
				}
			}
			$offset += count($events);
		}
		$this->event_revision->save();
	}

	/**
	 * @param $name string
	 *
	 * @return EventType
	 */
	private function getEventType($name)
	{
		$event_type = EventType::whereNameRu($name)->first();
		if (is_null($event_type)) {
			$event_type = new EventType();
			$event_type->name_ru = $name;
			$event_type->name_en = $name;
			$event_type->save();
		}

		return $event_type;
	}

	/**
	 * Return restaurant's ids by given external_id array, needed for events
	 *
	 * @param array $restaurants
	 *
	 * @return array
	 */
	private function getRestaurants($restaurants)
	{
		$restaurants = Restaurant::whereIn('external_id', $restaurants)->get(['id'])->pluck('id')->toArray();

		return $restaurants;
	}


	/**
	 * Create partners with logo and addresses
	 *
	 * @return void
	 */
	private function importPartners()
	{
		$requester = new NovikovRequester();
		$localPartners = Partner::whereNotNull('external_id')->get()->mapWithKeys(function($partner) {
			return [$partner['external_id'] => $partner];
		});
		$partners = $requester->getPartnersList();
		foreach ($partners as $partner) {
			if ($localPartners->has($partner['ID'])) {
				$localPartner = $localPartners->get($partner['ID']);
			} else {
				$localPartner = new Partner();
				$localPartner->external_id = $partner['ID'];
			}

			$localPartner->temp_id = $this->temp_id;
			$localPartner->name_ru = $partner['Name'];
			$localPartner->name_en = $partner['Name'];
			$localPartner->link = $partner['Href'];
			$localPartner->discount = (integer)$partner['Discount'];

			//если у партнера есть лого, но оно сменилось
			if ($localPartner->logo && $localPartner->logo->external_id != $partner['Logo']) {
				$old_logo_id = $localPartner->logo->id;
				Storage::delete($localPartner->logo->path);
				$logo = $this->saveLogo($partner['Logo'], $this->partner_logo_resolution);
				$localPartner->logo_id = $logo->id;
			} elseif (!$localPartner->logo) {
				$logo = $this->saveLogo($partner['Logo'], $this->partner_logo_resolution);
				$localPartner->logo_id = $logo->id;
			}

			$localPartner->save();
			if (isset($old_logo_id)) {
				Image::whereId($old_logo_id)->delete();
			}

			if (!empty($partner['Child'])) {
				$localAddresses = $localPartner->addresses
					->where('external_id', '!=', null)
					->mapWithKeys(function($address) {
						return [$address['text_ru'] => $address];
					});
				$this->syncAddresses($localAddresses, $partner['Child'], $localPartner->id);
			}
		}
	}

	/**
	 * Create projects with logo
	 *
	 * @return void
	 */
	private function importProjects()
	{
		$requester = new NovikovRequester();
		$localProjects = Project::whereNotNull('external_id')->get()->mapWithKeys(function($project) {
			return [$project['external_id'] => $project];
		});
		$projects = $requester->getProjectsList();
		foreach ($projects as $project) {
			if ($localProjects->has($project['ID'])) {
				$localProject = $localProjects->get($project['ID']);
				//если проект не обновлялось, то обновим ему только temp_id
				if ($project['LastUpdated'] <= $this->project_revision_value) {
					$localProject->temp_id = $this->temp_id;
					$localProject->save();
					continue;
				}
			} else {
				$localProject = new Project();
				$localProject->external_id = $project['ID'];
			}
			if ($this->project_revision->value < $project['LastUpdated'] ) {
				$this->project_revision->value = $project['LastUpdated'];
			}

			$localProject->temp_id = $this->temp_id;
			$localProject->name_ru = $project['Head'];
			$localProject->name_en = transliterate($project['Head']);
			$localProject->link = $project['Url'];

			//если у партнера есть лого, но оно сменилось
			if ($localProject->logo && $localProject->logo->external_id != $project['Img']) {
				$old_logo_id = $localProject->logo->id;
				Storage::delete($localProject->logo->path);
				$logo = $this->saveLogo($project['Img'], $this->project_logo_resolution);
				$localProject->logo_id = $logo->id;
			} elseif (!$localProject->logo) {
				$logo = $this->saveLogo($project['Img'], $this->project_logo_resolution);
				$localProject->logo_id = $logo->id;
			}

			$localProject->save();
			if (isset($old_logo_id)) {
				Image::whereId($old_logo_id)->delete();
			}
		}
		$this->project_revision->save();
	}

	/**
	 * @param array      $category
	 * @param int        $restaurant_id
	 * @param Collection $localCategories
	 *
	 * @return DeliveryCategory
	 */
	private function importDeliveryCategory($category, $restaurant_id, $localCategories)
	{
		if ($localCategories->has($category['ID'])) {
			$deliveryCategory = $localCategories->get($category['ID']);
		} else {
			$deliveryCategory = new DeliveryCategory();
			$deliveryCategory->external_id = $category['ID'];
		}
		$deliveryCategory->name_ru = $category['Name'];
		$deliveryCategory->name_en = $category['Name'];
		$deliveryCategory->restaurant_id = $restaurant_id;
		$deliveryCategory->temp_id = $this->temp_id;
		$deliveryCategory->save();

		return $deliveryCategory;
	}

	/**
	 * @param array      $dishes
	 * @param int        $category_id
	 * @param Collection $localDishes
	 *
	 * @return void
	 */
	private function importDeliveryItems($dishes, $category_id, $localDishes)
	{
		foreach ($dishes as $dish) {
			//если блюдо не обновлялось, то обновим ему только temp_id
			if ($localDishes->has($dish['ID'])) {
				$deliveryDish = $localDishes->get($dish['ID']);
				if ($dish['LastUpdated'] <= $this->dishes_revision_value) {
					$deliveryDish->temp_id = $this->temp_id;
					$deliveryDish->save();
					continue;
				}
			} else {
				$deliveryDish = new DeliveryDish();
				$deliveryDish->external_id = $dish['ID'];
			}
			if ($this->dishes_revision->value < $dish['LastUpdated']) {
				$this->dishes_revision->value = $dish['LastUpdated'];
			}

			$deliveryDish->delivery_category_id = $category_id;
			$deliveryDish->name_ru = $dish['Name'];
			$deliveryDish->name_en = $dish['Name'];
			$deliveryDish->price = $dish['Price'];
			$deliveryDish->description_ru = $dish['Description'];
			$deliveryDish->composition_ru = $dish['Composition'];
			$deliveryDish->weight = $dish['Weight'];
			$deliveryDish->volume = $dish['Size'];
			$deliveryDish->temp_id = $this->temp_id;

			if (isset($dish['Image']) && $dish['Image'] != '') {
				if ($deliveryDish->image && $deliveryDish->image->external_id != $dish['Image']) {
					$old_logo_id = $deliveryDish->image->id;
					Storage::delete($deliveryDish->image->path);
					if ($logo = $this->saveLogo($dish['Image'], $this->delivery_dish_image_resolution)) {
					    $deliveryDish->image_id = $logo->id;
                    } else {
					    $deliveryDish->image_id = null;
                    }
				} elseif (!$deliveryDish->image || ($deliveryDish->image && !Storage::exists($deliveryDish->image->path))) {
                    if ($logo = $this->saveLogo($dish['Image'], $this->delivery_dish_image_resolution)) {
                        $deliveryDish->image_id = $logo->id;
                    } else {
                        $deliveryDish->image_id = null;
                    }
				}
			}

			$deliveryDish->save();
			if (isset($old_logo_id)) {
				Image::whereId($old_logo_id)->delete();
			}
		}
	}

	/**
	 * Create menu category, its subcategories and dishes
	 *
	 * @param array $category
	 * @param int   $restaurant_id
	 */
	private function importMenuCategory($category, $restaurant_id)
	{
		$parent_category_id = $this->createMenuCategory($category, $restaurant_id);

		foreach ($category['submenus'] as $subcategory) {
			$child_category_id = $this->createMenuCategory($subcategory, $restaurant_id, $parent_category_id);

			foreach ($subcategory['items'] as $item) {
				$this->createMenuDish($item, $child_category_id);
			}
		}
	}

	/**
	 * @param array    $category
	 * @param int      $restaurant_id
	 * @param null|int $category_id
	 *
	 * @return int
	 */
	private function createMenuCategory($category, $restaurant_id, $category_id = null)
	{
		$query = MenuCategory::query();
		if ($category_id == null) {
			$query->whereNull('menu_category_id');
		} else {
			$query->where('menu_category_id', $category_id);
		}
		$menuCategory = $query->where('name_ru', $category['name'])->where('restaurant_id', $restaurant_id)->first();
		if ($menuCategory == null) {
			$menuCategory = new MenuCategory();
			$menuCategory->external_id = 'imported'; //в данном случае это просто означает, что категория была добавлена во время импорта
			$menuCategory->restaurant_id = $restaurant_id;
			$menuCategory->menu_category_id = $category_id;
		}
		$menuCategory->temp_id = $this->temp_id;
		$menuCategory->name_ru = $category['name'];
		$menuCategory->name_en = $category['name'];
		$menuCategory->save();

		return $menuCategory->id;
	}

	/**
	 * @param array $item
	 * @param int   $category_id
	 *
	 * @return void
	 */
	private function createMenuDish($item, $category_id)
	{
		$name = preg_replace('/\s+/', ' ', $item['name']);
		$menuDish = MenuDish::where('name_ru', $name)->where('menu_category_id', $category_id)->first();
		if ($menuDish == null) {
			$menuDish = new MenuDish();
			$menuDish->external_id = 'imported'; //в данном случае это просто означает, что блюдо было добавлено во время импорта
			$menuDish->menu_category_id = $category_id;
		}
		$menuDish->temp_id = $this->temp_id;
		$menuDish->name_ru = $name;
		$menuDish->name_en = $name;
		$menuDish->price = $item['price'];
		$menuDish->save();
	}

	/**
	 * Delete restaurants which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateUnusedRestaurants()
	{
		$restaurants = Restaurant::whereNotNull('external_id')
            ->where('temp_id', '!=', $this->temp_id)
            ->pluck('id')
            ->toArray();

		if (count($restaurants)) {
			Restaurant::destroy($restaurants);
		}
	}

	/**
	 * Delete events which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateUnusedEvents()
	{
		$events = Event::whereNotNull('external_id')
            ->where('temp_id', '!=', $this->temp_id)
            ->pluck('id')
            ->toArray();

		if (count($events)) {
			Event::destroy($events);
		}
	}

	/**
	 * Delete ticket types which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateUnusedTicketTypes()
	{
		$ticketTypes = TicketType::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($ticketTypes)) {
			TicketType::destroy($ticketTypes);
		}
	}

	/**
	 * Delete menu and delivery categories and dishes which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateRestaurantsMenu()
	{
		$categories = MenuCategory::whereNotNull('external_id')
            ->where('temp_id', '!=', $this->temp_id)
            ->pluck('id')
            ->toArray();

		if (count($categories)) {
			MenuCategory::destroy($categories);
		}

		$dishes = MenuDish::whereNotNull('external_id')
            ->where('temp_id', '!=', $this->temp_id)
            ->pluck('id')
            ->toArray();

		if (count($dishes)) {
			MenuDish::destroy($dishes);
		}

		$delivery_categories = DeliveryCategory::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($delivery_categories)) {
			DeliveryCategory::destroy($delivery_categories);
		}

		$delivery_dishes = DeliveryDish::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($delivery_dishes)) {
			DeliveryDish::destroy($delivery_dishes);
		}
	}

	/**
	 * Delete partners which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateUnusedPartners()
	{
		$partners = Partner::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($partners)) {
			Partner::destroy($partners);
		}
	}

	/**
	 * Delete projects which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateUnusedProjects()
	{
		$projects = Project::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($projects)) {
			Project::destroy($projects);
		}
	}

	/**
	 * Delete ticket delivery methods which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateProductDeliveryMethods()
	{
		$methods = ProductDeliveryMethod::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($methods)) {
			ProductDeliveryMethod::destroy($methods);
		}
	}

	/**
	 * Delete delivery methods for restaurants and orders which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateDeliveryMethods()
	{
		$methods = DeliveryMethod::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($methods)) {
			DeliveryMethod::destroy($methods);
		}
	}

	/**
	 * Delete delivery payment methods for restaurants and orders which are not in the current import but added in the previous imports
	 *
	 * @return void
	 */
	private function deactivateDeliveryPaymentMethods()
	{
		$methods = DeliveryPaymentMethod::whereNotNull('external_id')
			->where('temp_id', '!=', $this->temp_id)
			->pluck('id')
			->toArray();

		if (count($methods)) {
			DeliveryPaymentMethod::destroy($methods);
		}
	}

    /**
     * Delete delivery email which are not in the current import but added in the previous imports
     *
     * @return void
     */
    private function deactivateDeliveryEmails()
    {
        $methods = DeliveryEmail::whereNotNull('external_id')
            ->where('temp_id', '!=', $this->temp_id)
            ->pluck('id')
            ->toArray();

        if (count($methods)) {
            DeliveryEmail::destroy($methods);
        }
    }
}
