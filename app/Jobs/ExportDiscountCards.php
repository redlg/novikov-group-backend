<?php

namespace App\Jobs;

use App\Http\Controllers\CRM\DiscountCardController;
use App\Http\Controllers\CRM\LegalEntityController;
use App\Models\DiscountCard;
use App\Models\Restaurant;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;

class ExportDiscountCards implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request_array;
    protected $filename;

	/**
	 * Create a new job instance.
	 *
	 * @param array $request_array
	 * @param string $filename
	 */
    public function __construct($request_array, $filename)
    {
	    $this->request_array = $request_array;
	    $this->filename = $filename;
    }

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
    public function handle()
    {
	    $request = new Request($this->request_array);
	    $data = (new DiscountCardController())->index($request, true);
	    Excel::create($this->filename, function($excel) use($data) {
		    $excel->sheet('Дисконтные карты', function($sheet) use($data) {
			    $legal_entities = LegalEntityController::getFilters();
			    $issuers = $legal_entities['issuers']->mapWithKeys(function($item) {
				    return [$item['id'] => $item['name']];
			    });
			    $contractors = $legal_entities['contractors']->mapWithKeys(function($item) {
				    return [$item['id'] => $item['name']];
			    });
			    $restaurant = Restaurant::all()->mapWithKeys(function($item) {
				    return [$item['id'] => $item['name']];
			    });
			    //header row
			    $sheet->appendRow([
				    'ID',
				    'Номер',
				    'Статус',
				    'Тип',
				    'Гость',
				    'Эмитент',
				    'Контрагент',
				    'Ресторан',
				    'Дата выпуска',
				    'Дата активации',
				    'Дата окончания',
				    'Дата блокировки',
			    ]);
			    //body rows
			    foreach ($data as $card) {
				    /** @var DiscountCard $card */
				    $sheet->appendRow([
					    'id'         => $card->id,
					    'number'     => $card->number,
					    'status'     => $card->status_name,
					    'type'       => $card->type->name,
					    'guest'      => empty($card->guest) ? null : trim($card->guest->first_name . ' ' . $card->guest->last_name),
					    'issuer'     => $issuers->get($card->issuer_id),
					    'contractor' => $contractors->get($card->contractor_id),
					    'restaurant' => $restaurant->get($card->restaurant_id),
					    'created_at' => $card->created_at,
					    'started_at' => $card->started_at,
					    'ended_at'   => $card->ended_at,
					    'blocked_at' => $card->blocked_at,
				    ]);
			    }
		    });
	    })->store('xls');
    }
}
