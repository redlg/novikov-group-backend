<?php
namespace App\Jobs;

use App\Models\Address;
use App\Models\Restaurant;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportRestaurantsData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the import.
     *
     * @return void
     */
    public function handle()
    {
	    $this->importRestaurantsData();
	    echo "Импорт данных в рестораны завершен\n\n";
    }

	private function addDataToRestaurant($external_id, $data, $address_data)
	{
	    $restaurant = Restaurant::whereExternalId($external_id)->first();

	    if ($restaurant) {
	    	$data['phone'] = $data['phone'] ? format_phone($data['phone']) : null;
	    	$data['delivery_phone'] = $data['delivery_phone'] ? format_phone($data['delivery_phone']) : null;
	        $restaurant->fill($data);
	        $restaurant->save();
	        $restaurant->address->fill($address_data);
	        $restaurant->address->save();
        }
	}

	private function importRestaurantsData()
	{
	    //Crabber на Красном Октябре
        $this->addDataToRestaurant(258, [
            'phone'                   => '7 (495) 114-56-55',
            'phone_string'            => '7 (905) 700-46-08',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Берсеневский пер.',
            'building' => 'д. 3/10, строение 8',
        ]);
        //Гастромаркет Вокруг Света
//        $this->addDataToRestaurant(260, [
//            'phone'                   => '7 (495) 114-56-55',
//            'phone_string'            => '7 (905) 700-46-08',
//            'delivery_phone'          => null,
//            'work_time_weekdays_from' => '12:00:00',
//            'work_time_weekdays_to'   => '00:00:00',
//            'work_time_friday_from'   => '12:00:00',
//            'work_time_friday_to'     => '00:00:00',
//            'work_time_saturday_from' => '12:00:00',
//            'work_time_saturday_to'   => '00:00:00',
//            'work_time_sunday_from'   => '12:00:00',
//            'work_time_sunday_to'     => '00:00:00',
//            'resto_id'                => null,
//            'iiko_id'                 => null,
//        ], [
//            'street'   => 'Берсеневский пер.',
//            'building' => 'д. 3/10, строение 8',
//        ]);
        //Китайская Забегаловка
        $this->addDataToRestaurant(244, [
            'phone'                   => '7 (495) 803-33-04',
            'phone_string'            => '7 (495) 803-33-04, 7 (495) 624-93-21, 7 (495) 621-30-64',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Кутузовский Проспект',
            'building' => 'д. 12, строение 1',
        ]);
        //Колбасный Цех Шереметьево
        $this->addDataToRestaurant(248, [
            'phone'                   => null,
            'phone_string'            => null,
            'delivery_phone'          => null,
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => null,
            'work_time_friday_to'     => null,
            'work_time_saturday_from' => null,
            'work_time_saturday_to'   => null,
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Аэропорт Шереметьево',
            'building' => 'Терминал Е',
        ]);
        //Магадан
        $this->addDataToRestaurant(246, [
            'phone'                   => '7 (495) 803-32-46',
            'phone_string'            => '7 (495) 803-32-47',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '06:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '06:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Кутузовский проспект',
            'building' => 'д. 12, строение 1',
        ]);
        //Сыроварня Санкт-Петербург
        $this->addDataToRestaurant(252, [
            'phone'                   => '7 (812) 207-01-00',
            'phone_string'            => '7 (812) 207-01-00',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '01:30:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '01:30:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Ковенский переулок',
            'building' => '5',
        ]);
        //#FARШ
        $this->addDataToRestaurant(173, [
            'phone'                   => '',
            'phone_string'            => '',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '10:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '10:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '10:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '10:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //15/17 BAR&GRILL
//        $this->addDataToRestaurant(233, [
//            'phone'                   => '7 (495) 295-15-17',
//            'phone_string'            => '7 (495) 295-15-17',
//            'delivery_phone'          => null,
//            'work_time_weekdays_from' => '10:00:00',
//            'work_time_weekdays_to'   => '00:00:00',
//            'work_time_friday_from'   => '10:00:00',
//            'work_time_friday_to'     => '00:00:00',
//            'work_time_saturday_from' => '10:00:00',
//            'work_time_saturday_to'   => '00:00:00',
//            'work_time_sunday_from'   => '10:00:00',
//            'work_time_sunday_to'     => '00:00:00',
//            'resto_id'                => null,
//            'iiko_id'                 => null,
//        ], [
//            'street'   => '',
//            'building' => '',
//        ]);
        //5642 Высота
        $this->addDataToRestaurant(58, [
            'phone'                   => '7 (495) 624 93-21',
            'phone_string'            => '7 (495) 621-30-64',
            'delivery_phone'          => '7 (938) 888-56-42',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '01:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '01:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'B247D922-9BDF-E548-B931-CA5ACE7D4657',
            'iiko_id'                 => '6433F156-C651-4D91-B154-A7CA1F74C7F7',
        ], [
            'street'   => 'Большой Черкасский пер',
            'building' => '15-17, стр. 1',
        ]);
        //5642 ВЫСОТА КИСЛОВОДСК
        $this->addDataToRestaurant(219, [
            'phone'                   => '7 (928) 900-56-42',
            'phone_string'            => '7 (495) 249-55-60',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'C1D7FBC5-24FC-834F-9838-D2AFA6ECA81E',
            'iiko_id'                 => 'F0CB7651-7D33-417A-AC76-7CBCDEEA595A',
        ], [
            'street'   => 'Курортный б-р',
            'building' => '13',
        ]);
        //5642 ВЫСОТА СОЧИ
        $this->addDataToRestaurant(212, [
            'phone'                   => '7 (988) 288-56-42',
            'phone_string'            => '7 (988) 288-56-42',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '09:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '09:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '09:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '09:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'E9E517E5-C838-5A4E-BA5A-C2223FD6A6E7',
            'iiko_id'                 => 'CD241725-F4AD-4863-8860-00A2C534FF8A',
        ], [
            'street'   => 'ул. 65 лет Победы',
            'building' => '50',
        ]);
        //Bolshoi
        $this->addDataToRestaurant(59, [
            'phone'                   => '7 (495) 789-86-52',
            'phone_string'            => '7 (495) 789-86-52',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '08:00:00',
            'work_time_weekdays_to'   => '03:00:00',
            'work_time_friday_from'   => '08:00:00',
            'work_time_friday_to'     => '03:00:00',
            'work_time_saturday_from' => '08:00:00',
            'work_time_saturday_to'   => '03:00:00',
            'work_time_sunday_from'   => '08:00:00',
            'work_time_sunday_to'     => '03:00:00',
            'resto_id'                => '6288171F-550D-8A49-AD12-C791C65432B8',
            'iiko_id'                 => '62392F78-01E2-49C4-BF6F-045E925970EA',
        ], [
            'street'   => 'ул. Петровка',
            'building' => '3/6, стр. 2',
        ]);
        //Brisket BBQ
        $this->addDataToRestaurant(199, [
            'phone'                   => '7 (964) 647-01-07',
            'phone_string'            => '7 (964) 647-01-07',
            'delivery_phone'          => '7 (964) 647-01-07',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Смоленский б-р',
            'building' => '15',
        ]);
        //BRO&N
//        $this->addDataToRestaurant(225, [
//            'phone'                   => '7(495) 650-00-50',
//            'phone_string'            => '7(495) 650-00-50, 7 (909) 650-66-50',
//            'delivery_phone'          => null,
//            'work_time_weekdays_from' => '12:00:00',
//            'work_time_weekdays_to'   => '00:00:00',
//            'work_time_friday_from'   => '12:00:00',
//            'work_time_friday_to'     => '00:00:00',
//            'work_time_saturday_from' => '12:00:00',
//            'work_time_saturday_to'   => '00:00:00',
//            'work_time_sunday_from'   => '12:00:00',
//            'work_time_sunday_to'     => '00:00:00',
//            'resto_id'                => null,
//            'iiko_id'                 => null,
//        ], [
//            'street'   => '',
//            'building' => '',
//        ]);
        //Cantinetta Antinori
        $this->addDataToRestaurant(38, [
            'phone'                   => '7 (499) 241-33-25',
            'phone_string'            => '7 (499) 241-33-25, ДОСТАВКА: +7 985 760 48 00',
            'delivery_phone'          => '7 (985) 760-48-00',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7432-3000-0000-000000000000',
            'iiko_id'                 => '986E1B12-3A4A-4330-9105-00CCFDA13840',
        ], [
            'street'   => 'Денежный пер.',
            'building' => '20',
        ]);
        //China Сlub
        $this->addDataToRestaurant(19, [
            'phone'                   => '7 (495) 232-27-78',
            'phone_string'            => '7 (495) 249-55-60',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7432-3100-0000-000000000000',
            'iiko_id'                 => '5122A151-9577-47B8-B11C-84333AFCB957',
        ], [
            'street'   => 'ул. Красина',
            'building' => '21',
        ]);
        //Chips
        $this->addDataToRestaurant(22, [
            'phone'                   => '7 (495) 628-68-67',
            'phone_string'            => '7 (495) 628-68-67',
            'delivery_phone'          => '7 (495) 628-68-67',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'DE0557DC-34D6-E049-9504-E918F2DEA221',
            'iiko_id'                 => '8C86A6F4-E3EF-4359-B244-B965894D94E4',
        ], [
            'street'   => 'ул. Кузнецкий Мост',
            'building' => '7',
        ]);
        //Crabber
        $this->addDataToRestaurant(242, [
            'phone'                   => '7 (495) 739-69-81',
            'phone_string'            => '7 (495) 739-69-81, 7 (495) 739-73-74',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '11:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '11:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '11:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '11:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Пресненская наб',
            'building' => '10',
        ]);
        //Francesco
        $this->addDataToRestaurant(21, [
            'phone'                   => '7 (812) 275-05-52',
            'phone_string'            => '7 (812) 275-05-52',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '01:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '01:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '01:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '01:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Суворовский пр-т',
            'building' => '47',
        ]);
        //Fumisawa Sushi
        $this->addDataToRestaurant(165, [
            'phone'                   => '7 (495) 775-19-69',
            'phone_string'            => '7 (495) 775-19-69',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '2DD87FBB-4CE7-7443-9DE8-5A8C7C6610A8',
            'iiko_id'                 => '37804C6B-8C6E-41EC-BC46-F9078239CF93',
        ], [
            'street'   => 'ул. Петровка',
            'building' => '5',
        ]);
        //Krispy Kreme
        $this->addDataToRestaurant(40, [
            'phone'                   => '',
            'phone_string'            => '',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '08:00:00',
            'work_time_weekdays_to'   => '23:00:00',
            'work_time_friday_from'   => '08:00:00',
            'work_time_friday_to'     => '23:00:00',
            'work_time_saturday_from' => '08:00:00',
            'work_time_saturday_to'   => '23:00:00',
            'work_time_sunday_from'   => '08:00:00',
            'work_time_sunday_to'     => '23:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //Luce
        $this->addDataToRestaurant(42, [
            'phone'                   => '7 (495) 797-63-08',
            'phone_string'            => '7 (495) 797-63-08, 7 (495) 797-63-09',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'ул. 1-я Тверская-Ямская',
            'building' => '21',
        ]);
        //Mr. Lee
        $this->addDataToRestaurant(45, [
            'phone'                   => '7 (495) 628-76-78',
            'phone_string'            => '7 (495) 628-76-78',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '01:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '01:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '01:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '01:00:00',
            'resto_id'                => '77A99F39-21A1-5148-BC09-316B2BC0811D',
            'iiko_id'                 => 'AD5A1139-1A00-464F-B808-A124E3687D75',
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //Nofar
        $this->addDataToRestaurant(221, [
            'phone'                   => '7 (495) 933-21-23',
            'phone_string'            => '7 (495) 933-21-23',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Кутузовский проспект',
            'building' => '12 стр. 3',
        ]);
        //Novikov Catering
        $this->addDataToRestaurant(67, [
            'phone'                   => '7 (985) 768-83-33',
            'phone_string'            => 'ДОСТАВКА 24/7: 7 (985) 768-83-33. БАНКЕТЫ: 7 (985) 226-56-04',
            'delivery_phone'          => '7 (985) 768-83-33',
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => null,
            'work_time_friday_to'     => null,
            'work_time_saturday_from' => null,
            'work_time_saturday_to'   => null,
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Берсеневский пер.',
            'building' => '2 стр. 1',
        ]);
        //NOVIKOV DUBAI
        $this->addDataToRestaurant(177, [
            'phone'                   => null,
            'phone_string'            => '+971 (0) 43 888 744',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '18:00:00',
            'work_time_weekdays_to'   => '02:00:00',
            'work_time_friday_from'   => '18:00:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '18:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '18:00:00',
            'work_time_sunday_to'     => '02:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Sheikh Zayed Road',
            'building' => '3',
        ]);
        //NOVIKOV London
        $this->addDataToRestaurant(70, [
            'phone'                   => null,
            'phone_string'            => '+44 20 7399 43 30',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => null,
            'work_time_friday_to'     => null,
            'work_time_saturday_from' => null,
            'work_time_saturday_to'   => null,
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Berkeley Street, Mayfair',
            'building' => '50A',
        ]);
        //NOVIKOV Miami
        $this->addDataToRestaurant(254, [
            'phone'                   => null,
            'phone_string'            => '+1 (305) 489-1000',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => null,
            'work_time_friday_to'     => null,
            'work_time_saturday_from' => null,
            'work_time_saturday_to'   => null,
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'South Biscayne Boulevard',
            'building' => '300',
        ]);
        //Novikov Restaurant & Bar
        $this->addDataToRestaurant(73, [
            'phone'                   => '7 (495) 797-57-20',
            'phone_string'            => '7 (495) 797-57-20',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '58ED245E-8480-054D-A949-18983D86F37D',
            'iiko_id'                 => 'F9B7F46D-8F55-4F12-A8BD-4BAD2BD8FACD',
        ], [
            'street'   => 'Тверская ул.,',
            'building' => '3',
        ]);
        //Novikov Riyadh
        $this->addDataToRestaurant(256, [
            'phone'                   => null,
            'phone_string'            => null,
            'delivery_phone'          => null,
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => null,
            'work_time_friday_to'     => null,
            'work_time_saturday_from' => null,
            'work_time_saturday_to'   => null,
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //Novikov School
//        $this->addDataToRestaurant(223, [
//            'phone'                   => null,
//            'phone_string'            => null,
//            'delivery_phone'          => null,
//            'work_time_weekdays_from' => null,
//            'work_time_weekdays_to'   => null,
//            'work_time_friday_from'   => null,
//            'work_time_friday_to'     => null,
//            'work_time_saturday_from' => null,
//            'work_time_saturday_to'   => null,
//            'work_time_sunday_from'   => null,
//            'work_time_sunday_to'     => null,
//            'resto_id'                => null,
//            'iiko_id'                 => null,
//        ], [
//            'street'   => '',
//            'building' => '',
//        ]);
        //Prime Star
        $this->addDataToRestaurant(51, [
            'phone'                   => null,
            'phone_string'            => null,
            'delivery_phone'          => null,
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => null,
            'work_time_friday_to'     => null,
            'work_time_saturday_from' => null,
            'work_time_saturday_to'   => null,
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Улица Арбат',
            'building' => '1',
        ]);
        //Shore House
        $this->addDataToRestaurant(57, [
            'phone'                   => '7 (495) 727-26-78',
            'phone_string'            => '7 (495) 727-26-78',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7432-3400-0000-000000000000',
            'iiko_id'                 => '82E3B0ED-0359-43F0-BA58-2676496A062E',
        ], [
            'street'   => 'Крокус Сити',
            'building' => '66 км МКАД',
        ]);
        //Sirena
        $this->addDataToRestaurant(16, [
            'phone'                   => '7 (495) 608-14-12',
            'phone_string'            => '7 (495) 608-14-12, 7 (495) 767-14-12',
            'delivery_phone'          => '7 (495) 608-14-12',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7431-3600-0000-000000000000',
            'iiko_id'                 => 'CE115956-3C5A-4AFA-8EB7-AC1B1F6F95DF',
        ], [
            'street'   => 'Большая Спасская',
            'building' => '15',
        ]);
        //Tatler Club
        $this->addDataToRestaurant(49, [
            'phone'                   => '7 (495) 229-83-05',
            'phone_string'            => '7 (495) 229-83-05',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '2D949AC9-9040-374D-BC00-CD70D2B792D7',
            'iiko_id'                 => 'F0D6E3A3-F5B7-48C9-BB29-5F1D09961434',
        ], [
            'street'   => 'Кутузовский пр-т',
            'building' => '2/1',
        ]);
        //Umma
//        $this->addDataToRestaurant(251, [
//            'phone'                   => '7 (495) 229-83-05',
//            'phone_string'            => '7 (495) 229-83-05',
//            'delivery_phone'          => null,
//            'work_time_weekdays_from' => '12:00:00',
//            'work_time_weekdays_to'   => '00:00:00',
//            'work_time_friday_from'   => '12:00:00',
//            'work_time_friday_to'     => '00:00:00',
//            'work_time_saturday_from' => '12:00:00',
//            'work_time_saturday_to'   => '00:00:00',
//            'work_time_sunday_from'   => '12:00:00',
//            'work_time_sunday_to'     => '00:00:00',
//            'resto_id'                => null,
//            'iiko_id'                 => null,
//        ], [
//            'street'   => 'Кутузовский пр-т',
//            'building' => '2/1',
//        ]);
        //VALENOK
        $this->addDataToRestaurant(198, [
            'phone'                   => '7 (903) 505-60-45',
            'phone_string'            => '7 (903) 505-60-45, 7 (499) 290-02-15, 7 (499) 290-02-16',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '05:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '06:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'FA9B3DB9-6FD1-9E48-824F-ECC0AF95DF5E',
            'iiko_id'                 => '87E8F07E-537D-4B01-9F5A-0D068C6E2CAD',
        ], [
            'street'   => 'Цветной б-р',
            'building' => '5',
        ]);
        //Vaниль
        $this->addDataToRestaurant(31, [
            'phone'                   => '7 (495) 637-10-82',
            'phone_string'            => '7 (495) 637-10-82',
            'delivery_phone'          => '7 (495) 637-10-82',
            'work_time_weekdays_from' => '08:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '08:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '10:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '10:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7435-0000-0000-000000000000',
            'iiko_id'                 => 'B6AFF055-3A55-4027-B97F-099F78AB6E95',
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //Vogue cafe
        $this->addDataToRestaurant(33, [
            'phone'                   => '7 (495) 623-17-01',
            'phone_string'            => '7 (495) 623-17-01',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '08:30:00',
            'work_time_weekdays_to'   => '01:00:00',
            'work_time_friday_from'   => '08:30:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '01:00:00',
            'resto_id'                => '00726573-7432-0000-0000-000000000000',
            'iiko_id'                 => 'E5999E92-342D-4647-BFA2-EF5AED92B4CB',
        ], [
            'street'   => 'Кузнецкий Мост',
            'building' => '7/9',
        ]);
        //Vоdный
        $this->addDataToRestaurant(17, [
            'phone'                   => '7 (495) 783-13-90',
            'phone_string'            => '7 (495) 783-13-90',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '6F8087EA-DD1C-6445-A6A5-26A868404DAC',
            'iiko_id'                 => '81847493-28D5-4D8D-99F8-0D9FA72F76BB',
        ], [
            'street'   => 'Ленинградское ш.',
            'building' => '39 стр. 6',
        ]);
        //White Сafe
        $this->addDataToRestaurant(30, [
            'phone'                   => '7 (495) 690-71-60',
            'phone_string'            => '7 (495) 690-71-60, ДОСТАВКА: 7 (964) 574-12-36',
            'delivery_phone'          => '7 (964) 574-12-36',
            'work_time_weekdays_from' => '09:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '09:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '237BD727-FD92-0747-97C0-DBDE5B4B9D72',
            'iiko_id'                 => 'D10CD2AB-595C-40C5-868B-42693AB6E24B',
        ], [
            'street'   => 'Новый Арбат',
            'building' => '12',
        ]);
        //Yoko
        $this->addDataToRestaurant(66, [
            'phone'                   => '7 (495) 637-13-70',
            'phone_string'            => '7 (495) 637-13-70, 7 (495) 637-13-71',
            'delivery_phone'          => '7 (495) 637-13-71',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'AEC9708D-7245-5B4C-B22D-3688BD3514CC',
            'iiko_id'                 => 'E92EC106-DE32-452E-AE2C-D753656E6317',
        ], [
            'street'   => 'Соймоновский пр-д',
            'building' => '5',
        ]);
        //YOKO НОВЫЙ АРБАТ
        $this->addDataToRestaurant(197, [
            'phone'                   => '7 (495) 995-88-86',
            'phone_string'            => '7 (495) 995-88-86, 7 (495) 995-88-87',
            'delivery_phone'          => '7 (495) 995-88-87',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '2C3A9F62-AEA5-904F-BCAA-4373BFC5B279',
            'iiko_id'                 => '8412369D-F816-4281-AD31-C95E07960E6D',
        ], [
            'street'   => 'Новый Арбат',
            'building' => '32',
        ]);
        //ZOО Beer&Grill
        $this->addDataToRestaurant(208, [
            'phone'                   => '7 (495) 114-51-93',
            'phone_string'            => '7 (495) 114-51-93, ДОСТАВКА: 7 (965) 145-04-34',
            'delivery_phone'          => '7 (965) 145-04-34',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'CA952924-CD1A-9F4B-B7BE-E4409A17DD47',
            'iiko_id'                 => 'C6525DD0-3CAC-41FB-B4E3-A913F5233060',
        ], [
            'street'   => 'Конюшковская',
            'building' => '34 стр.1',
        ]);
        //Аист
        $this->addDataToRestaurant(28, [
            'phone'                   => '7 (499) 940-70-40',
            'phone_string'            => '7 (499) 940-70-40, 7 (499) 940-70-30, 7 (985) 470-70-30',
            'delivery_phone'          => '7 (499) 940-70-40',
            'work_time_weekdays_from' => '09:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '09:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '13:00:00',
            'work_time_saturday_to'   => '01:00:00',
            'work_time_sunday_from'   => '10:00:00',
            'work_time_sunday_to'     => '01:00:00',
            'resto_id'                => '00726573-7433-3000-0000-000000000000',
            'iiko_id'                 => '872606B8-F2F4-4EED-AA53-E6408795CB6F',
        ], [
            'street'   => 'Малая Бронная',
            'building' => '8/1',
        ]);
        //Бараshка на Арбате
        $this->addDataToRestaurant(26, [
            'phone'                   => '7 (495) 228-37-30',
            'phone_string'            => '7 (495) 228-37-30, 7 (495) 228-37-31',
            'delivery_phone'          => '7 (495) 228-37-30',
            'work_time_weekdays_from' => '11:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '11:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '11:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '11:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '04341208-3FE7-5140-85F1-532EA11E4764',
            'iiko_id'                 => '302803F7-9D65-41E4-A5CD-1DDB0CE72BD4',
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //Бараshка на Красной Пресне
        $this->addDataToRestaurant(27, [
            'phone'                   => '7 (495) 653-83-03',
            'phone_string'            => '7 (495) 653-83-03, 7 (495) 653-83-84',
            'delivery_phone'          => '7 (495) 653-83-84',
            'work_time_weekdays_from' => '11:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '11:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '11:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '11:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'C8DFC2D3-AD85-B64F-A0EF-5DF23C621B2C',
            'iiko_id'                 => 'D24F27FA-A55D-4A20-864E-F9F5DB683D80',
        ], [
            'street'   => '1905 года',
            'building' => '2 стр. 1',
        ]);
        //Бараshка на Петровке
        $this->addDataToRestaurant(20, [
            'phone'                   => '7 (495) 625-28-92',
            'phone_string'            => '7 (495) 625-28-92',
            'delivery_phone'          => '7 (495) 625-28-92',
            'work_time_weekdays_from' => '11:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '11:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '11:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '11:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7432-3200-0000-000000000000',
            'iiko_id'                 => '7A545CCF-11CD-4616-8AAE-F035B647D6B2',
        ], [
            'street'   => 'Петровка',
            'building' => '20/1',
        ]);
        //Белое солнце пустыни
        $this->addDataToRestaurant(29, [
            'phone'                   => '7 (495) 625-25-96',
            'phone_string'            => '7 (495) 625-25-96, 7 (495) 625-33-93, 7 (495) 625-29-47',
            'delivery_phone'          => '7 (495) 625-25-96',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7434-0000-0000-000000000000',
            'iiko_id'                 => '8B711676-5803-4E3D-9008-6911DECBE7BD',
        ], [
            'street'   => 'Неглинная',
            'building' => '29',
        ]);
        //БОК
//        $this->addDataToRestaurant(229, [
//            'phone'                   => '7 (495) 625-26-06',
//            'phone_string'            => '7 (495) 625-26-06',
//            'delivery_phone'          => null,
//            'work_time_weekdays_from' => '12:00:00',
//            'work_time_weekdays_to'   => '00:00:00',
//            'work_time_friday_from'   => '12:00:00',
//            'work_time_friday_to'     => '00:00:00',
//            'work_time_saturday_from' => '12:00:00',
//            'work_time_saturday_to'   => '00:00:00',
//            'work_time_sunday_from'   => '12:00:00',
//            'work_time_sunday_to'     => '00:00:00',
//            'resto_id'                => null,
//            'iiko_id'                 => null,
//        ], [
//            'street'   => '',
//            'building' => '',
//        ]);
        //Веранда у Дачи
        $this->addDataToRestaurant(61, [
            'phone'                   => '7 (495) 635-33-94',
            'phone_string'            => '+7 495 635 3394, +7 916 706 6856, доставка: +7 916 706 6856',
            'delivery_phone'          => '7 (916) 706-68-56',
            'work_time_weekdays_from' => '08:30:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '08:30:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7437-0000-0000-000000000000',
            'iiko_id'                 => '930512C3-2708-4727-A377-0333A38AD49F',
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //Ветерок
        $this->addDataToRestaurant(32, [
            'phone'                   => '7 (495) 920-57-34',
            'phone_string'            => '7 (495) 920-57-34',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '1F3C5C42-9C44-854C-9003-3D2350581FDD',
            'iiko_id'                 => 'EBB3E162-250E-4623-BB4D-BB664BCECE87',
        ], [
            'street'   => 'Рублево-Успенское ш., пос. Горки-2',
            'building' => '24',
        ]);
        //Донна Маргарита
        $this->addDataToRestaurant(35, [
            'phone'                   => '7 (499) 682-70-00',
            'phone_string'            => '7 (499) 682-70-00',
            'delivery_phone'          => '7 (499) 682-70-00',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '1D8E7C79-4BAC-024C-B029-3AA3B1548D87',
            'iiko_id'                 => 'FD8D9AC1-AC16-4521-8D06-C4DE7DD0F379',
        ], [
            'street'   => 'ул. 1905 года',
            'building' => '2 стр. 1',
        ]);
        //Кавказская пленница
        $this->addDataToRestaurant(36, [
            'phone'                   => '7 (495) 680-51-11',
            'phone_string'            => '7 (495) 680-51-11, 7 (495) 680-51-77, 7 (925) 361-93-43',
            'delivery_phone'          => '7 (925) 361-93-43',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7431-3000-0000-000000000000',
            'iiko_id'                 => '418816A1-1915-4164-B42B-4347700BA502',
        ], [
            'street'   => 'Проспект Мира',
            'building' => '36',
        ]);
        //Камчатка на Кузнецком Мосту
        $this->addDataToRestaurant(94, [
            'phone'                   => '7 (495) 624-88-25',
            'phone_string'            => '7 (495) 624-88-25',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '01:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '06:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '06:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '01:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Кузнецкий Мост',
            'building' => '7',
        ]);
        //Клёво
        $this->addDataToRestaurant(179, [
            'phone'                   => '7 (495) 790-15-96',
            'phone_string'            => '7 (495) 790-15-96',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Петровка',
            'building' => '27',
        ]);
        //Колбасный Цех
        $this->addDataToRestaurant(238, [
            'phone'                   => '7 (495) 249-55-60',
            'phone_string'            => '7 (495) 249-55-60',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Кутузовский проспект',
            'building' => 'д. 12, строение 1',
        ]);
        //Куршевель
        $this->addDataToRestaurant(41, [
            'phone'                   => '7 (495) 724-88-83',
            'phone_string'            => '7 (495) 724-88-83',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '21:00:00',
            'work_time_weekdays_to'   => '06:00:00',
            'work_time_friday_from'   => '21:00:00',
            'work_time_friday_to'     => '08:00:00',
            'work_time_saturday_from' => '21:00:00',
            'work_time_saturday_to'   => '08:00:00',
            'work_time_sunday_from'   => '21:00:00',
            'work_time_sunday_to'     => '06:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Кузнецкий Мост',
            'building' => '7',
        ]);
        //Маркет
        $this->addDataToRestaurant(43, [
            'phone'                   => '7 (495) 650-37-70',
            'phone_string'            => '7 (495) 650-37-70, 7 (495) 650-41-31',
            'delivery_phone'          => '7 (495) 650-41-31',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7431-3200-0000-000000000000',
            'iiko_id'                 => 'A06798F2-0B6C-4449-8CA8-2B879C4AF32B',
        ], [
            'street'   => 'Садовая-Самотечная',
            'building' => '18 стр. 1',
        ]);
        //Недальний восток
        $this->addDataToRestaurant(14, [
            'phone'                   => '7 (495) 694-06-41',
            'phone_string'            => '7 (495) 694-06-41, 7 (495) 694-01-54',
            'delivery_phone'          => '7 (495) 694-01-54',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7433-3200-0000-000000000000',
            'iiko_id'                 => 'BF331B11-0DC1-404D-A41E-134240F62489',
        ], [
            'street'   => 'Тверской б-р',
            'building' => '15 стр. 2',
        ]);
        //Причал
        $this->addDataToRestaurant(52, [
            'phone'                   => '7 (495) 635-40-32',
            'phone_string'            => '7 (495) 635-40-32, 7 (495) 978-06-78',
            'delivery_phone'          => '7 (495) 978-06-78',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7431-3400-0000-000000000000',
            'iiko_id'                 => '849310E6-DCF5-4EAF-ACF0-782EB2487C26',
        ], [
            'street'   => 'Ильинское шоссе',
            'building' => '2-й км, стр. 1',
        ]);
        //Рыбы нет
        $this->addDataToRestaurant(194, [
            'phone'                   => '7 (495) 258-42-06',
            'phone_string'            => '7 (495) 258-42-06',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '02:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '02:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '7AC84A6E-AEDA-264E-860E-4F8D4FB166B8',
            'iiko_id'                 => '070F7A04-D610-4253-8300-FA53E95C7E9A',
        ], [
            'street'   => 'Никольская',
            'building' => '12',
        ]);
        //Страна которой нет
        $this->addDataToRestaurant(53, [
            'phone'                   => '7 (495) 259-70-80',
            'phone_string'            => '7 (495) 259-70-80',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '10:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '10:00:00',
            'work_time_friday_to'     => '01:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '01:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'A6DE4852-5170-7E44-9FC7-A11D1EA4E4B8',
            'iiko_id'                 => '6347D837-8EE4-4B01-B7F4-367784C8138E',
        ], [
            'street'   => 'Охотный Ряд',
            'building' => '2',
        ]);
        //Сыроварня
        $this->addDataToRestaurant(180, [
            'phone'                   => '7 (495) 803-24-01',
            'phone_string'            => '7 (495) 803-24-01',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '01:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '01:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '804C5F80-F144-1043-BD72-5DE4AB595D3A',
            'iiko_id'                 => '417BFC1B-91D1-457A-8A27-019CAB437B73',
        ], [
            'street'   => 'Кутузовский пр-т',
            'building' => '12, стр. 1',
        ]);
        //Сыроварня на Октябре
        $this->addDataToRestaurant(217, [
            'phone'                   => '7 (495) 727-38-80',
            'phone_string'            => '7 (495) 727-38-80',
            'delivery_phone'          => '7 (495) 727-38-80',
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => null,
            'work_time_friday_to'     => null,
            'work_time_saturday_from' => null,
            'work_time_saturday_to'   => null,
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => 'B1E9B37C-7CBE-8041-9B93-859F479A2081',
            'iiko_id'                 => 'BAA88EE5-AD17-46FF-8494-8E0BC0C43AB7',
        ], [
            'street'   => '',
            'building' => '',
        ]);
        //Узбекистан
        $this->addDataToRestaurant(54, [
            'phone'                   => '7 (495) 623-05-85',
            'phone_string'            => '7 (495) 623-05-85, 7 (495) 624-60-53, 7 (495) 625-32-84',
            'delivery_phone'          => '7 (495) 625-32-84',
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => '00726573-7431-3700-0000-000000000000',
            'iiko_id'                 => 'A2B572B1-C771-4AA5-8098-E5CDEB64D12F',
        ], [
            'street'   => 'Неглинная',
            'building' => '29',
        ]);
        //Царская охота
        $this->addDataToRestaurant(55, [
            'phone'                   => '7 (495) 635-79-82',
            'phone_string'            => '7 (495) 635-79-82',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '23:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '23:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '23:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '23:00:00',
            'resto_id'                => '00726573-7431-3800-0000-000000000000',
            'iiko_id'                 => '64357426-54EF-44B4-997A-BB81B17EB40F',
        ], [
            'street'   => 'Рублево-Успенское ш., дер. Жуковка',
            'building' => '186А',
        ]);
        //Чайковский
        $this->addDataToRestaurant(60, [
            'phone'                   => '7 (495) 699-91-14',
            'phone_string'            => '7 (495) 699-91-14, 7 (495) 699-92-41',
            'delivery_phone'          => '7 (495) 699-92-41',
            'work_time_weekdays_from' => '08:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '08:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '10:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '10:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => 'CC1FE040-8635-A94E-BC31-06B11347A492',
            'iiko_id'                 => 'A514CC19-388B-4C9F-AB5C-5935CDF6A216',
        ], [
            'street'   => 'Триумфальная пл',
            'building' => '4/31',
        ]);
        //Чайковский в Тифлисе
        $this->addDataToRestaurant(75, [
            'phone'                   => '7 (495) 699-90-12',
            'phone_string'            => '7 (495) 699-90-12',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => '12:00:00',
            'work_time_weekdays_to'   => '00:00:00',
            'work_time_friday_from'   => '12:00:00',
            'work_time_friday_to'     => '00:00:00',
            'work_time_saturday_from' => '12:00:00',
            'work_time_saturday_to'   => '00:00:00',
            'work_time_sunday_from'   => '12:00:00',
            'work_time_sunday_to'     => '00:00:00',
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Триумфальная пл',
            'building' => '4/31',
        ]);
        //Чердак
        $this->addDataToRestaurant(56, [
            'phone'                   => '7 (495) 628 76-78',
            'phone_string'            => '7 (495) 628 76-78',
            'delivery_phone'          => null,
            'work_time_weekdays_from' => null,
            'work_time_weekdays_to'   => null,
            'work_time_friday_from'   => '18:00:00',
            'work_time_friday_to'     => '05:00:00',
            'work_time_saturday_from' => '18:00:00',
            'work_time_saturday_to'   => '05:00:00',
            'work_time_sunday_from'   => null,
            'work_time_sunday_to'     => null,
            'resto_id'                => null,
            'iiko_id'                 => null,
        ], [
            'street'   => 'Кузнецкий Мост',
            'building' => '7',
        ]);
	}
}
