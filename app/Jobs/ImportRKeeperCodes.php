<?php
namespace App\Jobs;

use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportRKeeperCodes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the import.
     *
     * @return void
     */
    public function handle()
    {
	    $this->importDiscountCardTypeCodes();
	    $this->importRestaurantRKeeperCodes();
	    echo "Импорт кодов Rkeeper завершен\n\n";
    }

	private function addRKeeperCodeToDiscountCardType($external_id, $rkeeper_id = null)
	{
		DB::table('discount_card_types')
			->where('external_id', $external_id)
			->update([
				'rkeeper_id' => $rkeeper_id,
			]);
	}

	private function addRKeeperCodeToRestaurant($external_id, $rkeeper_id = null)
	{
		DB::table('restaurants')
			->where('external_id', $external_id)
			->update([
				'rkeeper_id' => $rkeeper_id,
			]);
	}

	private function importDiscountCardTypeCodes()
	{
		//Antinori Вино
		$this->addRKeeperCodeToDiscountCardType('B9195637-51A5-AA49-BEB6-07C44308AAF2', '31');
        //Директор 70%
        $this->addRKeeperCodeToDiscountCardType('4CA22C4C-24FB-F742-BB7C-07E51500FC64', '64');
        //Директор 50%
        $this->addRKeeperCodeToDiscountCardType('B390C356-3DA4-924B-B6F1-0E0BF3B100F7', '61');
        //скидка  25%
        $this->addRKeeperCodeToDiscountCardType('9A038D2A-290E-8344-8D3A-26F8C6F3B8DB', '25');
        //Северсталь (авиа)
        $this->addRKeeperCodeToDiscountCardType('292C1ACB-A464-FC48-AAA2-297D502EDBAA', '75');
        //АБ золотая карта
        $this->addRKeeperCodeToDiscountCardType('B11B2743-BF07-5D4F-BB4D-30CA7D80F48D', '99');
        //Дневная скидка 20%
        $this->addRKeeperCodeToDiscountCardType('52952DEB-800C-4649-9302-37984245FF69', '60');
        //Страна Вино
        $this->addRKeeperCodeToDiscountCardType('9D706573-08EB-BE44-9B6F-3B74A32CD30F', '80');
        //АБ списание
        $this->addRKeeperCodeToDiscountCardType('9EB7A0BB-0D78-6644-A80B-3BCADCBAB546', '97');
        //Самолеты 30%
        $this->addRKeeperCodeToDiscountCardType('E9DA2321-D11A-1945-81C9-40EBDA680DD1', '72');
        //Директор 15%
        $this->addRKeeperCodeToDiscountCardType('14DE4335-8217-E148-B1D3-44E4827C5D88', '66');
        //Скидка 50%
        $this->addRKeeperCodeToDiscountCardType('36056B74-B7FC-2D44-BDCF-4CE33226D0D5', '28');
        //Скидка 5%
        $this->addRKeeperCodeToDiscountCardType('13610BB8-6AA6-6945-9299-50279948464F', '77');
        //Директор 75%
        $this->addRKeeperCodeToDiscountCardType('C1885DDC-298F-354F-A4CC-64CB9FE8A202', '76');
        //Самолеты напитки
        $this->addRKeeperCodeToDiscountCardType('EE83265D-4D55-AB40-BFB5-681351E4E1DD', '74');
        //Директор 35%
        $this->addRKeeperCodeToDiscountCardType('C392B752-0D7D-6D49-B070-6A8BEF098ED4', '57');
        //Директор 40%
        $this->addRKeeperCodeToDiscountCardType('BC06CCDB-3D14-924F-A4D9-709AFAF8870C', '62');
        //Театральная скидка 10
        $this->addRKeeperCodeToDiscountCardType('63FCBC16-9603-0047-9764-7105E6137759', '11');
        //Скидка 20%
        $this->addRKeeperCodeToDiscountCardType('6992E8FE-6A29-7E4A-8C53-71474CFCDA89', '40');
        //Скидка 10%
        $this->addRKeeperCodeToDiscountCardType('0F76450B-4028-83DF-010F-76450BF00001', '30');
        //Luce Wine X
        $this->addRKeeperCodeToDiscountCardType('91E59A05-8ABD-CF40-AB91-8053C4D62B19', '91');
        //Обслуживание 10%
        $this->addRKeeperCodeToDiscountCardType('043A4119-3F59-3549-92A6-86F9FC53C820', '73');
        //VISA 5
        $this->addRKeeperCodeToDiscountCardType('DD6EF9DF-28AF-6043-8D57-8DFBBED8470A', '26');
        //VISA 10
        $this->addRKeeperCodeToDiscountCardType('E6B45D4B-256E-B941-B30B-907575DD5652', '24');
        //АБ начисление
        $this->addRKeeperCodeToDiscountCardType('E70A6B81-38A2-674B-811D-9238B0F43A88', '98');
        //Директор 60%
        $this->addRKeeperCodeToDiscountCardType('120649EF-4397-B940-8190-9B0B27D8AB54', '63');
        //Директор 30%
        $this->addRKeeperCodeToDiscountCardType('CD31AF83-CC1E-FB48-AC74-A8CAB6F69450', '59');
        //Директор 10%
        $this->addRKeeperCodeToDiscountCardType('DAC7824B-974F-BE41-8B7E-ACF5AF573CDF', '55');
        //План Водный
        $this->addRKeeperCodeToDiscountCardType('87BFE248-8909-3D4F-9208-B77EFA2D7E66', '70');
        //Чайковский-Гинза
        $this->addRKeeperCodeToDiscountCardType('6794A785-E31C-B446-915A-BAA479088428', '10');
        //Нет скидки
        $this->addRKeeperCodeToDiscountCardType('9D33F0E0-7756-7448-ADDC-CBD8267322BF', '100');
        //Директор 20%
        $this->addRKeeperCodeToDiscountCardType('10920A34-BE9D-A24F-BAC3-D5E07437C3BB', '56');
        //Директор 25%
        $this->addRKeeperCodeToDiscountCardType('E1D6E644-05EA-0944-9688-D9ECBE3762EF', '58');
        //Директор 3,5%
        $this->addRKeeperCodeToDiscountCardType('65985AE2-C155-E047-834E-E129D754B565', '27');
        //Благотворительный взнос
        $this->addRKeeperCodeToDiscountCardType('E4245975-8EF8-9442-AC56-E22B563D51AB', '22');
        //MasterCard
        $this->addRKeeperCodeToDiscountCardType('E55D1B8B-B71C-7D42-8DF4-E4BC428A105C', '23');
        //RestoTube
        $this->addRKeeperCodeToDiscountCardType('C318E99F-EB45-664D-B87D-E9EAC947C121', '90');
        //Прайм
        $this->addRKeeperCodeToDiscountCardType('3E78A007-463F-2540-A887-F145DCD33C85', '50');
        //Театральная скидка 5
        $this->addRKeeperCodeToDiscountCardType('56654639-2F87-A443-963B-F1FE000803EB', '12');
        //ToGo алкоголь
        $this->addRKeeperCodeToDiscountCardType('D1E6780D-46A7-A440-9C79-F6D88BE3EBF9', '71');
        //Luce Wine XX
        $this->addRKeeperCodeToDiscountCardType('D10009B2-3358-5148-A975-F830A13B5FF7', '92');
        //Директор 90%
        $this->addRKeeperCodeToDiscountCardType('68C4B13E-102B-2A4D-A198-FA9A255AF026', '67');
		//Дневная скидка 30%
		$this->addRKeeperCodeToDiscountCardType('D40583A5-526C-C241-B5CA-BA7ECABA0478', '68');
        //Скидка 30%
        $this->addRKeeperCodeToDiscountCardType('45C176E7-CA64-9548-89E9-FE084DA18223', '29');
		//Директор 80%
		$this->addRKeeperCodeToDiscountCardType('A69A6B56-334C-444A-8C5B-DA37F28C279A', '65');
	}

	private function importRestaurantRKeeperCodes()
	{
		//Crabber на Красном Октябре
		$this->addRKeeperCodeToRestaurant('258', '78');
		//Гастромаркет Вокруг Света
		$this->addRKeeperCodeToRestaurant('260');
		//Китайская Забегаловка
		$this->addRKeeperCodeToRestaurant('244', '74');
		//Колбасный Цех
		$this->addRKeeperCodeToRestaurant('238', '72');
		//Колбасный Цех Шереметьево
		$this->addRKeeperCodeToRestaurant('248', '79');
		//Магадан
		$this->addRKeeperCodeToRestaurant('246', '77');
		//Сыроварня Санкт-Петербург
		$this->addRKeeperCodeToRestaurant('252', '76');
		//#FARШ
		$this->addRKeeperCodeToRestaurant('173');
		//15/17 BAR&GRILL
		$this->addRKeeperCodeToRestaurant('233', '71');
		//5642 Высота
		$this->addRKeeperCodeToRestaurant('58', '58');
		//5642 ВЫСОТА КИСЛОВОДСК
		$this->addRKeeperCodeToRestaurant('219', '68');
		//5642 ВЫСОТА СОЧИ
		$this->addRKeeperCodeToRestaurant('212', '66');
		//Bolshoi
		$this->addRKeeperCodeToRestaurant('59', '42');
		//Brisket BBQ
		$this->addRKeeperCodeToRestaurant('199', '33');
		//Bro&N
		$this->addRKeeperCodeToRestaurant('225', '69');
		//Cantinetta Antinori
		$this->addRKeeperCodeToRestaurant('38', '20');
		//China Сlub
		$this->addRKeeperCodeToRestaurant('19', '21');
		//Chips
		$this->addRKeeperCodeToRestaurant('22', '55');
		//Crabber
		$this->addRKeeperCodeToRestaurant('242', '73');
		//Francesco
		$this->addRKeeperCodeToRestaurant('21');
		//Fumisawa Sushi
		$this->addRKeeperCodeToRestaurant('165', '59');
		//Krispy Kreme
		$this->addRKeeperCodeToRestaurant('40');
		//Luce
		$this->addRKeeperCodeToRestaurant('42', '47');
		//Mr. Lee
		$this->addRKeeperCodeToRestaurant('45', '43');
		//Nofar
		$this->addRKeeperCodeToRestaurant('221', '67');
		//Novikov Catering
		$this->addRKeeperCodeToRestaurant('67');
		//NOVIKOV DUBAI
		$this->addRKeeperCodeToRestaurant('177');
		//Novikov London
		$this->addRKeeperCodeToRestaurant('70');
		//Novikov Miami
		$this->addRKeeperCodeToRestaurant('254');
		//Novikov Restaurant & Bar
		$this->addRKeeperCodeToRestaurant('73', '57');
		//Novikov Riyadh
		$this->addRKeeperCodeToRestaurant('256');
		//Prime Star
		$this->addRKeeperCodeToRestaurant('51');
		//Shore House
		$this->addRKeeperCodeToRestaurant('57', '24');
		//Sirena
		$this->addRKeeperCodeToRestaurant('16', '16');
		//Tatler Club
		$this->addRKeeperCodeToRestaurant('49', '46');
		//Umma
		$this->addRKeeperCodeToRestaurant('251', '75');
		//VALENOK
		$this->addRKeeperCodeToRestaurant('198', '62');
		//Vaниль
		$this->addRKeeperCodeToRestaurant('31', '5');
		//Vogue cafe
		$this->addRKeeperCodeToRestaurant('33', '22');
		//Vоdный
		$this->addRKeeperCodeToRestaurant('17', '141');
		//White Сafe
		$this->addRKeeperCodeToRestaurant('30', '41');
		//Yoko
		$this->addRKeeperCodeToRestaurant('66', '35');
		//YOKO НОВЫЙ АРБАТ
		$this->addRKeeperCodeToRestaurant('197', '63');
		//ZOО Beer&Grill
		$this->addRKeeperCodeToRestaurant('208', '64');
		//Аист
		$this->addRKeeperCodeToRestaurant('28', '30');
		//Бараshка на Арбате
		$this->addRKeeperCodeToRestaurant('26', '39');
		//Бараshка на Красной Пресне
		$this->addRKeeperCodeToRestaurant('27', '50');
		//Бараshка на Петровке
		$this->addRKeeperCodeToRestaurant('20', '2');
		//Белое солнце пустыни
		$this->addRKeeperCodeToRestaurant('29', '4');
		//БОК
		$this->addRKeeperCodeToRestaurant('229', '51');
		//Веранда у Дачи
		$this->addRKeeperCodeToRestaurant('61', '7');
		//Ветерок
		$this->addRKeeperCodeToRestaurant('32', '40');
		//Донна Маргарита
		$this->addRKeeperCodeToRestaurant('35', '48');
		//Кавказская пленница
		$this->addRKeeperCodeToRestaurant('36', '10');
		//Камчатка на Кузнецком Мосту
		$this->addRKeeperCodeToRestaurant('94');
		//Клёво
		$this->addRKeeperCodeToRestaurant('179', '8');
		//Куршевель
		$this->addRKeeperCodeToRestaurant('41');
		//Маркет
		$this->addRKeeperCodeToRestaurant('43', '12');
		//Недальний восток
		$this->addRKeeperCodeToRestaurant('14', '32');
		//Причал
		$this->addRKeeperCodeToRestaurant('52', '14');
		//Рыбы нет
		$this->addRKeeperCodeToRestaurant('194', '60');
		//Страна которой нет
		$this->addRKeeperCodeToRestaurant('53', '56');
		//Сыроварня
		$this->addRKeeperCodeToRestaurant('180', '61');
		//Сыроварня на Октябре
		$this->addRKeeperCodeToRestaurant('217', '65');
		//Узбекистан
		$this->addRKeeperCodeToRestaurant('54', '17');
		//Царская охота
		$this->addRKeeperCodeToRestaurant('55', '18');
		//Чайковский
		$this->addRKeeperCodeToRestaurant('60', '139');
		//Чайковский в Тифлисе
		$this->addRKeeperCodeToRestaurant('75', '81');
		//Чердак
		$this->addRKeeperCodeToRestaurant('56', '54');
		//Салон
		$this->addRKeeperCodeToRestaurant('263', '80');
	}
}
