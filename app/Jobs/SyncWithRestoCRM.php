<?php
namespace App\Jobs;

ini_set('max_execution_time', 3600);

use App\Models\Abstracts\Card;
use App\Models\Address;
use App\Models\DiscountCard;
use App\Models\DiscountCardType;
use App\Models\Guest;
use App\Models\Revision;
use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncWithRestoCRM implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the sync.
     *
     * @return void
     */
    public function handle()
    {
	    echo "Синхронизация типов дисконтных карт началась " . date("Y-m-d H:i:s")  . "\n";
	    $this->importDiscountCardTypes();
	    echo "Синхронизация типов дисконтных карт завершена " . date("Y-m-d H:i:s")  . "\n";
	    echo "Синхронизация дисконтных карт началась " . date("Y-m-d H:i:s")  . "\n";
	    $this->importDiscountCards();
	    echo "Синхронизация дисконтных карт завершена " . date("Y-m-d H:i:s")  . "\n\n";
    }

	private function importDiscountCardTypes()
	{
		$rows = DB::connection('restoCRM')
			->select(DB::raw('
		        SELECT
					Id,
					Name
				FROM DiscountType
				WHERE Deleted = 0
            '));

		$discount_card_types = DiscountCardType::whereNotNull('external_id')
			->get()
			->mapWithKeys(function($type) {
				return [$type['external_id'] => $type];
			});
		foreach ($rows as $row) {
			if ($discount_card_types->has($row->Id)) {
				$discount_card_type = $discount_card_types->get($row->Id);
			} else {
				$discount_card_type = new DiscountCardType();
				$discount_card_type->external_id = $row->Id;
			}
			$discount_card_type->name = $row->Name;
			$discount_card_type->save();
		}
	}

	private function importDiscountCards()
	{
		$cards_types = DiscountCardType::select(['id', 'life_time', 'external_id'])->get();
		$revision = Revision::whereName(Revision::DISCOUNT_CARDS)->first();
		$revision_date = $revision->date;
		$date_now = Carbon::now();
		$count_row = DB::connection('restoCRM')
			->select(DB::raw('
		        SELECT
					COUNT(*) AS count
				FROM Card
				WHERE CAST(modified AS DATETIME2) > CAST(? AS DATETIME2)
	        '), [$revision_date]);
		$count = $count_row[0]->count;
		$offset = 0;
		$next = 2000;

		while ($offset < $count) {
			$rows = DB::connection('restoCRM')
				->select(DB::raw('
			        SELECT
						c.id
						, c.modified as updated_at
						, c.number
						, c.created as created_at
						, c.activatedinfo_date as started_at
						, c.blockedinfo_date as blocked_at
						, c.expirationdate as ended_at
						, c.firstusedate as first_used_at
						, c.surname as last_name
						, c.name as first_name
						, c.cellphone as phone
						, c.email
						, c.birthdate
						, c.address
						, c.gender
						, c.codeword
						--, r.id as restaurant_sold_id
						--, r.name as restaurant_sold
						, dt.id as discount_type_id
					FROM Card AS c 
					--LEFT JOIN Restaurant AS r ON r.id = c.restaurantsold
					LEFT JOIN DiscountType AS dt ON dt.id = c.DiscountType
			        WHERE CAST(c.modified AS DATETIME2) > CAST(? AS DATETIME2)
				   	ORDER BY c.modified OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
	        	'), [$revision_date, $offset, $next]);

			foreach ($rows as $row) {
				$card_type = $cards_types->where('external_id', $row->discount_type_id)->first();
				if ($card_type === null) {
					$card_type = $cards_types->where('external_id', DiscountCardType::DEFAULT_CARD_TYPE_UUID)->first();
				}

				$row_revision = new Carbon($row->updated_at);
				//обновляем ревизию, если она больше ревизии из нашей базы и меньше чем текущая дата (баг с неправильным modified)
				if ($row_revision->diffInSeconds($revision->date, false) < 0 && $row_revision->diffInSeconds($date_now, false) > 0) {
					$revision->date = $row_revision;
				}

				if ($row->created_at !== null) {
					$created_at = new Carbon($row->created_at);
				} else {
					$created_at = null;
				}

				if ($row->started_at !== null) {
					$started_at = new Carbon($row->started_at);
					$created_at = is_null($created_at) ? $started_at : $created_at;
					if ($started_at->diffInMinutes($created_at) > 5) {
						$status = Card::ACTIVATED_IN_RESTAURANT;
					} else {
						$status = Card::ACTIVATED_IN_OFFICE;
					}
				} else {
					$started_at = null;
					$created_at = is_null($created_at) ? Carbon::now() : $created_at;
					$status = Card::NOT_ACTIVATED;
				}

				if ($row->ended_at !== null) {
					$ended_at = new Carbon($row->ended_at);
				} else if ($started_at !== null) {
					$ended_at = $started_at->copy()->addDays($card_type->life_time);
				} else {
					$ended_at = null;
				}

				if ($row->blocked_at !== null) {
					$blocked_at = new Carbon($row->blocked_at);
					if ($blocked_at == $ended_at) {
						$status = Card::BLOCKED_EXPIRED;
					} else {
						$status = Card::BLOCKED_MANUALLY;
					}
				} else if (Carbon::now()->diffInDays($ended_at, false) < 0) { //если карта не заблокирована, но истек срок годности
					$blocked_at = $ended_at;
					$status = Card::BLOCKED_EXPIRED;
				} else {
					$blocked_at = null;
				}

				$first_used_at = is_null($row->first_used_at) ? null : new Carbon($row->first_used_at);

				$discount_card = DiscountCard::whereNumber($row->number)->first();

				if ($discount_card === null) {
					$discount_card = new DiscountCard();
					$discount_card->number = $row->number;
					$discount_card->external_id = $row->id;
					$discount_card->created_at = $created_at;
				}

				if ($row->phone !== null) {
					if (!$discount_card->guest_id) {
						$guest = new Guest();
					} else {
						$guest = Guest::whereId($discount_card->guest_id)->with(['addresses'])->first();
					}

					$guest->phone = $row->phone;
					$guest->email = $row->email;
					$guest->first_name = $row->first_name;
					$guest->last_name = $row->last_name;
					$guest->birthday_at = is_null($row->birthdate) ? null : new Carbon($row->birthdate);
					$guest->sex = ($row->gender == 0 || $row->gender == 1) ? $row->gender + 1 : 0;
					$guest->save();

					if ($row->address !== null && $row->address != '') {
						if (!$guest->addresses->count()) {
							$address = new Address();
						} elseif (!($address = $guest->addresses->where('main', '=', true)->first())) {
							$address = $guest->addresses->first();
						}
						$address->main = true;
						$address->addressable_id = $guest->id;
						$address->addressable_type = Guest::class;
						$address->text_ru = trim($row->address);
						$address->city = '';
						$address->street = '';
						$address->building = '';
						$address->save();
					}

					$guest_id = $guest->id;
				} else {
					$guest_id = null;
				}

				$discount_card->card_type_id = $card_type->id;
				$discount_card->guest_id = $guest_id;
				$discount_card->codeword = $row->codeword;
				$discount_card->started_at = $started_at;
				$discount_card->ended_at = $ended_at;
				$discount_card->first_used_at = $first_used_at;
				$discount_card->blocked_at = $blocked_at;
				$discount_card->status = $status;
				$discount_card->save();
			}

			$offset += $next;
		}

		$revision->save();
	}
}
