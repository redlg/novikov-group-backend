<?php
namespace App\Jobs;

ini_set('max_execution_time', 3600);

use App\Models\Abstracts\Card;
use App\Models\Certificate;
use App\Models\CertificateType;
use App\Models\Revision;
use Carbon\Carbon;
use DB;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SyncWithiikoCard implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Execute the sync.
     *
     * @return void
     */
    public function handle()
    {
	    echo "Синхронизация сертификатов началась " . date("Y-m-d H:i:s")  . "\n";
	    $this->syncCertificates();
	    echo "Синхронизация сертификатов завершена " . date("Y-m-d H:i:s")  . "\n\n";
    }

    private function syncCertificates()
    {
	    $certificate_types = CertificateType::select(['id', 'name', 'life_time'])->get();
	    $revision = Revision::whereName(Revision::CERTIFICATES)->first();
	    $revision_value = $revision->value;

	    $count_row = DB::connection('iikoCard')
		    ->select(DB::raw('
				SELECT			
					COUNT(*) as count
				FROM (			
					SELECT 
						a.ExternalId AS Track
						, MAX(act.Revision) AS Revision			
					FROM Accessor a			
					JOIN Account ac ON ac.AccountableId = a.Id			
					JOIN Accountable act ON act.Id = ac.AccountableId			
					GROUP BY a.ExternalId			
				) AS crd			
				JOIN Accessor a ON a.ExternalId = crd.Track			
				JOIN CardAccessor ca ON ca.Id = a.Id			
				JOIN Account ac ON ac.AccountableId = a.Id			
				JOIN Accountable act ON act.Id = ac.AccountableId			
				JOIN AccountCounter acc ON acc.AccountId = ac.Id			
				JOIN AbstractCounter abc ON abc.Id = acc.Id			
				JOIN AccessorGroup ag ON ag.Id = a.GroupId			
				WHERE			
					act.Revision = crd.Revision			
					AND abc.CounterTypeId IS NOT NULL
					AND act.Revision > ?
	        '), [$revision_value]);
	    $count = $count_row[0]->count;
	    $offset = 0;
	    $next = 1000;

	    while ($offset < $count) {
		    $rows = DB::connection('iikoCard')
			    ->select(DB::raw('
					SELECT			
						crd.Track			
						, ca.Id AS CardId	
						, abc.CounterValue AS Balance					
						, act.Revision			
						, ag.Name AS GroupName		
						, a.WhenActivated	
						, a.DeactivationDate
						, a.WhenBlocked
						, a.CreationDate
					FROM (			
						SELECT 
							a.ExternalId AS Track
							, MAX(act.Revision) AS Revision			
						FROM Accessor a			
						JOIN Account ac ON ac.AccountableId = a.Id			
						JOIN Accountable act ON act.Id = ac.AccountableId			
						GROUP BY a.ExternalId			
					) AS crd			
					JOIN Accessor a ON a.ExternalId = crd.Track			
					JOIN CardAccessor ca ON ca.Id = a.Id			
					JOIN Account ac ON ac.AccountableId = a.Id			
					JOIN Accountable act ON act.Id = ac.AccountableId			
					JOIN AccountCounter acc ON acc.AccountId = ac.Id			
					JOIN AbstractCounter abc ON abc.Id = acc.Id			
					JOIN AccessorGroup ag ON ag.Id = a.GroupId			
					WHERE			
						act.Revision = crd.Revision			
						AND abc.CounterTypeId IS NOT NULL
						AND act.Revision > ?
					ORDER BY act.Revision DESC, crd.Track, abc.Code	OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
	        	'), [$revision_value, $offset, $next]);

		    foreach ($rows as $row) {
			    $denomination = intval(preg_replace('/[^\d]+/', '', (str_replace(' ', '', $row->GroupName))));
			    if ($denomination == 3000) {
				    $certificate_type = $certificate_types->where('name', CertificateType::LITE)->first();
			    } else if ($denomination == 5000) {
				    $certificate_type = $certificate_types->where('name', CertificateType::STANDARD)->first();
			    } elseif ($denomination == 10000) {
				    $certificate_type = $certificate_types->where('name', CertificateType::CLASSIC)->first();
			    } elseif ($denomination == 15000) {
				    $certificate_type = $certificate_types->where('name', CertificateType::PLATINUM)->first();
			    } else {
				    $certificate_type = $certificate_types->where('name', CertificateType::EXCLUSIVE)->first();
			    }

			    if ($revision->value < $row->Revision) {
				    $revision->value = $row->Revision;
			    }

			    if ($row->CreationDate !== null) {
			    	$created_at = new Carbon($row->CreationDate);
			    } else {
			    	$created_at = null;
			    }

			    if ($row->WhenActivated !== null) {
				    $started_at = new Carbon($row->WhenActivated);
				    $created_at = is_null($created_at) ? $started_at : $created_at;
				    if ($started_at->diffInMinutes($created_at) > 5) {
					    $status = Card::ACTIVATED_IN_RESTAURANT;
				    } else {
					    $status = Card::ACTIVATED_IN_OFFICE;
				    }
			    } else {
				    $started_at = null;
				    $created_at = is_null($created_at) ? Carbon::now() : $created_at;
				    $status = Card::NOT_ACTIVATED;
			    }

			    if ($row->DeactivationDate !== null) {
				    $ended_at = new Carbon($row->DeactivationDate);
			    } else if ($started_at !== null) {
				    $ended_at = $started_at->copy()->addDays($certificate_type->life_time);
			    } else {
				    $ended_at = null;
			    }

			    if ($row->WhenBlocked !== null) {
				    $blocked_at = new Carbon($row->WhenBlocked);
				    if ($row->Balance == 0 || (
					    	$certificate_type->sum != $row->Balance
						    && $certificate_type->name != CertificateType::EXCLUSIVE
				    )) {
					    $status = Certificate::BLOCKED_SERVICE;
				    } else if ($blocked_at == $ended_at) {
					    $status = Card::BLOCKED_EXPIRED;
				    } else {
					    $status = Card::BLOCKED_MANUALLY;
				    }
			    } else if (Carbon::now()->diffInDays($ended_at, false) < 0) { //если карта не заблокирована, но истек срок годности
				    $blocked_at = $ended_at;
				    $status = Card::BLOCKED_EXPIRED;
			    } else {
				    $blocked_at = null;
			    }

			    $certificate = Certificate::whereNumber($row->Track)->first();

			    if ($certificate === null) {
				    $certificate = new Certificate();
				    $certificate->number = $row->Track;
				    $certificate->external_id = $row->CardId;
				    $certificate->created_at = $created_at;
			    }
			    $certificate->card_type_id = $certificate_type->id;
			    $certificate->balance = $row->Balance;
			    $certificate->started_at = $started_at;
			    $certificate->ended_at = $ended_at;
			    $certificate->blocked_at = $blocked_at;
			    $certificate->status = $status;
			    $certificate->save();
		    }

		    $offset += $next;
	    }

	    $revision->save();
    }
}
