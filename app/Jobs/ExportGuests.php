<?php

namespace App\Jobs;

use App\Http\Controllers\CRM\GuestController;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\Guest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ExportGuests implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request_array;
    protected $filename;

	/**
	 * Create a new job instance.
	 *
	 * @param array $request_array
	 * @param string  $filename
	 */
    public function __construct($request_array, $filename)
    {
        $this->request_array = $request_array;
        $this->filename = $filename;
    }

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
    public function handle()
    {
    	$request = new Request($this->request_array);
    	$data = (new GuestController())->index($request, true);
	    Excel::create($this->filename, function($excel) use($data) {
		    $excel->sheet('Гости', function($sheet) use($data) {
			    $sheet->getStyle('L')->getAlignment()->setWrapText(true); //Дисконтные карты
			    $sheet->getStyle('M')->getAlignment()->setWrapText(true); //Сертификаты
			    //header row
			    $sheet->appendRow([
				    'ID',
				    'Имя',
				    'Фамилия',
				    'Пол',
				    'Телефон',
				    'Email',
				    'Дата рождения',
				    'Дата последней операции',
				    'Всего операций',
				    'Сумма заказов',
				    'Средний чек',
				    'Дисконтные карты',
				    'Сертификаты',
			    ]);
			    //body rows
			    foreach ($data as $guest) {
				    /** @var Guest $guest */
				    $discount_cards = $guest->discount_cards->map(function(DiscountCard $card) {
					    return $card->number. ' (' . $card->type->name . ')';
				    });
				    $certificates = $guest->certificates->map(function(Certificate $card) {
					    return $card->number. ' (' . $card->type->name . ')';
				    });
				    $sheet->appendRow([
					    'id'                  => $guest->id,
					    'first_name'          => $guest->first_name,
					    'last_name'           => $guest->last_name,
					    'sex'                 => $guest->gender,
					    'phone'               => $guest->phone,
					    'email'               => $guest->email,
					    'birthday_at'         => $guest->birthday_at,
					    'last_operation_date' => $guest->last_operation_date,
					    'transactions_count'  => $guest->transactions_count,
					    'total_check_sum'     => $guest->total_check_sum,
					    'average_check_sum'   => $guest->average_check_sum,
					    'discount_cards'      => implode(PHP_EOL, $discount_cards->toArray()),
					    'certificates'        => implode(PHP_EOL, $certificates->toArray()),
				    ]);
			    }
		    });
	    })->store('xls');
    }
}
