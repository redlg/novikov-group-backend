<?php

namespace App\Jobs;

use App\Http\Controllers\CRM\LegalEntityController;
use App\Http\Controllers\CRM\OperationController;
use App\Models\Abstracts\Card;
use App\Models\Certificate;
use App\Models\Operation;
use App\Models\Restaurant;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class ExportOperations implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $request_array;
    protected $filename;

	/**
	 * Create a new job instance.
	 *
	 * @param array $request_array
	 * @param string  $filename
	 */
    public function __construct($request_array, $filename)
    {
        $this->request_array = $request_array;
        $this->filename = $filename;
    }

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
    public function handle()
    {
    	$request = new Request($this->request_array);
    	$data = (new OperationController())->index($request, true);
	    Excel::create($this->filename, function($excel) use($data) {
		    $excel->sheet('Операции', function($sheet) use($data) {
			    //header row
			    $sheet->appendRow([
				    'ID',
				    'Дата',
				    'Гость',
				    'Тип операции',
				    'Инициатор',
				    'Эмитент',
				    'Ресторан',
				    'Карта',
				    'Тип карты',
				    'Номер карты',
				    'Номинал/скидка',
				    'Баланс',
				    'Сумма транзакций',
				    'Комментарий',
			    ]);
			    $legal_entities = LegalEntityController::getFilters();
			    $issuers = $legal_entities['issuers']->mapWithKeys(function($item) {
				    return [$item['id'] => $item['name']];
			    });
			    $restaurant = Restaurant::all()->mapWithKeys(function($item) {
				    return [$item['id'] => $item['name']];
			    });
			    //body rows
			    foreach ($data as $operation) {
				    /** @var Operation $operation */
				    $sum = $operation->transactions->sum('sum');
				    $sheet->appendRow([
					    'id'               => $operation->id,
					    'date'             => $operation->created_at,
					    'guest'            => empty($operation->card->guest) ? null : trim($operation->card->guest->first_name . ' ' . $operation->card->guest->last_name),
					    'operation_type'   => $operation->type->name,
					    'initiator'        => $operation->initiator,
					    'issuer'           => $issuers->get($operation->card->issuer_id),
					    'restaurant'       => $restaurant->get($operation->card->restaurant_id),
					    'card'             => $operation->card_type == Certificate::class
						    ? Card::RELATION_CERTIFICATE_NAME
						    : Card::RELATION_DISCOUNT_CARD_NAME,
					    'card_type'        => $operation->card->type->name,
					    'card_number'      => $operation->card->number,
					    'sum/discount'     => $operation->card_type == Certificate::class
						    ? (($operation->card->type->sum == -1) ? 'Неограничено' : $operation->card->type->sum)
						    : $operation->card->type->discounct,
					    'balance'          => $operation->card_type == Certificate::class
						    ? $operation->card->balance
						    : null,
					    'transactions_sum' => $sum,
					    'comment'          => $operation->comment,
				    ]);
			    }
		    });
	    })->store('xls');
    }
}
