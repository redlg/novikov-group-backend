<?php

namespace App\Scopes;

use App\Models\Role;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class RestaurantIdScope implements Scope
{
	/**
	 * Apply the scope to a given Eloquent query builder.
	 *
	 * @param  \Illuminate\Database\Eloquent\Builder  $builder
	 * @param  \Illuminate\Database\Eloquent\Model  $model
	 * @return void
	 */
	public function apply(Builder $builder, Model $model)
	{
		$user = \Auth::getUser();
		if ($user && $user->hasRole(Role::RESTAURANT_MANAGER) && $user->restaurant_id) {
			$builder->where('restaurant_id', $user->restaurant_id);
		}
	}
}