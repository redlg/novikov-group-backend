<?php

namespace App\Traits;

use App\Models\Address;
use App\Models\DeliveryOrder;
use App\Models\Device;

trait DeviceTrait
{
	public function guestFields(Device $device)
	{
		if ($device->guest_id) {
			$device->guest->makeHidden([
				'sex',
				'code',
			]);
			$device->guest->addresses->each(function(Address $address) {
				$address
					->makeHidden([
						'text_ru',
						'text_en',
						'timezone',
					])
					->append([
						'text',
					]);
			});
		}

		return $device;
	}

	public function ordersFields(DeliveryOrder $order)
	{
		$order
			->append([
				'restaurant_name',
				'restaurant_phone',
			]);

		if ($order->address_id) {
			$order
				->address
				->append([
					'text',
				])
				->makeHidden([
					'text_ru',
					'text_en',
					'country',
					'timezone',
				]);
		}

		$order
			->dishes
			->makeHidden([
				'image_id',
				'image',
				'description_ru',
				'description_en',
				'composition_ru',
				'composition_en',
				'name_ru',
				'name_en',
			])
			->makeVisible([
				'favorite',
				'title_image',
				'name',
				'description',
				'composition',
				'delivery_category_name',
			]);

		return $order;
	}
}