<?php

namespace App\Traits;

use App\Models\Address;
use App\Models\DeliveryDish;
use App\Models\DeliveryMethod;
use App\Models\DeliveryOrder;
use App\Models\DeliveryPaymentMethod;
use App\Models\Guest;
use App\Requests\NovikovRequester;
use Illuminate\Http\Request;

trait DeliveryOrderTrait
{

	/**
	 * @param DeliveryOrder $deliveryOrder
	 *
	 * @param Request|null $request
	 *
	 * @return array|mixed
	 */
	public function sendOrder(DeliveryOrder $deliveryOrder, Request $request = null)
	{
	    $request = is_null($request) ? new Request() : $request;
		$dishes = $deliveryOrder->dishes->mapWithKeys(function(DeliveryDish $dish) {
			return [$dish->external_id => $dish->pivot->count];
		});
		$requester = new NovikovRequester();
		$guest = Guest::whereId($deliveryOrder->guest_id)->first();
		$address = Address::whereId($deliveryOrder->address_id)->first();
		$deliveryMethod = DeliveryMethod::whereId($deliveryOrder->delivery_method_id)->first();
		$deliveryPaymentMethod = DeliveryPaymentMethod::whereId($deliveryOrder->delivery_payment_method_id)->first();
		$first_name = $guest->first_name ?: $request->get('first_name', ' ');
		$last_name = $guest->last_name ?: $request->get('last_name', ' ');
		$email = $guest->email ?: $request->get('email', 'unspecifiedemail@novikovgroup.ru');
		$orderParams = [
			'Items'            => $dishes,
			'Name'             => $first_name . ' ' . $last_name,
			'Phone'            => $guest->phone,
			'Email'            => $email,
			'Person'           => $deliveryOrder->number_of_persons,
			'Payment_Method'   => $deliveryPaymentMethod->external_id,
			'Delivery_Method'  => $deliveryMethod->external_id,
			'Comments'         => $deliveryOrder->comment,
		];
		if ($deliveryMethod->name != 'Самовывоз') {
			$apartment = $address->apartment ?: $request->get('apartment', ' ');
			$orderParams['Metro'] = ' ';
			$orderParams['Street'] = $address->street;
			$orderParams['Home'] = $address->building;
			$orderParams['Apartment'] = $apartment;
			$orderParams['Delivery_To'] = is_null($deliveryOrder->delivered_at) ? 'speed' : 'time';
			$orderParams['Delivery_To_Day'] = is_null($deliveryOrder->delivered_at) ? '' : $deliveryOrder->delivered_at->toDateString();
			$orderParams['Delivery_To_Time'] = is_null($deliveryOrder->delivered_at) ? '' : $deliveryOrder->delivered_at->toTimeString();
		}

		$order = $requester->sendDeliveryOrder($orderParams);
		$deliveryOrder->external_id = $order['Order_ID'];
		$deliveryOrder->save();

		return $deliveryOrder->external_id;
	}
}