<?php

namespace App\Traits;

use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\DiscountCode;

trait HandleRkeeperRequest
{
	public function parseNode($node)
	{
		$result = [];
		if ($node) {
			foreach ($node as $item) {
				$result[] = isset(((array)$item)['@attributes']) ? ((array)$item)['@attributes'] : (array)$item;
			}
		}

		return $result;
	}

	public function parse($raw_data)
	{
		try {
			$rkeeper = new \SimpleXMLElement($raw_data);
		} catch(\Exception $e) {
			return false;
		}

		$result = [
			'cardCode'         => (string)$rkeeper->attributes()->cardCode,
			'restaurantCode'   => (string)$rkeeper->CHECK->attributes()->restaurantcode,
			'cashboxNumber'    => (string)$rkeeper->CHECK->attributes()->stationcode,
			'checkNumber'      => (string)$rkeeper->CHECK->CHECKDATA->attributes()->checknum,
			'chmode'           => (string)$rkeeper->CHECK->attributes()->chmode,
			'PCard29Info'      => (array)$rkeeper->PCard29Info,
			'RKTransactInfoEx' => $this->parseNode($rkeeper->RKTransactInfoEx),
			'categories'       => $this->parseNode($rkeeper->CHECK->CHECKDATA->CHECKCATEGS->CATEG),
			'persons'          => $this->parseNode($rkeeper->CHECK->CHECKDATA->CHECKPERSONS->PERSON),
			'dishes'           => $this->parseNode($rkeeper->CHECK->CHECKDATA->CHECKLINES->LINE),
			'payments'         => $this->parseNode($rkeeper->CHECK->CHECKDATA->CHECKPAYMENTS->PAYMENT),
			'discounts'        => $this->parseNode($rkeeper->CHECK->CHECKDATA->CHECKDISCOUNTS->DISCOUNT),
			'taxes'            => $this->parseNode($rkeeper->CHECK->CHECKDATA->CHECKTAXES->TAX),
		];

		return $result;
	}

	/**
	 * @param string $card_number
	 *
	 * @return DiscountCard|Certificate|DiscountCode|null
	 */
	public function findCard($card_number)
	{
	    //todo переделать мобильный код
		if (strlen($card_number) == DiscountCode::CODE_LENGTH) { //дисконтный код отличается длинной от номеров карт
			$code = DiscountCode::whereCode($card_number)->first();
			$card = !is_null($code) ? DiscountCard::whereId($code->discount_card_id)->first() : null;
		} else {
			$card = DiscountCard::whereNumber($card_number)->first() ?: Certificate::whereNumber($card_number)->first();
		}

		return $card;
	}
}