<?php

namespace App\Traits;

use App\Models\DeliveryCategory;
use App\Models\DeliveryDishesCompilation;
use App\Models\Restaurant;

trait RestaurantTrait
{

	/**
	 * @param Restaurant|\Illuminate\Database\Eloquent\Model  $restaurant
	 *
	 * @return Restaurant
	 */
	public function visibleFields(Restaurant $restaurant)
	{
		$restaurant->makeVisible([
			'announce',
			'compilation_title_image',
			'detail_images',
			'description',
			'favorite',
			'kitchen',
			'list_title_image',
			'name',
			'work_time_from',
			'work_time_to',
		]);

		return $restaurant;
	}

	/**
	 * @param Restaurant|\Illuminate\Database\Eloquent\Model $restaurant
	 *
	 * @return Restaurant
	 */
	public function hiddenFields(Restaurant $restaurant)
	{
		$restaurant->makeHidden([
			'address_id',
			'announce_ru',
			'announce_en',
			'average_check_string',
			'chef_id',
			'description_ru',
			'description_en',
			'images',
			'kitchens',
			'legal_entity_id',
			'logo_id',
			'name_ru',
			'name_en',
			'phone_string',
			'work_time_string',
            'delivery_menu',
		]);

		return $restaurant;
	}

	/**
	 * @param Restaurant|\Illuminate\Database\Eloquent\Model  $restaurant
	 *
	 * @return Restaurant
	 */
	public function addressFields(Restaurant $restaurant)
	{
		$restaurant
			->address
			->append(['text'])
			->makeHidden([
				'apartment',
				'comment',
				'country',
				'intercom',
				'floor',
				'text_ru',
				'text_en',
				'timezone',
			]);

		return $restaurant;
	}

	/**
	 * @param Restaurant|\Illuminate\Database\Eloquent\Model  $restaurant
	 *
	 * @return Restaurant
	 */
	public function tagFields(Restaurant $restaurant)
	{
		$restaurant
			->tags
			->makeVisible([
				'name',
			])->makeHidden([
				'name_ru',
				'name_en',
			]);

		return $restaurant;
	}

	/**
	 * @param Restaurant|\Illuminate\Database\Eloquent\Model  $restaurant
	 *
	 * @return Restaurant
	 */
	public function dishesCompilationFields(Restaurant $restaurant)
	{
		$restaurant
			->dishes_compilations
			->each(function (DeliveryDishesCompilation $compilation) {
				$compilation->append(['dish_ids']);
				$compilation->makeHidden(['tags']);
			});


		return $restaurant;
	}

	/**
	 * @param Restaurant|\Illuminate\Database\Eloquent\Model  $restaurant
	 *
	 * @return Restaurant
	 */
	public function deliveryMenuFields(Restaurant $restaurant)
	{
		$restaurant
			->delivery_menu
			->makeHidden([
				'delivery_category_id',
				'restaurant_id',
				'name_ru',
				'name_en',
			])
			->makeVisible([
				'name',
			])
			->each(function(DeliveryCategory $delivery_category) {
				$delivery_category
					->dishes
					->makeHidden([
						'delivery_category_id',
						'image_id',
						'image',
						'description_ru',
						'description_en',
						'composition_ru',
						'composition_en',
						'name_ru',
						'name_en',
					])
					->makeVisible([
						'favorite',
						'title_image',
						'name',
						'description',
						'composition',
					]);
			});

		return $restaurant;
	}
}