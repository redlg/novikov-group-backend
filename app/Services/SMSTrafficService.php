<?php

namespace App\Services;

use App\Models\DeliveryMethod;
use App\Models\Event;
use App\Models\TicketTypeOrder;
use App\Models\TicketType;
use App\Requests\NovikovRequester;
use Exception;
use GuzzleHttp\Client;

/**
 * Class SMSTrafficService
 */
class SMSTrafficService {
	protected $login;
	protected $password;
	protected $api;

	public function __construct()
	{
		$this->login = config('crm.smstraffic_login');
		$this->password = config('crm.smstraffic_password');
		$this->api = config('crm.smstraffic_api_url');
	}

	public function sendSms($phone, $message)
	{
		$client = new Client();
		$request = $client->request('POST', $this->api, [
			'form_params' => [
				'login'    => $this->login,
				'password' => $this->password,
				'phones'   => $phone,
				'message'  => $message,
			]
		]);

		return $request->getBody();
	}
}
