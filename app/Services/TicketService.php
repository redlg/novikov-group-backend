<?php

namespace App\Services;

use App\Models\Event;
use App\Models\Guest;
use App\Models\ProductDeliveryMethod;
use App\Models\TicketTypeOrder;
use App\Models\TicketType;
use App\Requests\NovikovRequester;
use Exception;

/**
 * Class TicketService
 */
class TicketService {

    /**
     * Buy ticket on event from the novikovgroup.ru
     *
     * @param integer $ticket_type_id
     * @param integer $product_delivery_method_id
     * @param integer $quantity
     * @param integer $guest_id
     *
     * @return array
     * @throws Exception
     */
	public function buy($ticket_type_id, $product_delivery_method_id, $quantity, $guest_id)
	{
		$requester = new NovikovRequester();

		/** @var TicketType $ticketType */
		$ticketType = TicketType::findOrFail($ticket_type_id);
		/** @var ProductDeliveryMethod $productDeliveryMethod */
		$productDeliveryMethod = ProductDeliveryMethod::findOrFail($product_delivery_method_id);
        $guest = Guest::whereId($guest_id)->first();
		$price = $quantity * $ticketType->price;

		$form_params = [
			'Product_ID'      => $ticketType->external_id,
			'Name'            => trim($guest->first_name . ' ' . $guest->last_name),
			'Phone'           => $guest->phone,
			'Email'           => $guest->email,
			'Qty'             => $quantity,
			'Delivery_Method' => $productDeliveryMethod->external_id,
			'Price'           => $price,
		];

		$request = $requester->buyTicket($form_params);

		if (isset($request['error'])) {
			throw new Exception($request['message']);
		} else {
			$ticketTypeOrder = new TicketTypeOrder();
			$ticketTypeOrder->ticket_type_id = $ticket_type_id;
			$ticketTypeOrder->product_delivery_method_id = $productDeliveryMethod->id;
			$ticketTypeOrder->server_url = $request['server_url'];
			$ticketTypeOrder->cost = $request['amount'];
			$ticketTypeOrder->customer_name = $request['customer_name'];
			$ticketTypeOrder->customer_phone = $request['customer_phone'];
			$ticketTypeOrder->customer_email = $request['customer_email'];
			$ticketTypeOrder->order_id = $request['order_id'];
			$ticketTypeOrder->invoice_id = $request['invoice_id'];
			$ticketTypeOrder->save();

//			https://paykeeper.ru/paykeeper/usage/html_form/
			return [
                'server_url'       => $ticketTypeOrder->server_url,
                'paykeeper_params' => [
                        'sum'          => $ticketTypeOrder->cost,
                        'clientid'     => $ticketTypeOrder->customer_name,
                        'orderid'      => $ticketTypeOrder->order_id,
                        'service_name' => $ticketTypeOrder->invoice_id,
                        'client_email' => $ticketTypeOrder->customer_email,
                        'client_phone' => $ticketTypeOrder->customer_phone,
                ],
            ];
		}
	}

	/**
	 * @param integer $invoice_id
	 *
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 */
	public function success($invoice_id)
	{
		$ticketTypeOrder = TicketTypeOrder::whereInvoiceId($invoice_id)->first();

		if ($ticketTypeOrder) {
			$ticketTypeOrder->paid = true;
			$ticketTypeOrder->save();
			$this->updateAvailableCount($ticketTypeOrder->type);
			return response(['result' => "Ticket status updated"], 200);
		} else {
			return response(['error' => "Ticket with this invoice_id doesn't exist"], 400);
		}
	}

	/**
	 * @param TicketType|\Illuminate\Database\Eloquent\Model $ticketType
	 *
	 * @return TicketType
	 */
	public function updateAvailableCount(TicketType $ticketType)
	{
		/** @var Event $event */
		$event = Event::findOrFail($ticketType->event_id);
		$requester = new NovikovRequester();
		$event = $requester->getEvent($event->external_id);
		$ticketType->available = $event['BuyProduct']['Available'];
		$ticketType->save();

		return $ticketType;
	}
}
