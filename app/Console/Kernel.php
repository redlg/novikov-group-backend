<?php

namespace App\Console;

use App\Console\Commands\CronDeactivateExpiredCards;
use App\Console\Commands\CronDeleteExpiredDiscountCodes;
use App\Console\Commands\CronDeleteExpiredExportFiles;
use App\Console\Commands\CronDeleteRefusedImages;
use App\Console\Commands\ImportNovikov;
use App\Console\Commands\ImportRestaurantsData;
use App\Console\Commands\ImportRKeeperCodes;
use App\Console\Commands\SyncCertificates;
use App\Console\Commands\SyncDiscountCards;
use App\Models\DiscountCode;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
	    SyncCertificates::class,
        SyncDiscountCards::class,
        ImportNovikov::class,
        ImportRestaurantsData::class,
        ImportRKeeperCodes::class,
        CronDeleteExpiredDiscountCodes::class,
        CronDeleteExpiredExportFiles::class,
        CronDeleteRefusedImages::class,
        CronDeactivateExpiredCards::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('cron:delete-expired-discount-codes')
	         ->cron('*/' . DiscountCode::TTL . ' * * * *');
        $schedule->command('cron:delete-expired-export-files')
		    ->cron('0 */2 * * *');
        $schedule->command('cron:delete-refused-images')
            ->dailyAt('05:00');
        $schedule->command('cron:deactivate-expired-cards')
            ->dailyAt('04:00');
        $schedule->command('import:novikov')
            ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
