<?php

namespace App\Console\Commands;

use App\Jobs\SyncWithiikoCard;
use Illuminate\Console\Command;

/**
 * Class SyncCertificates
 * @package App\Console\Commands
 */
class SyncCertificates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:certificates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize certificates with data from iikoCard DB';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$import = new SyncWithiikoCard();
    	$import->handle();
        $this->info('Certificates synchronization completed ' . date('Y-m-d H:i:s'));
    }
}
