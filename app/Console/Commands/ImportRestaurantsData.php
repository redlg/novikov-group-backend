<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * Class ImportRestaurantIds
 * @package App\Console\Commands
 */
class ImportRestaurantsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:restaurants-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Insert data to restaurants. Use it only after import:novikov";

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $import = new \App\Jobs\ImportRestaurantsData();
	    $import->handle();
		$this->info('Import data for restaurants completed ' . date('Y-m-d H:i:s'));
    }
}
