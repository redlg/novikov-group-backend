<?php

namespace App\Console\Commands;

use App\Jobs\ImportFromNovikovgroup;
use Illuminate\Console\Command;

/**
 * Class ImportNovikov
 * @package App\Console\Commands
 */
class ImportNovikov extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:novikov';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from novikovgroup.ru';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $import = new ImportFromNovikovgroup();
       $import->handle();
       $this->info('Import from novikovgroup.ru completed ' . date('Y-m-d H:i:s'));
    }
}
