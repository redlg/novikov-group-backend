<?php

namespace App\Console\Commands;

use App\Models\Image;
use Illuminate\Console\Command;

class CronDeleteRefusedImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:delete-refused-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete images which refused';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $images = Image::whereConfirmed(false)->get();
        foreach ($images as $image) {
            if ($image->external_id != IMAGE::TEMPORARY_ID) {
                \Storage::delete($image->path);
            }
            $image->delete();
        }
    }
}
