<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * Class ImportRestaurantIds
 * @package App\Console\Commands
 */
class ImportRKeeperCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:rkeeper-codes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Import rkeeper codes to discount card types and restaurants. Use it only after import:novikov and sync:discount-cards";

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$import = new \App\Jobs\ImportRKeeperCodes();
    	$import->handle();
        $this->info('Import codes from RKeeper completed ' . date('Y-m-d H:i:s'));
    }
}
