<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

class CronDeleteExpiredExportFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:delete-expired-export-files';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete export files which expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$now = Carbon::now();
    	foreach (glob(public_path('/upload/exports/*')) as $file) {
    		if ($now->diffInMinutes(Carbon::createFromTimestamp(filemtime($file))) > 120) {
				unlink($file);
		    }
	    }
    }
}
