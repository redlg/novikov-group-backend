<?php

namespace App\Console\Commands;

use App\Models\Abstracts\Card;
use App\Models\Certificate;
use App\Models\DiscountCard;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class CronDeactivateExpiredCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:deactivate-expired-cards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deactivate discount cards and certificates which expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DiscountCard::query()
            ->where(function($query) {
                $query->where('status', Card::ACTIVATED_IN_OFFICE)
                    ->orWhere('status', Card::ACTIVATED_IN_RESTAURANT);
            })
            ->where('ended_at', '<', Carbon::now())
            ->each(function(DiscountCard $card) {
                $card->deactivate();
            });
        Certificate::query()
            ->where(function($query) {
                $query->where('status', Card::ACTIVATED_IN_OFFICE)
                    ->orWhere('status', Card::ACTIVATED_IN_RESTAURANT);
            })
            ->where('ended_at', '<', Carbon::now())
            ->each(function(Certificate $card) {
                $card->deactivate();
            });
    }
}
