<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class CronDeleteExpiredDiscountCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:delete-expired-discount-codes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete discount codes which expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('discount_codes')
	        ->where('expiration_at', '<', Carbon::now())
	        ->delete();
    }
}
