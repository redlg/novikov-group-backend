<?php

namespace App\Console\Commands;

use App\Jobs\SyncWithRestoCRM;
use Illuminate\Console\Command;

/**
 * Class SyncDiscountCards
 * @package App\Console\Commands
 */
class SyncDiscountCards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:discount-cards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize discount cards with data from RestoCRM DB';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$import = new SyncWithRestoCRM();
    	$import->handle();
        $this->info('Discount cards synchronization completed ' . date('Y-m-d H:i:s'));
    }
}
