<?php

namespace App\Http\Controllers\Mobile;

use App\Services\TicketService;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TicketTypeOrderController extends Controller
{
	protected $ticketService;

	public function __construct(TicketService $ticketService)
	{
		$this->ticketService = $ticketService;
	}

	/**
	 * @param Request $request
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function buy(Request $request)
    {
	    $validator = Validator::make($request->all(), [
		    'ticket_type_id'             => 'required|numeric',
		    'product_delivery_method_id' => 'required|numeric',
	    ]);

	    if ($validator->fails()) {
		    throw new HttpResponseException(response(['error' => "You don't pass all required parameters in your request"], 403));
	    }

	    $guest_id = $request->get('guest_id');
	    $ticket_type_id = $request->get('ticket_type_id');
	    $product_delivery_method_id = $request->get('product_delivery_method_id');
	    $quantity = $request->get('quantity', 1);

	    return $this->ticketService->buy($ticket_type_id, $product_delivery_method_id, $quantity, $guest_id);
    }
}
