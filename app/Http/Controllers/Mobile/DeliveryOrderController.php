<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\DeliveryDish;
use App\Models\DeliveryMethod;
use App\Models\DeliveryOrder;
use App\Models\Guest;
use App\Traits\DeliveryOrderTrait;
use DB;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Validator;

class DeliveryOrderController extends Controller
{
	use DeliveryOrderTrait;

	public function show(Request $request, $id)
	{
		$order =  DeliveryOrder::with(['restaurant'])->whereId($id)->first()->append(['restaurant_phone', 'restaurant_name']);

		if ($order->guest_id != $request->get('guest_id')) {
			throw new HttpResponseException(response(["error" => "The guest doesn't have an order with this number"], 400));
		}

		$order
			->address
			->append([
				'text',
			])
			->makeHidden([
				'text_ru',
				'text_en',
				'country',
				'timezone',
			]);

		$order
			->dishes
			->makeHidden([
				'image_id',
				'image',
				'description_ru',
				'description_en',
				'composition_ru',
				'composition_en',
				'name_ru',
				'name_en',
			])
			->makeVisible([
				'favorite',
				'title_image',
				'name',
				'description',
				'composition',
				'delivery_category_name',
			]);

		return $order;
	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function store(Request $request)
	{
		return DB::transaction(function() use ($request) {
			$data = $request->all();
			$validator = Validator::make($data, [
				'restaurant_id'       => 'required|numeric',
				'payment_service_id'  => 'required|numeric',
				'delivery_service_id' => 'required|numeric',
				'dishes'              => 'required|json',
			]);

			if ($validator->fails()) {
				throw new HttpResponseException(response($validator->errors(), 400));
			}

			$guest = Guest::whereId($request->get('guest_id'))->first();
			if ($first_name = $request->get('first_name')) {
			    $guest->first_name = $first_name;
            }
            if ($last_name = $request->get('last_name')) {
                $guest->last_name = $last_name;
            }
            if ($email = $request->get('email')) {
                $guest->email = $email;
            }
            if ($first_name || $last_name || $email) {
			    $guest->save();
            }
			$delivery_method = DeliveryMethod::whereId($request->get('delivery_service_id'))->first();
			//создаем адрес, если не передан его id и тип доставки не самовывоз
			if (!($address_id = $request->get('address_id')) && $delivery_method->name != 'Самовывоз') {
				$address_validator = Validator::make($data, [
					'street'   => 'required',
					'building' => 'required',
				]);

				if ($address_validator->fails()) {
					throw new HttpResponseException(response($address_validator->errors(), 400));
				}

				$address = new Address();
				$address->addressable_id = $guest->id;
				$address->addressable_type = Guest::class;
				$address->city = $request->get('city', Address::DEFAULT_CITY);
				$address->street = $request->get('street');
				$address->building = $request->get('building');
				$address->block = $request->get('block');
				$address->apartment = $request->get('apartment');
				$address->main = $guest->addresses->count() ? false : true;
				$address->save();
				$address_id = $address->id;
			}

			$dishes = json_decode($request->get('dishes'), true);
			$attach = [];
			foreach ($dishes as $dish) {
				$attach[$dish['id']] = ['count' => $dish['count']];
			}

			$deliveryOrder = new DeliveryOrder();
			$deliveryOrder->guest_id = $guest->id;
			$deliveryOrder->address_id = $address_id;
			$deliveryOrder->restaurant_id = $request->get('restaurant_id');
			$deliveryOrder->delivery_payment_method_id = $request->get('payment_service_id');
			$deliveryOrder->delivery_method_id = $request->get('delivery_service_id');
			$deliveryOrder->discount_card_id = $request->get('discount_card_id', null);
			$deliveryOrder->certificate_id = $request->get('certificate_id', null);
			$deliveryOrder->delivered_at = $request->get('delivered_at', null);
			$deliveryOrder->number_of_persons = $request->get('number_of_persons', 1);
			$deliveryOrder->sum = $this->calculateSum($attach);
			$deliveryOrder->comment = $request->get('comment', null);
			$deliveryOrder->saveOrFail();
			$deliveryOrder->dishes()->attach($attach);
			$this->sendOrder($deliveryOrder, $request);

			if ($address_id) {
				$deliveryOrder
					->address
					->append([
						'text',
					])
					->makeHidden([
						'text_ru',
						'text_en',
						'country',
						'timezone',
					]);
			}

            $deliveryOrder
                ->dishes
                ->makeHidden([
                    'image_id',
                    'image',
                    'description_ru',
                    'description_en',
                    'composition_ru',
                    'composition_en',
                    'name_ru',
                    'name_en',
                ])
                ->makeVisible([
                    'favorite',
                    'title_image',
                    'name',
                    'description',
                    'composition',
                    'delivery_category_name',
                ]);;

            return $deliveryOrder;
		});
	}

	/**
	 * $dishes must be array like '[
	 *      [1 => ["count" => 2],
	 *      [2 => ["count" => 3],
	 *      [3 => ["count" => 1]
	 * ]'
	 *
	 * @param array $dishes
	 *
	 * @return float
	 */
	private function calculateSum($dishes)
	{
		$sum = 0;
		$dish_ids = array_keys($dishes);
		$delivery_dishes = DeliveryDish::whereIn('id', $dish_ids)->select(['id', 'price'])->get();
		foreach ($delivery_dishes as $dish) {
			$count = $dishes[$dish->id]['count'];
			$sum += $count * $dish->price;
		}

		return $sum;
	}
}
