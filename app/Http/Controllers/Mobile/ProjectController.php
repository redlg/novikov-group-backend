<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Project;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        $partners = Project::all();

        $partners->each(function(Project $partner) {
        	$partner
		        ->append([
			        'name',
			        'announce',
			        'description',
		        ])
		        ->makeHidden([
			        'name_ru',
			        'name_en',
			        'announce_ru',
			        'announce_en',
			        'description_ru',
			        'description_en',
		        ]);
        });

        return $partners;
    }
}