<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\DiscountCard;
use App\Models\DiscountCode;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\Device;
use Validator;

class DiscountCodeController extends Controller
{
    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'card_number' => 'required',
	    ]);

    	if ($validator->fails()) {
		    throw new HttpResponseException(response(['error' => "You don't have card_number param in your request"], 403));
	    }

	    $card_number = $request->get('card_number');
    	$discount_card = DiscountCard::whereNumber($card_number)->first();
    	if (is_null($discount_card)) {
		    throw new HttpResponseException(response(['error' => "Discount card doesn't exist"], 400));
	    } else {
    		$code = strtoupper(str_random(DiscountCode::CODE_LENGTH));
		    DiscountCode::updateOrInsert(
			    [
				    'device_id'        => $request->get('device_id'),
				    'discount_card_id' => $discount_card->id,
			    ],
			    [
				    'discount_card_type_id' => $discount_card->card_type_id,
				    'code'                  => $code,
				    'expiration_at'         => Carbon::now()->addMinutes(DiscountCode::TTL),
		        ]
		    );

		    return response(['code' => $code])->header('Content-Type', 'application/json');
	    }
    }
}
