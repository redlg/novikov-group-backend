<?php

namespace App\Http\Controllers\Mobile;

use App\Models\DeliveryDishesCompilation;
use App\Http\Controllers\Controller;

class DeliveryDishesCompilationController extends Controller
{
    public function index()
    {
	    $compilations = DeliveryDishesCompilation::all();

	    $compilations->each(function(DeliveryDishesCompilation $compilation) {
	    	$compilation->append(['dish_ids']);
		    $compilation->tags
			    ->makeHidden([
				    'delivery_dishes',
				    'name_ru',
				    'name_en',
			    ])->makeVisible([
				    'name',
			    ]);
	    });

	    return $compilations;
    }
}
