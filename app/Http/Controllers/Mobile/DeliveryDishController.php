<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\DeliveryDish;
use Illuminate\Http\Request;

class DeliveryDishController extends Controller
{
    public function index(Request $request)
    {
    	$dishes = DeliveryDish::query();

	    if ($restaurant_id = $request->input('restaurant_id', false)) {
		    $dishes = $dishes->whereHas('delivery_category', function($query) use ($restaurant_id) {
		    	$query->where('restaurant_id', $restaurant_id);
		    });
	    }

	    $dishes = $dishes->paginate(
	    	$request->get('per_page', 15),
		    ['*'],
		    'page',
		    $request->get('cur_page', 1)
	    );

	    $d_pagination = [
		    'total'        => $dishes->total(),
		    'current_page' => $dishes->currentPage(),
		    'from'         => $dishes->firstItem(),
		    'to'           => $dishes->lastItem(),
		    'last_page'    => $dishes->lastPage()
	    ];
	    $d_items = collect($dishes->items())->each(function(DeliveryDish $dish) {
		    $dish
			    ->makeHidden([
				    'image',
				    'description_ru',
				    'description_en',
				    'composition_ru',
				    'composition_en',
				    'name_ru',
				    'name_en',
			    ])
			    ->makeVisible([
				    'title_image',
				    'favorite',
				    'restaurant_attributes',
				    'name',
				    'description',
				    'composition',
				    'delivery_category_name',
			    ]);
	    });

        return [
	        'pagination' => $d_pagination,
	        'items'      => $d_items,
        ];
    }

	public function show($id)
	{
		return DeliveryDish::findOrFail($id)
			->append([
				'with_this_ordered',
			])
			->makeHidden([
				'image',
				'description_ru',
				'description_en',
				'composition_ru',
				'composition_en',
				'name_ru',
				'name_en',
			])
			->makeVisible([
				'favorite',
				'title_image',
				'restaurant_attributes',
				'name',
				'description',
				'composition',
				'delivery_category_name',
			]);
	}

	public function bulk(Request $request)
	{
		$ids = $request->get('ids');

		if ($ids) {
			return DeliveryDish::whereIn('id', $ids)
				->get()
				->makeHidden([
					'image',
					'description_ru',
					'description_en',
					'composition_ru',
					'composition_en',
					'name_ru',
					'name_en',
				])
				->makeVisible([
					'favorite',
					'title_image',
					'restaurant_attributes',
					'name',
					'description',
					'composition',
					'delivery_category_name',
				]);
		} else {
			return [];
		}
	}
}
