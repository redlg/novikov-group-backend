<?php

namespace App\Http\Controllers\Mobile;

use App\Models\RestaurantsCompilation;
use App\Http\Controllers\Controller;

class RestaurantsCompilationController extends Controller
{
    public function index()
    {
        $compilations = RestaurantsCompilation::all();

        $compilations->each(function(RestaurantsCompilation $compilation) {
        	$compilation->append(['restaurant_ids']);
        	$compilation->tags
		        ->makeHidden([
		        	'restaurants',
			        'name_ru',
			        'name_en',
		        ])->makeVisible([
		        	'name',
		        ]);
        });

        return $compilations;
    }
}
