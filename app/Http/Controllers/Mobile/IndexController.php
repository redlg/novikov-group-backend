<?php

namespace App\Http\Controllers\Mobile;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return [
            'description' => config('mobile.description'),
            'version'     => config('mobile.version')
        ];
    }
}
