<?php

namespace App\Http\Controllers\Mobile;

use App\Models\ProductDeliveryMethod;
use App\Http\Controllers\Controller;

class ProductDeliveryMethodController extends Controller
{
    public function index()
    {
        return ProductDeliveryMethod::all();
    }
}
