<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Event;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;
use App\Traits\RestaurantTrait;

class EventController extends Controller
{
	use RestaurantTrait;

	public function index()
	{
		$events = Event::with(['restaurants'])->get();

		$events->each(function(Event $event) {
			$event
				->append([
					'name',
					'announce',
					'description',
				])
				->makeHidden([
					'name_ru',
					'name_en',
					'announce_ru',
					'announce_en',
					'description_ru',
					'description_en',
				])
				->type
					->append(['name'])
					->makeHidden([
						'name_ru',
						'name_en',
					]);

			$event->tags
				->makeVisible([
					'name',
				])->makeHidden([
					'name_ru',
					'name_en',
				]);

			$event->restaurants->each(function(Restaurant $restaurant) {
				$this->visibleFields($restaurant);
				$this->hiddenFields($restaurant);
				$this->addressFields($restaurant);
				$this->tagFields($restaurant);
			});
		});

		return $events;
	}

	public function show($id)
	{
		$event = Event::with(['restaurants'])
			->findOrFail($id)
			->append([
				'name',
				'announce',
				'description',
			])
			->makeHidden([
				'name_ru',
				'name_en',
				'announce_ru',
				'announce_en',
				'description_ru',
				'description_en',
			]);

			$event
				->type
				->append(['name'])
				->makeHidden([
					'name_ru',
					'name_en',
				]);

			$event->tags
				->makeVisible([
					'name',
				])->makeHidden([
					'name_ru',
					'name_en',
				]);

			$event->restaurants->each(function(Restaurant $restaurant) {
					$this->visibleFields($restaurant);
					$this->hiddenFields($restaurant);
					$this->addressFields($restaurant);
					$this->tagFields($restaurant);
			});

		return $event;
	}
}