<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Certificate;
use App\Models\DeliveryDish;
use App\Models\DeliveryOrder;
use App\Models\DiscountCard;
use App\Models\Guest;
use App\Requests\NovikovRequester;
use App\Services\SMSTrafficService;
use App\Traits\DeviceTrait;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\Device;
use Validator;

class DeviceController extends Controller
{
	use DeviceTrait;

	protected $SMSTrafficService;

	public function __construct(SMSTrafficService $SMSTrafficService)
	{
		$this->SMSTrafficService = $SMSTrafficService;
	}

	public function auth(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'device_id' => 'required',
	    ]);

    	if ($validator->fails()) {
		    throw new HttpResponseException(response(['error' => "You don't have device_id param in your request"], 403));
	    }

    	$device_id = $request->input('device_id');

        $device = Device::firstOrCreate([
            'device_id' => $device_id
        ]);

        return [
            'token' => $device->token,
        ];
    }

    public function getPurchaseInformation()
    {
        return (new NovikovRequester())->getPurchaseCardInformation();
    }

    public function getDiscountCardInfo(Request $request)
    {
        $device = Device::with(['guest.discount_cards.transactions'])->whereId($request->get('device_id'))->first();

        return $device->guest->discount_cards->sortBy('ended_at')->first();
    }

    public function getProfile(Request $request)
    {
	    $device = Device::with(['guest.addresses'])->whereId($request->get('device_id'))->first();
		$this->guestFields($device);

	    return $device;
    }

    public function updateProfile(Request $request) {
	    $device = Device::whereId($request->get('device_id'))->first();
	    $device->guest->fillable([
	    	'first_name',
	    	'last_name',
	    	'email',
	    ])->fill($request->all());
	    $device->guest->save();
	    $this->guestFields($device);

	    return $device;
    }

	public function getCode(Request $request)
	{
		$data = $request->all();
		$validator = Validator::make($data, [
			'phone' => 'required',
		]);

		if ($validator->fails()) {
			throw new HttpResponseException(response(['error' => $validator->errors()], 400));
		}

		$device = Device::whereId($request->get('device_id'))->first();
		$phone = format_phone($data['phone']);
		$code = generate_random_number_string(6);
		$device->phone = $phone;
		$device->code = $code;
		$device->code_valid_until = Carbon::now()->addMinutes(5);
		$device->save();

		return $this->SMSTrafficService->sendSms($phone, $code);
	}

	public function register(Request $request)
	{
		$data = $request->all();
		$validator = Validator::make($data, [
			'code'  => 'required|min:6|max:6',
		]);

		if ($validator->fails()) {
			throw new HttpResponseException(response(['error' => $validator->errors()], 400));
		}

		$device = Device::whereId($request->get('device_id'))->first();
		$phone = $device->phone;
		$code = $data['code'];

		if ($device->code != $code || Carbon::now()->gt($device->code_valid_until)) {
			throw new HttpResponseException(response(['error' => "Code is invalid or expired"], 400));
		}

		$guest = Guest::with(['device'])->wherePhone($phone)->first();
		if (!$guest && ($device->guest_id !== null)) { //смена номера если у девайса есть пользователь и такой номер в системе не зарегестрирован
			$device->guest->update(['phone' => $phone]);
		} elseif (!$guest && ($device->guest_id === null)) { //создание нового пользователя если у девайса нет пользователя и такого номер в системе нет
			$guest = new Guest();
			$guest->phone = $phone;
			$guest->save();
		} elseif ($guest && ($guest->id != $device->guest_id) && $guest->device) { //если есть пользователь с таким номером, но к девайсу прикреплен другой пользователь
			if ($guest->device) { //отвязываем предыдущий девайс если есть
				$guest->device->guest()->dissociate();
				$guest->device->save();
			}
		}
		$device->guest_id = $guest ? $guest->id : $device->guest_id; //прикрепляем к девайсу пользователя
		$device->phone = null;
		$device->code = null;
		$device->code_valid_until = null;
		$device->save();

		$device->refresh();
		$this->guestFields($device);

		return $device;
	}

	public function logout(Request $request)
	{
		$device = Device::whereId($request->get('device_id'))->first();
		$device->guest_id = null;
		$device->save();

		return $device->refresh();
	}

	public function getCards(Request $request)
	{
		if ($guest_id = $request->get('guest_id')) {
			return [
				'discount_cards' => DiscountCard::whereGuestId($guest_id)->get()->makeHidden(['guest']),
				'certificates'   => Certificate::whereGuestId($guest_id)->get()->makeHidden(['guest']),
			];
		} else {
			return null;
		}
	}

	public function getOrders(Request $request)
	{
		$orders = DeliveryOrder::with(['restaurant'])->whereGuestId($request->get('guest_id'));
		$filter = $request->get('filter');
		if ($filter) {
			if ($filter == DeliveryOrder::STATUS_CANCELED) {
				$orders->whereNotNull('canceled_at');
			} elseif ($filter == DeliveryOrder::STATUS_COMPLETED) {
				$orders->whereNotNull('completed_at');
			} elseif ($filter == DeliveryOrder::STATUS_IN_PROGRESS) {
				$orders->whereNull('canceled_at')->whereNull('completed_at');
			}
		}

		return $orders
			->get()
			->each(function(DeliveryOrder $order) {
				$this->ordersFields($order);
			});
	}

	public function addRestaurantToFavorites(Request $request)
	{
		$device = Device::find($request->get('device_id'));
		$restaurant_ids = $request->get('favorite_restaurant_ids');
		$restaurant_id = $request->get('restaurant_id');
		if (!$restaurant_ids->contains($restaurant_id)) {
			$device->restaurants()->attach($restaurant_id);
			return $device->fresh();
		} else {
			return $device;
		}
	}

	public function deleteRestaurantFromFavorites(Request $request)
	{
		$device = Device::find($request->get('device_id'));
		$restaurant_ids = $request->get('favorite_restaurant_ids');
		$restaurant_id = $request->get('restaurant_id');
		if ($restaurant_ids->contains($restaurant_id)) {
			$device->restaurants()->detach($restaurant_id);
			return $device->fresh();
		} else {
			return $device;
		}
	}

	public function addDishToFavorites(Request $request)
	{
		$device = Device::find($request->get('device_id'));
		$dish_ids = $request->get('favorite_dish_ids');
		$dish_id = $request->get('dish_id');
		if (!$dish_ids->contains($dish_id)) {
			$device->delivery_dishes()->attach($dish_id);
			return $device->fresh();
		} else {
			return $device;
		}
	}

	public function deleteDishFromFavorites(Request $request)
	{
		$device = Device::find($request->get('device_id'));
		$dish_ids = $request->get('favorite_dish_ids');
		$dish_id = $request->get('dish_id');
		if ($dish_ids->contains($dish_id)) {
			$device->delivery_dishes()->detach($dish_id);
			return $device->fresh();
		} else {
			return $device;
		}
	}
}
