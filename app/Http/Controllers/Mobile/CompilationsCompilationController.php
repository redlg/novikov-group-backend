<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Models\CompilationsCompilation;

class CompilationsCompilationController extends Controller
{
   public function getCompilationsForDelivery()
   {
   	    return CompilationsCompilation::whereScreenType(CompilationsCompilation::SCREEN_TYPE_DELIVERY_ID)->first();
   }
}
