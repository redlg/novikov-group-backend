<?php

namespace App\Http\Controllers\Mobile;

use App\Models\DeliveryCategory;
use App\Models\DeliveryMethod;
use App\Models\DeliveryPaymentMethod;
use App\Models\RestaurantsCompilation;
use App\Traits\RestaurantTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;
use Validator;

class RestaurantController extends Controller
{
	use RestaurantTrait;

	const SEARCH_RADIUS = 1; //search radius km
	const EARTH_RADIUS = 6371; //Earth radius in km
	const NOVIKOV_COMPILATION_NAME = 'По совету Новикова';

	public function index(Request $request)
    {
        $restaurants = Restaurant::query();

	    if (isset($request['filter'])) {
	    	$filter = $request['filter'];
	    	if ($filter == 'nearby') {
	    		$this::validateLatLngFromRequest($request);
			    $coordinates = $this::getMaxMinCoordinates($request->get('lat'), $request->get('lng'));
			    $restaurants = $restaurants->whereHas('address', function($query) use ($coordinates) {
				    $query->whereBetween('lat', [$coordinates['min_lat'], $coordinates['max_lat']])
					    ->whereBetween('lng', [$coordinates['min_lng'], $coordinates['max_lng']]);
			    });
		    } elseif ($filter == 'novikovAdvise') {
				$compilation = RestaurantsCompilation::whereName(self::NOVIKOV_COMPILATION_NAME)->first();
				if (!is_null($compilation)) {
			        $restaurants = $restaurants->whereIn('id', $compilation->restaurant_ids);
				} else {
					throw new HttpResponseException(response(['error' => "Novikov's compilation not found"], 400));
				}
		    } else {
			    throw new HttpResponseException(response(['error' => "Filter name is incorrect"], 400));
		    }
	    }

	    $restaurants = $restaurants->orderBy(isset($request['sort_by']) ? $request['sort_by'] : 'id');

	    $pagination = $restaurants->paginate(
		    $request->input('per_page', 15),
		    ['*'],
		    'page',
		    $request->input('current_page', 1)
	    );

	    return [
		    'pagination' => [
			    'total'        => $pagination->total(),
			    'current_page' => $pagination->currentPage(),
			    'from'         => $pagination->firstItem(),
			    'to'           => $pagination->lastItem(),
			    'last_page'    => $pagination->lastPage()
		    ],
		    'items' => collect($pagination->items())->each(function(Restaurant $restaurant) {
			    	$this->visibleFields($restaurant);
			    	$this->hiddenFields($restaurant);
			    	$this->addressFields($restaurant);
			    	$this->tagFields($restaurant);
			    })
	    ];
    }

    public function show($id)
    {
        /** @var Restaurant $restaurant */
        $restaurant = Restaurant::with([
		        'social_links',
	            'dishes_compilations',
                'delivery_menu',
		    ])
		    ->findOrFail($id);

        $this->visibleFields($restaurant);
        $this->hiddenFields($restaurant);
	    $this->addressFields($restaurant);
	    $this->tagFields($restaurant);
	    $this->dishesCompilationFields($restaurant);
	    $restaurant->delivery_menu->each(function(DeliveryCategory $deliveryCategory) use ($restaurant) {
	        $fake_compilation['id'] = 'dc_' . $deliveryCategory->id;
            $fake_compilation['name'] = $deliveryCategory->name_ru;
            $fake_compilation['restaurant_id'] = $deliveryCategory->restaurant_id;
            $fake_compilation['dish_ids'] = $deliveryCategory->dishes->pluck('id');
            $restaurant->dishes_compilations->add($fake_compilation);
        });

        return $restaurant;
    }

	public function bulk(Request $request)
	{
		$ids = $request->get('ids');

		if ($ids) {
			$restaurants = Restaurant::whereIn('id', $ids)->get()->each(function(Restaurant $restaurant) {
					$this->visibleFields($restaurant);
					$this->hiddenFields($restaurant);
					$this->addressFields($restaurant);
					$this->tagFields($restaurant);
				});

			return $restaurants;
		} else {
			return [];
		}
	}

	private static function getMaxMinCoordinates($lat, $lng)
	{
		return $coordinates = [
			'max_lat' => $lat + rad2deg(self::SEARCH_RADIUS/self::EARTH_RADIUS),
			'min_lat' => $lat - rad2deg(self::SEARCH_RADIUS/self::EARTH_RADIUS),
			'max_lng' => $lng + rad2deg(self::SEARCH_RADIUS/self::EARTH_RADIUS/cos(deg2rad($lat))),
			'min_lng' => $lng - rad2deg(self::SEARCH_RADIUS/self::EARTH_RADIUS/cos(deg2rad($lat))),
		];
	}

	private static function validateLatLngFromRequest(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'lat' => 'required|numeric',
			'lng' => 'required|numeric',
		]);

		if ($validator->fails()) {
			throw new HttpResponseException(response($validator->errors(), 400));
		}
	}

	public function getNearest(Request $request)
	{
		$this::validateLatLngFromRequest($request);
		$coordinates = $this::getMaxMinCoordinates($request->get('lat'), $request->get('lng'));

		$restaurants = Restaurant::whereHas('address', function($query) use ($coordinates) {
				$query->whereBetween('lat', [$coordinates['min_lat'], $coordinates['max_lat']])
					->whereBetween('lng', [$coordinates['min_lng'], $coordinates['max_lng']]);
			})
			->get()->each(function(Restaurant $restaurant) {
				$this->visibleFields($restaurant);
				$this->hiddenFields($restaurant);
				$this->addressFields($restaurant);
				$this->tagFields($restaurant);
			});

		return $restaurants;
	}

	public function getDeliveryMethods() {
		return DeliveryMethod::all();
	}

	public function getDeliveryPaymentMethods() {
		return DeliveryPaymentMethod::all();
	}

	public function getPaymentMethods(Restaurant $restaurant) {
		return $restaurant->payment_services;
	}
}
