<?php

namespace App\Http\Controllers\Mobile;

use App\Models\DeliveryDish;
use App\Traits\RestaurantTrait;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;
use Validator;

class SearchController extends Controller
{
	use RestaurantTrait;

	public function index(Request $request) {
		$validator = Validator::make($request->all(), [
			'query' => 'required|min:3',
		]);

		if ($validator->fails()) {
			throw new HttpResponseException(response($validator->errors(), 400));
		}

		$q = $request->get('query');

		$restaurants = Restaurant::where('name_ru', '~*', $q)
			->paginate(
				$request->input('r_per_page', 15),
				['*'],
				'r_page',
				$request->input('r_cur_page', 1)
			);
		$r_pagination = [
			'total'        => $restaurants->total(),
			'current_page' => $restaurants->currentPage(),
			'from'         => $restaurants->firstItem(),
			'to'           => $restaurants->lastItem(),
			'last_page'    => $restaurants->lastPage()
		];
		$r_items = collect($restaurants->items())->each(function(Restaurant $restaurant) {
			$this->visibleFields($restaurant);
			$this->hiddenFields($restaurant);
			$this->addressFields($restaurant);
			$this->tagFields($restaurant);
		});

		$dishes = DeliveryDish::where('name_ru', '~*', $q)
			->paginate(
				$request->input('d_per_page', 15),
				['*'],
				'd_page',
				$request->input('d_cur_page', 1)
			);
		$d_pagination = [
			'total'        => $dishes->total(),
			'current_page' => $dishes->currentPage(),
			'from'         => $dishes->firstItem(),
			'to'           => $dishes->lastItem(),
			'last_page'    => $dishes->lastPage()
		];
		$d_items = collect($dishes->items())->each(function(DeliveryDish $dish) {
			$dish
				->makeHidden([
					'image',
					'description_ru',
					'description_en',
					'composition_ru',
					'composition_en',
					'name_ru',
					'name_en',
				])
				->makeVisible([
					'title_image',
					'favorite',
					'restaurant_attributes',
					'name',
					'description',
					'composition',
					'delivery_category_name',
				]);
		});

		return [
			'restaurants' => [
				'pagination' => $r_pagination,
				'items'      => $r_items,
			],
			'dishes'      => [
				'pagination' => $d_pagination,
				'items'      => $d_items,
			],
		];
	}
}
