<?php

namespace App\Http\Controllers\Mobile;

use App\Models\Address;
use App\Models\Partner;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
    public function index()
    {
        $partners = Partner::query()->orderBy('general', 'desc')->get();

        $partners->each(function(Partner $partner) {
        	$partner
		        ->append([
			        'name',
			        'description',
		        ])
		        ->makeHidden([
			        'name_ru',
			        'name_en',
			        'description_ru',
			        'description_en',
		        ]);

	        $partner->addresses->each(function(Address $address) {
		        $address
			        ->makeVisible([
				        'text',
			        ])
			        ->makeHidden([
				        'text_ru',
				        'text_en',
			        ]);
	        });
        });

        return $partners;
    }
}