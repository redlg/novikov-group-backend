<?php

namespace App\Http\Controllers\Rkeeper;

use App\Http\Controllers\Controller;
use App\Models\Abstracts\ErrorCode;

class IndexController extends Controller
{
    public function index()
    {
        return [
	        'description' => config('rkeeper.description'),
	        'version'     => config('rkeeper.version'),
	        'errors'      => ErrorCode::RKEERER_ERRORS,
        ];
    }
}
