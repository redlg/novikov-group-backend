<?php

namespace App\Http\Controllers\Rkeeper;

use App\Http\Controllers\Controller;
use App\Models\Abstracts\Card;
use App\Models\Abstracts\ErrorCode;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\DiscountCode;
use App\Models\Operation;
use App\Models\OperationType;
use App\Models\Restaurant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\HandleRkeeperRequest;

class CardController extends Controller
{
	use HandleRkeeperRequest;

    public function store(Request $request)
    {
        $request_data = $request->input('RequestData', '');

        file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/storage/logs/' . time() . '_card_request_log.txt', print_r($_REQUEST, true));

	    if (!($data = $this->parse($request_data))) {
		    return response()->json([
			    'error'      => 'RequestData содержит невалидный XML',
			    'error_code' => ErrorCode::INVALID_XML,
		    ], 400);
	    }

	    $card = $this->findCard($data['cardCode']);
        $restaurant = Restaurant::whereRkeeperId($data['restaurantCode'])->first();

        //базовый шаблон ответа
	    $response = new \SimpleXMLElement('<response/>');
	    $response->addChild('status')->addAttribute('code', '0');
	    $PCard29Info = $response->addChild('PCard29Info');
	    $PCard29Info->addChild('Size', $data['PCard29Info']['Size']); //размер структуры, берется из самого запроса
	    $PCard29Info->addChild('Deleted', '0'); //карта существовала, но была удалена
	    $PCard29Info->addChild('StopDate', '0'); //карты надо изъять
	    $PCard29Info->addChild('Seize', '0'); //истек срок действия
	    $PCard29Info->addChild('Holy', '0'); //сейчас карта не действует
	    $PCard29Info->addChild('Manager', '0'); //нужно ли подтвержение менеджера
	    $PCard29Info->addChild('Locked', '0'); //карта заблокирована
	    $PCard29Info->addChild('WhyLock'); //причина блокировки - будет показана на кассе
	    $PCard29Info->addChild('Holder'); //имя владельца карты
	    $PCard29Info->addChild('HolderID', '0'); //идентификатор владельца карты
	    $PCard29Info->addChild('CardNumber', '0'); //номер счета
	    $PCard29Info->addChild('Unpay', '0'); //тип неплательщика
	    $PCard29Info->addChild('Bonus', '0'); //номер бонуса
	    $PCard29Info->addChild('Discount', '0'); //номер скидки
	    $PCard29Info->addChild('DiscLimit', '0'); //предельная сумма скидки, в копейках
	    $PCard29Info->addChild('Summa', '0'); //сумма, достаточная для оплаты счета, в копейках
	    $PCard29Info->addChild('Sum2', '0'); //сумма на карточном счете N 2, в копейках
	    $PCard29Info->addChild('Sum3', '0'); //сумма на карточном счете N 3, в копейках
	    $PCard29Info->addChild('Sum4', '0'); //сумма на карточном счете N 4, в копейках
	    $PCard29Info->addChild('Sum5', '0'); //сумма на карточном счете N 5, в копейках
	    $PCard29Info->addChild('Sum6', '0'); //сумма на карточном счете N 6, в копейках
	    $PCard29Info->addChild('Sum7', '0'); //сумма на карточном счете N 7, в копейках
	    $PCard29Info->addChild('Sum8', '0'); //сумма на карточном счете N 8, в копейках
	    $PCard29Info->addChild('DopInfo'); //произвольная информация о карте
	    $PCard29Info->addChild('ScreenInfo'); //информация для вывода на экран кассы
	    $PCard29Info->addChild('PrintInfo'); //информация для распечатки на принтере
        //TODO chmode 3 это отмена чека разблокировать сертификат и удалить транзакцию

	    $operation_type_name = OperationType::ROLLING;
        if ($card instanceof DiscountCard) {
            if ($card->virtual) {
                $PCard29Info->WhyLock = 'Карта привязана к мобильному приложению';
                $PCard29Info->Seize = 1; //или 0 если не надо изъять
                $PCard29Info->ScreenInfo = 'Дисконтная карта привязана к приложению и не может быть использована физически';
                $PCard29Info->PrintInfo = 'Дисконтная карта привязана к приложению и не может быть использована физически';
                $comment = 'Запрос типа скидки карты. Карта привязана к приложению и не может быть использована физически.';
            } elseif ($card->isBlocked()) {
	        	$reason = $this->getBlockReason($card);
		        $PCard29Info->WhyLock = $reason['why'];
		        $PCard29Info->Seize = $reason['seize'];
		        $PCard29Info->ScreenInfo = 'Дисконтная карта заблокирована';
		        $PCard29Info->PrintInfo = 'Дисконтная карта заблокирована';
		        $PCard29Info->Locked = 1;
		        $comment = 'Запрос типа скидки карты. Карта заблокирована.';
	        } elseif ($card->isActive()) {
		        $PCard29Info->ScreenInfo = 'Доступна скидка';
		        $PCard29Info->PrintInfo = 'Доступна скидка';
                $PCard29Info->DiscLimit = 10000000000; //возвращать сумму чека
		        $PCard29Info->Discount = $card->type->rkeeper_id;
		        $comment = 'Запрос типа скидки карты';
	        } elseif ($card->status == Card::NOT_ACTIVATED) {
		        $operation_type_name = OperationType::ACTIVATION;
		        $this->activateCard($card);
		        $PCard29Info->ScreenInfo = 'Карта активирована. Доступна скидка';
		        $PCard29Info->PrintInfo = 'Карта активирована. Доступна скидка';
		        $PCard29Info->DiscLimit = 10000000000; //возвращать сумму чека
		        $PCard29Info->Discount = $card->type->rkeeper_id;
		        $comment = 'Запрос типа скидки карты. Карта активирована';
	        }
	        $card_type = DiscountCard::class;
        } elseif ($card instanceof Certificate) {
			if ($card->isBlocked()) {
				$reason = $this->getBlockReason($card);
				$PCard29Info->WhyLock = $reason['why'];
				$PCard29Info->Seize = $reason['seize'];
				$PCard29Info->ScreenInfo = 'Сертификат заблокирован';
				$PCard29Info->PrintInfo = 'Сертификат заблокирован';
				$PCard29Info->Locked = 1;
				$comment = 'Запрос баланса сертификата. Сертификат заблокирован.';
			} elseif ($card->isActive()) {
				$pennies = round(fmod($card->balance, 1), 2) * 100;
				$PCard29Info->ScreenInfo = 'Доступный баланс ' . (int)($card->balance) . 'р ' . $pennies . 'к';
				$PCard29Info->PrintInfo = 'Доступный баланс ' . (int)($card->balance) . 'р ' . $pennies . 'к';
				$PCard29Info->Summa = $card->balance * 100;
				$comment = 'Запрос баланса сертификата';
			} elseif ($card->status == Card::NOT_ACTIVATED) {
				$operation_type_name = OperationType::ACTIVATION;
				$this->activateCard($card);
				$pennies = round(fmod($card->balance, 1), 2) * 100;
				$PCard29Info->ScreenInfo = 'Сертификат активирован. Доступный баланс ' . (int)($card->balance) . 'р ' . $pennies . 'к';
				$PCard29Info->PrintInfo = 'Сертификат активирован. Доступный баланс '  . (int)($card->balance) . 'р ' . $pennies . 'к';
				$PCard29Info->Summa = $card->balance * 100;
				$comment = 'Запрос баланса сертификата. Сертификат активирован';
			}
	        $card_type = Certificate::class;
        } else {
	        $PCard29Info->WhyLock = 'Карта не найдена';
	        $PCard29Info->ScreenInfo = 'Карта не найдена';
	        $PCard29Info->PrintInfo = 'Карта не найдена';
	        $card_type = null;
        }

        if ($card_type) {
	        $PCard29Info->CardNumber = $card->number;
	        $operation = new Operation();
	        $operation->operation_type_id = OperationType::whereName($operation_type_name)->first()->id;
	        $operation->card_id = $card->id;
	        $operation->card_type = $card_type;
	        $operation->guest_id = $card->guest_id;
	        $operation->restaurant_id = is_null($restaurant) ? null : $restaurant->id;
	        $operation->comment = isset($comment) ? $comment : null;
	        $operation->save();
        }

        return response($response->asXML())->header('Content-Type', 'text/xml');
    }

    private function getBlockReason($card)
    {
	    $card->load(['operations']);
	    $expired_id = OperationType::whereName(OperationType::BLOCKING_EXPIRED)->first()->id;
	    $manually_id = OperationType::whereName(OperationType::BLOCKING_MANUALLY)->first()->id;
	    $service_id = OperationType::whereName(OperationType::BLOCKING_SERVICE)->first()->id;
	    if ($card->operations->whereIn('operation_type_id', [$expired_id, $manually_id, $service_id])->count()) {
		    //берем последнюю операцию блокировки
		    $last_operation = $card->operations->sortBy('created_at')->last();
		    $reason = trim($last_operation->type->name . '. ' . $last_operation->comment);
		    $seize = $last_operation->type->name == OperationType::BLOCKING_EXPIRED;
	    } else if ($card->ended_at == $card->blocked_at) { //если по карте нет операций блокировки, то эта карта импортирована из restoCRM
		    $reason = OperationType::BLOCKING_EXPIRED;
		    $seize = true;
	    } else if ($card instanceof Certificate && ($card->balance != $card->type->sum || $card->balance == 0)) {
		    $reason = OperationType::BLOCKING_SERVICE;
		    $seize = false;
	    } else {
		    $reason = OperationType::BLOCKING_MANUALLY;
		    $seize = false;
	    }

	    return [
		    'why'     => $reason,
		    'seize'   => intval($seize),
	    ];
    }

    private function activateCard($card)
    {
	    $card->status = Card::ACTIVATED_IN_RESTAURANT;
	    $card->started_at = Carbon::now();
	    $card->ended_at = Carbon::now()->addDays($card->type->life_time);
	    $card->save();
    }
}
