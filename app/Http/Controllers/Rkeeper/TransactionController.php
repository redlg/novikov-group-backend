<?php

namespace App\Http\Controllers\Rkeeper;

use App\Http\Controllers\Controller;
use App\Models\Abstracts\ErrorCode;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\Operation;
use App\Models\OperationType;
use App\Models\PaymentMethod;
use App\Models\Restaurant;
use App\Models\Transaction;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Traits\HandleRkeeperRequest;

// This controller is not relevant to the Transaction model
class TransactionController extends Controller
{
	use HandleRkeeperRequest;

	/**
	 * @param Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        //todo посмотреть CHECKPAYMENTS PAYMENT paytype для метода оплаты
	    $request_data = $request->input('RequestData', '');

	    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/storage/logs/' . time() . '_transaction_request_log.txt', print_r($_REQUEST, true));

	    if (!($data = $this->parse($request_data))) {
		    return response()->json([
			    'error'      => 'RequestData содержит невалидный XML',
			    'error_code' => ErrorCode::INVALID_XML,
		    ], 400);
	    }

	    return DB::transaction(function() use ($data) {
		    $card = $this->findCard($data['cardCode']);
            //todo если несколько карт, то кардкод берем из самой транзакции
		    $restaurant = Restaurant::whereRkeeperId($data['restaurantCode'])->first();
		    $chmode = $data['chmode'];
		    //todo если chmode 3 удалять транзакцию
		    $error_code = 0;
		    $err_text = 'no error';
		    if ((count($data['RKTransactInfoEx']) == 1 && empty($data['RKTransactInfoEx'][0])) || empty($data['RKTransactInfoEx'])) {
			    $transactions = false;
		    } else {
			    $transactions = $data['RKTransactInfoEx'];
		    }
		    $response = new \SimpleXMLElement('<response/>');
		    $response->addChild('status')->addAttribute('code', '0');
		    $rkeeper = $response->addChild('rkeeper');
		    $transaction_response = $rkeeper->addChild('TRRESPONSE');
		    $transaction_response->addAttribute('dts', Carbon::now());
		    //есть транзакции по картам
		    if ($transactions && $card) {
		    	$operation_type = OperationType::whereName(OperationType::PAYMENT)->first();
		    	$operation = new Operation();
		    	$operation->operation_type_id = $operation_type->id;
		    	$operation->card_id = $card->id;
		    	$operation->card_type = get_class($card);
		    	$operation->guest_id = $card->guest_id;
		    	$operation->restaurant_id = $restaurant->id;
		    	$operation->save();
			    foreach ($transactions as $key => $transaction_node) {
				    $check_number = $transaction_node['RKCheck'];
				    $cashbox_number = $transaction_node['RKUnit'];
				    $kind = $transaction_node['Kind'];
				    $sum = (float)$transaction_node['Summa'] / 100;
				    $n_transaction = new Transaction();
				    if ($kind == 0) { //платеж (снятие денег со счета)
					    $operation_type = OperationType::whereName(OperationType::PAYMENT)->first();
					    $payment_method = PaymentMethod::whereNameRu(PaymentMethod::CERTIFICATE)->first();
					    $n_transaction->sum = abs($sum);
					    if ($card->balance < abs($sum)) {
						    $error_code = 1;
						    $err_text = 'На карте недостаточно средств для оплаты';
						    break;
					    } else {
						    $card->balance += $sum;
						    //если сертификат одноразовый, то заблокируем его и создадим операцию блокировки
						    if (!$card->type->reusable) {
								$card->blocked_at = Carbon::now();
								$card->status = Certificate::BLOCKED_SERVICE;
							    $operation_type_service = OperationType::whereName(OperationType::BLOCKING_SERVICE)->first();
							    $operation_service = new Operation();
							    $operation_service->operation_type_id = $operation_type_service->id;
							    $operation_service->card_id = $card->id;
							    $operation_service->card_type = get_class($card);
							    $operation_service->guest_id = $card->guest_id;
							    $operation_service->restaurant_id = $restaurant->id;
							    $operation_service->save();
						    }
						    $card->save();
					    }
				    } elseif ($kind == 1) { //скидка
					    $operation_type = OperationType::whereName(OperationType::DISCOUNT)->first();
					    $payment_method = PaymentMethod::whereNameRu(PaymentMethod::DISCOUNT_CARD)->first();
					    $n_transaction->discount_sum = abs($sum);
				    } elseif ($kind == 2) { //бонус (начисление денег на счет)
						//В нашей системе такой тип транзакций не поддерживается
				    } elseif ($kind == 3) { //потраты гостя (сколько заплатил своих денег)
					    $operation_type = OperationType::whereName(OperationType::PAYMENT)->first();
					    $payment_method = PaymentMethod::whereNameRu(PaymentMethod::EXPENSES)->first();
					    $n_transaction->sum = abs($sum);
				    }
				    $n_transaction->operation_type_id = $operation_type->id;
				    $n_transaction->payment_method_id = $payment_method->id;
				    $n_transaction->restaurant_id =$restaurant->id;
				    $n_transaction->card_id = $card->id;
				    $n_transaction->card_type = get_class($card);
				    $n_transaction->guest_id = $card->guest_id;
				    $n_transaction->operation_id = $operation->id;
				    $n_transaction->card_input_method = $card->virtual ? Transaction::VIRTUAL_NAME : Transaction::PHYSICAL_NAME;
				    $n_transaction->check_number = $check_number;
				    $n_transaction->cashbox_number = $cashbox_number;
				    $n_transaction->save();
				    $transaction = $transaction_response->addChild('TRANSACTION');
				    $transaction->addAttribute('cardcode', $data['cardCode']);
				    $transaction->addAttribute('num', ++$key);
				    $transaction->addAttribute('ext_id', $n_transaction->id);
			    }
		    } elseif ($transactions && !$card) { //карта не найдена в базе
		        $error_code = 1;
                $err_text = 'Карта с таким номером не найдена';
            } else { //денежный перевод без участия карты
		        //заглушка возврата
			    $sum = 0;
				foreach ($data['dishes'] as $dish) {
					$sum += $dish['quantity'] * $dish['price'];
				}
				$operation_type = OperationType::whereName(OperationType::PAYMENT)->first();
				$payment_method = PaymentMethod::whereNameRu(PaymentMethod::EXPENSES)->first();
				$n_transaction = new Transaction();
				$n_transaction->operation_type_id = $operation_type->id;
				$n_transaction->restaurant_id = $restaurant->id;
			    $n_transaction->payment_method_id = $payment_method->id;
			    $n_transaction->sum = $sum;
			    $n_transaction->check_number = $data['checkNumber'];
			    $n_transaction->cashbox_number = $data['cashboxNumber'];
			    $n_transaction->save();
		    }
		    $transaction_response->addAttribute('error_code', $error_code);
		    $transaction_response->addAttribute('err_text', $err_text);
		    //$print_document =$transaction_response->addChild('PRINTDOCUMENT');

		    return response($response->asXML())->header('Content-Type', 'text/xml');
	    });
    }
}
