<?php

namespace App\Http\Controllers\CRM;

use App\Models\Abstracts\ErrorCode;
use App\Http\Controllers\Controller;

class ErrorCodeController extends Controller
{
	public function index()
	{
		return ErrorCode::CRM_ERRORS;
	}
}
