<?php

namespace App\Http\Controllers\CRM;

use App\Models\LegalEntity;
use App\Models\LegalEntityType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LegalEntityController extends Controller
{
	public function index(Request $request)
	{
		$legal_entities = LegalEntity::query();

		if ($legal_entity_type_id = $request->get('legal_entity_type_id')) {
			$legal_entities->where('legal_entity_type_id', $legal_entity_type_id);
		}

		if ($inn = $request->get('inn')) {
			$legal_entities->where('inn', '~*', $inn);
		}

		if ($kpp = $request->get('kpp')) {
			$legal_entities->where('kpp', '~*', $kpp);
		}

		if ($name = $request->get('name')) {
			$legal_entities->where('name', '~*', $name);
		}

		if ($payment_account = $request->get('payment_account')) {
			$legal_entities->where('payment_account', '~*', $payment_account);
		}

		if ($correspondent_account = $request->get('correspondent_account')) {
			$legal_entities->where('correspondent_account', '~*', $correspondent_account);
		}

		if ($bik = $request->get('bik')) {
			$legal_entities->where('bik', '~*', $bik);
		}

		if ($ogrn = $request->get('ogrn')) {
			$legal_entities->where('ogrn', '~*', $ogrn);
		}

		if ($phone = $request->get('phone')) {
			$legal_entities->where('phone', '~*', $phone);
		}

		$order_type = $request->get('sort_type') ?: 'asc';
		$order_by = $request->get('sort_by') ?: 'name';
		$legal_entities->orderBy($order_by, $order_type);

		$pagination = $legal_entities->paginate(
			$request->input('per_page', 25),
			['*'],
			'page',
			$request->input('cur_page', 1)
		);

		return [
			'pagination' => [
				'total'        => $pagination->total(),
				'current_page' => $pagination->currentPage(),
				'from'         => $pagination->firstItem(),
				'to'           => $pagination->lastItem(),
				'last_page'    => $pagination->lastPage(),
			],
			'items'      => $pagination->items(),
		];
	}

	public function show($id)
	{
		return LegalEntity::findOrFail($id);
	}

	/**
	 * @param Request $request
	 *
	 * @return LegalEntity
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $legalEntity = new LegalEntity($request->all());
        $legalEntity->saveOrFail();

        return $legalEntity;
    }

	public static function getFilters()
	{
		$types = LegalEntityType::all()
			->map(function($item) {
				return [
					'id'   => $item->id,
					'name' => $item->name,
				];
			});

		$issuer = $types->where('name', '=', LegalEntityType::ISSUER)->first();
		$issuers = LegalEntity::whereLegalEntityTypeId($issuer['id'])
            ->select(['id', 'name'])
            ->get()
            ->makeHidden(['legal_entity_type']);
        $contractor = $types->where('name', '=', LegalEntityType::COUNTERPARTY)->first();
        $contractors = LegalEntity::whereLegalEntityTypeId($contractor['id'])
            ->select(['id', 'name'])
            ->get()
            ->makeHidden(['legal_entity_type']);
		$partner = $types->where('name', '=', LegalEntityType::PARTNER)->first();
		$partners = LegalEntity::whereLegalEntityTypeId($partner['id'])
			->select(['id', 'name'])
			->get()
			->makeHidden(['legal_entity_type']);

		return [
            'legal_entities_types' => $types,
            'issuers'              => $issuers,
            'contractors'          => collect($contractors)->concat($partners),
		];
	}

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $legalEntity = LegalEntity::findOrFail($id);
        $legalEntity->fill($request->all());
        $legalEntity->saveOrFail();

        return $legalEntity;
    }

    public function destroy($id)
    {
        LegalEntity::destroy($id);
    }
}
