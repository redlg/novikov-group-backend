<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\Controller;


class IndexController extends Controller
{
    public function index()
    {
        return [
            'description' => config('crm.description'),
            'version'     => config('crm.version')
        ];
    }
}
