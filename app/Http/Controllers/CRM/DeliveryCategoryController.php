<?php

namespace App\Http\Controllers\CRM;

use Illuminate\Support\Facades\DB;
use App\Models\DeliveryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryCategoryController extends Controller
{
    public function index(Request $request)
    {
    	$delivery_categories = DeliveryCategory::query();
	    $lang = $request->get('language');

	    if ($name = $request->get('name')) {
		    $delivery_categories->where('name_' . $lang, '~*', $name);
	    }

        if ($restaurant_id = $request->get('restaurant_id')) {
	        $delivery_categories->where('restaurant_id', $restaurant_id);
        }

        return $delivery_categories->orderBy('id', 'asc')->get();
    }

    public function show($id)
    {
        return DeliveryCategory::findOrFail($id);
    }

	/**
	 * @param Request $request
	 *
	 * @return DeliveryCategory
	 * @throws \Throwable
	 */
	public function store(Request $request)
	{
		$category = new DeliveryCategory($request->all());
		$category->saveOrFail();

		return $category;
	}

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
	{
		$category = DeliveryCategory::findOrFail($id);
		$category->fill($request->all());
		$category->saveOrFail();

		return $category;
	}

    public function destroy($id)
    {
	    DeliveryCategory::destroy($id);
    }
}
