<?php

namespace App\Http\Controllers\CRM;

use App\Models\MenuCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuCategoryController extends Controller
{
    public function index(Request $request)
    {
    	$menu_categories = MenuCategory::query();
	    $lang = $request->get('language');

	    if ($name = $request->get('name')) {
		    $menu_categories->where('name_' . $lang, '~*', $name);
	    }

        if ($restaurant_id = $request->get('restaurant_id')) {
	        $menu_categories->where('restaurant_id', $restaurant_id);
        }

        return $menu_categories->orderBy('id', 'asc')->get();
    }

    public function show($id)
    {
        return MenuCategory::findOrFail($id);
    }

	/**
	 * @param Request $request
	 *
	 * @return MenuCategory
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
	    $category = new MenuCategory($request->all());
	    $category->saveOrFail();

	    return $category;
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
	{
		$category = MenuCategory::findOrFail($id);
		$category->fill($request->all());
		$category->saveOrFail();

		return $category;
	}

    public function destroy($id)
    {
	    MenuCategory::destroy($id);
    }
}
