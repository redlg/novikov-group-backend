<?php

namespace App\Http\Controllers\CRM;

use App\Models\Address;
use App\Models\Image;
use App\Models\LegalEntity;
use App\Models\LegalEntityType;
use DB;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Http\Controllers\Controller;

class RestaurantController extends Controller
{
    public function index(Request $request)
    {
    	$restaurants = Restaurant::with(['legal_entity']);
    	$lang = $request->get('language');

	    if ($name = $request->get('query')) {
		    $restaurants->where('name_' . $lang, '~*', $name)
		        ->orWhereHas('legal_entity', function($query) use ($name) {
		        	$query->where('name', '~*', $name);
		        });
	    }

	    if ($city = $request->get('city')) {
		    $restaurants->whereHas('address', function($query) use ($city) {
		    	$query->where('city', '=', $city);
		    });
	    }

	    if ($seats_from = $request->get('seats_from')) {
	    	$restaurants->where('number_of_seats', '>=', $seats_from);
	    }

	    if ($seats_to = $request->get('seats_to')) {
		    $restaurants->where('number_of_seats', '<=', $seats_to);
	    }

	    if ($delivery = $request->get('delivery')) {
	    	$restaurants->where('delivery', true);
	    }

	    $order_type = $request->get('sort_type') ?: 'asc';
	    $order_by = $request->get('sort_by') ?: 'name_' . $lang;
	    if (str_contains($order_by, '.')) {
		    $split = explode('.', $order_by);
		    $restaurants->addField($order_by);
		    $restaurants->orderBy($split[0] . "_" . $split[1], $order_type);
	    } else {
		    $restaurants->orderBy($order_by, $order_type);
	    }

	    $pagination = $restaurants->paginate(
		    $request->input('per_page', 25),
		    ['*'],
		    'page',
		    $request->input('cur_page', 1)
	    );

	    return [
		    'pagination' => [
			    'total'        => $pagination->total(),
			    'current_page' => $pagination->currentPage(),
			    'from'         => $pagination->firstItem(),
			    'to'           => $pagination->lastItem(),
			    'last_page'    => $pagination->lastPage(),
		    ],
		    'items'      => $pagination->items(),
	    ];
    }

	public function show($id)
	{
		return Restaurant::findOrFail($id);
	}

    public function getFilters()
    {
    	$cities = DB::table('restaurants')
		    ->join('addresses', 'restaurants.address_id', '=', 'addresses.id')
		    ->select('addresses.city')
		    ->distinct()
	        ->get()
		    ->map(function($item) {
			    return [
				    'id'   => $item->city,
				    'name' => $item->city,
			    ];
		    });

        return [
            'cities' => $cities,
        ];
    }

	public function getValues()
	{
		return Restaurant::select([
				'id',
				'name_ru',
				'delivery'
			])
			->orderBy('name_ru')
			->get()
			->map(function(Restaurant $restaurant) {
				return [
					'id'       => $restaurant->id,
					'name'     => $restaurant->name_ru,
					'delivery' => $restaurant->delivery,
				];
			});
	}

	/**
	 * @param Request $request
	 *
	 * @return Restaurant
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
    	$legal_entity_type = LegalEntityType::whereName(LegalEntityType::RESTAURANT)->first();
    	$legal_entity_request = $request->get('legal_entity', []);
	    $legal_entity_request['legal_entity_type_id'] = $legal_entity_type->id;
    	$address_request = $request->get('address', []);
	    $address_request['addressable_type'] = Restaurant::class;
    	$restaurant_request = $request->get('restaurant', []);

    	$legal_entity = new LegalEntity();
    	$legal_entity->fill($legal_entity_request);
    	$legal_entity->saveOrFail();
	    $restaurant_request['legal_entity_id'] = $legal_entity->id;

	    $address = new Address();
	    $address->fill($address_request);
	    $address->saveOrFail();
	    $restaurant_request['address_id'] = $address->id;

	    $restaurant = new Restaurant();
	    $restaurant->fill($restaurant_request);
	    $restaurant->saveOrFail();
        if (!is_null($restaurant->logo_id)) {
            Image::whereId($restaurant->logo_id)->first()->confirm();
        }

	    if (isset($restaurant_request['kitchen_ids']) && !empty($restaurant_request['kitchen_ids'])) {
	    	$restaurant->kitchens()->attach($restaurant_request['kitchen_ids']);
	    }

	    if (isset($restaurant_request['image_ids']) && !empty($restaurant_request['image_ids'])) {
		    $restaurant->images()->attach($restaurant_request['image_ids']);
		    Image::query()->whereIn('id', $restaurant_request['image_ids'])->get()->each(function(Image $image) {
			    $image->confirm();
		    });
	    }

	    if (isset($restaurant_request['tag_ids']) && !empty($restaurant_request['tag_ids'])) {
		    $restaurant->tags()->attach($restaurant_request['tag_ids']);
	    }

	    return $restaurant->refresh();
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
	    /** @var Restaurant $restaurant */
	    $restaurant = Restaurant::with(['images'])->findOrFail($id);
        if (($logo_id = $request->get('logo_id')) && ($logo_id != $restaurant->logo_id)) {
            Image::whereId($logo_id)->first()->confirm();
            Image::whereId($restaurant->logo_id)->first()->refuse();
        }
	    $restaurant->fill($request->all());
	    $restaurant->saveOrFail();

	    $kitchen_ids = $request->get('kitchen_ids');
	    if (is_array($kitchen_ids)) {
		    $restaurant->kitchens()->sync($kitchen_ids);
	    }

	    $image_ids = $request->get('image_ids');
	    if (is_array($image_ids)) {
	    	$restaurant_images = $restaurant->images->pluck('id');
		    $to_delete = $restaurant_images->diff($image_ids)->toArray();
		    $to_add = collect($image_ids)->diff($restaurant_images)->toArray();
		    $restaurant->images()->sync($image_ids);
		    if (!empty($to_delete)) {
				Image::whereIn('id', $to_delete)->get()->each(function(Image $image) {
					$image->refuse();
				});
		    }
		    if (!empty($to_add)) {
			    Image::whereIn('id', $to_add)->get()->each(function(Image $image) {
				    $image->confirm();
			    });
		    }
	    }

	    $tag_ids = $request->get('tag_ids');
	    if (is_array($tag_ids)) {
		    $restaurant->tags()->sync($tag_ids);
	    }

	    return $restaurant->refresh();
    }

    public function destroy($id)
    {
        Restaurant::destroy($id);
    }
}