<?php

namespace App\Http\Controllers\CRM;

use App\Models\Address;
use App\Models\DeliveryDish;
use App\Models\DeliveryMethod;
use App\Models\DeliveryOrder;
use App\Models\DeliveryPaymentMethod;
use App\Models\Guest;
use App\Traits\DeliveryOrderTrait;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class DeliveryOrderController extends Controller
{
	use DeliveryOrderTrait;

    public function index(Request $request)
    {
        $delivery_orders = DeliveryOrder::query();

        if ($guest_id = $request->get('guest_id')) {
            $delivery_orders->where('guest_id', $guest_id);
        }

        if ($restaurant_id = $request->get('restaurant_id')) {
            $delivery_orders->where('restaurant_id', $restaurant_id);
        }

        if ($created_at_from = $request->get('created_at_from')) {
            $delivery_orders->whereDate('created_at', '>=', $created_at_from);
        }

        if ($created_at_to = $request->get('created_at_to')) {
            $delivery_orders->whereDate('created_at', '<=', $created_at_to);
        }

        if ($canceled_at_from = $request->get('canceled_at_from')) {
            $delivery_orders->whereDate('canceled_at', '>=', $canceled_at_from);
        }

        if ($canceled_at_to = $request->get('canceled_at_to')) {
            $delivery_orders->whereDate('canceled_at', '<=', $canceled_at_to);
        }

        if ($completed_at_from = $request->get('completed_at_from')) {
            $delivery_orders->whereDate('completed_at', '>=', $completed_at_from);
        }

        if ($completed_at_to = $request->get('completed_at_to')) {
            $delivery_orders->whereDate('completed_at', '<=', $completed_at_to);
        }

        if ($sum_from = $request->get('sum_from')) {
            $delivery_orders->where('sum', '>=', $sum_from);
        }

        if ($sum_to = $request->get('sum_to')) {
            $delivery_orders->where('sum', '<=', $sum_to);
        }

        if ($delivery_payment_method_id = $request->get('delivery_payment_method_id')) {
            $delivery_orders->where('delivery_payment_method_id', $delivery_payment_method_id);
        }

        if ($delivery_method_id = $request->get('delivery_method_id')) {
            $delivery_orders->where('delivery_method_id', $delivery_method_id);
        }

        $status = $request->get('status');
        if ($status) {
            if ($status == DeliveryOrder::STATUS_CANCELED) {
                $delivery_orders->whereNotNull('canceled_at');
            } elseif ($status == DeliveryOrder::STATUS_COMPLETED) {
                $delivery_orders->whereNotNull('completed_at');
            } elseif ($status == DeliveryOrder::STATUS_IN_PROGRESS) {
                $delivery_orders->whereNull('canceled_at')->whereNull('completed_at');
            }
        }

        $order_type = $request->get('sort_type') ?: 'desc';
        $order_by = $request->get('sort_by') ?: 'created_at';
        if (str_contains($order_by, '.')) {
            $split = explode('.', $order_by);
            $delivery_orders->addField($order_by);
            $delivery_orders->orderBy($split[0] . "_" . $split[1], $order_type);
        } else {
            $delivery_orders->orderBy($order_by, $order_type);
        }

        $pagination = $delivery_orders->paginate(
            $request->input('per_page', 50),
            ['*'],
            'page',
            $request->input('cur_page', 1)
        );

        return [
            'pagination' => [
                'total'        => $pagination->total(),
                'current_page' => $pagination->currentPage(),
                'from'         => $pagination->firstItem(),
                'to'           => $pagination->lastItem(),
                'last_page'    => $pagination->lastPage()
            ],
            'items' => $pagination->items(),
        ];
    }

    public function show($id)
    {
        return DeliveryOrder::findOrFail($id)->makeVisible(['discount_card', 'certificate']);
    }

	/**
	 * @param Request $request
	 *
	 * @return DeliveryOrder
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
		return DB::transaction(function() use ($request) {
			$data = $request->all();
			$validator = Validator::make($data, [
				'guest_id'                   => 'numeric|required_without:phone,first_name,last_name,email',
				'phone'                      => 'required_without:guest_id',
				'first_name'                 => 'required_without:guest_id',
				'last_name'                  => 'required_without:guest_id',
				'email'                      => 'required_without:guest_id',
				'address_id'                 => 'numeric|required_without:street,building,apartment',
				'street'                     => 'required_without:address_id',
				'building'                   => 'required_without:address_id',
				'apartment'                  => 'required_without:address_id',
				'restaurant_id'              => 'required|numeric',
				'delivery_payment_method_id' => 'required|numeric',
				'delivery_method_id'         => 'required|numeric',
				'dishes'                     => 'required|array',
			]);

			if ($validator->fails()) {
				throw new HttpResponseException(response($validator->errors(), 400));
			}

			//если не передан гость, то создаем нового
			$guest_id = $request->get('guest_id');
			if (!$guest_id) {
				$guest = new Guest();
				$guest->phone = format_phone($request->get('phone'));
				$guest->first_name = $request->get('first_name');
				$guest->last_name = $request->get('last_name');
				$guest->email = $request->get('email');
				$guest->birthday_at = $request->get('birthday_at', null);
				$guest->save();
			} else {
				$guest = Guest::whereId($guest_id)->first();
			}

			//если не передан адрес, то создаем новый
			if (!($address_id = $request->get('address_id'))) {
				$address = new Address();
				$address->addressable_id = $guest->id;
				$address->addressable_type = Guest::class;
				$address->city = $request->get('city', Address::DEFAULT_CITY);
				$address->street = $request->get('street');
				$address->building = $request->get('building');
				$address->block = $request->get('block', null);
				$address->apartment = $request->get('apartment');
				$address->floor = $request->get('floor', null);
				$address->main = $guest->addresses->count() ? false : true;
				$address->save();
				$address_id = $address->id;
			}

			$dishes = collect($request->get('dishes'))->mapWithKeys(function($dish) {
				return [$dish['id'] => ['count' => $dish['count']]];
			});

			$deliveryOrder = new DeliveryOrder();
			$deliveryOrder->guest_id = $guest->id;
			$deliveryOrder->address_id = $address_id;
			$deliveryOrder->restaurant_id = $request->get('restaurant_id');
			$deliveryOrder->delivery_payment_method_id = $request->get('delivery_payment_method_id');
			$deliveryOrder->delivery_method_id = $request->get('delivery_method_id');
			$deliveryOrder->discount_card_id = $request->get('discount_card_id', null);
			$deliveryOrder->certificate_id = $request->get('certificate_id', null);
			$deliveryOrder->delivered_at = $request->get('delivered_at') ? Carbon::createFromFormat('Y-m-d H:i:s', $request->get('delivered_at')): null;
			$deliveryOrder->number_of_persons = $request->get('number_of_persons', 1);
			$deliveryOrder->sum = $this->calculateSum($dishes->toArray());
			$deliveryOrder->comment = $request->get('comment', null);
			$deliveryOrder->saveOrFail();
			$deliveryOrder->dishes()->attach($dishes);
			$this->sendOrder($deliveryOrder);

			return $deliveryOrder;
		});
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
	    /** @var DeliveryOrder $deliveryOrder */
	    $deliveryOrder = DeliveryOrder::findOrFail($id);
        $data = $request->all();
        //новый адрес доставки
        if (!($address_id = $request->get('address_id'))) {
	        $address = new Address();
	        $address->addressable_id = $data['guest_id'];
	        $address->addressable_type = Guest::class;
	        $address->city = isset($data['address']['city']) ? $data['address']['city'] : Address::DEFAULT_CITY;
	        $address->street = $data['address']['street'];
	        $address->building = $data['address']['building'];
	        $address->block = isset($data['address']['block']) ? $data['address']['block'] : null;
	        $address->apartment = $data['address']['apartment'];
	        $address->floor = isset($data['address']['floor']) ? $data['address']['floor'] : null;
	        $address->save();
	        $data['address_id'] = $address->id;
        } elseif ($deliveryOrder->address_id != $address_id) {
        	$address = Address::whereId($address_id)->first();
	        $address->fill($data['address']);
	        $address->saveOrFail();
        } else {
	        $deliveryOrder->address->fill($data['address']);
	        $deliveryOrder->address->saveOrFail();
        }

        if ($data['delivered_at']) {
            $data['delivered_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $data['delivered_at']);
        }

        $deliveryOrder->fill($data);
        $dishes = collect($data['dishes'])->mapWithKeys(function($dish) {
	        return [$dish['id'] => ['count' => $dish['count']]];
        });
	    $deliveryOrder->sum = $this->calculateSum($dishes->toArray());
        $deliveryOrder->saveOrFail();
        $deliveryOrder->dishes()->sync($dishes);

        return $deliveryOrder->refresh();
    }

    public function destroy($id)
    {
        DeliveryOrder::destroy($id);
    }

    public function getFilters()
    {
        return [
            'statuses'                 => DeliveryOrder::STATUSES,
            'delivery_methods'         => DeliveryMethod::all()->makeHidden(['price']),
            'delivery_payment_methods' => DeliveryPaymentMethod::all(),
        ];
    }

    public function complete(Request $request, $id)
    {
        $datetime = $request->get('datetime');
	    /** @var DeliveryOrder $deliveryOrder */
	    $deliveryOrder = DeliveryOrder::findOrFail($id);
        $deliveryOrder->complete($datetime);

        return $deliveryOrder;
    }

    public function cancel(Request $request, $id)
    {
        $message = $request->get('message');
	    /** @var DeliveryOrder $deliveryOrder */
        $deliveryOrder = DeliveryOrder::findOrFail($id);
        $deliveryOrder->cancel($message);

        return $deliveryOrder;
    }

	/**
	 * $dishes must be array like '[
	 *      [1 => ["count" => 2],
	 *      [2 => ["count" => 3],
	 *      [3 => ["count" => 1]
	 * ]'
	 *
	 * @param array $dishes
	 *
	 * @return float
	 */
	private function calculateSum($dishes)
	{
		$sum = 0;
		$dish_ids = array_keys($dishes);
		$delivery_dishes = DeliveryDish::whereIn('id', $dish_ids)->select(['id', 'price'])->get();
		foreach ($delivery_dishes as $dish) {
			$count = $dishes[$dish->id]['count'];
			$sum += $count * $dish->price;
		}

		return $sum;
	}
}
