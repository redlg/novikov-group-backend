<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\Controller;
use App\Models\CompilationsCompilation;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CompilationsCompilationController extends Controller
{
	public function index(Request $request)
	{
		$compilations = CompilationsCompilation::query();

		if ($name = $request->get('name')) {
			$compilations->where('name', '~*', $name);
		}

		if ($screen_type = $request->get('screen_type')) {
			$compilations->where('screen_type', '=', $screen_type);
		}

		return $compilations->get()->makeHidden(['compilations']);
	}

	/**
	 * @param Request $request
	 *
	 * @return CompilationsCompilation
	 * @throws \Throwable
	 */
	public function store(Request $request)
	{
		$compilationsCompilation = new CompilationsCompilation($request->all());
		$compilationsCompilation->saveOrFail();

		return $compilationsCompilation;
	}

	public function show($id)
	{
		return CompilationsCompilation::findOrFail($id)
			->makeHidden(['compilations'])
			->makeVisible(['delivery_dishes_compilations', 'restaurants_compilations']);
	}

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
	{
		/** @var CompilationsCompilation $compilationsCompilation */
		$compilationsCompilation = CompilationsCompilation::findOrFail($id);
		$compilationsCompilation->fill($request->all());
		$compilationsCompilation->saveOrFail();

		$dishes = $this->prependArrayToSync($request->get('delivery_dishes_compilations', []));
		$restaurants = $this->prependArrayToSync($request->get('restaurants_compilations', []));
		$compilationsCompilation->delivery_dishes_compilations()->sync($dishes);
		$compilationsCompilation->restaurants_compilations()->sync($restaurants);

		return $compilationsCompilation->refresh();
	}

	public function destroy($id)
	{
		CompilationsCompilation::destroy($id);
	}

	public function getFilters()
	{
		return [
			'screen_types' => CompilationsCompilation::SCREEN_TYPES,
		];
	}

	/**
	 * @param array $array
	 *
	 * @return Collection
	 */
	protected function prependArrayToSync($array)
	{
		return collect($array)->mapWithKeys(function($item) {
			return [
				$item['pivot']['compilable_id'] => [
					'sort' => $item['pivot']['sort'],
					'geo' => $item['pivot']['geo'],
				]
			];
		});
	}
}
