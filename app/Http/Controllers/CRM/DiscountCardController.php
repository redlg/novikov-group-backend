<?php

namespace App\Http\Controllers\CRM;

use App\Jobs\ExportDiscountCards;
use App\Models\Abstracts\Card;
use App\Models\Abstracts\ErrorCode;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\DiscountCardType;
use App\Models\Guest;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountCardController extends Controller
{

	/**
	 * @param Request $request
	 * @param bool    $to_excel
	 * @param bool    $check_quantity
	 *
	 * @return array|\Illuminate\Database\Eloquent\Collection|int|static[]
	 */
	public function index(Request $request, $to_excel = false, $check_quantity = false)
    {
	    $discount_cards = DiscountCard::with(['issuer', 'contractor']);

        if ($number = $request->get('query')) {
            $discount_cards->where(function($query) use($number) {
                $query->where('number', '~*', $number)
                    ->orWhereHas('guest', function($subquery) use ($number) {
                        $subquery->whereRaw("CONCAT(last_name, ' ', first_name) ~* ?", [$number])
                            ->orWhereRaw("CONCAT(first_name, ' ', last_name) ~* ?", [$number]);
                    });
            });
        }

        if ($status = $request->get('status')) {
	        $discount_cards->where('status', '=', $status);
        }

        if ($type = $request->get('type')) {
            $discount_cards->where('card_type_id', '=', $type);
        }

        if ($contractor_id = $request->get('contractor_id')) {
        	$discount_cards->where('contractor_id', '=', $contractor_id);
        }

	    if ($issuer_id = $request->get('issuer_id')) {
		    $discount_cards->where('issuer_id', '=', $issuer_id);
	    }

	    if ($restaurant_id = $request->get('restaurant_id')) {
		    $discount_cards->where('restaurant_id', '=', $restaurant_id);
	    }

        if ($created_at_from = $request->get('created_at_from')) {
            $discount_cards->whereDate('created_at', '>=', $created_at_from);
        }

        if ($created_at_to = $request->get('created_at_to')) {
            $discount_cards->whereDate('created_at', '<=', $created_at_to);
        }

        if ($started_at_from = $request->get('started_at_from')) {
            $discount_cards->whereDate('started_at', '>=', $started_at_from);
        }

        if ($started_at_to = $request->get('started_at_to')) {
            $discount_cards->whereDate('started_at', '<=', $started_at_to);
        }

        if ($ended_at_from = $request->get('ended_at_from')) {
            $discount_cards->whereDate('ended_at', '>=', $ended_at_from);
        }

        if ($ended_at_to = $request->get('ended_at_to')) {
            $discount_cards->whereDate('ended_at', '<=', $ended_at_to);
        }

        if ($blocked_at_from = $request->get('blocked_at_from')) {
            $discount_cards->whereDate('blocked_at', '>=', $blocked_at_from);
        }

        if ($blocked_at_to = $request->get('blocked_at_to')) {
            $discount_cards->whereDate('blocked_at', '<=', $blocked_at_to);
        }

        $order_type = $request->get('sort_type') ?: 'asc';
        $order_by = $request->get('sort_by') ?: 'id';
        if (str_contains($order_by, '.')) {
            $split = explode('.', $order_by);
            $discount_cards->addField($order_by);
            $discount_cards->orderBy($split[0] . "_" . $split[1], $order_type);
        } else {
            $discount_cards->orderBy($order_by, $order_type);
        }

	    if ($check_quantity) {
		    return $discount_cards->count(['id']);
	    } elseif ($to_excel) {
		    return $discount_cards->get();
	    } else {
		    $pagination = $discount_cards->paginate(
			    $request->input('per_page', 50),
			    ['*'],
			    'page',
			    $request->input('cur_page', 1)
		    );

		    return [
			    'pagination' => [
				    'total'        => $pagination->total(),
				    'current_page' => $pagination->currentPage(),
				    'from'         => $pagination->firstItem(),
				    'to'           => $pagination->lastItem(),
				    'last_page'    => $pagination->lastPage()
			    ],
			    'items' => $pagination->items(),
		    ];
	    }
    }

    public function export(Request $request)
    {
	    if ($this->index($request, false, true) > config('crm.max_entries_count_for_export')) {
		    throw new HttpResponseException(response([
			    'error_code' => ErrorCode::TOO_MANY_RECORDS_FOR_EXPORT,
			    'error'      => "Количество записей на экспорт превышает "
				    . config('crm.max_entries_count_for_export')
				    . ", пожалуйста, примените фильтры и попробуйте ещё раз.",
		    ], 200));
	    } else {
		    $request_array = $request->toArray();
		    $filename = 'discount_cards' . time();
		    dispatch(new ExportDiscountCards($request_array, $filename));

		    return $filename . '.xls';
	    }
    }

	/**
	 * @param Request $request
	 *
	 * @return DiscountCard
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
    	$data = $request->all();
    	if ($error = $this->isInvalidNumber($data['number'])) {
		    throw new HttpResponseException(response([
			    'error_code' => ErrorCode::CARD_CREATE_VALIDATION,
			    'error'      => $error,
		    ], 200));
	    }
	    if (!isset($data['guest_id']) || empty($data['guest_id'])) {
    		$guest = new Guest();
    		$guest->phone = $data['phone'];
    		$guest->last_name = $data['last_name'];
    		$guest->first_name = $data['first_name'];
    		$guest->save();
    		$data['guest_id'] = $guest->id;
	    }
	    $discountCardType = DiscountCardType::whereId($data['card_type_id'])->first();
    	$data['user_id'] = \Auth::id();
    	$data['status'] = Card::ACTIVATED_IN_OFFICE;
    	if (!isset($data['started_at']) || empty($data['started_at'])) {
		    $data['started_at'] = Carbon::now();
		    $data['ended_at'] = Carbon::now()->addDays($discountCardType->life_time);
	    } else {
            $data['started_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $data['started_at']);
		    $ended_at = new Carbon($data['started_at']);
		    $data['ended_at'] = $ended_at->addDays($discountCardType->life_time);
	    }
        $discountCard = new DiscountCard($data);
        $discountCard->saveOrFail();

        return $discountCard;
    }

	/**
	 * @param Request $request
	 *
	 * @return array
	 * @throws \Throwable
	 */
	public function bulkStore(Request $request)
    {
    	$numbers = $request->get('numbers', []);
    	$errors = [];
    	foreach ($numbers as $number) {
    		if ($error = $this->isInvalidNumber($number)) {
    			$errors[] = $error;
		    }
	    }
	    if (!empty($errors)) {
		    throw new HttpResponseException(response([
			    'error_code' => ErrorCode::CARD_CREATE_EXISTS,
			    'error'      => $errors,
		    ], 200));
	    } else {
		    $data = $request->all();
		    $data['user_id'] = \Auth::id();
		    $data['status'] = Card::NOT_ACTIVATED;
		    $cards = [];
		    foreach ($numbers as $number) {
			    $data['number'] = $number;
			    $discountCard = new DiscountCard($data);
			    $discountCard->saveOrFail();
                $cards[] = [
                    'id'     => $discountCard->id,
                    'number' => $discountCard->number,
                ];
		    }

		    return $cards;
	    }
    }

	/**
	 * @param Request $request
	 *
	 * @return array
	 * @throws \Throwable
	 */
	public function massStore(Request $request)
    {
    	$from = $request->get('from_number');
    	$quantity = $request->get('count');
    	$force = $request->get('force', false);
    	if ((Card::MAX_NUMBER - $quantity + 1 < (int)$from) && !$force) {
		    throw new HttpResponseException(response([
			    'error_code' => ErrorCode::CARD_CREATE_OVER_LIMIT,
			    'error'      => "Вы не можете создать столько карт, т.к. получаются номера больше " . Card::MAX_NUMBER,
		    ], 200));
	    }
	    if ($force) {
		    $numbers_array = $this->forceGenerateNumbersArray($from, $quantity);
		    $error = $numbers_array['error'];
		    $numbers = $numbers_array['numbers'];
		    if (!empty($error)) {
			    throw new HttpResponseException(response([
				    'error_code' => ErrorCode::CARD_CREATE_LESS,
				    'error'      => $error,
				    'numbers'    => $numbers,
			    ], 200));
		    }
	    } else {
		    $numbers_array = $this->generateNumbersArray($from, $quantity);
		    $errors = $numbers_array['errors'];
		    if (!empty($errors)) {
			    throw new HttpResponseException(response([
				    'error_code' => ErrorCode::CARD_CREATE_EXISTS,
				    'error'      => $errors,
			    ], 200));
		    }
		    $numbers = $numbers_array['numbers'];
	    }
	    $data = $request->all();
	    $data['user_id'] = \Auth::id();
	    $data['status'] = Card::NOT_ACTIVATED;
	    $cards = [];
	    foreach ($numbers as $number) {
		    $data['number'] = $number;
		    $discountCard = new DiscountCard($data);
		    $discountCard->saveOrFail();
            $cards[] = [
                'id'     => $discountCard->id,
                'number' => $discountCard->number,
            ];
	    }

	    return $cards;
    }

    protected function forceGenerateNumbersArray($number, $quantity) {
    	$numbers = [];
    	while (count($numbers) != $quantity && $number <= CARD::MAX_NUMBER) {
		    $number = $this->prependZeroes2Number($number);
		    if ($this->isInvalidNumber($number)) {
			    $number++;
			    continue;
		    }
		    $numbers[] = (string)$number++;
	    }

	    if (count($numbers) != $quantity) {
    		$error = 'Вы можете создать в выбранном диапазоне только ' . count($numbers) . ' ' . end_of_word(count($numbers), ['карту', 'карты', 'карт']);
	    }

	    return [
		    'numbers' => $numbers,
		    'error'   => isset($error) ? $error : null,
	    ];
    }

	/**
	 * @param string $from
	 * @param int    $quantity
	 *
	 * @return array
	 */
	protected function generateNumbersArray($from, $quantity)
    {
    	$numbers = [];
    	$errors = [];
		for ($i = 0; $i < $quantity; $i++) {
			$from = $this->prependZeroes2Number($from);
			if ($error = $this->isInvalidNumber((string)$from)) {
				$errors[] = $error;
			}
			$numbers[] = (string)$from++;
		}

		return [
			'errors'  => $errors,
			'numbers' => $numbers,
		];
    }

    protected function prependZeroes2Number($number)
    {
	    if (strlen((int)$number) < Card::NUMBER_LENGTH) {
		    $zeroes = '';
		    for ($j = 0; $j < Card::NUMBER_LENGTH - strlen($number); $j++) {
			    $zeroes .= '0';
		    }

		    $number = $zeroes . $number;
	    }

	    return $number;
    }

	/**
	 * @param string $number
	 *
	 * @return array|bool
	 */
	protected function isInvalidNumber($number)
    {
	    if (strlen($number) != Card::NUMBER_LENGTH) {
	    	$error = [
			    'number' => $number,
			    'reason' => "Номер карты должен быть девятизначным",
		    ];
	    } elseif ($card = Certificate::whereNumber($number)->first()) {
		    $error = [
			    'number' => $number,
			    'reason' => "Существует сертификат с таким номером",
		    ];
	    } elseif ($card = DiscountCard::whereNumber($number)->first()) {
		    $error = [
		    	'number' => $number,
		    	'reason' => "Существует дисконтная карта с таким номером",
		    ];
	    } else {
	    	$error = false;
	    }

	    return $error;
    }

    public function show($id)
    {
        return DiscountCard::with(['issuer', 'contractor', 'operations', 'transactions'])->findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $discountCard = DiscountCard::findOrFail($id);
        $discountCard->fill($request->all());
        $discountCard->saveOrFail();

        return $discountCard;
    }

    public function destroy($id)
    {
        DiscountCard::destroy($id);
    }

    public function activate($id)
    {
	    $discountCard = DiscountCard::findOrFail($id);
	    $discountCard->activate();

	    return $discountCard;
    }

    public function block(Request $request, $id)
    {
	    $message = $request->get('message');
	    $discountCard = DiscountCard::findOrFail($id);
	    $discountCard->block($message);

	    return $discountCard;
    }

	public function unblock(Request $request, $id)
	{
		$message = $request->get('message');
		$discountCard = DiscountCard::findOrFail($id);
		$discountCard->unblock($message);

		return $discountCard;
	}

	public function getStatuses()
    {
        $statuses = (new DiscountCard())->statuses;
        $result = [];
        foreach ($statuses as $key => $status) {
            $result[] = [
                'id'   => $key,
                'name' => $status,
            ];
        }

        return $result;
    }
}
