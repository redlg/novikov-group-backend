<?php

namespace App\Http\Controllers\CRM;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	public function me()
	{
		return \Auth::user();
	}

	public function getRolesAndPermissions()
	{
		$roles = Role::all()->map(function(Role $role) {
			return [
				'id'   => $role->id,
				'name' => $role->display_name,
			];
		});

		$permissions = Permission::all()->map(function(Permission $permission) {
			return [
				'id'   => $permission->id,
				'name' => $permission->name,
			];
		});

		return [
			'roles'       => $roles,
			'permissions' => $permissions,
		];
	}

	public function index(Request $request)
    {
        $users =  User::query();

        if ($email = $request->get('email')) {
        	$users->where('email', '~*', $email);
        }

	    if ($name = $request->get('name')) {
		    $users->where('name', '~*', $name);
	    }

	    if ($restaurant_id = $request->get('restaurant_id')) {
		    $users->where('restaurant_id', $restaurant_id);
	    }

	    if ($role_id = $request->get('role_id')) {
        	$users->whereHas('roles', function($query) use ($role_id) {
        		$query->where('id', $role_id);
	        });
	    }

	    $order_type = $request->get('sort_type') ?: 'asc';
	    $order_by = $request->get('sort_by') ?: 'id';
        $users->orderBy($order_by, $order_type);

	    $pagination = $users->paginate(
		    $request->input('per_page', 25),
		    ['*'],
		    'page',
		    $request->input('cur_page', 1)
	    );

	    return [
		    'pagination' => [
			    'total'        => $pagination->total(),
			    'current_page' => $pagination->currentPage(),
			    'from'         => $pagination->firstItem(),
			    'to'           => $pagination->lastItem(),
			    'last_page'    => $pagination->lastPage(),
		    ],
		    'items'      => $pagination->items(),
	    ];
    }

	/**
	 * @param Request $request
	 *
	 * @return User
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user = new User($data);
        $user->saveOrFail();

        $user->attachRole($data['role_id']);

        return $user;
    }

    public function show($id)
    {
	    return User::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
    	$manager = Role::whereName(Role::RESTAURANT_MANAGER)->first();
        $data = $request->all();
	    /** @var User $user */
	    $user = User::findOrFail($id);
        if (isset($data['password'])) {
	        $data['password'] = Hash::make($data['password']);
        }
	    if ($data['role_id'] != $manager->id) {
			$data['restaurant_id'] = null;
	    }
        $user->fill($data);
        $user->saveOrFail();
        $user->roles()->sync($data['role_id']);

        return $user;
    }

    public function destroy($id)
    {
	    User::destroy($id);
    }
}
