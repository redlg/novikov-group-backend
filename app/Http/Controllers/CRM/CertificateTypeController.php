<?php

namespace App\Http\Controllers\CRM;

use App\Models\CertificateType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CertificateTypeController extends Controller
{
    public function index()
    {
	    return CertificateType::all();
    }

	/**
	 * @param Request $request
	 *
	 * @return CertificateType
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $certificateType = new CertificateType($request->all());
        $certificateType->saveOrFail();

        return $certificateType;
    }

    public function show($id)
    {
        return CertificateType::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $certificateType = CertificateType::findOrFail($id);
        $certificateType->fill($request->all());
        $certificateType->saveOrFail();

        return $certificateType;
    }

    public function destroy($id)
    {
	    CertificateType::destroy($id);
    }
}
