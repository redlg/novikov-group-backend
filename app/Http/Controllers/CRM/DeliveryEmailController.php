<?php

namespace App\Http\Controllers\CRM;

use App\Models\DeliveryEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryEmailController extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @return DeliveryEmail
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $deliveryEmail = new DeliveryEmail($request->all());
        $deliveryEmail->saveOrFail();

        return $deliveryEmail;
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $deliveryEmail = DeliveryEmail::findOrFail($id);
        $deliveryEmail->fill($request->all());
        $deliveryEmail->saveOrFail();

        return $deliveryEmail;
    }

    public function destroy($id)
    {
	    DeliveryEmail::destroy($id);
    }
}
