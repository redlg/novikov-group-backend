<?php

namespace App\Http\Controllers\CRM;

use App\Jobs\ExportGuests;
use App\Models\Abstracts\ErrorCode;
use App\Models\Address;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\Guest;
use DB;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GuestController extends Controller
{

	/**
	 * @param Request $request
	 * @param bool    $to_excel
	 * @param bool    $check_quantity
	 *
	 * @return $this|array|int
	 */
	public function index(Request $request, $to_excel = false, $check_quantity = false)
    {
	    $guests = !$to_excel
		    ? Guest::with(['addresses', 'discount_cards', 'certificates'])
		    : Guest::with(['discount_cards', 'certificates']);

	    if ($phone = $request->get('query')) {
	    	$guests->where(function($query) use ($phone) {
			    $query->where('phone', '~*', $phone)
				    ->orWhereRaw("CONCAT(last_name, ' ', first_name) ~* ?", [$phone])
				    ->orWhereRaw("CONCAT(first_name, ' ', last_name) ~* ?", [$phone]);
		    });
	    }

	    if ($email = $request->get('email')) {
		    $guests->where('email', '~*', $email);
	    }

	    $last_operation_from = $request->get('last_operation_from');
	    $last_operation_to = $request->get('last_operation_to');
	    if ($last_operation_from && !$last_operation_to) {
		    $guests->whereHas('transactions', function($query) use ($last_operation_from) {
			    $query->whereDate('created_at', '>=', $last_operation_from);
		    });
	    } elseif (!$last_operation_from && $last_operation_to) {
		    $guests->whereHas('transactions', function($query) use ($last_operation_to) {
			    $query->whereDate('created_at', '<=', $last_operation_to);
		    })->whereDoesntHave('transactions', function($query) use ($last_operation_to) {
			    $query->whereDate('created_at', '>', $last_operation_to);
		    });
	    } elseif ($last_operation_from && $last_operation_to) {
		    $guests->where(function($query) use ($last_operation_from, $last_operation_to) {
			    $query->whereHas('transactions', function($subquery) use ($last_operation_from) {
				    $subquery->whereDate('created_at', '>=', $last_operation_from);
			    })->whereHas('transactions', function($subquery) use ($last_operation_to) {
				    $subquery->whereDate('created_at', '<=', $last_operation_to);
			    })->whereDoesntHave('transactions', function($subquery) use ($last_operation_to) {
				    $subquery->whereDate('created_at', '>', $last_operation_to);
			    });
		    });
	    }

	    if ($transactions_count_from = $request->get('transactions_count_from')) {
	    	$guests->has('transactions', '>=', $transactions_count_from);
	    }

	    if ($transactions_count_to = $request->get('transactions_count_to')) {
		    $guests->has('transactions', '<=', $transactions_count_to);
	    }

	    if ($total_check_from = $request->get('total_check_from')) {
		    $guests->whereHas('transactions', function($query) use ($total_check_from) {
			    $query->select(['operation_id'])
				    ->groupBy('operation_id')
				    ->havingRaw('SUM(sum) >= ?', [$total_check_from]);
		    });
	    }

	    if ($total_check_to = $request->get('total_check_to')) {
		    $guests->whereHas('transactions', function($query) use ($total_check_to) {
			    $query->select(['operation_id'])
				    ->groupBy('operation_id')
				    ->havingRaw('SUM(sum) <= ?', [$total_check_to]);
		    });
	    }

	    if ($average_check_from = $request->get('average_check_from')) {
		    $guests->whereHas('transactions', function($query) use ($average_check_from) {
			    $query->select(['operation_id'])
				    ->groupBy('operation_id')
				    ->havingRaw('AVG(sum) >= ?', [$average_check_from]);
		    });
	    }

	    if ($average_check_to = $request->get('average_check_to')) {
		    $guests->whereHas('transactions', function($query) use ($average_check_to) {
			    $query->select(['operation_id'])
				    ->groupBy('operation_id')
				    ->havingRaw('AVG(sum) <= ?', [$average_check_to]);
		    });
	    }

	    if ($birthday_at_from = $request->get('birthday_at_from')) {
		    $guests->whereDate('birthday_at', '>=', $birthday_at_from);
	    }

	    if ($birthday_at_to = $request->get('birthday_at_to')) {
		    $guests->whereDate('birthday_at', '<=', $birthday_at_to);
	    }

	    $order_type = $request->get('sort_type') ?: 'asc';
	    $order_by = $request->get('sort_by') ?: 'id';
	    $nulls = $order_type == 'asc' ? 'ASC NULLS FIRST' : 'DESC NULLS LAST';
	    if ($order_by == 'transactions_count') {
			$guests->leftJoin(
				DB::raw('(SELECT guest_id, COUNT(guest_id) AS transactions_count FROM transactions GROUP BY guest_id) as t'),
				't.guest_id', '=', 'guests.id'
			)->orderByRaw("transactions_count $nulls");
	    } elseif ($order_by == 'total_check_sum') {
		    $guests->leftJoin(
			    DB::raw('(SELECT guest_id, SUM(sum) AS total_check_sum FROM transactions GROUP BY guest_id) as t'),
			    't.guest_id', '=', 'guests.id'
		    )->orderByRaw("total_check_sum $nulls");
	    } elseif ($order_by == 'average_check_sum') {
		    $guests->leftJoin(
			    DB::raw('(SELECT guest_id, AVG(sum) AS average_check_sum FROM transactions GROUP BY guest_id) as t'),
			    't.guest_id', '=', 'guests.id'
		    )->orderByRaw("average_check_sum $nulls");
	    } elseif ($order_by == 'last_operation_date') {
		    $guests->leftJoin(
			    DB::raw('(SELECT guest_id, MAX(created_at) AS last_operation_date FROM transactions GROUP BY guest_id) as t'),
			    't.guest_id', '=', 'guests.id'
		    )->orderByRaw("last_operation_date $nulls");
	    } else {
		    $guests->orderBy($order_by, $order_type);
	    }

	    if ($check_quantity) {
		    return $guests->count(['id']);
	    } elseif ($to_excel) {
		    return $guests->get()->each(function(Guest $guest) {
			    $guest->append([
				    'last_operation_date',
				    'transactions_count',
				    'total_check_sum',
				    'average_check_sum',
			    ]);
			    $guest->discount_cards->makeHidden(['guest']);
			    $guest->certificates->makeHidden(['guest']);
		    });
	    } else {
		    $pagination = $guests->paginate(
			    $request->input('per_page', 50),
			    ['*'],
			    'page',
			    $request->input('cur_page', 1)
		    );

		    return [
			    'pagination' => [
				    'total'        => $pagination->total(),
				    'current_page' => $pagination->currentPage(),
				    'from'         => $pagination->firstItem(),
				    'to'           => $pagination->lastItem(),
				    'last_page'    => $pagination->lastPage()
			    ],
			    'items' => collect($pagination->items())->each(function(Guest $guest) {
				    $guest->append([
					    'last_operation_date',
					    'transactions_count',
					    'total_check_sum',
					    'average_check_sum',
				    ]);
				    $guest->discount_cards->makeHidden(['guest']);
				    $guest->certificates->makeHidden(['guest']);
			    }),
		    ];
	    }
    }

    public function export(Request $request)
    {
    	if ($this->index($request, false, true) > config('crm.max_entries_count_for_export')) {
		    throw new HttpResponseException(response([
			    'error_code' => ErrorCode::TOO_MANY_RECORDS_FOR_EXPORT,
			    'error'      => "Количество записей на экспорт превышает "
				    . config('crm.max_entries_count_for_export')
				    . ", пожалуйста, примените фильтры и попробуйте ещё раз.",
		    ], 200));
	    } else {
		    $request_array = $request->toArray();
		    $filename = 'guests' . time();
		    dispatch(new ExportGuests($request_array, $filename));

		    return $filename . '.xls';
	    }
    }

	/**
	 * @param Request $request
	 *
	 * @return Guest
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
    	$data = $request->all();
    	$data['phone'] = format_phone($data['phone']);
    	if (Guest::wherePhone($data['phone'])->first()) {
            throw new HttpResponseException(response([
                'error_code' => ErrorCode::GUEST_EXISTS,
                'error'      => "Гость с таким номером телефона уже существует",
            ], 200));
        }
        $guest = new Guest($data);
        $guest->saveOrFail();

        return $guest;
    }

    public function show($id)
    {
	    /** @var Guest $guest */
	    $guest = Guest::with(['addresses', 'discount_cards', 'certificates'])->findOrFail($id);
        $guest->addresses->each(function(Address $address) {
        	$address->makeVisible('main');
        });
	    $guest->discount_cards->each(function(DiscountCard $discountCard) {
		    $discountCard->makeHidden(['guest']);
	    });
	    $guest->certificates->each(function(Certificate $certificate) {
		    $certificate->makeHidden(['guest']);
	    });

        return $guest;
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
	    $data = $request->all();
	    $data['phone'] = format_phone($data['phone']);
        if (($samePhoneGuest = Guest::wherePhone($data['phone'])->first()) && $samePhoneGuest->id != $id) {
            throw new HttpResponseException(response([
                'error_code' => ErrorCode::GUEST_EXISTS,
                'error'      => "Гость с таким номером телефона уже существует",
            ], 200));
        }
        $guest = Guest::findOrFail($id);
        $guest->fill($data);
        $guest->saveOrFail();

        return $guest;
    }

    public function destroy($id)
    {
        Guest::destroy($id);
    }
}
