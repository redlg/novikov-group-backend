<?php

namespace App\Http\Controllers\CRM;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
	public function index(Request $request)
	{
		$tags = Tag::query();
		$lang = $request->get('language');

		if ($name = $request->get('name')) {
			$tags->where('name_' . $lang, '~*', $name);
		}

		$order_type = $request->get('sort_type') ?: 'asc';
		$order_by = $request->get('sort_by') ?: 'name_' . $lang;
		$tags->orderBy($order_by, $order_type);

		return $tags->get();
	}

	/**
	 * @param Request $request
	 *
	 * @return Tag
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $tag = new Tag($request->all());
        $tag->saveOrFail();

        return $tag;
    }

    public function show($id)
    {
        return Tag::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);
        $tag->fill($request->all());
        $tag->saveOrFail();

        return $tag;
    }

    public function destroy($id)
    {
        Tag::destroy($id);
    }
}
