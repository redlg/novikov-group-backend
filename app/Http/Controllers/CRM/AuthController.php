<?php

namespace App\Http\Controllers\CRM;

use App\Models\Abstracts\ErrorCode;
use App\Models\User;
use Illuminate\Http\Request;
use Hash;
use JWTAuth;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthController extends Controller
{
    public function login(Request $request)
    {
	    $credentials = $request->only('email', 'password');

	    $user = User::where('email', $credentials['email'])->first();
	    if (!$user) {
		    return [
			    'error'      => 'Пользователя с таким email не существует',
			    'error_code' => ErrorCode::AUTH_USER_NOT_FOUND,
		    ];
	    }

        if (Hash::check( $credentials['password'], $user->password) === false) {
            return [
	            'error'      => 'Неверно указан пароль',
	            'error_code' => ErrorCode::AUTH_PASSWORD_IS_INCORRECT,
            ];
        }

        return [
            'token' => JWTAuth::fromUser($user),
            'user' => $user,
        ];
    }

	public function refreshToken() {
		$token = JWTAuth::getToken();
		if (!$token) {
			throw new BadRequestHttpException('Token not provided');
		}
		try {
			$token = JWTAuth::refresh($token);
		} catch (JWTException $e) {
			return response()->json([
				'error'      => 'Токен недействителен',
				'error_code' => ErrorCode::TOKEN_INVALID,
				'logout'     => true,
			]);
		}

		return response()->json(['token' => $token]);
	}
}
