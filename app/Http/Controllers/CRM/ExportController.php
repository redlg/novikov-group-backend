<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\Controller;
use App\Models\Abstracts\ErrorCode;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Storage;
use Validator;

class ExportController extends Controller
{

	/**
	 * @param Request $request
	 *
	 * @return array
	 * @throws HttpResponseException
	 */
	public function checkFileExistence(Request $request)
	{
		$data = $request->all();
		$validator = Validator::make($data, [
			'file'       => 'required|string',
		]);

		if ($validator->fails()) {
			throw new HttpResponseException(response([
				'error_code' => ErrorCode::PARAMS_IN_REQUEST_ARE_REQUIRED,
				'error'      => $validator->errors(),
			], 200));
		}

		$path = 'exports/' . $request->get('file');

		if (Storage::exists($path)) {
			return [
				'file_url' => Storage::url($path)
			];
		} else {
			throw new HttpResponseException(response([
				'error_code' => ErrorCode::FILE_NOT_EXISTS,
				'error'      => 'Файл не найден.',
			], 200));
		}
	}
}
