<?php

namespace App\Http\Controllers\CRM;

use App\Jobs\ExportTransactions;
use App\Models\Abstracts\Card;
use App\Models\Abstracts\ErrorCode;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\OperationType;
use App\Models\PaymentMethod;
use App\Models\Transaction;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{

	/**
	 * @param Request $request
	 * @param bool    $to_excel
	 * @param bool    $check_quantity
	 *
	 * @return array|\Illuminate\Database\Eloquent\Collection|int|static[]
	 */
	public function index(Request $request, $to_excel = false, $check_quantity = false)
	{
		$transactions = Transaction::query();

		if ($guest_id = $request->get('guest_id')) {
			$transactions->where('guest_id', $guest_id);
		}

		if ($operation_type_id = $request->get('operation_type_id')) {
			$transactions->where('operation_type_id', $operation_type_id);
		}

		if ($card_input_method = $request->get('card_input_method')) {
			if ($card_input_method == Transaction::PHYSICAL_ID) {
				$transactions->where('card_input_method', Transaction::PHYSICAL_NAME);
			} elseif ($card_input_method == Transaction::VIRTUAL_ID) {
				$transactions->where('card_input_method', Transaction::VIRTUAL_NAME);
			}
		}

		if ($relation = $request->get('relation')) {
			if ($relation == Card::RELATION_DISCOUNT_CARD_ID) {
				$model = DiscountCard::class;
			} elseif ($relation == Card::RELATION_CERTIFICATE_ID) {
				$model = Certificate::class;
			}

			if (isset($model)) {
				$transactions->where('card_type', $model);

				if ($card_type_id = $request->get('card_type_id')) {
					$transactions->whereHas($relation, function($query) use ($card_type_id) {
						$query->where('card_type_id', $card_type_id);
					});
				}
			}
		}

		if ($operation_id = $request->get('operation_id')) {
			$transactions->where('operation_id', $operation_id);
		}

		if ($cashbox_number = $request->get('cashbox_number')) {
			$transactions->where('cashbox_number', '~*', $cashbox_number);
		}

		if ($check_number = $request->get('check_number')) {
			$transactions->where('check_number', '~*', $check_number);
		}

		if ($restaurant_id = $request->get('restaurant_id')) {
			$transactions->where('restaurant_id', $restaurant_id);
		}

		if ($sum_from = $request->get('sum_from')) {
			$transactions->where('sum', '>=', $sum_from);
		}

		if ($sum_to = $request->get('sum_to')) {
			$transactions->where('sum', '<=', $sum_to);
		}

		if ($discount_sum_from = $request->get('discount_sum_from')) {
			$transactions->where('discount_sum', '>=', $discount_sum_from);
		}

		if ($discount_sum_to = $request->get('discount_sum_to')) {
			$transactions->where('discount_sum', '<=', $discount_sum_to);
		}

		if ($created_at_from = $request->get('created_at_from')) {
			$transactions->whereDate('created_at', '>=', $created_at_from);
		}

		if ($created_at_to = $request->get('created_at_to')) {
			$transactions->whereDate('created_at', '<=', $created_at_to);
		}

		if ($payment_method_id = $request->get('payment_method_id')) {
			$transactions->where('payment_method_id', $payment_method_id);
		}

		$order_type = $request->get('sort_type') ?: 'asc';
		$order_by = $request->get('sort_by') ?: 'id';
		if (str_contains($order_by, '.')) {
			$split = explode('.', $order_by);
			$transactions->addField($order_by);
			$transactions->orderBy($split[0] . "_" . $split[1], $order_type);
		} else {
			$transactions->orderBy($order_by, $order_type);
		}

		if ($check_quantity) {
			return $transactions->count(['id']);
		} elseif ($to_excel) {
			return $transactions->get();
		} else {
			$pagination = $transactions->paginate(
				$request->input('per_page', 50),
				['*'],
				'page',
				$request->input('cur_page', 1)
			);

			return [
				'pagination' => [
					'total'        => $pagination->total(),
					'current_page' => $pagination->currentPage(),
					'from'         => $pagination->firstItem(),
					'to'           => $pagination->lastItem(),
					'last_page'    => $pagination->lastPage()
				],
				'items' => $pagination->items(),
			];
		}
	}

	public function export(Request $request)
	{
		if ($this->index($request, false, true) > config('crm.max_entries_count_for_export')) {
			throw new HttpResponseException(response([
				'error_code' => ErrorCode::TOO_MANY_RECORDS_FOR_EXPORT,
				'error'      => "Количество записей на экспорт превышает "
					. config('crm.max_entries_count_for_export')
					. ", пожалуйста, примените фильтры и попробуйте ещё раз.",
			], 200));
		} else {
			$request_array = $request->toArray();
			$filename = 'transactions' . time();
			dispatch(new ExportTransactions($request_array, $filename));

			return $filename . '.xls';
		}
	}

    public function show($id)
    {
        return Transaction::findOrFail($id);
    }

	/**
	 * @param Request $request
	 *
	 * @return Transaction
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
	    $data = $request->all();
	    if (isset($data['card_type'])) {
		    if ($data['card_type'] == Card::RELATION_CERTIFICATE_ID) {
			    $data['card_type'] = Certificate::class;
		    } elseif ($data['card_type'] == CARD::RELATION_DISCOUNT_CARD_ID) {
			    $data['card_type'] = DiscountCard::class;
		    }
	    }
	    if (isset($data['card_input_method'])) {
		    if ($data['card_input_method'] == Transaction::PHYSICAL_ID) {
			    $data['card_input_method'] = Transaction::PHYSICAL_NAME;
		    } elseif ($data['card_input_method'] == Transaction::VIRTUAL_ID) {
			    $data['card_input_method'] = Transaction::VIRTUAL_NAME;
		    }
	    }
	    $transaction = new Transaction($data);
	    $transaction->saveOrFail();

	    return $transaction;
    }

	public function getFilters()
	{
		$types =  OperationType::all();
		$transactions = $types->where('type', OperationType::TRANSACTION)->values();
		$payment_methods = PaymentMethod::all()->map(function($item) {
			return [
				'id'   => $item->id,
				'name' => $item->name_ru,
			];
		});

		return [
			'transaction_types'  => $transactions,
			'payment_methods'    => $payment_methods,
			'card_input_methods' => Transaction::CARD_INPUT_METHODS,
		];
	}
}
