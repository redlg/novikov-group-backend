<?php

namespace App\Http\Controllers\CRM;

use App\Models\Image;
use Illuminate\Http\Request;
use Storage;
use App\Http\Controllers\Controller;


class ImageController extends Controller
{

	/**
	 * @param Request $request
	 *
	 * @return Image
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $file = $request->file('file');
        $type = empty($request->get('type')) ? Image::PHOTO : $request->get('type');
        $path = Storage::put('images', $file);

        $image = new Image();
        $image->name = $file->getClientOriginalName();
        $image->path = $path;
        $image->type = $type;
        $image->saveOrFail();

        return $image;
    }

    public function getFilters()
    {
	    $types = [
	    	[
			    'id'   => Image::LOGO,
			    'name' => 'Лого',
		    ],
		    [
			    'id'   => Image::PHOTO,
			    'name' => 'Фото',
		    ],
	    ];

	    return [
		    'image_types' => $types,
	    ];
    }
}
