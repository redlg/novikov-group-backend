<?php

namespace App\Http\Controllers\CRM;

use App\Jobs\ExportCertificates;
use App\Models\Abstracts\Card;
use App\Models\Abstracts\ErrorCode;
use App\Models\Certificate;
use App\Models\CertificateType;
use App\Models\DiscountCard;
use App\Models\Guest;
use Carbon\Carbon;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CertificateController extends Controller
{

	/**
	 * @param Request $request
	 * @param bool    $to_excel
	 * @param bool    $check_quantity
	 *
	 * @return array|\Illuminate\Database\Eloquent\Collection|int|static[]
	 */
	public function index(Request $request, $to_excel = false, $check_quantity = false)
    {
	    $certificates = Certificate::with(['issuer', 'contractor']);;

        if ($number = $request->get('query')) {
            $certificates->where(function($query) use($number) {
                $query->where('number', '~*', $number)
                    ->orWhereHas('guest', function($subquery) use ($number) {
                        $subquery->whereRaw("CONCAT(last_name, ' ', first_name) ~* ?", [$number])
                            ->orWhereRaw("CONCAT(first_name, ' ', last_name) ~* ?", [$number]);
                    });
            });
        }

	    if ($status = $request->get('status')) {
		    $certificates->where('status', '=', $status);
	    }

        if ($type = $request->get('type')) {
            $certificates->where('card_type_id', '=', $type);
        }

	    if ($contractor_id = $request->get('contractor_id')) {
		    $certificates->where('contractor_id', '=', $contractor_id);
	    }

	    if ($issuer_id = $request->get('issuer_id')) {
		    $certificates->where('issuer_id', '=', $issuer_id);
	    }

	    if ($restaurant_id = $request->get('restaurant_id')) {
		    $certificates->where('restaurant_id', '=', $restaurant_id);
	    }

        if ($created_at_from = $request->get('created_at_from')) {
            $certificates->whereDate('created_at', '>=', $created_at_from);
        }

        if ($created_at_to = $request->get('created_at_to')) {
            $certificates->whereDate('created_at', '<=', $created_at_to);
        }

        if ($started_at_from = $request->get('started_at_from')) {
            $certificates->whereDate('started_at', '>=', $started_at_from);
        }

        if ($started_at_to = $request->get('started_at_to')) {
            $certificates->whereDate('started_at', '<=', $started_at_to);
        }

        if ($ended_at_from = $request->get('ended_at_from')) {
            $certificates->whereDate('ended_at', '>=', $ended_at_from);
        }

        if ($ended_at_to = $request->get('ended_at_to')) {
            $certificates->whereDate('ended_at', '<=', $ended_at_to);
        }

        if ($blocked_at_from = $request->get('blocked_at_from')) {
            $certificates->whereDate('blocked_at', '>=', $blocked_at_from);
        }

        if ($blocked_at_to = $request->get('blocked_at_to')) {
            $certificates->whereDate('blocked_at', '<=', $blocked_at_to);
        }

        $order_type = $request->get('sort_type') ?: 'asc';
        $order_by = $request->get('sort_by') ?: 'id';
        if (str_contains($order_by, '.')) {
            $split = explode('.', $order_by);
            $certificates->addField($order_by);
            $certificates->orderBy($split[0] . "_" . $split[1], $order_type);
        } else {
            $certificates->orderBy($order_by, $order_type);
        }

	    if ($check_quantity) {
		    return $certificates->count(['id']);
	    } elseif ($to_excel) {
		    return $certificates->get();
	    } else {
		    $pagination = $certificates->paginate(
			    $request->input('per_page', 50),
			    ['*'],
			    'page',
			    $request->input('cur_page', 1)
		    );

		    return [
			    'pagination' => [
				    'total'        => $pagination->total(),
				    'current_page' => $pagination->currentPage(),
				    'from'         => $pagination->firstItem(),
				    'to'           => $pagination->lastItem(),
				    'last_page'    => $pagination->lastPage()
			    ],
			    'items'      => $pagination->items(),
		    ];
	    }
    }

	public function export(Request $request)
	{
		if ($this->index($request, false, true) > config('crm.max_entries_count_for_export')) {
			throw new HttpResponseException(response([
				'error_code' => ErrorCode::TOO_MANY_RECORDS_FOR_EXPORT,
				'error'      => "Количество записей на экспорт превышает "
					. config('crm.max_entries_count_for_export')
					. ", пожалуйста, примените фильтры и попробуйте ещё раз.",
			], 200));
		} else {
			$request_array = $request->toArray();
			$filename = 'certificates' . time();
			dispatch(new ExportCertificates($request_array, $filename));

			return $filename . '.xls';
		}
	}

	/**
	 * @param Request $request
	 *
	 * @return Certificate
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
	    $data = $request->all();
	    if ($error = $this->isInvalidNumber($data['number'])) {
		    throw new HttpResponseException(response([
			    'error_code' => ErrorCode::CARD_CREATE_VALIDATION,
			    'error'      => $error,
		    ], 200));
	    }
	    if (!isset($data['guest_id']) || empty($data['guest_id'])) {
		    $guest = new Guest();
		    $guest->phone = $data['phone'];
		    $guest->last_name = $data['last_name'];
		    $guest->first_name = $data['first_name'];
		    $guest->save();
		    $data['guest_id'] = $guest->id;
	    }
	    $certificateType = CertificateType::whereId($data['card_type_id'])->first();
	    $data['user_id'] = \Auth::id();
	    $data['status'] = Card::ACTIVATED_IN_OFFICE;
	    if ($certificateType->name != CertificateType::EXCLUSIVE) {
		    $data['balance'] = $certificateType->sum;
	    }
	    if (!isset($data['started_at']) || empty($data['started_at'])) {
		    $data['started_at'] = Carbon::now();
		    $data['ended_at'] = Carbon::now()->addDays($certificateType->life_time);
	    } else {
            $data['started_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $data['started_at']);
		    $ended_at = new Carbon($data['started_at']);
		    $data['ended_at'] = $ended_at->addDays($certificateType->life_time);
	    }
	    $certificate = new Certificate($data);
	    $certificate->saveOrFail();

	    return $certificate;
    }

	/**
	 * @param Request $request
	 *
	 * @return array
	 * @throws \Throwable
	 */
	public function bulkStore(Request $request)
    {
        $numbers = $request->get('numbers', []);
        $errors = [];
        foreach ($numbers as $number) {
            if ($error = $this->isInvalidNumber($number)) {
                $errors[] = $error;
            }
        }
        if (!empty($errors)) {
            throw new HttpResponseException(response([
                'error_code' => ErrorCode::CARD_CREATE_EXISTS,
                'error'      => $errors,
            ], 200));
        } else {
            $data = $request->all();
            $data['user_id'] = \Auth::id();
            $data['status'] = Card::NOT_ACTIVATED;
            $certificates = [];
            foreach ($numbers as $number) {
                $data['number'] = $number;
                $certificate = new Certificate($data);
                $certificate->saveOrFail();
                $certificates[] = [
                    'id'     => $certificate->id,
                    'number' => $certificate->number,
                ];
            }

            return $certificates;
        }
    }

	/**
	 * @param Request $request
	 *
	 * @return array
	 * @throws \Throwable
	 */
	public function massStore(Request $request)
    {
        $from = $request->get('from_number');
        $quantity = $request->get('count');
        $force = $request->get('force', false);
        if ((Card::MAX_NUMBER - $quantity + 1 < (int)$from) && !$force) {
            throw new HttpResponseException(response([
                'error_code' => ErrorCode::CARD_CREATE_OVER_LIMIT,
                'error'      => "Вы не можете создать столько сертификатов, т.к. получаются номера больше " . Card::MAX_NUMBER,
            ], 200));
        }
        if ($force) {
            $numbers_array = $this->forceGenerateNumbersArray($from, $quantity);
            $error = $numbers_array['error'];
            $numbers = $numbers_array['numbers'];
            if (!empty($error)) {
                throw new HttpResponseException(response([
                    'error_code' => ErrorCode::CARD_CREATE_LESS,
                    'error'      => $error,
                    'numbers'    => $numbers,
                ], 200));
            }
        } else {
            $numbers_array = $this->generateNumbersArray($from, $quantity);
            $errors = $numbers_array['errors'];
            if (!empty($errors)) {
                throw new HttpResponseException(response([
                    'error_code' => ErrorCode::CARD_CREATE_EXISTS,
                    'error'      => $errors,
                ], 200));
            }
            $numbers = $numbers_array['numbers'];
        }
        $data = $request->all();
        $data['user_id'] = \Auth::id();
        $data['status'] = Card::NOT_ACTIVATED;
        $certificates = [];
        foreach ($numbers as $number) {
            $data['number'] = $number;
            $certificate = new Certificate($data);
            $certificate->saveOrFail();
            $certificates[] = [
                'id'     => $certificate->id,
                'number' => $certificate->number,
            ];
        }

        return $certificates;
    }

    protected function forceGenerateNumbersArray($number, $quantity) {
        $numbers = [];
        while (count($numbers) != $quantity && $number <= CARD::MAX_NUMBER) {
            $number = $this->prependZeroes2Number($number);
            if ($this->isInvalidNumber($number)) {
                $number++;
                continue;
            }
            $numbers[] = (string)$number++;
        }

        if (count($numbers) != $quantity) {
            $error = 'Вы можете создать в выбранном диапазоне только ' . count($numbers) . ' ' . end_of_word(count($numbers), ['сертификат', 'сертификата', 'сертификатов']);
        }

        return [
            'numbers' => $numbers,
            'error'   => isset($error) ? $error : null,
        ];
    }

    /**
     * @param string $from
     * @param int    $quantity
     *
     * @return array
     */
    protected function generateNumbersArray($from, $quantity)
    {
        $numbers = [];
        $errors = [];
        for ($i = 0; $i < $quantity; $i++) {
            $from = $this->prependZeroes2Number($from);
            if ($error = $this->isInvalidNumber((string)$from)) {
                $errors[] = $error;
            }
            $numbers[] = (string)$from++;
        }

        return [
            'errors'  => $errors,
            'numbers' => $numbers,
        ];
    }

    protected function prependZeroes2Number($number)
    {
        if (strlen((int)$number) < Card::NUMBER_LENGTH) {
            $zeroes = '';
            for ($j = 0; $j < Card::NUMBER_LENGTH - strlen($number); $j++) {
                $zeroes .= '0';
            }

            $number = $zeroes . $number;
        }

        return $number;
    }

    /**
     * @param string $number
     *
     * @return array|bool
     */
    protected function isInvalidNumber($number)
    {
        if (strlen($number) != Card::NUMBER_LENGTH) {
            $error = [
                'number' => $number,
                'reason' => "Номер карты должен быть девятизначным",
            ];
        } elseif ($card = Certificate::whereNumber($number)->first()) {
            $error = [
                'number' => $number,
                'reason' => "Существует сертификат с таким номером",
            ];
        } elseif ($card = DiscountCard::whereNumber($number)->first()) {
            $error = [
                'number' => $number,
                'reason' => "Существует дисконтная карта с таким номером",
            ];
        } else {
            $error = false;
        }

        return $error;
    }

	/**
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 */
	public function show($id)
    {
        return Certificate::with(['issuer', 'contractor', 'operations', 'transactions'])->findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $certificate = Certificate::findOrFail($id);
        $certificate->fill($request->all());
        $certificate->saveOrFail();

        return $certificate;
    }

    public function destroy($id)
    {
        Certificate::destroy($id);
    }

	public function activate($id)
	{
		$certificate = Certificate::findOrFail($id);
		$certificate->activate();

		return $certificate;
	}

	public function block(Request $request, $id)
	{
		$message = $request->get('message');
		$certificate = Certificate::findOrFail($id);
		$certificate->block($message);

		return $certificate;
	}

	public function unblock(Request $request, $id)
	{
		$message = $request->get('message');
		$certificate = Certificate::findOrFail($id);
		$certificate->unblock($message);

		return $certificate;
	}

    public function getStatuses()
    {
        $statuses = (new Certificate())->statuses;
        $result = [];
        foreach ($statuses as $key => $status) {
            $result[] = [
                'id'   => $key,
                'name' => $status,
            ];
        }

        return $result;
    }
}
