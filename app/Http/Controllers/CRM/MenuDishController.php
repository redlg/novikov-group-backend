<?php

namespace App\Http\Controllers\CRM;

use App\Models\MenuDish;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuDishController extends Controller
{
    public function index(Request $request)
    {
    	$dishes = MenuDish::with(['menu_category']);
	    $lang = $request->get('language');

    	if ($name = $request->get('name')) {
    		$dishes->where('name_' . $lang, '~*', $name);
	    }

    	if ($category_id = $request->get('category_id')) {
    		$dishes = $dishes->where('menu_category_id', $category_id);
	    }

	    if ($restaurant_id = $request->get('restaurant_id')) {
    		$dishes = $dishes->whereHas('menu_category', function($query) use ($restaurant_id) {
    			$query->where('restaurant_id', $restaurant_id);
		    });
	    }

	    return $dishes->get()->makeHidden(['menu_category']);
    }

    public function show($id)
    {
        return MenuDish::with(['menu_category'])->findOrFail($id);
    }

	/**
	 * @param Request $request
	 *
	 * @return MenuDish
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
	    $dish = new MenuDish($request->all());
	    $dish->saveOrFail();

	    return $dish;
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
	{
		$dish = MenuDish::findOrFail($id);
		$dish->fill($request->all());
		$dish->saveOrFail();

		return $dish;
	}

    public function destroy($id)
    {
	    MenuDish::destroy($id);
    }
}
