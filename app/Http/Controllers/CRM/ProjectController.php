<?php

namespace App\Http\Controllers\CRM;

use App\Models\Image;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
	    $projects = Project::query();
	    $lang = $request->get('language');

	    if ($name = $request->get('name')) {
		    $projects->where('name_' . $lang, '~*', $name);
	    }

	    $order_type = $request->get('sort_type') ?: 'asc';
	    $order_by = $request->get('sort_by') ?: 'name_' . $lang;
	    $projects->orderBy($order_by, $order_type);

	    $pagination = $projects->paginate(
		    $request->input('per_page', 25),
		    ['*'],
		    'page',
		    $request->input('cur_page', 1)
	    );

	    return [
		    'pagination' => [
			    'total'        => $pagination->total(),
			    'current_page' => $pagination->currentPage(),
			    'from'         => $pagination->firstItem(),
			    'to'           => $pagination->lastItem(),
			    'last_page'    => $pagination->lastPage(),
		    ],
		    'items'      => $pagination->items(),
	    ];
    }

	/**
	 * @param Request $request
	 *
	 * @return Project
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $data = $request->all();
        $project = new Project($data);
        $project->saveOrFail();

        if ($logo_id = $request->get('logo_id')) {
            Image::whereId($logo_id)->first()->confirm();
        }

        if (isset($data['image_ids']) && !empty($data['image_ids'])) {
		    $project->images()->attach($data['image_ids']);
		    Image::query()->whereIn('id', $data['image_ids'])->get()->each(function(Image $image) {
			    $image->confirm();
		    });
	    }

	    if (isset($data['partner_ids']) && !empty($data['partner_ids'])) {
		    $project->partners()->attach($data['partner_ids']);
	    }

	    if (isset($data['event_ids']) && !empty($data['event_ids'])) {
		    $project->events()->attach($data['event_ids']);
	    }

        return $project;
    }

    public function show($id)
    {
        return Project::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $data = $request->all();
	    /** @var Project $project */
	    $project = Project::with(['images'])->findOrFail($id);
        if (($logo_id = $request->get('logo_id')) && ($logo_id != $project->logo_id)) {
            Image::whereId($logo_id)->first()->confirm();
            Image::whereId($project->logo_id)->first()->refuse();
        }
        $project->fill($data);
        $project->saveOrFail();

	    $partner_ids = $request->get('partner_ids');
	    if (is_array($partner_ids)) {
		    $project->partners()->sync($partner_ids);
	    }

	    $event_ids = $request->get('event_ids');
	    if (is_array($event_ids)) {
		    $project->events()->sync($event_ids);
	    }

	    $image_ids = $request->get('image_ids');
	    if (is_array($image_ids)) {
		    $restaurant_images = $project->images->pluck('id');
		    $to_delete = $restaurant_images->diff($image_ids)->toArray();
		    $to_add = collect($image_ids)->diff($restaurant_images)->toArray();
		    $project->images()->sync($image_ids);
		    if (!empty($to_delete)) {
			    Image::whereIn('id', $to_delete)->get()->each(function(Image $image) {
				    $image->refuse();
			    });
		    }
		    if (!empty($to_add)) {
			    Image::whereIn('id', $to_add)->get()->each(function(Image $image) {
				    $image->confirm();
			    });
		    }
	    }

        return $project;
    }

    public function destroy($id)
    {
        Project::destroy($id);
    }
}