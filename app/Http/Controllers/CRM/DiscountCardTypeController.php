<?php

namespace App\Http\Controllers\CRM;

use App\Models\DiscountCardType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiscountCardTypeController extends Controller
{
    public function index()
    {
	    return DiscountCardType::all();
    }

	/**
	 * @param Request $request
	 *
	 * @return DiscountCardType
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $discountCardType = new DiscountCardType($request->all());
        $discountCardType->saveOrFail();

        return $discountCardType;
    }

    public function show($id)
    {
        return DiscountCardType::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $discountCardType = DiscountCardType::findOrFail($id);
        $discountCardType->fill($request->all());
        $discountCardType->saveOrFail();

        return $discountCardType;
    }

    public function destroy($id)
    {
	    DiscountCardType::destroy($id);
    }
}
