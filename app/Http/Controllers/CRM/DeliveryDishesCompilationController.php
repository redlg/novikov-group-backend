<?php

namespace App\Http\Controllers\CRM;

use App\Models\DeliveryDishesCompilation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryDishesCompilationController extends Controller
{
    public function index(Request $request)
    {
	    $compilations = DeliveryDishesCompilation::query();

	    if ($name = $request->get('name')) {
		    $compilations->where('name', '~*', $name);
	    }

	    if ($restaurant_id = $request->get('restaurant_id')) {
		    $compilations->where('restaurant_id', '=', $restaurant_id);
	    }

	    if ($tag_id = $request->get('tag_id')) {
		    $compilations->whereHas('tags', function($query) use ($tag_id) {
			    $query->where('tags.id', '=', $tag_id);
		    });
	    }

	    $order_type = $request->get('sort_type') ?: 'asc';
	    $order_by = $request->get('sort_by') ?: 'id';
	    if (str_contains($order_by, '.')) {
		    $split = explode('.', $order_by);
		    $compilations->addField($order_by);
		    $compilations->orderBy($split[0] . "_" . $split[1], $order_type);
	    } else {
		    $compilations->orderBy($order_by, $order_type);
	    }

	    $pagination = $compilations->paginate(
		    $request->input('per_page', 25),
		    ['*'],
		    'page',
		    $request->input('cur_page', 1)
	    );

	    return [
		    'pagination' => [
			    'total'        => $pagination->total(),
			    'current_page' => $pagination->currentPage(),
			    'from'         => $pagination->firstItem(),
			    'to'           => $pagination->lastItem(),
			    'last_page'    => $pagination->lastPage(),
		    ],
		    'items'      => $pagination->items(),
	    ];
    }

	public function show($id)
	{
		return DeliveryDishesCompilation::whereId($id)->first();
	}

	/**
	 * @param Request $request
	 *
	 * @return DeliveryDishesCompilation
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $data = $request->all();
        $compilation = new DeliveryDishesCompilation($data);
        $compilation->saveOrFail();

	    if ($tag_ids = $request->get('tag_ids')) {
		    $compilation->tags()->attach($tag_ids);
	    }

	    return $compilation->fresh();
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $data = $request->all();
        $compilation = DeliveryDishesCompilation::findOrFail($id);
        $compilation->fill($data);
        $compilation->saveOrFail();

	    $tag_ids = $request->get('tag_ids');
	    if ($tag_ids || is_array($tag_ids)) {
		    $compilation->tags()->sync($tag_ids);
	    }

	    return $compilation->refresh();
    }

    public function destroy($id)
    {
	    DeliveryDishesCompilation::destroy($id);
    }
}
