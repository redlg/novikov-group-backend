<?php

namespace App\Http\Controllers\CRM;

use App\Models\TicketType;
use App\Services\TicketService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketTypeController extends Controller
{
	protected $ticketService;

	public function __construct(TicketService $ticketService)
	{
		$this->ticketService = $ticketService;
	}

	public function index()
    {
        return TicketType::all();
    }

	/**
	 * @param Request $request
	 *
	 * @return TicketType
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $data = $request->all();
        $ticketType = new TicketType($data);
        $ticketType->saveOrFail();

        return $ticketType;
    }

    public function show($id)
    {
	    /** @var TicketType $ticketType */
	    $ticketType = TicketType::findOrFail($id);
	    if (!is_null($ticketType->external_id)) {
		    $ticketType = $this->ticketService->updateAvailableCount($ticketType);
	    }

        return $ticketType;
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $data = $request->all();
        $ticketType = TicketType::findOrFail($id);
        $ticketType->fill($data);
        $ticketType->saveOrFail();

        return $ticketType;
    }

    public function destroy($id)
    {
        TicketType::destroy($id);
    }
}
