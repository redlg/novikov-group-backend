<?php

namespace App\Http\Controllers\CRM;

use App\Models\TicketTypeOrder;
use App\Services\TicketService;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TicketTypeOrderController extends Controller
{
	protected $ticketService;

	public function __construct(TicketService $ticketService)
	{
		$this->ticketService = $ticketService;
	}

	public function index()
    {
        return TicketTypeOrder::all();
    }

    public function show($id)
    {
        return TicketTypeOrder::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $data = $request->all();
        $ticket = TicketTypeOrder::findOrFail($id);
        $ticket->fill($data);
        $ticket->saveOrFail();

        return $ticket;
    }

    public function destroy($id)
    {
        TicketTypeOrder::destroy($id);
    }

    public function invoiceSuccess(Request $request)
    {
	    $validator = Validator::make($request->all(), [
		    'key'        => 'required',
		    'invoice_id' => 'required',
	    ]);

	    if ($validator->fails()) {
		    throw new HttpResponseException(response(['error' => "You don't have key or invoice_id parameters in your request"], 403));
	    }

	    $key = $request->get('key');
	    $novikov_key = config('crm.novikov_key');

	    if ($key != $novikov_key) {
		    throw new HttpResponseException(response(['error' => "The key doesn't match"], 403));
	    }

	    $invoice_id = $request->get('invoice_id');

	    return $this->ticketService->success($invoice_id);
    }
}
