<?php

namespace App\Http\Controllers\CRM;

use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventType;
use App\Http\Controllers\Controller;


class EventController extends Controller
{
    public function index(Request $request)
    {
    	$events = Event::query();
	    $lang = $request->get('language');

	    if ($name = $request->get('name')) {
		    $events->where('name_' . $lang, '~*', $name);
	    }

        if ($type = $request->get('type')) {
	        $events->where('event_type_id', '=', $type);
        }

	    if ($started_at_from = $request->get('started_at_from')) {
		    $events->whereDate('started_at', '>=', $started_at_from);
	    }

	    if ($started_at_to = $request->get('started_at_to')) {
		    $events->whereDate('started_at', '<=', $started_at_to);
	    }

	    if ($ended_at_from = $request->get('ended_at_from')) {
		    $events->whereDate('ended_at', '>=', $ended_at_from);
	    }

	    if ($ended_at_to = $request->get('ended_at_to')) {
		    $events->whereDate('ended_at', '<=', $ended_at_to);
	    }

	    $order_type = $request->get('sort_type') ?: 'desc';
	    $order_by = $request->get('sort_by') ?: 'id';
	    $events->orderBy($order_by, $order_type);

        $pagination = $events->paginate(
            $request->input('per_page', 25),
            ['*'],
            'page',
            $request->input('cur_page', 1)
        );

        return [
	        'pagination' => [
		        'total'        => $pagination->total(),
		        'current_page' => $pagination->currentPage(),
		        'from'         => $pagination->firstItem(),
		        'to'           => $pagination->lastItem(),
		        'last_page'    => $pagination->lastPage(),
	        ],
	        'items'      => $pagination->items(),
        ];
    }

    public function getFilters()
    {
        return [
            'types' => EventType::query()->select(['id', 'name_ru'])->get()->map(function($item) {
	            return [
		            'id'   => $item->id,
		            'name' => $item->name_ru,
	            ];
            }),
        ];
    }

	/**
	 * @param Request $request
	 *
	 * @return Event
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $data = $request->all();
        $data['started_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $data['started_at']);
        $data['ended_at'] = isset($data['ended_at']) ? Carbon::createFromFormat('Y-m-d H:i:s', $data['ended_at']): null;
        $event = new Event($data);
        $event->saveOrFail();

        if ($logo_id = $request->get('logo_id')) {
            Image::whereId($logo_id)->first()->confirm();
        }

	    if ($image_ids = $request->get('image_ids')) {
		    $event->images()->attach($image_ids);
		    Image::query()->whereIn('id', $image_ids)->get()->each(function(Image $image) {
			    $image->confirm();
		    });
	    }

	    if ($tag_ids = $request->get('tag_ids')) {
		    $event->tags()->attach($tag_ids);
	    }

	    if ($restaurant_ids = $request->get('restaurant_ids')) {
		    $event->restaurants()->attach($tag_ids);
	    }

	    if ($partners_ids = $request->get('partners_ids')) {
		    $event->partners()->attach($partners_ids);
	    }

	    if ($project_ids = $request->get('project_ids')) {
		    $event->projects()->attach($project_ids);
	    }

        return $event;
    }

    public function show($id)
    {
        return Event::with(['images', 'ticket_types', 'restaurants', 'partners', 'projects'])
	        ->findOrFail($id)
	        ->append([
	        	'restaurant_ids',
		        'addresses',
	        ])
	        ->makeHidden(['restaurants']);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $data = $request->all();
	    /** @var Event $event */
	    $event = Event::with(['images'])->findOrFail($id);
        if (($logo_id = $request->get('logo_id')) && ($logo_id != $event->logo_id)) {
            Image::whereId($logo_id)->first()->confirm();
            Image::whereId($event->logo_id)->first()->refuse();
        }
        $data['started_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $data['started_at']);
        $data['ended_at'] = isset($data['ended_at']) ? Carbon::createFromFormat('Y-m-d H:i:s', $data['ended_at']): null;
        $event->fill($data);
        $event->saveOrFail();

	    $image_ids = $request->get('image_ids');
	    if (is_array($image_ids)) {
		    $event_images = $event->images->pluck('id');
		    $to_delete = $event_images->diff($image_ids)->toArray();
		    $to_add = collect($image_ids)->diff($event_images)->toArray();
		    $event->images()->sync($image_ids);
		    if (!empty($to_delete)) {
			    Image::query()->whereIn('id', $to_delete)->get()->each(function(Image $image) {
				    $image->refuse();
			    });
		    }
		    if (!empty($to_add)) {
			    Image::query()->whereIn('id', $to_add)->get()->each(function(Image $image) {
				    $image->confirm();
			    });
		    }
	    }

	    $tag_ids = $request->get('tag_ids');
	    if (is_array($tag_ids)) {
		    $event->tags()->sync($tag_ids);
	    }

	    $restaurant_ids = $request->get('restaurant_ids');
	    if (is_array($restaurant_ids)) {
		    $event->restaurants()->sync($restaurant_ids);
	    }

        return $event->refresh();
    }

    /**
     * @param int $id
     */
    public function destroy($id)
    {
        Event::destroy($id);
    }
}
