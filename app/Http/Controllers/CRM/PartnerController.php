<?php

namespace App\Http\Controllers\CRM;

use App\Models\Address;
use App\Models\Image;
use App\Models\LegalEntity;
use App\Models\LegalEntityType;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
    public function index(Request $request)
    {
    	$partners = Partner::with(['legal_entity']);
	    $lang = $request->get('language');

	    if ($name = $request->get('query')) {
		    $partners->where('name_' . $lang, '~*', $name)
			    ->orWhereHas('legal_entity', function($query) use ($name) {
				    $query->where('name', '~*', $name);
			    });
	    }

	    if ($phone = $request->get('phone')) {
	    	$partners->where('phone', '~*', $phone);
	    }

	    if ($discount_from = $request->get('discount_from')) {
	    	$partners->where('discount', '>=', $discount_from);
	    }

	    if ($discount_to = $request->get('discount_to')) {
		    $partners->where('discount', '<=', $discount_to);
	    }

	    $order_type = $request->get('sort_type') ?: 'asc';
	    $order_by = $request->get('sort_by') ?: 'name_' . $lang;
	    if (str_contains($order_by, '.')) {
		    $split = explode('.', $order_by);
		    $partners->addField($order_by);
		    $partners->orderBy($split[0] . "_" . $split[1], $order_type);
	    } else {
		    $partners->orderBy($order_by, $order_type);
	    }

	    $pagination = $partners->paginate(
		    $request->input('per_page', 25),
		    ['*'],
		    'page',
		    $request->input('cur_page', 1)
	    );

	    return [
		    'pagination' => [
			    'total'        => $pagination->total(),
			    'current_page' => $pagination->currentPage(),
			    'from'         => $pagination->firstItem(),
			    'to'           => $pagination->lastItem(),
			    'last_page'    => $pagination->lastPage(),
		    ],
		    'items'      => $pagination->items(),
	    ];
    }

	/**
	 * @param Request $request
	 *
	 * @return Partner
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
    	$legal_entity_type = LegalEntityType::whereName(LegalEntityType::PARTNER)->first();
    	$legal_entity_request = $request->get('legal_entity', []);
    	$legal_entity_request['legal_entity_type_id'] = $legal_entity_type->id;
	    $address_request = $request->get('address', []);
	    $address_request['addressable_type'] = Partner::class;
	    $partner_request = $request->get('partner', []);

	    $legal_entity = new LegalEntity();
	    $legal_entity->fill($legal_entity_request);
	    $legal_entity->saveOrFail();
	    $partner_request['legal_entity_id'] = $legal_entity->id;

	    $partner = new Partner();
	    $partner->fill($partner_request);
	    $partner->saveOrFail();
        if (!is_null($partner->logo_id)) {
            Image::whereId($partner->logo_id)->first()->confirm();
        }
	    $address_request['addressable_id'] = $partner->id;

	    $address = new Address();
	    $address->fill($address_request);
	    $address->saveOrFail();

	    if (isset($partner_request['image_ids']) && !empty($partner_request['image_ids'])) {
		    $partner->images()->attach($partner_request['image_ids']);
		    Image::query()->whereIn('id', $partner_request['image_ids'])->get()->each(function(Image $image) {
			    $image->confirm();
		    });
	    }

	    if (isset($partner_request['tag_ids']) && !empty($partner_request['tag_ids'])) {
		    $partner->tags()->attach($partner_request['tag_ids']);
	    }

	    if (isset($partner_request['project_ids']) && !empty($partner_request['project_ids'])) {
		    $partner->projects()->attach($partner_request['project_ids']);
	    }

	    if (isset($partner_request['event_ids']) && !empty($partner_request['event_ids'])) {
		    $partner->events()->attach($partner_request['event_ids']);
	    }

        return $partner;
    }

    public function show($id)
    {
        return Partner::with(['addresses', 'legal_entity', 'images', 'projects', 'events'])->findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
    	$data = $request->all();
	    /** @var Partner $partner */
	    $partner = Partner::with(['images'])->findOrFail($id);
        if (($logo_id = $request->get('logo_id')) && ($logo_id != $partner->logo_id)) {
            Image::whereId($logo_id)->first()->confirm();
            Image::whereId($partner->logo_id)->first()->refuse();
        }
        $partner->fill($data);
        $partner->saveOrFail();

	    $tag_ids = $request->get('tag_ids');
	    if (is_array($tag_ids)) {
		    $partner->tags()->sync($tag_ids);
	    }

	    $project_ids = $request->get('project_ids');
	    if (is_array($project_ids)) {
		    $partner->projects()->sync($project_ids);
	    }

	    $event_ids = $request->get('event_ids');
	    if (is_array($event_ids)) {
		    $partner->events()->sync($event_ids);
	    }

	    $image_ids = $request->get('image_ids');
	    if (is_array($image_ids)) {
		    $restaurant_images = $partner->images->pluck('id');
		    $to_delete = $restaurant_images->diff($image_ids)->toArray();
		    $to_add = collect($image_ids)->diff($restaurant_images)->toArray();
		    $partner->images()->sync($image_ids);
		    if (!empty($to_delete)) {
			    Image::whereIn('id', $to_delete)->get()->each(function(Image $image) {
				    $image->refuse();
			    });
		    }
		    if (!empty($to_add)) {
			    Image::whereIn('id', $to_add)->get()->each(function(Image $image) {
				    $image->confirm();
			    });
		    }
	    }

        return $partner;
    }

    public function destroy($id)
    {
        Partner::destroy($id);
    }
}