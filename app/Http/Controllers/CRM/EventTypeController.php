<?php

namespace App\Http\Controllers\CRM;

use App\Models\EventType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventTypeController extends Controller
{
    public function index()
    {
	    return EventType::all();
    }

	/**
	 * @param Request $request
	 *
	 * @return EventType
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $certificateType = new EventType($request->all());
        $certificateType->saveOrFail();

        return $certificateType;
    }

    public function show($id)
    {
        return EventType::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $certificateType = EventType::findOrFail($id);
        $certificateType->fill($request->all());
        $certificateType->saveOrFail();

        return $certificateType;
    }

    public function destroy($id)
    {
	    EventType::destroy($id);
    }
}