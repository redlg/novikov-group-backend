<?php

namespace App\Http\Controllers\CRM;

use App\Models\CardOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CardOrderController extends Controller
{
    public function index()
    {
        return CardOrder::all();
    }

	/**
	 * @param Request $request
	 *
	 * @return CardOrder
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $data = $request->all();
        $cardOrder = new CardOrder($data);
        $cardOrder->saveOrFail();

        return $cardOrder;
    }

    public function show($id)
    {
        return CardOrder::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $data = $request->all();
        $cardOrder = CardOrder::findOrFail($id);
        $cardOrder->fill($data);
        $cardOrder->saveOrFail();

        return $cardOrder;
    }

    public function destroy($id)
    {
        CardOrder::destroy($id);
    }
}
