<?php

namespace App\Http\Controllers\CRM;

use App\Models\Chef;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChefController extends Controller
{
    public function show($id)
    {
        return Chef::find($id);
    }

	/**
	 * @param Request $request
	 *
	 * @return Chef
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
	    $chef = new Chef($request->all());
	    $chef->saveOrFail();

	    if ($chef->image_id) {
	    	$chef->refresh();
		    $chef->image->confirm();
	    }

		if ($restaurant_id = $request->get('restaurant_id')) {
			$restaurant = Restaurant::findOrFail($restaurant_id);
			$restaurant->chef_id = $chef->id;
			$restaurant->saveOrFail();
		}

        return $chef;
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
	{
		$chef = Chef::findOrFail($id);
		$old_image_id = $chef->image_id;
		$chef->fill($request->all());
		$chef->saveOrFail();

		$image_id = $request->get('image_id');
		if ($image_id && ($image_id != $old_image_id)) {
			$chef->refresh();
			$chef->image->confirm();
		}

		return $chef;
	}

	public function destroy($id)
	{
		Chef::destroy($id);
	}
}
