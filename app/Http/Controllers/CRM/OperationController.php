<?php

namespace App\Http\Controllers\CRM;

use App\Jobs\ExportOperations;
use App\Models\Abstracts\Card;
use App\Models\Abstracts\ErrorCode;
use App\Models\Certificate;
use App\Models\DiscountCard;
use App\Models\Operation;
use App\Models\OperationType;
use DB;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OperationController extends Controller
{
    public function index(Request $request, $to_excel = false, $check_quantity = false)
    {
	    $operations = Operation::query();

	    if ($created_at_from = $request->get('created_at_from')) {
		    $operations->whereDate('created_at', '>=', $created_at_from);
	    }

	    if ($created_at_to = $request->get('created_at_to')) {
		    $operations->whereDate('created_at', '<=', $created_at_to);
	    }

	    if ($guest_id = $request->get('guest_id')) {
	    	$operations->where('guest_id', $guest_id);
	    }

	    if ($operation_type_id = $request->get('operation_type_id')) {
	    	$operations->where('operation_type_id', $operation_type_id);
	    }

	    if ($initiator_id = $request->get('initiator')) {
		    if ($initiator_id == Operation::INITIATOR_USER_ID) {
			    $operations->whereNotNull('user_id');
		    } elseif ($initiator_id == Operation::INITIATOR_RESTAURANT_ID) {
			    $operations->whereNotNull('restaurant_id');
		    } elseif ($initiator_id == Operation::INITIATOR_SYSTEM_ID) {
			    $operations->whereNull('user_id')
				    ->whereNull('restaurant_id');
		    }
	    }

	    if ($issuer_id = $request->get('issuer_id')) {
	    	$operations
			    ->whereHas('discount_card', function($query) use ($issuer_id) {
				    $query->where('issuer_id', $issuer_id);
			    })->orWhereHas('certificate', function($query) use ($issuer_id) {
				    $query->where('issuer_id', $issuer_id);
			    });
	    }

	    if ($restaurant_id = $request->get('restaurant_id')) {
		    $operations->where('restaurant_id', $restaurant_id);
	    }

	    if ($quick_filter = $request->get('quick_filter')) {
			if ($quick_filter == Operation::QUICK_FILTER_DISCOUNT_CARD_ID) {
				$request->request->add(['relation' => Card::RELATION_DISCOUNT_CARD_ID]);
			} elseif ($quick_filter == Operation::QUICK_FILTER_DISPOSABLE_ID) {
				$request->request->add(['relation' => Card::RELATION_CERTIFICATE_ID]);
				$operations->whereHas('certificate', function($query) {
					$query->whereHas('type', function($subquery) {
						$subquery->where('reusable', false);
					});
				});
			} elseif ($quick_filter == Operation::QUICK_FILTER_REUSABLE_ID) {
				$request->request->add(['relation' => Card::RELATION_CERTIFICATE_ID]);
				$operations->whereHas('certificate', function($query) {
					$query->whereHas('type', function($subquery) {
						$subquery->where('reusable', true);
					});
				});
			}
	    }

	    if ($relation = $request->get('relation')) {
	    	if ($relation == Card::RELATION_DISCOUNT_CARD_ID) {
	    		$model = DiscountCard::class;
		    } elseif ($relation == Card::RELATION_CERTIFICATE_ID) {
	    		$model = Certificate::class;
		    }

		    if (isset($model)) {
	    		$operations->where('card_type', $model);

			    if ($card_type_id = $request->get('card_type_id')) {
				    $operations->whereHas($relation, function($query) use ($card_type_id) {
					    $query->where('card_type_id', $card_type_id);
				    });
			    }
		    }
	    }

	    if ($number = $request->get('number')) {
	    	$operations
			    ->whereHas('discount_card', function($query) use ($number) {
		            $query->where('number', '~*', $number);
			    })->orWhereHas('certificate', function($query) use ($number) {
				    $query->where('number', '~*', $number);
			    });
	    }

	    if ($sum_from = $request->get('sum_from')) {
		    $operations->whereHas('transactions', function($query) use ($sum_from) {
				    $query->select(['operation_id'])
			            ->groupBy('operation_id')
			            ->havingRaw('SUM(sum) >= ?', [$sum_from]);
			    });
	    }

	    if ($sum_to = $request->get('sum_to')) {
		    $operations->whereHas('transactions', function($query) use ($sum_to) {
			    $query->select(['operation_id'])
				    ->groupBy('operation_id')
				    ->havingRaw('SUM(sum) <= ?', [$sum_to]);
		    });
	    }

	    $order_type = $request->get('sort_type') ?: 'asc';
	    $order_by = $request->get('sort_by') ?: 'id';
	    if (($order_by == 'number' || $order_by == 'issuer_id') && isset($relation)) {
            $order_by = $relation . '.' . $order_by;
        }
        if ($order_by == 'transaction_sum') {
            $nulls = $order_type == 'asc' ? 'ASC NULLS FIRST' : 'DESC NULLS LAST';
            $operations->leftJoin(
                DB::raw('(SELECT operation_id, SUM(sum) AS transaction_sum FROM transactions GROUP BY operation_id) as t'),
                't.operation_id', '=', 'operations.id'
            )->orderByRaw("transaction_sum $nulls");
        } elseif (str_contains($order_by, '.')) {
		    $split = explode('.', $order_by);
		    $operations->addField($order_by);
		    $operations->orderBy($split[0] . "_" . $split[1], $order_type);
	    } else {
		    $operations->orderBy($order_by, $order_type);
	    }

	    if ($check_quantity) {
		    return $operations->count(['id']);
	    } elseif ($to_excel) {
		    return $operations->get();
	    } else {
		    $pagination = $operations->paginate(
			    $request->input('per_page', 50),
			    ['*'],
			    'page',
			    $request->input('cur_page', 1)
		    );

		    return [
			    'pagination' => [
				    'total'        => $pagination->total(),
				    'current_page' => $pagination->currentPage(),
				    'from'         => $pagination->firstItem(),
				    'to'           => $pagination->lastItem(),
				    'last_page'    => $pagination->lastPage()
			    ],
			    'items' => $pagination->items(),
		    ];
	    }
    }

    public function export(Request $request)
    {
	    if ($this->index($request, false, true) > config('crm.max_entries_count_for_export')) {
		    throw new HttpResponseException(response([
			    'error_code' => ErrorCode::TOO_MANY_RECORDS_FOR_EXPORT,
			    'error'      => "Количество записей на экспорт превышает "
				    . config('crm.max_entries_count_for_export')
				    . ", пожалуйста, примените фильтры и попробуйте ещё раз.",
		    ], 200));
	    } else {
		    $request_array = $request->toArray();
		    $filename = 'operations' . time();
		    dispatch(new ExportOperations($request_array, $filename));

		    return $filename . '.xls';
	    }
    }

	public function show($id)
	{
		return Operation::with(['transactions'])->findOrFail($id);
	}

	public function getFilters()
	{
		return [
			'operation_types' => OperationType::all(),
			'initiators'      => Operation::INITIATORS,
			'relations'       => Card::RELATIONS,
			'quick_filter'    => OPERATION::QUICK_FILTER,
		];
	}
}
