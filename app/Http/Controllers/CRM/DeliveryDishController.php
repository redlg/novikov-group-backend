<?php

namespace App\Http\Controllers\CRM;

use App\Models\DeliveryDish;
use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeliveryDishController extends Controller
{
	public function index(Request $request)
	{
		$dishes = DeliveryDish::with(['tags']);
		$lang = $request->get('language');

		if ($name = $request->get('name')) {
			$dishes->where('name_' . $lang, '~*', $name);
		}

		if ($category_id = $request->get('category_id')) {
			$dishes = $dishes->where('delivery_category_id', $category_id);
		}

		if ($restaurant_id = $request->get('restaurant_id')) {
			$dishes = $dishes->whereHas('delivery_category', function($query) use ($restaurant_id) {
				$query->where('restaurant_id', $restaurant_id);
			});
		}

		return $dishes->get();
	}

    public function show($id)
    {
        return DeliveryDish::with(['tags'])->findOrFail($id);
    }

	/**
	 * @param Request $request
	 *
	 * @return DeliveryDish
	 * @throws \Throwable
	 */
	public function store(Request $request)
	{
		$dish = new DeliveryDish($request->all());
		$dish->saveOrFail();

        if ($image_id = $request->get('image_id')) {
            Image::whereId($image_id)->first()->confirm();
        }

		if ($tag_ids = $request->get('tag_ids')) {
			$dish->tags()->attach($tag_ids);
		}

		return $dish;
	}

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        /** @var DeliveryDish $deliveryDish */
        $deliveryDish = DeliveryDish::findOrFail($id);
        if (($image_id = $request->get('image_id')) && ($image_id != $deliveryDish->image_id)) {
            Image::whereId($image_id)->first()->confirm();
            Image::whereId($deliveryDish->image_id)->first()->refuse();
        }
        $deliveryDish->fill($request->all());
        $deliveryDish->saveOrFail();

	    $tag_ids = $request->get('tag_ids');
	    if ($tag_ids || is_array($tag_ids)) {
		    $deliveryDish->tags()->sync($tag_ids);
	    }

        return $deliveryDish;
    }

    public function destroy($id)
    {
	    DeliveryDish::destroy($id);
    }
}
