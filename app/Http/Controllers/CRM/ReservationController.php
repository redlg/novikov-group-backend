<?php

namespace App\Http\Controllers\CRM;

use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    public function index()
    {
        return Reservation::all();
    }

	/**
	 * @param Request $request
	 *
	 * @return Reservation
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $data = $request->all();
        $reservation = new Reservation($data);
        $reservation->saveOrFail();

        return $reservation;
    }

    public function show($id)
    {
        return Reservation::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $data = $request->all();
        $reservation = Reservation::findOrFail($id);
        $reservation->fill($data);
        $reservation->saveOrFail();

        return $reservation;
    }

    public function destroy($id)
    {
        Reservation::destroy($id);
    }
}
