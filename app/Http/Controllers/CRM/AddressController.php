<?php

namespace App\Http\Controllers\CRM;

use App\Models\Address;
use App\Models\Event;
use App\Models\Guest;
use App\Models\Partner;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddressController extends Controller
{
	public function show($id)
	{
		return Address::whereId($id)->first();
	}

	public function getFilters()
	{
		return [
			'address_types' => [[
				'id' => 'guest',
				'name' => 'Гость',
			], [
				'id' => 'event',
				'name' => 'Событие',
			], [
				'id' => 'partner',
				'name' => 'Партнер',
			], [
				'id' => 'restaurant',
				'name' => 'Ресторан',
			], [
				'id' => Address::NOT_SPECIFY,
				'name' => 'Не определен',
			]]
		];
	}

	/**
	 * @param Request $request
	 *
	 * @return Address
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
    	$data = $request->all();
    	if (isset($data['addressable_type'])) {
			if ($data['addressable_type'] == 'guest') {
				$data['addressable_type'] = Guest::class;
			} elseif ($data['addressable_type'] == 'event') {
				$data['addressable_type'] = Event::class;
			} elseif ($data['addressable_type'] == 'partner') {
				$data['addressable_type'] = Partner::class;
			} elseif ($data['addressable_type'] == 'restaurant') {
				$data['addressable_type'] = Restaurant::class;
			} else {
				$data['addressable_type'] = Address::NOT_SPECIFY;
			}
	    }
        $address = new Address($data);
        $address->saveOrFail();

        return $address;
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $address = Address::findOrFail($id);
        $address->fill($request->all());
        $address->saveOrFail();

        return $address;
    }

    public function destroy($id)
    {
        Address::destroy($id);
    }
}
