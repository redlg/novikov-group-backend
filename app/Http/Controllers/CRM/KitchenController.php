<?php

namespace App\Http\Controllers\CRM;

use App\Models\Kitchen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KitchenController extends Controller
{
	public function index(Request $request)
	{
		$kitchens = Kitchen::query();
		$lang = $request->get('language');

		if ($name = $request->get('name')) {
			$kitchens->where('name_' . $lang, '~*', $name);
		}

		$order_type = $request->get('sort_type') ?: 'asc';
		$order_by = $request->get('sort_by') ?: 'name_' . $lang;
		$kitchens->orderBy($order_by, $order_type);

		return $kitchens->get();
	}

	/**
	 * @param Request $request
	 *
	 * @return Kitchen
	 * @throws \Throwable
	 */
	public function store(Request $request)
    {
        $kitchen = new Kitchen($request->all());
        $kitchen->saveOrFail();

        return $kitchen;
    }

    public function show($id)
    {
        return Kitchen::findOrFail($id);
    }

	/**
	 * @param Request $request
	 * @param integer $id
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
	 * @throws \Throwable
	 */
	public function update(Request $request, $id)
    {
        $kitchen = Kitchen::findOrFail($id);
        $kitchen->fill($request->all());
        $kitchen->saveOrFail();

        return $kitchen;
    }

    public function destroy($id)
    {
	    Kitchen::destroy($id);
    }
}
