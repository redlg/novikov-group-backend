<?php
namespace App\Http\Middleware;

use App\Models\Abstracts\ErrorCode;
use Closure;
use JWTAuth;
use Exception;

class authJWT {
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if (!JWTAuth::getToken()) {
			return response()->json([
				'error'      => 'Токен отсутствует',
				'error_code' => ErrorCode::TOKEN_NOT_FOUND,
			], 401);
		}

		try {
			JWTAuth::parseToken()->authenticate();
		} catch (Exception $e) {
			if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
				return response()->json([
					'error'      => 'Токен недействителен',
					'error_code' => ErrorCode::TOKEN_INVALID,
				], 401);
			} else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
				return response()->json([
					'error'      => 'Токен истек',
					'error_code' => ErrorCode::TOKEN_EXPIRED,
				], 401);
			} else {
				return response()->json([
					'error'      => 'Что-то не так',
					'error_code' => ErrorCode::TOKEN_ABSENT,
				], 401);
			}
		}

		return $next($request);
	}
}