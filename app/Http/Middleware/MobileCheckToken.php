<?php

namespace App\Http\Middleware;

use App\Models\Device;
use Closure;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class MobileCheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
    	if (!$request->header('Token')) {
		    throw new HttpResponseException(response(['error' => "You don't have Token header in your request"], 403));
	    } else {
		    $token = $request->header('Token');
		    $device = Device::whereToken($token)->first();
		    if (is_null($device)) {
			    throw new HttpResponseException(response(['error' => "Device with this token not found"], 403));
		    } else {
		    	$request->attributes->add([
				    'device_id'               => $device->id,
				    'guest_id'                => $device->guest_id,
				    'favorite_restaurant_ids' => $device->favorite_restaurant_ids,
				    'favorite_dish_ids'       => $device->favorite_dish_ids,
			    ]);
		    }
	    }

        return $next($request);
    }
}
