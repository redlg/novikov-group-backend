<?php

namespace App\Http\Middleware;

use App\Models\Device;
use Closure;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;

class AcceptedLanguage
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
    	$locale = $request->header('Accept-Language', false);
    	$locale = ($locale && ($locale == 'ru' || $locale == 'en')) ? $locale : 'ru';
	    $request->attributes->add([
		    'language' => $locale,
	    ]);

	    $request->headers->set('Content-Language', $locale);
	    $response = $next($request);

        return $response;
    }
}
