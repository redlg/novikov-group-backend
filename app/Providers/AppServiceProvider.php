<?php

namespace App\Providers;

use App\Models\Address;
use App\Models\Certificate;
use App\Models\Device;
use App\Models\Partner;
use App\Observers\AddressObserver;
use App\Observers\CertificateObserver;
use App\Observers\DeviceObserver;
use App\Observers\PartnerObserver;
use App\Services\SMSTrafficService;
use App\Services\TicketService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Certificate::observe(CertificateObserver::class);
        Device::observe(DeviceObserver::class);
        Address::observe(AddressObserver::class);
        Partner::observe(PartnerObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

	    //TicketService
	    $this->app->singleton(TicketService::class, function() {
		    return new TicketService();
	    });

	    //SMSTrafficService
	    $this->app->singleton(SMSTrafficService::class, function() {
		    return new SMSTrafficService();
	    });
    }
}
