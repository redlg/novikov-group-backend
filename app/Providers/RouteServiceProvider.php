<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

	/**
	 * This namespace is applied to your mobile controller routes.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $mobile_namespace = 'App\Http\Controllers\Mobile';

	/**
	 * This namespace is applied to your CRM controller routes.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $crm_namespace = 'App\Http\Controllers\CRM';

	/**
	 * This namespace is applied to your rkeeper controller routes.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $rkeeper_namespace = 'App\Http\Controllers\Rkeeper';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapMobileRoutes();

        $this->mapCRMRoutes();

        $this->mapRkeeperRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }

	/**
	 * Define the "mobile" routes for the application.
	 *
	 * These routes are typically stateless.
	 *
	 * @return void
	 */
	protected function mapMobileRoutes()
	{
		Route::prefix('mobile')
			->middleware('mobile')
			->namespace($this->mobile_namespace)
			->group(base_path('routes/mobile.php'));
	}

	/**
	 * Define the "CRM" routes for the application.
	 *
	 * These routes are typically stateless.
	 *
	 * @return void
	 */
	protected function mapCRMRoutes()
	{
		Route::prefix('crm')
			->middleware('crm')
			->namespace($this->crm_namespace)
			->group(base_path('routes/crm.php'));
	}

	/**
	 * Define the "Rkeeper" routes for the application.
	 *
	 * These routes are typically stateless.
	 *
	 * @return void
	 */
	protected function mapRkeeperRoutes()
	{
		Route::prefix('v1')
			->middleware('rkeeper')
			->namespace($this->rkeeper_namespace)
			->group(base_path('routes/rkeeper.php'));
	}
}