<?php

namespace App\Observers;

use App\Models\Partner;
use DB;

class PartnerObserver
{
	public function deleting(Partner $partner)
	{
		//удаляем связь с тегами
		DB::table('taggables')
			->where('taggable_type', Partner::class)
			->where('taggable_id', $partner->id)
			->delete();
		//удаляем все адреса
		$partner->addresses()->delete();
	}
}