<?php

namespace App\Observers;

use App\Models\Device;

class DeviceObserver
{
	public function creating(Device $device)
	{
		$device->token = md5($device->device_id) . time();
	}
}