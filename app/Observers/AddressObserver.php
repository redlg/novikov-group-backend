<?php

namespace App\Observers;

use App\Models\Address;
use App\Models\Guest;

class AddressObserver
{
	public function saving(Address $address)
	{
		if ($address->main && $address->addressable_type == Guest::class && isset($address->addressable_id)) {
			$guest = Guest::with(['addresses'])->whereId($address->addressable_id)->first();
			$main_address = $guest->addresses->where('main', true)->first();
			if ($main_address && (!$address->exists || ($address->exists && $address->id != $main_address->id))) {
				$main_address->main = false;
				$main_address->save();
			}
		}
	}
}