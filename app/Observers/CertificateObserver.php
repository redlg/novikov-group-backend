<?php

namespace App\Observers;

use App\Models\Certificate;
use App\Models\CertificateType;

class CertificateObserver
{
	public function creating(Certificate $certificate)
	{
		if (is_null($certificate->balance)) {
			$certificate_type = CertificateType::whereId($certificate->card_type_id)->first(['sum']);
			$certificate->balance = $certificate_type->sum;
		}
	}
}