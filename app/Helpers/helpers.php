<?php

use Intervention\Image\ImageManagerStatic as Image;

if (!function_exists('rename_column')) {
	/**
	 * @param string $table
	 * @param string $old_name
	 * @param string $new_name
	 */
	function rename_column($table, $old_name, $new_name) {
		DB::statement("sp_rename '$table.$old_name', '$new_name'");
	}
}

if (!function_exists('transliterate')) {
	/**
	 * @param string $string
	 *
	 * @return string
	 */
	function transliterate($string) {
		$converter = [
			'а' => 'a', 'б' => 'b', 'в' => 'v',
			'г' => 'g', 'д' => 'd', 'е' => 'e',
			'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
			'и' => 'i', 'й' => 'y', 'к' => 'k',
			'л' => 'l', 'м' => 'm', 'н' => 'n',
			'о' => 'o', 'п' => 'p', 'р' => 'r',
			'с' => 's', 'т' => 't', 'у' => 'u',
			'ф' => 'f', 'х' => 'h', 'ц' => 'c',
			'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
			'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
			'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

			'А' => 'A', 'Б' => 'B', 'В' => 'V',
			'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
			'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
			'И' => 'I', 'Й' => 'Y', 'К' => 'K',
			'Л' => 'L', 'М' => 'M', 'Н' => 'N',
			'О' => 'O', 'П' => 'P', 'Р' => 'R',
			'С' => 'S', 'Т' => 'T', 'У' => 'U',
			'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
			'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
			'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
			'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
		];

		return strtr($string, $converter);
	}
}

if (!function_exists('transliterate_to_url')) {
	/**
	 * @param string $string
	 *
	 * @return null|string|string[]
	 */
	function transliterate_to_url($string) {
		$string = transliterate($string);
		$string = strtolower($string);
		$string = preg_replace('~[^-a-z0-9_]+~u', '-', $string);
		$string = trim($string, "-");

		return $string;
	}
}

if (!function_exists('make_resize_images')) {
	/**
	 * @param \Illuminate\Database\Eloquent\Collection|Illuminate\Support\Collection $images
	 * @param string                                   $path
	 * @param int|null                                 $width
	 * @param int|null                                 $height
	 *
	 * @return array
	 * @throws Exception
	 */
	function make_resize_images($images, $path, $width = null, $height = null) {
		//create directory
		if (!file_exists($path)) {
			mkdir($path, 0755, true);
		}
		$result = [];
		foreach ($images as $image) {
			if (is_null($image)) {
				return [0 => []];
			}
			//create resize image
			if (!file_exists($path . $image->name)) {
				$new_image = Image::make($image->link);
				if ($width === null && $height === null) {
					throw new Exception('Sizes not specified');
				} else if ($width === null || $height === null) {
					$new_image->resize($width, $height, function($constraint) {
						$constraint->aspectRatio();
					});
				} else {
					$new_image->resize($width, $height);
				}
				$new_image->save($path . $image->name);
			}
			$resize_image = (object)[];
			$resize_image->id = $image->id;
			$resize_image->name = $image->name;
			$resize_image->link = env('APP_URL') . $path . $image->name;
			$result[] = $resize_image;
		}

		return $result;
	}
}

if (!function_exists('end_of_word')) {
	/**
	 * @param int $value
	 * @param array $words
	 *
	 * @return mixed
	 */
	function end_of_word($value, $words) {
		$array = array(2,0,1,1,1,2);

		return $words[($value % 100 > 4 && $value % 100 < 20)? 2 : $array[($value % 10 < 5) ? $value % 10 : 5]];
	}
}

if (!function_exists('generate_random_number_string')) {
	/**
	 * @param integer $length
	 *
	 * @return string
	 */
	function generate_random_number_string($length) {
		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}
}

if (!function_exists('format_phone')) {
	/**
	 * remove any non-numbers chars from phone number
	 *
	 * @param string $phone
	 *
	 * @return null|string|string[]
	 */
	function format_phone($phone) {
		return preg_replace('~\D+~', '', $phone);
	}
}