<?php
namespace App\Requests;

use GuzzleHttp\Client;
use Storage;

class NovikovRequester
{
    const LANG_RU = 'ru';
    const LANG_EN = 'en';
    public $last_error;

    protected $endpoint = 'http://novikovgroup.ru/api';
    protected $api_version = 'v1.17';
    protected $device_id;
    protected $lang = self::LANG_RU;

    public function __construct()
    {
        $this->device_id = config('crm.novikov_device_id');
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    /**
     * @return string
     */
    public function getApiVersion()
    {
        return $this->api_version;
    }

    /**
     * @param string $api_version
     */
    public function setApiVersion($api_version)
    {
        $this->api_version = $api_version;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        if (in_array($lang, [static::LANG_RU, static::LANG_EN])) {
	        $this->lang = $lang;
        }
    }

    /**
     * @return mixed
     */
    public function getDeviceId()
    {
        return $this->device_id;
    }

    /**
     * @param mixed $device_id
     */
    public function setDeviceId($device_id)
    {
        $this->device_id = $device_id;
    }

    public function getUser()
    {
        $result = $this->request('user/');
        if (!$this->checkForErrors($result)) {
            return false;
        }

        return $result['result'];
    }

    public function registerUser()
    {
        $result = $this->request('user/', [], true);
        if (!$this->checkForErrors($result)) {
            return false;
        }

        return $result['result'];
    }

    public function getRestaurantList($offset = 0, $limit = 10)
    {
        $result = $this->request('restaurant/', ['l' => $limit, 'n' => $offset]);
        if (!$this->checkForErrors($result)) {
            if ($result['message'] == 'rests not found') {
	            return [];
            }

            return false;
        }

        return $result['result'];
    }

    public function getRestaurant($id)
    {
        $response = $this->request("restaurant/$id/");
        if ($this->checkForErrors($response)) {
            return $response['result'];
        }

        return false;
    }

    public function getEventList($offset = 0, $limit = 10)
    {
        $result = $this->request('event/', ['l' => $limit, 'n' => $offset]);
        if (!$this->checkForErrors($result)) {
            if ($result['message'] == 'events not found') {
                return [];
            }

            return false;
        }

        return $result['result'];
    }

    public function getEvent($id)
    {
        $response = $this->request("event/$id/");
        if ($this->checkForErrors($response)) {
            return $response['result'];
        }

        return false;
    }

    public function buyTicket($params)
    {
	    $response = $this->request("order/", $params, true);

	    if ($this->checkForErrors($response)) {
		    return $response['result'];
	    } else {
		    return [
			    'error'   => true,
			    'message' => $this->last_error,
		    ];
	    }
    }

    public function sendDeliveryOrder($params)
    {
        $response = $this->request("delivery/order/", $params, true, true);

        if ($this->checkForErrors($response)) {
            return $response['result'];
        } else {
            return [
                'error'   => true,
                'message' => $this->last_error,
            ];
        }
    }

    public function getPurchaseCardInformation()
    {
        $result = $this->request('loyality/discount/');
        if ($this->checkForErrors($result)) {
            return $result['result'];
        }

        return false;
    }

	public function getPartnersList()
	{
		$result = $this->request('loyality/discount/partners/');
		if ($this->checkForErrors($result)) {
			return $result['result'];
		}

		return false;
	}

	public function getProjectsList()
	{
		$result = $this->request('projects');
		if ($this->checkForErrors($result)) {
			return $result['result'];
		}

		return false;
	}

    public function getRestaurantMenu($id)
    {
        $response = $this->request("restaurant/$id/menu/");

        if ($this->checkForErrors($response)) {
            return $response['result'];
        }

        return false;
    }

	public function getDeliveryCategories($id)
	{
		$response = $this->request("delivery/category/$id/");

		if ($this->checkForErrors($response)) {
			return $response['result'];
		}

		return false;
	}

	public function getDeliveryDishes($id)
	{
		$response = $this->request("delivery/item/$id/");

		if ($this->checkForErrors($response)) {
			return $response['result'];
		}

		return false;
	}

	public function getProductDeliveryMethods()
	{
		$response = $this->request("order/delivery/");

		if ($this->checkForErrors($response)) {
			return $response['result'];
		}

		return false;
	}

	public function getDeliveryMethods()
	{
		$response = $this->request("delivery/delivery_metod/");

		if ($this->checkForErrors($response)) {
			return $response['result'];
		}

		return false;
	}

	public function getDeliveryPaymentMethods()
	{
		$response = $this->request("delivery/payment_metod/");

		if ($this->checkForErrors($response)) {
			return $response['result'];
		}

		return false;
	}

	public function getRestaurantDeliveryEmails()
	{
		$response = $this->request("delivery/mails");

		if ($this->checkForErrors($response)) {
			return $response['result'];
		}

		return false;
	}

	protected function request($path, $params = [], $post = false, $json = false)
	{
		$client = new Client();

		if ($post) {
			$url = $this->makeUrl($path);
			if (!$json) {
				$request = $client->request('POST', $url, [
					'form_params' => $params
				]);
			} else {
				$request = $client->request('POST', $url, [
					'json' => $params,
				]);
			}
		} else {
			$url = $this->makeUrl($path, $params);
			$request = $client->request('GET', $url, ['http_errors' => false]);
		}

		return ($request->getStatusCode() == 200)
			? json_decode($request->getBody(), true)
			: [
				'success' => 0,
				'message' => $request->getHeaders(),
			];
	}

    protected function checkForErrors($response)
    {
        if ($response['success'] === 0) {
            $this->last_error = $response['message'];

            return false;
        }

        return true;
    }

    protected function makeUrl($path, $params = [])
    {
        $params = array_merge([
            'device_id' => $this->device_id,
            'lang' => $this->lang,
        ], $params);

        return implode("/", [$this->endpoint, $this->api_version, $path]) . "?" . http_build_query($params);
    }

	public function downloadImage($image_id, $resolution)
	{
		if (Storage::exists("images/$image_id.jpg")) {
			return Storage::get("images/$image_id.jpg");
		} else if (Storage::exists("images/$image_id.png")) {
			return Storage::get("images/$image_id.png");
		} else {
			$url = "$this->endpoint/cache/{$image_id}_{$resolution[0]}x{$resolution[1]}";
			$headers = @get_headers($url);
			if(strpos($headers[0], '200')) {
				return file_get_contents($url);
			} else {
				return false;
			}
		}
	}
}