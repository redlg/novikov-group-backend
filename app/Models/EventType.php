<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\EventType
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read mixed $name
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventType whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventType whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\EventType withoutTrashed()
 * @mixin \Eloquent
 */
class EventType extends Model
{
	use SoftDeletes;

	const CONCERT = 'Концерт';
	const COURSE = 'Курс';
	const PARTY = 'Вечеринка';
	const CONTEST = 'Конкурс';

    protected $fillable = [
        'name_ru',
        'name_en',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

    public function events()
    {
        return $this->hasMany(Event::class);
    }

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}
}
