<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TicketType
 *
 * @property int $id
 * @property int $event_id
 * @property string $name
 * @property float $price
 * @property bool $not_fix_price
 * @property int $in_stock
 * @property int $available
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Event $event
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TicketTypeOrder[] $ticket_type_orders
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TicketType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereInStock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereNotFixPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TicketType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TicketType withoutTrashed()
 * @mixin \Eloquent
 */
class TicketType extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'event_id',
        'name',
        'price',
        'not_fix_price',
	    'in_stock',
	    'available',
    ];

    protected $hidden = [
    	'external_id',
	    'temp_id',
	    'created_at',
	    'updated_at',
	    'deleted_at',
    ];

    protected $casts = [
	    'event_id'      => 'int',
	    'price'         => 'float',
	    'not_fix_price' => 'bool',
	    'in_stock'      => 'int',
	    'available'     => 'int',
    ];

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function ticket_type_orders()
    {
        return $this->hasMany(TicketTypeOrder::class);
    }
}
