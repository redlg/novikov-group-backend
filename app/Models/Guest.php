<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Guest
 *
 * @property int $id
 * @property string $phone
 * @property string|null $email
 * @property string|null $first_name
 * @property string|null $last_name
 * @property int $sex
 * @property \Carbon\Carbon|null $birthday_at
 * @property string|null $code
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Address[] $addresses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Certificate[] $certificates
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @property-read \App\Models\Device $device
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DiscountCard[] $discount_cards
 * @property-read mixed $average_check_sum
 * @property-read mixed $gender
 * @property-read mixed $last_operation_date
 * @property-read mixed $total_check_sum
 * @property-read mixed $transactions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Operation[] $operations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reservation[] $reservations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TicketTypeOrder[] $tickets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Guest onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereBirthdayAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Guest whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Guest withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Guest withoutTrashed()
 * @mixin \Eloquent
 */
class Guest extends Model
{
	use SoftDeletes;

    /* https://en.wikipedia.org/wiki/ISO/IEC_5218 */
    const NOT_KNOWN = 0;
    const MALE = 1;
    const FEMALE = 2;
    const NOT_APPLICABLE = 9;

    protected $fillable = [
        'phone',
        'email',
        'first_name',
        'last_name',
        'sex',
        'birthday_at',
        'code',
    ];

    protected $hidden = [
    	'created_at',
	    'updated_at',
	    'deleted_at',
	    'transactions',
	    'operations',
    ];

    protected $casts = [
	    'sex' => 'int',
    ];

    protected $dates = [
        'birthday_at',
    ];

    protected $appends = ['gender'];

	public function addresses()
	{
		return $this->morphMany(Address::class, 'addressable');
	}

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }

    public function discount_cards()
    {
        return $this->hasMany(DiscountCard::class);
    }

    public function transactions()
    {
    	return $this->hasMany(Transaction::class);
    }

    public function operations()
    {
    	return $this->hasMany(Operation::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function delivery_orders()
    {
        return $this->hasMany(DeliveryOrder::class);
    }

    public function tickets()
    {
        return $this->hasMany(TicketTypeOrder::class);
    }

	public function device()
	{
		return $this->hasOne(Device::class);
	}

    public function getGenderAttribute()
    {
        if ($this->sex == Guest::NOT_KNOWN) {
            $gender = 'Неизвестно';
        } else if ($this->sex == Guest::MALE) {
            $gender = 'Мужской';
        } else if ($this->sex == Guest::FEMALE) {
            $gender = 'Женский';
        } else {
            $gender = 'Неприменимо';
        }

        return $gender;
    }

    public function getLastOperationDateAttribute()
    {
    	return $this->transactions->count() ? $this->transactions->sortBy('created_at')->last()->created_at->toDateTimeString() : null;
    }

	public function getAverageCheckSumAttribute()
	{
		return $this->transactions->count() ? round($this->transactions->sum('sum') / $this->transactions->count()) : null;
	}

	public function getTotalCheckSumAttribute()
	{
		return $this->transactions->count() ? round($this->transactions->sum('sum')) : null;
	}

	public function getTransactionsCountAttribute()
	{
		return $this->transactions->count() ?: null;
	}
}
