<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\MenuDish
 *
 * @property int $id
 * @property int $menu_category_id
 * @property string $name_ru
 * @property string $name_en
 * @property float $price
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\MenuCategory $menu_category
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MenuDish onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereMenuCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuDish whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MenuDish withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MenuDish withoutTrashed()
 * @mixin \Eloquent
 */
class MenuDish extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'menu_category_id',
        'name_ru',
        'name_en',
        'price',
    ];

    protected $hidden = [
    	'created_at',
	    'updated_at',
	    'deleted_at',
	    'external_id',
	    'temp_id',
    ];

    protected $casts = [
	    'menu_category_id' => 'int',
	    'price'            => 'float',
    ];

    public function menu_category()
    {
        return $this->belongsTo(MenuCategory::class);
    }
}
