<?php

namespace App\Models;

use App\Scopes\RestaurantIdScope;
use App\Traits\AddField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Operation
 *
 * @property int $id
 * @property int $operation_type_id
 * @property int $card_id
 * @property string $card_type
 * @property int $restaurant_id
 * @property int $user_id
 * @property string|null $comment
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $guest_id
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $card
 * @property-read \App\Models\Certificate $certificate
 * @property-read \App\Models\DiscountCard $discount_card
 * @property-read mixed $initiator
 * @property-read \App\Models\Guest|null $guest
 * @property-read \App\Models\Restaurant|null $restaurant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @property-read \App\Models\OperationType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operation onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereOperationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Operation whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Operation withoutTrashed()
 * @mixin \Eloquent
 */
class Operation extends Model
{
	use SoftDeletes;
	use AddField;

	const INITIATOR_USER_ID = 'user';
	const INITIATOR_USER_NAME = 'Пользователь';
	const INITIATOR_RESTAURANT_ID = 'restaurant';
	const INITIATOR_RESTAURANT_NAME = 'Ресторан';
	const INITIATOR_SYSTEM_ID = 'system';
	const INITIATOR_SYSTEM_NAME = 'Система';
	const INITIATORS = [[
			'id'   => self::INITIATOR_USER_ID,
			'name' => self::INITIATOR_USER_NAME,
		], [
			'id'   => self::INITIATOR_RESTAURANT_ID,
			'name' => self::INITIATOR_RESTAURANT_NAME,
		], [
			'id'   => self::INITIATOR_SYSTEM_ID,
			'name' => self::INITIATOR_SYSTEM_NAME,
	]];
	const QUICK_FILTER_DISCOUNT_CARD_ID = 'discount_cards';
	const QUICK_FILTER_DISCOUNT_CARD_NAME = 'Дисконтные карты';
	const QUICK_FILTER_DISPOSABLE_ID = 'disposable';
	const QUICK_FILTER_DISPOSABLE_NAME = 'Одноразовые сертификаты';
	const QUICK_FILTER_REUSABLE_ID = 'reusable';
	const QUICK_FILTER_REUSABLE_NAME = 'Многоразовые сертификаты';
	const QUICK_FILTER = [[
		'id'   => self::QUICK_FILTER_DISCOUNT_CARD_ID,
		'name' => self::QUICK_FILTER_DISCOUNT_CARD_NAME,
	], [
		'id'   => self::QUICK_FILTER_DISPOSABLE_ID,
		'name' => self::QUICK_FILTER_DISPOSABLE_NAME,
	], [
		'id'   => self::QUICK_FILTER_REUSABLE_ID,
		'name' => self::QUICK_FILTER_REUSABLE_NAME,
	]];

	protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = [
        'operation_type_id',
        'card_id',
        'card_type',
	    'guest_id',
        'restaurant_id',
	    'user_id',
	    'comment',
    ];

    protected $casts = [
	    'operation_type_id' => 'int',
	    'card_id'           => 'int',
	    'guest_id'          => 'int',
	    'restaurant_id'     => 'int',
	    'user_id'           => 'int',
    ];

    protected $hidden = [
    	'deleted_at',
    ];

    protected $with = ['type', 'card', 'transactions'];

    protected $appends = ['initiator'];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(new RestaurantIdScope);
	}

    public function card()
    {
		return $this->morphTo();
    }

    public function discount_card()
    {
	    return $this->belongsTo(DiscountCard::class, 'card_id')
		    ->whereCardType(DiscountCard::class);
    }

	public function certificate()
	{
		return $this->belongsTo(Certificate::class, 'card_id')
			->whereCardType(Certificate::class);
	}

	public function guest()
	{
		return $this->belongsTo(Guest::class);
	}

	public function type()
	{
		return $this->belongsTo(OperationType::class, 'operation_type_id');
	}

	public function restaurant()
	{
		return $this->belongsTo(Restaurant::class);
	}

	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}

	public function getInitiatorAttribute()
	{
		if ($this->restaurant_id !== null) {
			return self::INITIATOR_RESTAURANT_NAME;
		} else if ($this->user_id !== null) {
			return self::INITIATOR_USER_NAME;
		} else {
			return self::INITIATOR_SYSTEM_NAME;
		}
	}
}
