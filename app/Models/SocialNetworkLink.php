<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SocialNetworkLink
 *
 * @property int $id
 * @property int $restaurant_id
 * @property string $link
 * @property string $type
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Restaurant $restaurant
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialNetworkLink onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialNetworkLink whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialNetworkLink whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialNetworkLink whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialNetworkLink whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialNetworkLink whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialNetworkLink whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SocialNetworkLink whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialNetworkLink withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SocialNetworkLink withoutTrashed()
 * @mixin \Eloquent
 */
class SocialNetworkLink extends Model
{
	use SoftDeletes;

    const VK = 'vk';
    const FB = 'fb';
    const TW = 'tw';
    const INSTA = 'insta';

    protected $fillable = [
        'restaurant_id',
        'link',
        'type',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $casts = [
        'restaurant_id' => 'int',
    ];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }
}
