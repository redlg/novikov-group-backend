<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Project
 *
 * @property int $id
 * @property int $logo_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $link
 * @property string|null $announce_ru
 * @property string|null $announce_en
 * @property string|null $description_ru
 * @property string|null $description_en
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read mixed $announce
 * @property-read mixed $description
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read \App\Models\Image $logo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Partner[] $partners
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereAnnounceEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereAnnounceRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereLogoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Project withoutTrashed()
 * @mixin \Eloquent
 */
class Project extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'logo_id',
        'name_ru',
        'name_en',
        'link',
        'announce_ru',
        'announce_en',
        'description_ru',
        'description_en',
    ];

    protected $casts = [
        'logo_id' => 'int',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
		'external_id',
		'temp_id',
	];

    protected $with = ['logo'];

    public function logo()
    {
        return $this->belongsTo(Image::class);
    }

    public function images()
    {
        return $this->belongsToMany(Image::class)->withTimestamps();
    }

    public function partners()
    {
        return $this->belongsToMany(Partner::class)->withTimestamps();
    }

    public function events()
    {
        return $this->belongsToMany(Event::class)->withTimestamps();
    }

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}

	public function getDescriptionAttribute()
	{
		$description = 'description_' . request()->get('language');

		return $this->$description;
	}

	public function getAnnounceAttribute()
	{
		$announce = 'announce_' . request()->get('language');

		return $this->$announce;
	}
}
