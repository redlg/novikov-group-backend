<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\LegalEntity
 *
 * @property int $id
 * @property int $legal_entity_type_id
 * @property string $inn
 * @property string $kpp
 * @property string $name
 * @property string $legal_address
 * @property string $actual_address
 * @property string|null $brand
 * @property string|null $bank
 * @property string $payment_account
 * @property string $correspondent_account
 * @property string $bik
 * @property string $ogrn
 * @property string $general_manager
 * @property string $phone
 * @property string $email
 * @property string $contact_person
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\LegalEntityType $legal_entity_type
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LegalEntity onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereActualAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereBank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereBik($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereContactPerson($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereCorrespondentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereGeneralManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereInn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereKpp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereLegalAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereLegalEntityTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereOgrn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity wherePaymentAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntity whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LegalEntity withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LegalEntity withoutTrashed()
 * @mixin \Eloquent
 */
class LegalEntity extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name',
        'legal_entity_type_id',
        'inn',
        'kpp',
        'name',
        'legal_address',
        'actual_address',
        'brand',
        'bank',
        'payment_account',
        'correspondent_account',
        'bik',
        'ogrn',
        'general_manager',
        'phone',
        'email',
        'contact_person',
    ];

    protected $hidden = [
		'deleted_at',
	    'created_at',
	    'updated_at',
    ];

    protected $with = ['legal_entity_type'];

    protected $casts = [
      'legal_entity_type_id' => 'int',
    ];

    public function legal_entity_type()
    {
        return $this->belongsTo(LegalEntityType::class);
    }
}
