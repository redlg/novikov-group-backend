<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Permission whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Permission extends EntrustPermission
{
    const USERS_EDIT = 'users_edit';
    const RESTAURANTS_CREATE = 'restaurants_create';
    const RESTAURANTS_EDIT = 'restaurants_edit';
    const PARTNERS_EDIT = 'partners_edit';
    const PROJECTS_EDIT = 'projects_edit';
    const EVENTS_EDIT = 'events_edit';
    const TAGS_EDIT = 'tags_edit';
    const CARDS_EDIT = 'cards_edit';
    const CARDS_EXPORT = 'cards_export';
    const GUESTS_EDIT = 'guests_edit';
    const GUESTS_EXPORT = 'guests_export';
    const OPERATIONS_READ = 'operations_read';
    const OPERATIONS_EXPORT = 'operations_export';
    const LEGAL_ENTITIES_EDIT = 'legal_entities_edit';
    const COMPILATIONS_EDIT = 'compilations_edit';
    const PRODUCTS_EDIT = 'products_edit';
    const IMAGES_EDIT = 'images_edit';
    const ADDRESSES_EDIT = 'addresses_edit';

    protected $table = 'permissions';
}
