<?php

namespace App\Models;

use App\Models\Abstracts\Card;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Certificate
 *
 * @property int $id
 * @property int $card_type_id
 * @property int $issuer_id
 * @property int $contractor_id
 * @property int $guest_id
 * @property int $restaurant_id
 * @property int $user_id
 * @property string $number
 * @property float $balance
 * @property \Carbon\Carbon|null $started_at
 * @property \Carbon\Carbon|null $ended_at
 * @property \Carbon\Carbon|null $blocked_at
 * @property string $status
 * @property string|null $external_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\LegalEntity|null $contractor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @property-read mixed $status_name
 * @property-read \App\Models\Guest|null $guest
 * @property-read \App\Models\LegalEntity|null $issuer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Operation[] $operations
 * @property-read \App\Models\Restaurant|null $restaurant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @property-read \App\Models\CertificateType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Abstracts\Card addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Certificate onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereBlockedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereCardTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereContractorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereIssuerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Certificate whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Certificate withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Certificate withoutTrashed()
 * @mixin \Eloquent
 */
class Certificate extends Card
{
	use SoftDeletes;

	const BLOCKED_SERVICE = 'blocked_service'; //услуга по одноразовому сертификату оказана

	public function __construct(array $attributes = [])
	{
		$this->fillable = array_merge($this->fillable, [
			'balance',
		]);
		$this->casts = array_merge($this->casts, [
            'balance' => 'float',
		]);
		$this->statuses = array_merge($this->statuses, [
            Certificate::BLOCKED_SERVICE  => 'Использован',
            Card::NOT_ACTIVATED           => 'Не активирован',
            Card::ACTIVATED_IN_OFFICE     => 'Активирован в офисе',
            Card::ACTIVATED_IN_RESTAURANT => 'Активирован в ресторане',
            Card::BLOCKED_MANUALLY        => 'Заблокирован вручную',
            Card::BLOCKED_EXPIRED         => 'Истек',
        ]);
		parent::__construct($attributes);
	}

    public function type()
    {
        return $this->belongsTo(CertificateType::class, 'card_type_id');
    }

	public function isBlocked()
	{
		return $this->status == Card::BLOCKED_MANUALLY || $this->status == Card::BLOCKED_EXPIRED || $this->status == Certificate::BLOCKED_SERVICE;
	}
}
