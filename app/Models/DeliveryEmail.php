<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DeliveryEmail
 *
 * @property int $id
 * @property int $restaurant_id
 * @property string $name
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Restaurant $restaurant
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryEmail onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryEmail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryEmail withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryEmail withoutTrashed()
 * @mixin \Eloquent
 */
class DeliveryEmail extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'restaurant_id',
        'name',
    ];

    protected $casts = [
        'restaurant_id' => 'int',
    ];

    protected $hidden = [
        'deleted_at',
        'created_at',
        'updated_at',
        'external_id',
        'temp_id',
        'restaurant_id'
    ];

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }
}
