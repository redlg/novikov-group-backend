<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\ProductDeliveryMethod
 *
 * @property int $id
 * @property string|null $delivery_group
 * @property string $name
 * @property float $price
 * @property bool $show_address
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductDeliveryMethod onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereDeliveryGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereShowAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductDeliveryMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductDeliveryMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\ProductDeliveryMethod withoutTrashed()
 * @mixin \Eloquent
 */
class ProductDeliveryMethod extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'delivery_group',
	    'name',
	    'price',
	    'show_address',
    ];

    protected $hidden = [
    	'external_id',
	    'temp_id',
	    'created_at',
	    'updated_at',
	    'deleted_at',
    ];

    protected $casts = [
	    'price'        => 'float',
	    'show_address' => 'bool',
    ];
}
