<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Address
 *
 * @property int $id
 * @property int $addressable_id
 * @property string|null $addressable_type
 * @property string|null $text_ru
 * @property string|null $text_en
 * @property string|null $country
 * @property string $city
 * @property string|null $timezone
 * @property string $street
 * @property string $building
 * @property string|null $block
 * @property string|null $apartment
 * @property string|null $floor
 * @property string|null $intercom
 * @property string|null $comment
 * @property mixed $main
 * @property string|null $partner_name
 * @property float $partner_discount
 * @property float $lng
 * @property float $lat
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $addressable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @property-read mixed $address_type
 * @property-read mixed $text
 * @property-read \App\Models\Restaurant $restaurant
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Address onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAddressableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAddressableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereApartment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereBlock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereBuilding($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereFloor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereIntercom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address wherePartnerDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address wherePartnerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereTextEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereTextRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Address withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Address withoutTrashed()
 * @mixin \Eloquent
 */
class Address extends Model
{
	use SoftDeletes;

	const DEFAULT_CITY = 'Москва';
	const NOT_SPECIFY = 'not_specify';
	const ADDRESS_TYPES = [
		Guest::class      => 'Гость',
		Event::class      => 'Событие',
		Partner::class    => 'Партнер',
		Restaurant::class => 'Ресторан',
		self::NOT_SPECIFY => 'Не определен',
	];

    protected $fillable = [
	    'addressable_id',
	    'addressable_type',
	    'text_ru',
	    'text_en',
        'country',
        'city',
        'timezone',
        'street',
        'building',
        'block',
        'apartment',
        'floor',
        'intercom',
        'comment',
	    'main',
	    'partner_name',
	    'partner_discount',
        'lng',
        'lat',
    ];

    protected $hidden = [
	    'addressable_id',
	    'addressable_type',
	    'created_at',
	    'updated_at',
	    'deleted_at',
	    'pivot',
	    'external_id',
	    'temp_id',
    ];

    protected $casts = [
	    'addressable_id'   => 'int',
	    'lng'              => 'float',
	    'lat'              => 'float',
	    'main'             => 'bool,',
	    'partner_discount' => 'float',
    ];

    protected $appends = ['address_type'];

    public function restaurant()
    {
        return $this->hasOne(Restaurant::class);
    }

    public function delivery_orders()
    {
        return $this->hasMany(DeliveryOrder::class);
    }

    public function addressable()
    {
    	return $this->morphTo();
    }

	public function getTextAttribute()
	{
		$text = 'text_' . request()->get('language');

		return $this->$text;
	}

	public function getAddressTypeAttribute()
	{
		$addressable_type = $this->addressable_type ?: Address::NOT_SPECIFY;

		return self::ADDRESS_TYPES[$addressable_type];
	}
}
