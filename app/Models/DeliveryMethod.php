<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DeliveryMethod
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryMethod onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryMethod withoutTrashed()
 * @mixin \Eloquent
 */
class DeliveryMethod extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'name',
	    'price',
    ];

    protected $hidden = [
	    'external_id',
	    'temp_id',
        'created_at',
	    'updated_at',
	    'deleted_at',
    ];

	protected $casts = [
		'price'  => 'float',
    ];
	
	public function delivery_orders()
	{
		return $this->hasMany(DeliveryOrder::class);
	}
}
