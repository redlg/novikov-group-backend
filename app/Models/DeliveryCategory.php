<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DeliveryCategory
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property int $delivery_category_id
 * @property int $restaurant_id
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryDish[] $dishes
 * @property-read mixed $name
 * @property-read \App\Models\Restaurant $restaurant
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryCategory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereDeliveryCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryCategory withoutTrashed()
 * @mixin \Eloquent
 */
class DeliveryCategory extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name_ru',
        'name_en',
	    'delivery_category_id',
	    'restaurant_id',
    ];

    protected $hidden = [
    	'created_at',
	    'updated_at',
	    'deleted_at',
	    'external_id',
	    'temp_id',
	    'restaurant',
	    'name',
    ];

    protected $casts = [
	    'delivery_category_id' => 'int',
	    'restaurant'           => 'int',
    ];

    protected $appends = ['name'];

//    protected $with = ['restaurant'];

    public function dishes()
    {
    	return $this->hasMany(DeliveryDish::class);
    }

    public function restaurant()
    {
    	return $this->belongsTo(Restaurant::class);
    }

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}
}
