<?php

namespace App\Models;

use App\Scopes\RestaurantIdScope;
use App\Traits\AddField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Transaction
 *
 * @property int $id
 * @property int $operation_type_id
 * @property int $restaurant_id
 * @property int $payment_method_id
 * @property int $card_id
 * @property string|null $card_type
 * @property int $operation_id
 * @property string|null $card_input_method
 * @property float $sum
 * @property float $discount_sum
 * @property string $check_number
 * @property string $cashbox_number
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int $guest_id
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $card
 * @property-read \App\Models\Certificate|null $certificate
 * @property-read \App\Models\DiscountCard|null $discount_card
 * @property-read \App\Models\Guest|null $guest
 * @property-read \App\Models\Operation|null $operation
 * @property-read \App\Models\PaymentMethod $payment_method
 * @property-read \App\Models\Restaurant $restaurant
 * @property-read \App\Models\OperationType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Transaction onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCardInputMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCashboxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCheckNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereDiscountSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereOperationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereOperationTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Transaction withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Transaction withoutTrashed()
 * @mixin \Eloquent
 */
class Transaction extends Model
{
	use SoftDeletes;
	use AddField;

	CONST PHYSICAL_ID = 'card';
	CONST PHYSICAL_NAME = 'Физическая карта';
	CONST VIRTUAL_ID = 'mobile';
	CONST VIRTUAL_NAME = 'Мобильное приложение';
	CONST CARD_INPUT_METHODS = [[
		'id'   => self::PHYSICAL_ID,
		'name' => self::PHYSICAL_NAME,
	], [
		'id'   => self::VIRTUAL_ID,
		'name' => self::VIRTUAL_NAME,
	]];

	protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = [
        'operation_type_id',
        'restaurant_id',
        'payment_method_id',
	    'operation_id',
	    'card_id',
	    'card_type',
	    'guest_id',
        'card_input_method',
        'sum',
        'discount_sum',
        'check_number',
        'cashbox_number',
    ];

    protected $casts = [
	    'operation_type_id' => 'int',
	    'restaurant_id'     => 'int',
	    'payment_method_id' => 'int',
	    'card_id'           => 'int',
	    'guest_id'          => 'int',
	    'operation_id'      => 'int',
	    'sum'               => 'float',
	    'discount_sum'      => 'float',
    ];

    protected $with = ['type', 'payment_method', 'card'];

	protected $hidden = [
		'deleted_at',
	];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(new RestaurantIdScope);
	}

    public function type()
    {
    	return $this->belongsTo(OperationType::class, 'operation_type_id');
    }

    public function restaurant()
    {
    	return $this->belongsTo(Restaurant::class);
    }

	public function card()
	{
		return $this->morphTo();
	}

	public function discount_card()
	{
		return $this->belongsTo(DiscountCard::class, 'card_id')
			->whereCardType(DiscountCard::class);
	}

	public function certificate()
	{
		return $this->belongsTo(Certificate::class, 'card_id')
			->whereCardType(Certificate::class);
	}

	public function guest()
	{
		return $this->belongsTo(Guest::class);
	}

    public function payment_method()
    {
    	return $this->belongsTo(PaymentMethod::class);
    }

    public function operation()
    {
	    return $this->belongsTo(Operation::class);
    }
}
