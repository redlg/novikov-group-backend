<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DiscountCardType
 *
 * @property int $id
 * @property string $name
 * @property float $discount
 * @property int $life_time
 * @property string|null $external_id
 * @property string|null $rkeeper_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CardOrder[] $card_orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DiscountCard[] $cards
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DiscountCardType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereLifeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereRkeeperId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCardType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DiscountCardType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DiscountCardType withoutTrashed()
 * @mixin \Eloquent
 */
class DiscountCardType extends Model
{
	use SoftDeletes;

	const DEFAULT_CARD_TYPE_UUID = '0F76450B-4028-83DF-010F-76450BF00001'; //Скидка 10%

    protected $fillable = [
        'name',
        'discount',
        'life_time',
    ];

    protected $casts = [
        'discount'  => 'float',
        'life_time' => 'int',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
		'external_id',
	];

    public function cards()
    {
        return $this->hasMany(DiscountCard::class);
    }

	public function card_orders()
	{
		return $this->morphMany(CardOrder::class, 'card_type');
	}
}
