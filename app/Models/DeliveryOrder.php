<?php

namespace App\Models;

use App\Scopes\RestaurantIdScope;
use App\Traits\AddField;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DeliveryOrder
 *
 * @property int $id
 * @property int $guest_id
 * @property int $address_id
 * @property int $restaurant_id
 * @property int $delivery_payment_method_id
 * @property int $delivery_method_id
 * @property int $discount_card_id
 * @property int $certificate_id
 * @property \Carbon\Carbon|null $delivered_at
 * @property \Carbon\Carbon|null $canceled_at
 * @property \Carbon\Carbon|null $completed_at
 * @property int $number_of_persons
 * @property float $sum
 * @property string|null $comment
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $external_id
 * @property-read \App\Models\Address|null $address
 * @property-read \App\Models\Certificate|null $certificate
 * @property-read \App\Models\DeliveryMethod $delivery_method
 * @property-read \App\Models\DeliveryPaymentMethod $delivery_payment_method
 * @property-read \App\Models\DiscountCard|null $discount_card
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryDish[] $dishes
 * @property-read mixed $discount_sum
 * @property-read mixed $restaurant_name
 * @property-read mixed $restaurant_phone
 * @property-read mixed $status
 * @property-read mixed $status_name
 * @property-read \App\Models\Guest $guest
 * @property-read \App\Models\Restaurant $restaurant
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryOrder onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereCanceledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereCertificateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereCompletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereDeliveredAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereDeliveryMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereDeliveryPaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereDiscountCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereNumberOfPersons($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryOrder withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryOrder withoutTrashed()
 * @mixin \Eloquent
 */
//todo add total_sum field (calculate the sum with discount sum)
class DeliveryOrder extends Model
{
	use SoftDeletes;
	use AddField;

	const STATUS_IN_PROGRESS = 'progress';
	const STATUS_IN_PROGRESS_NAME = 'Выполняется';
	const STATUS_CANCELED = 'canceled';
	const STATUS_CANCELED_NAME = 'Отменен';
	const STATUS_COMPLETED = 'completed';
	const STATUS_COMPLETED_NAME = 'Выполнен';
	const STATUSES = [[
	    'id' => self::STATUS_IN_PROGRESS,
        'name' => self::STATUS_IN_PROGRESS_NAME,
    ], [
        'id' => self::STATUS_CANCELED,
        'name' => self::STATUS_CANCELED_NAME,
    ], [
        'id' => self::STATUS_COMPLETED,
        'name' => self::STATUS_COMPLETED_NAME,
    ]];

	protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = [
        'guest_id',
        'address_id',
        'restaurant_id',
	    'delivery_method_id',
        'delivery_payment_method_id',
        'discount_card_id',
        'certificate_id',
        'delivered_at',
        'canceled_at',
	    'completed_at',
        'number_of_persons',
        'comment',
    ];

    protected $casts = [
	    'guest_id'                   => 'int',
	    'address_id'                 => 'int',
	    'restaurant_id'              => 'int',
	    'delivery_payment_method_id' => 'int',
	    'delivery_method_id'         => 'int',
	    'discount_card_id'           => 'int',
	    'certificate_id'             => 'int',
	    'number_of_persons'          => 'int',
	    'sum'                        => 'float',
    ];

    protected $dates = [
        'delivered_at',
        'canceled_at',
	    'completed_at',
    ];

    protected $hidden = [
        'deleted_at',
    	'discount_card',
	    'certificate',
	    'restaurant',
    ];

	protected $appends = ['discount_sum', 'status', 'status_name'];

    protected $with = ['guest', 'address', 'discount_card', 'certificate', 'delivery_payment_method', 'delivery_method', 'dishes'];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(new RestaurantIdScope);
	}

    public function guest()
    {
        return $this->belongsTo(Guest::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class);
    }

    public function delivery_payment_method()
    {
        return $this->belongsTo(DeliveryPaymentMethod::class);
    }

	public function delivery_method()
	{
		return $this->belongsTo(DeliveryMethod::class);
	}

    public function discount_card()
    {
        return $this->belongsTo(DiscountCard::class);
    }

    public function certificate()
    {
        return $this->belongsTo(Certificate::class);
    }

    public function dishes()
    {
        return $this->belongsToMany(DeliveryDish::class)->withTimestamps()->withPivot('count');
    }

    public function getDiscountSumAttribute()
    {
    	if ($this->discount_card_id) {
    		$discount = $this->discount_card->type->discount ?: 10; //если у типа дисконтной карты не задан процент скидки, то выставляем его по умолчанию
		    return $this->sum / $discount;
	    } else {
    		return 0;
	    }
    }

    public function getStatusAttribute()
    {
		if ($this->canceled_at) {
			return $this::STATUS_CANCELED;
		} elseif ($this->completed_at) {
			return $this::STATUS_COMPLETED;
		} else {
			return $this::STATUS_IN_PROGRESS;
		}
    }

    public function getStatusNameAttribute()
    {
        if ($this->canceled_at) {
            return $this::STATUS_CANCELED_NAME;
        } elseif ($this->completed_at) {
            return $this::STATUS_COMPLETED_NAME;
        } else {
            return $this::STATUS_IN_PROGRESS_NAME;
        }
    }

	public function getRestaurantNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->restaurant->$name;
	}

	public function getRestaurantPhoneAttribute()
	{
		return $this->restaurant->delivery_phone;
	}

    public function complete($datetime)
    {
        //завершить можно только неотмененный и невыполненный заказ
        if (is_null($this->canceled_at) && is_null($this->completed_at)) {
            $date = $datetime ? Carbon::createFromFormat('Y-m-d H:i', $datetime) : Carbon::now();
            $this->completed_at = $date;
            $this->save();
        }
    }

    public function cancel($message)
    {
        //отменить можно только неотмененный и невыполненный заказ
        if (is_null($this->canceled_at) && is_null($this->completed_at)) {
            $this->comment = $message;
            $this->canceled_at = Carbon::now();
            $this->save();
        }
    }
}
