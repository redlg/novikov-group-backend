<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Tag
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $name
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tag onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tag whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tag withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Tag withoutTrashed()
 * @mixin \Eloquent
 */
class Tag extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name_ru',
	    'name_en',
    ];

    protected $hidden = [
	    'created_at',
	    'updated_at',
    	'deleted_at',
	    'pivot',
	    'name',
    ];

	protected $appends = ['name'];

	public function delivery_dishes()
	{
		return $this->morphedByMany(DeliveryDish::class, 'taggable')->withTimestamps();
	}

    public function restaurants()
    {
        return $this->morphedByMany(Restaurant::class, 'taggable')->withTimestamps();
    }

    public function partners()
    {
        return $this->morphedByMany(Partner::class, 'taggable')->withTimestamps();
    }

    public function events()
    {
        return $this->morphedByMany(Event::class, 'taggable')->withTimestamps();
    }

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}
}
