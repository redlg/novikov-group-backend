<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Reservation
 *
 * @property int $id
 * @property int $guest_id
 * @property int $restaurant_id
 * @property \Carbon\Carbon $reserved_at
 * @property int $number_of_persons
 * @property string|null $comment
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Guest $guest
 * @property-read \App\Models\Restaurant $restaurant
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reservation onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereNumberOfPersons($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereReservedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reservation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reservation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Reservation withoutTrashed()
 * @mixin \Eloquent
 */
class Reservation extends Model
{
	use SoftDeletes;

	protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = [
        'guest_id',
        'restaurant_id',
        'reserved_at',
        'number_of_persons',
        'comment',
    ];

    protected $casts = [
        'guest_id'          => 'int',
        'restaurant_id'     => 'int',
        'number_of_persons' => 'int',
    ];

    protected $dates = [
        'reserved_at',
    ];

    protected $with = ['guest', 'restaurant'];

    public function guest()
    {
        return $this->belongsTo('App\Models\Guest');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }
}
