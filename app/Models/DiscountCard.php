<?php

namespace App\Models;

use App\Models\Abstracts\Card;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DiscountCard
 *
 * @property int $id
 * @property int $card_type_id
 * @property int $issuer_id
 * @property int $contractor_id
 * @property int $guest_id
 * @property int $restaurant_id
 * @property int $user_id
 * @property string $number
 * @property string|null $codeword
 * @property \Carbon\Carbon|null $started_at
 * @property \Carbon\Carbon|null $ended_at
 * @property \Carbon\Carbon|null $first_used_at
 * @property \Carbon\Carbon|null $blocked_at
 * @property string $status
 * @property bool $virtual
 * @property string|null $external_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\LegalEntity|null $contractor
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @property-read mixed $status_name
 * @property-read \App\Models\Guest|null $guest
 * @property-read \App\Models\LegalEntity|null $issuer
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Operation[] $operations
 * @property-read \App\Models\Restaurant|null $restaurant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @property-read \App\Models\DiscountCardType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Abstracts\Card addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DiscountCard onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereBlockedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereCardTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereCodeword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereContractorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereFirstUsedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereIssuerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCard whereVirtual($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DiscountCard withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DiscountCard withoutTrashed()
 * @mixin \Eloquent
 */
class DiscountCard extends Card
{
	use SoftDeletes;

	public function __construct(array $attributes = [])
	{
		$this->fillable = array_merge($this->fillable, [
			'codeword',
			'first_used_at',
            'virtual',
		]);
		$this->dates = array_merge($this->dates, [
			'first_used_at',
		]);
        $this->casts = array_merge($this->casts, [
            'virtual' => 'boolean',
        ]);
		parent::__construct($attributes);
	}

    public function type()
    {
        return $this->belongsTo(DiscountCardType::class, 'card_type_id');
    }
}