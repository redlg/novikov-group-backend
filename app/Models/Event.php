<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Event
 *
 * @property int $id
 * @property int $event_type_id
 * @property int $logo_id
 * @property string $name_ru
 * @property string|null $name_en
 * @property string $announce_ru
 * @property string|null $announce_en
 * @property string $description_ru
 * @property string|null $description_en
 * @property string|null $phone
 * @property \Carbon\Carbon $started_at
 * @property \Carbon\Carbon|null $ended_at
 * @property bool $repeat
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $addresses
 * @property-read mixed $announce
 * @property-read mixed $description
 * @property-read mixed $link
 * @property-read mixed $name
 * @property-read mixed $restaurant_ids
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read \App\Models\Image $logo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Partner[] $partners
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Restaurant[] $restaurants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TicketType[] $ticket_types
 * @property-read \App\Models\EventType $type
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereAnnounceEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereAnnounceRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereEndedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereEventTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereLogoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereRepeat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Event withoutTrashed()
 * @mixin \Eloquent
 */
class Event extends Model
{
	use SoftDeletes;

	protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = [
        'event_type_id',
        'logo_id',
        'name_ru',
        'name_en',
        'phone',
        'announce_ru',
        'announce_en',
        'description_ru',
        'description_en',
        'started_at',
        'ended_at',
        'repeat',
    ];

    protected $hidden = [
    	'created_at',
	    'updated_at',
	    'deleted_at',
	    'external_id',
	    'temp_id',
	    'pivot',
    ];

    protected $casts = [
        'event_type_id' => 'int',
        'logo_id'       => 'int',
        'repeat'        => 'bool',
    ];

    protected $dates = [
    	'started_at',
	    'ended_at',
    ];

    protected $appends = ['link'];

    protected $with = ['logo', 'type', 'ticket_types', 'tags'];

    public function type()
    {
        return $this->belongsTo(EventType::class, 'event_type_id');
    }

    public function logo()
    {
        return $this->belongsTo(Image::class);
    }

	public function tags()
	{
		return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
	}

    public function getAddressesAttribute()
    {
    	return $this->restaurants->map(function(Restaurant $restaurant) {
    		return $restaurant->address;
	    });
    }

//    public function addresses()
//    {
//	    return $this->morphMany(Address::class, 'addressable');
//    }

    public function images()
    {
        return $this->belongsToMany(Image::class)->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class)->withTimestamps();
    }

    public function restaurants()
    {
        return $this->belongsToMany(Restaurant::class)->withTimestamps();
    }

    public function partners()
    {
        return $this->belongsToMany(Partner::class)->withTimestamps();
    }

    public function ticket_types()
    {
        return $this->hasMany(TicketType::class);
    }

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}

	public function getDescriptionAttribute()
	{
		$description = 'description_' . request()->get('language');

		return $this->$description;
	}

	public function getAnnounceAttribute()
	{
		$announce = 'announce_' . request()->get('language');

		return $this->$announce;
	}

	public function getRestaurantIdsAttribute()
	{
		return $this->restaurants->pluck('id');
	}

	public function getLinkAttribute()
    {
        return $this->external_id ? 'http://www.novikovgroup.ru/events/events_' . $this->external_id . '.html' : null;
    }
}
