<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DeliveryPaymentMethod
 *
 * @property int $id
 * @property string $name
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryPaymentMethod onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryPaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryPaymentMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryPaymentMethod whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryPaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryPaymentMethod whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryPaymentMethod whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryPaymentMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryPaymentMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryPaymentMethod withoutTrashed()
 * @mixin \Eloquent
 */
class DeliveryPaymentMethod extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'name',
    ];

    protected $hidden = [
        'external_id',
	    'temp_id',
	    'created_at',
	    'updated_at',
	    'deleted_at',
    ];

	public function delivery_orders()
	{
		return $this->hasMany(DeliveryOrder::class);
	}
}
