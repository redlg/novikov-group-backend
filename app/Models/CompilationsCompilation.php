<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CompilationsCompilation
 *
 * @property int $id
 * @property string $name
 * @property string $screen_type
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $compilations
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompilationsCompilation onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompilationsCompilation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompilationsCompilation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompilationsCompilation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompilationsCompilation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompilationsCompilation whereScreenType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CompilationsCompilation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompilationsCompilation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CompilationsCompilation withoutTrashed()
 * @mixin \Eloquent
 */
class CompilationsCompilation extends Model
{
    use SoftDeletes;

    const SCREEN_TYPE_DELIVERY_ID = 'delivery';
    const SCREEN_TYPE_DELIVERY_NAME = 'Экран доставки';
    const SCREEN_TYPES = [[
	    'id'   => self::SCREEN_TYPE_DELIVERY_ID,
	    'name' => self::SCREEN_TYPE_DELIVERY_NAME,
    ]];

    protected $fillable = [
    	'name',
    ];

	protected $hidden = [
		'delivery_dishes_compilations',
		'restaurants_compilations',
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $with = ['delivery_dishes_compilations', 'restaurants_compilations'];

    protected $appends = ['compilations'];

	public function delivery_dishes_compilations()
	{
		return $this->morphedByMany(DeliveryDishesCompilation::class, 'compilable')
			->withTimestamps()
			->withPivot(['sort', 'geo']);
	}

	public function restaurants_compilations()
	{
		return $this->morphedByMany(RestaurantsCompilation::class, 'compilable')
			->withTimestamps()
			->withPivot(['sort', 'geo']);
	}

	public function getCompilationsAttribute()
	{
		$dishes_compilations = $this->delivery_dishes_compilations->each(function(DeliveryDishesCompilation $compilation) {
			$compilation->append([
				'dish_ids',
				'type',
			]);
			$compilation->tags
				->makeHidden([
					'delivery_dishes',
					'name_ru',
					'name_en',
				])->makeVisible([
					'name',
				]);
		})->toArray();
		$restaurants_compilations = $this->restaurants_compilations->each(function(RestaurantsCompilation $compilation) {
			$compilation->append([
				'restaurant_ids',
				'type',
			]);
			$compilation->tags
				->makeHidden([
					'restaurants',
					'name_ru',
					'name_en',
				])->makeVisible([
					'name',
				]);
		})->toArray();
		$compilations = array_merge($dishes_compilations, $restaurants_compilations);

		return collect($compilations)->sortBy(function($compilation) {
			return $compilation['pivot']['sort'];
		})->values();
	}
}
