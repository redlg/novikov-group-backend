<?php

namespace App\Models;

use App\Traits\AddField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Partner
 *
 * @property int $id
 * @property int $legal_entity_id
 * @property int $logo_id
 * @property string $name_ru
 * @property string $name_en
 * @property string|null $description_ru
 * @property string|null $description_en
 * @property string|null $link
 * @property string|null $phone
 * @property float $discount
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property bool $general
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Address[] $addresses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read mixed $description
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read \App\Models\LegalEntity|null $legal_entity
 * @property-read \App\Models\Image $logo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereDiscount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereGeneral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereLegalEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereLogoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Partner whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Partner withoutTrashed()
 * @mixin \Eloquent
 */
class Partner extends Model
{
	use SoftDeletes;
	use AddField;

    protected $fillable = [
        'legal_entity_id',
        'logo_id',
        'name_ru',
        'name_en',
        'description_ru',
        'description_en',
        'phone',
        'discount',
	    'link',
        'general',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
		'external_id',
		'temp_id',
	];

    protected $casts = [
        'legal_entity_id' => 'int',
        'logo_id'         => 'int',
        'discount'        => 'float',
        'general'         => 'boolean',
    ];

    protected $with = ['logo', 'tags'];

    public function legal_entity()
    {
        return $this->belongsTo(LegalEntity::class);
    }

    public function logo()
    {
        return $this->belongsTo(Image::class);
    }

	public function tags()
	{
		return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
	}

	public function addresses()
	{
		return $this->morphMany(Address::class, 'addressable');
	}

    public function images()
    {
        return $this->belongsToMany(Image::class)->withTimestamps();
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class)->withTimestamps();
    }

    public function events()
    {
        return $this->belongsToMany(Event::class)->withTimestamps();
    }

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}

	public function getDescriptionAttribute()
	{
		$description = 'description_' . request()->get('language');

		return $this->$description;
	}
}
