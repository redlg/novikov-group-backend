<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\OperationType
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Operation[] $operations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OperationType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OperationType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OperationType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\OperationType withoutTrashed()
 * @mixin \Eloquent
 */
class OperationType extends Model
{
	use SoftDeletes;

	/* Operation types */
	const EVENT = 'event';
	const TRANSACTION = 'transaction';

	/* Operation names */
	const TRANSFER_TO_THE_RESTAURANT = 'Передача ресторану';
	const ACTIVATION = 'Активация';
	const BLOCKING_EXPIRED = 'Блокировка - истек срок действия';
	const BLOCKING_MANUALLY = 'Блокировка - вручную';
	const BLOCKING_SERVICE = 'Блокировка - услуга оказана';
	const UNBLOCKING = 'Разблокировка';
	const ROLLING = 'Прокатка';
	const DISCOUNT = 'Скидка';
	const DISCOUNT_CANCEL = 'Отмена скидки';
	const PAYMENT = 'Оплата';
	const PAYMENT_CANCEL = 'Отмена оплаты';

    protected $fillable = [
        'name',
    ];

    protected $hidden = [
        'deleted_at',
	    'created_at',
	    'updated_at',
	    'type',
    ];

    public function transactions()
    {
    	return $this->hasMany(Transaction::class);
    }

    public function operations()
    {
    	return $this->hasMany(Operation::class);
    }
}
