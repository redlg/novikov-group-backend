<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Kitchen
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Restaurant[] $restaurants
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Kitchen onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kitchen whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kitchen whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kitchen whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kitchen whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kitchen whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kitchen whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Kitchen withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Kitchen withoutTrashed()
 * @mixin \Eloquent
 */
class Kitchen extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name_ru',
	    'name_en',
    ];

    protected $hidden = [
	    'created_at',
	    'updated_at',
	    'deleted_at',
	    'pivot',
	    'name',
    ];

	protected $appends = ['name'];

    public function restaurants()
    {
        return $this->belongsToMany(Restaurant::class)->withTimestamps();
    }

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}
}
