<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\LegalEntityType
 *
 * @property int $id
 * @property string $name
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\LegalEntity[] $legal_entities
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LegalEntityType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntityType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntityType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntityType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntityType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LegalEntityType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LegalEntityType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\LegalEntityType withoutTrashed()
 * @mixin \Eloquent
 */
class LegalEntityType extends Model
{
	use SoftDeletes;

	const ISSUER = 'Эмитент';
	const COUNTERPARTY = 'Контр-агент';
	const RESTAURANT = 'Ресторан';
	const PARTNER = 'Партнер';

    protected $fillable = [
        'name',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

    public function legal_entities()
    {
        return $this->hasMany(LegalEntity::class);
    }
}
