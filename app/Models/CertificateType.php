<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CertificateType
 *
 * @property int $id
 * @property string $name
 * @property float $sum
 * @property int $life_time
 * @property bool $reusable
 * @property string|null $external_id
 * @property string|null $rkeeper_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CardOrder[] $card_orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Certificate[] $certificates
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CertificateType onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereLifeTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereReusable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereRkeeperId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereSum($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CertificateType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CertificateType withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CertificateType withoutTrashed()
 * @mixin \Eloquent
 */
class CertificateType extends Model
{
	use SoftDeletes;

	const LITE = 'Сертификат Lite';
	const STANDARD = 'Сертификат Standard';
	const CLASSIC = 'Сертификат Classic';
	const PLATINUM = 'Сертификат Platinum';
	const EXCLUSIVE = 'Сертификат Exclusive';

    protected $fillable = [
        'name',
        'sum',
        'life_time',
        'reusable',
    ];

    protected $casts = [
        'sum'       => 'float',
        'life_time' => 'int',
        'reusable'  => 'bool',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
		'external_id',
		'rkeeper_id',
	];

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }

	public function card_orders()
	{
		return $this->morphMany(CardOrder::class, 'card_type');
	}
}
