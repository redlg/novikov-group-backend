<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $path
 * @property string $name
 * @property string $type
 * @property bool $confirmed
 * @property string|null $external_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Chef $chef
 * @property-read \App\Models\DeliveryDish $delivery_dish
 * @property-read \App\Models\Event $event
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read mixed $link
 * @property-read \App\Models\Partner $partner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Partner[] $partners
 * @property-read \App\Models\Project $project
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Project[] $projects
 * @property-read \App\Models\Restaurant $restaurant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Restaurant[] $restaurants
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Image whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Image extends Model
{
    const LOGO = 'logo';
    const PHOTO = 'photo';
    const TEMPORARY_ID = 'temporary_id';

    protected $fillable = [
        'name',
        'path',
        'type',
	    'confirmed',
	    'external_id',
    ];

    protected $hidden = [
	    'created_at',
	    'updated_at',
	    'confirmed',
	    'external_id',
	    'type',
	    'path',
	    'pivot',
    ];

    protected $casts = [
    	'confirmed' => 'bool',
    ];

    protected $appends = ['link'];

    public function delivery_dish()
    {
        return $this->hasOne(DeliveryDish::class);
    }

    public function chef()
    {
        return $this->hasOne(Chef::class, 'logo_id');
    }

    public function restaurant()
    {
        return $this->hasOne(Restaurant::class, 'logo_id');
    }

    public function restaurants()
    {
        return $this->belongsToMany(Restaurant::class);
    }

    public function partner()
    {
        return $this->hasOne(Partner::class, 'logo_id');
    }

    public function partners()
    {
        return $this->belongsToMany(Partner::class);
    }

    public function project()
    {
        return $this->hasOne(Project::class, 'logo_id');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'logo_id');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class);
    }

    public function confirm()
    {
        if ($this->confirmed === false) {
            $this->confirmed = true;
            $this->saveOrFail();
        }
    }

	public function refuse()
	{
		if ($this->confirmed === true) {
			$this->confirmed = false;
			$this->saveOrFail();
		}
	}

    public function getLinkAttribute()
    {
	    if (Storage::exists($this->path)) {
		    return Storage::url($this->path);
	    } else {
	    	return null;
	    }
    }
}
