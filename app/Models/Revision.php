<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Revision
 *
 * @property int $id
 * @property string $name
 * @property int $value
 * @property \Carbon\Carbon|null $date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Revision whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Revision whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Revision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Revision whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Revision whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Revision whereValue($value)
 * @mixin \Eloquent
 */
class Revision extends Model
{
	const CERTIFICATES = 'certificates';
	const DISCOUNT_CARDS = 'discount_cards';
	const NOVIKOV_RESTAURANTS_RU = 'novikov_restaurants_ru';
	const NOVIKOV_RESTAURANTS_EN = 'novikov_restaurants_en';
	const NOVIKOV_DELIVERY_DISHES = 'novikov_delivery_dishes';
	const NOVIKOV_EVENTS = 'novikov_events';
	const NOVIKOV_PROJECTS = 'novikov_projects';

	protected $casts = [
		'value' => 'int',
	];

	protected $dates = ['date'];
}
