<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Device
 *
 * @property int $id
 * @property int $guest_id
 * @property string $device_id
 * @property string $token
 * @property string|null $phone
 * @property string|null $code
 * @property \Carbon\Carbon|null $code_valid_until
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryDish[] $delivery_dishes
 * @property-read mixed $favorite_dish_ids
 * @property-read mixed $favorite_restaurant_ids
 * @property-read \App\Models\Guest|null $guest
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Restaurant[] $restaurants
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereCodeValidUntil($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Device whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Device extends Model
{
	protected $fillable = [
		'device_id',
	];

	protected $casts = [
		'guest_id' => 'int',
	];

	protected $hidden = [
		'restaurants',
		'delivery_dishes',
		'phone',
		'code',
		'code_valid_until',
		'created_at',
		'updated_at',
	];

	protected $dates = ['code_valid_until'];

	protected $with = ['restaurants', 'delivery_dishes', 'guest'];

	protected $appends = ['favorite_restaurant_ids', 'favorite_dish_ids'];

	public function restaurants()
	{
		return $this->belongsToMany(Restaurant::class);
	}

	public function delivery_dishes()
	{
		return $this->belongsToMany(DeliveryDish::class);
	}

	public function guest()
	{
		return $this->belongsTo(Guest::class);
	}

	public function getFavoriteRestaurantIdsAttribute()
	{
		return $this->restaurants->pluck('id');
	}

	public function getFavoriteDishIdsAttribute()
	{
		return $this->delivery_dishes->pluck('id');
	}
}
