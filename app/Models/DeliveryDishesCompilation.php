<?php

namespace App\Models;

use App\Traits\AddField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\DeliveryDishesCompilation
 *
 * @property int $id
 * @property int $restaurant_id
 * @property string $name
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompilationsCompilation[] $compilations_compilations
 * @property-read mixed $dish_ids
 * @property-read mixed $type
 * @property-read \App\Models\Restaurant $restaurant
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDishesCompilation addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryDishesCompilation onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDishesCompilation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDishesCompilation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDishesCompilation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDishesCompilation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDishesCompilation whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDishesCompilation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryDishesCompilation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryDishesCompilation withoutTrashed()
 * @mixin \Eloquent
 */
class DeliveryDishesCompilation extends Model
{
	use SoftDeletes;
	use AddField;

    protected $fillable = [
	    'restaurant_id',
    	'name',
    ];

    protected $casts = [
    	'restaurant_id' => 'int',
    ];

    protected $hidden = [
        'created_at',
	    'updated_at',
	    'deleted_at',
	    'restaurant',
    ];

    protected $with = ['tags', 'restaurant'];

    public function tags()
    {
    	return $this->belongsToMany(Tag::class)->withTimestamps();
    }

	public function compilations_compilations()
	{
		return $this->morphToMany(CompilationsCompilation::class, 'compilable');
	}

    public function restaurant()
    {
    	return $this->belongsTo(Restaurant::class);
    }

	public function getDishIdsAttribute()
	{
		$ids = [];
		$categories = $this->restaurant->load(['delivery_categories'])->delivery_categories->pluck('id');
		$this->tags->each(function(Tag $tag) use (&$ids, $categories) {
			$ids[] = $tag->delivery_dishes->whereIn('delivery_category_id', $categories)->pluck('id');
		});

		return collect($ids)->flatten()->unique()->values();
	}

	public function getTypeAttribute()
	{
		return self::class;
	}
}
