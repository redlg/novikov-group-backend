<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\RestaurantsCompilation
 *
 * @property int $id
 * @property string $name
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CompilationsCompilation[] $compilations_compilations
 * @property-read mixed $restaurant_ids
 * @property-read mixed $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RestaurantsCompilation onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RestaurantsCompilation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RestaurantsCompilation whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RestaurantsCompilation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RestaurantsCompilation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RestaurantsCompilation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RestaurantsCompilation withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\RestaurantsCompilation withoutTrashed()
 * @mixin \Eloquent
 */
class RestaurantsCompilation extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'name',
    ];

    protected $with = ['tags'];

    protected $hidden = [
        'created_at',
        'updated_at',
	    'deleted_at',
    ];

	public function tags()
	{
		return $this->belongsToMany(Tag::class)->withTimestamps();
	}

	public function compilations_compilations()
	{
		return $this->morphToMany(CompilationsCompilation::class, 'compilable');
	}

	public function getRestaurantIdsAttribute()
	{
		$ids = [];
		$this->tags->each(function(Tag $tag) use (&$ids) {
			$ids[] = $tag->restaurants->pluck('id');
		});

		return collect($ids)->flatten()->unique()->values();
	}

	public function getTypeAttribute()
	{
		return self::class;
	}
}
