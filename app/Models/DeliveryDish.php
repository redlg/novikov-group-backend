<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * App\Models\DeliveryDish
 *
 * @property int $id
 * @property int $delivery_category_id
 * @property int $image_id
 * @property string $name_ru
 * @property string $name_en
 * @property float $price
 * @property string|null $description_ru
 * @property string|null $description_en
 * @property string|null $composition_ru
 * @property string|null $composition_en
 * @property string|null $weight
 * @property string|null $volume
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\DeliveryCategory $delivery_category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @property-read mixed $composition
 * @property-read mixed $count
 * @property-read mixed $delivery_category_name
 * @property-read mixed $description
 * @property-read mixed $favorite
 * @property-read mixed $name
 * @property-read mixed $restaurant_attributes
 * @property-read mixed $title_image
 * @property-read mixed $with_this_ordered
 * @property-read \App\Models\Image|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryDish onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereCompositionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereCompositionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereDeliveryCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereVolume($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DeliveryDish whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryDish withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DeliveryDish withoutTrashed()
 * @mixin \Eloquent
 */
class DeliveryDish extends Model
{
	use SoftDeletes;

	const TITLE_IMAGE_PATH = 'upload/resized_images/delivery_dishes/title/';

    protected $fillable = [
        'delivery_category_id',
	    'image_id',
        'name_ru',
        'name_en',
        'price',
        'description_ru',
        'description_en',
        'composition_ru',
        'composition_en',
        'weight',
        'volume',
    ];

    protected $hidden = [
    	'created_at',
	    'updated_at',
	    'deleted_at',
	    'external_id',
	    'temp_id',
	    'delivery_category',
	    'title_image',
	    'favorite',
	    'restaurant_attributes',
	    'with_this_order',
	    'name',
	    'description',
	    'composition',
	    'delivery_category_name',
    ];

	protected $appends = [
		'title_image',
		'favorite',
		'restaurant_attributes',
		'name',
		'description',
		'composition',
		'delivery_category_name',
	];

    protected $casts = [
	    'delivery_category_id' => 'int',
	    'image_id'             => 'int',
	    'price'                => 'float',
    ];

    protected $with = ['delivery_category', 'image'];

    public function delivery_category()
    {
        return $this->belongsTo(DeliveryCategory::class);
    }

	public function tags()
	{
		return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
	}

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function delivery_orders()
    {
        return $this->belongsToMany(DeliveryOrder::class)->withTimestamps()->withPivot('count');
    }

	public function getTitleImageAttribute()
	{
		$images[] = $this->image;

		return make_resize_images(collect($images), self::TITLE_IMAGE_PATH, 500, 500)[0];
	}

	public function getFavoriteAttribute()
	{
		$ids = request()->get('favorite_dish_ids', collect())->toArray();

		return !is_null($ids) && in_array($this->id, $ids);
	}

	public function getRestaurantAttributesAttribute()
	{
		$name = 'name_' . request()->get('language');

		return [
			'id' => $this->delivery_category->restaurant->id,
			'name' => $this->delivery_category->restaurant->$name,
		];
	}

	public function getWithThisOrderedAttribute()
	{
		return $this->delivery_category
			->dishes()
			->inRandomOrder()
			->limit(4)
			->get()
			->makeVisible([
				'favorite',
				'title_image',
				'name',
				'description',
				'composition',
				'delivery_category_name',
				'restaurant_attributes',
			])
			->makeHidden([
				'image',
				'description_ru',
				'description_en',
				'composition_ru',
				'composition_en',
				'name_ru',
				'name_en',
			]);
	}

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return Str::ucfirst(Str::lower($this->$name));
	}

	public function getDescriptionAttribute()
	{
		$description = 'description_' . request()->get('language');

		return $this->$description;
	}

	public function getCompositionAttribute()
	{
		$composition = 'composition_' . request()->get('language');

		return $this->$composition;
	}

	public function getDeliveryCategoryNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->delivery_category->$name;
	}

	public function getCountAttribute()
	{
		return $this->pivot->count;
	}
}
