<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\MenuCategory
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property int $menu_category_id
 * @property int $restaurant_id
 * @property string|null $external_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuCategory[] $categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuDish[] $dishes
 * @property-read \App\Models\Restaurant $restaurant
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MenuCategory onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereMenuCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereRestaurantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MenuCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MenuCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\MenuCategory withoutTrashed()
 * @mixin \Eloquent
 */
class MenuCategory extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'name_ru',
        'name_en',
	    'menu_category_id',
	    'restaurant_id',
    ];

    protected $hidden = [
	    'external_id',
	    'temp_id',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
	    'menu_category_id' => 'int',
	    'restaurant'       => 'int',
    ];

    public function categories()
    {
    	return $this->hasMany(MenuCategory::class);
    }

    public function dishes()
    {
    	return $this->hasMany(MenuDish::class);
    }

    public function restaurant()
    {
    	return $this->belongsTo(Restaurant::class);
    }
}
