<?php

namespace App\Models\Abstracts;

use Illuminate\Database\Eloquent\Model;

abstract class ErrorCode extends Model
{
	/* CRM errors */
	const TOKEN_EXPIRED = 'TOKEN_EXPIRED'; //токен истек
	const TOKEN_INVALID = 'TOKEN_INVALID'; //неправильный токен
	const TOKEN_ABSENT = 'TOKEN_ABSENT'; //что-то не так
	const TOKEN_NOT_FOUND = 'TOKEN_NOT_FOUND'; //токен отсутсвтует
	const AUTH_USER_NOT_FOUND = 'AUTH_USER_NOT_FOUND'; //пользователя с таким email не существует
	const AUTH_PASSWORD_IS_INCORRECT = 'AUTH_PASSWORD_IS_INCORRECT'; //пароль не подходит
	const AUTH_USER_EXISTS = 'AUTH_USER_EXISTS'; //пользователь существует
	const CARD_CREATE_VALIDATION = 'CARD_CREATE_VALIDATION'; //ошибка создания карты - длина номера меньше 9 или карта с таким номер уже существует
	const CARD_CREATE_EXISTS = 'CARD_CREATE_EXISTS'; //ошибка создания карт - карты с такими номерами уже существуют
	const CARD_CREATE_OVER_LIMIT = 'CARD_CREATE_OVER_LIMIT'; //ошибка создания карт - номер карт в выбранном диапазоне превысил максимально возможное значение
	const CARD_CREATE_LESS = 'CARD_CREATE_LESS'; //ошибка создания карт - в выбранном диапазоне можно создать меньше карт, чем требуется
	const FILE_NOT_EXISTS = 'FILE_NOT_EXISTS'; //файл с таким именем не существует
	const PARAMS_IN_REQUEST_ARE_REQUIRED = 'PARAMS_IN_REQUEST_ARE_REQUIRED'; //параметр в запросе обязательный
	const TOO_MANY_RECORDS_FOR_EXPORT = 'TOO_MANY_RECORDS_FOR_EXPORT'; //слишком много записей на экспорт
	const NOT_ENOUGH_PERMISSIONS = 'NOT_ENOUGH_PERMISSIONS'; //недостаточно прав
	const GUEST_EXISTS = 'GUEST_EXISTS'; //такой телефон уже существует
	/* CRM errors end */

	/* Mobile errors */
	/* Mobile errors end*/

	/* RKeeper errors */
	const INVALID_XML = 'INVALID_XML'; //xml не может быть прочитан
	const CARD_NOT_FOUND = 'CARD_NOT_FOUND'; //карты не найдена
	const RESTAURANT_NOT_FOUND = 'RESTAURANT_NOT_FOUND'; //ресторана не найдена
	/* RKeeper errors end */

	const CRM_ERRORS = [[
		'code'        => ErrorCode::TOKEN_EXPIRED,
		'description' => 'Срок действия токена истек.',
	], [
		'code'        => ErrorCode::TOKEN_INVALID,
		'description' => 'Токен недействителен.',
	], [
		'code'        => ErrorCode::TOKEN_NOT_FOUND,
		'description' => 'Токен отсутствует.',
	], [
		'code'        => ErrorCode::TOKEN_ABSENT,
		'description' => 'Что-то не так.',
	], [
		'code'        => ErrorCode::AUTH_USER_NOT_FOUND,
		'description' => 'Пользователя с таким email не существует.',
	], [
		'code'        => ErrorCode::AUTH_PASSWORD_IS_INCORRECT,
		'description' => 'Неверно указан пароль для пользователя.',
	], [
		'code'        => ErrorCode::AUTH_USER_EXISTS,
		'description' => 'Пользователь с такими данными уже зарегестрирован.',
	], [
		'code'        => ErrorCode::CARD_CREATE_VALIDATION,
		'description' => 'Ошибка создания карты - длина номера меньше 9 или карта с таким номер уже существует.',
	], [
		'code'        => ErrorCode::CARD_CREATE_EXISTS,
		'description' => 'Ошибка создания карт - карты с такими номерами уже существуют.',
	], [
		'code'        => ErrorCode::CARD_CREATE_OVER_LIMIT,
		'description' => 'Ошибка создания карт - номер карты в выбранном диапазоне превысил максимально возможное значение.',
	], [
		'code'        => ErrorCode::CARD_CREATE_LESS,
		'description' => 'Ошибка создания карт - в выбранном диапазоне можно создать меньше карт, чем требуется. Возвращает дополнительный массив numbers.',
	], [
		'code'        => ErrorCode::FILE_NOT_EXISTS,
		'description' => 'Файл с таким именем не существует.',
	], [
		'code'        => ErrorCode::TOO_MANY_RECORDS_FOR_EXPORT,
		'description' => 'Слишком много записей на экспорт.',
	], [
		'code'        => ErrorCode::PARAMS_IN_REQUEST_ARE_REQUIRED,
		'description' => 'Возвращает список обязательных параметров в запросе обязательны.',
	], [
		'code'        => ErrorCode::NOT_ENOUGH_PERMISSIONS,
		'description' => 'У пользователя недостаточно прав.',
	], [
        'code'        => ErrorCode::GUEST_EXISTS,
        'description' => 'Гость с таким номером телефона уже существует.',
    ]];

	const RKEERER_ERRORS = [
	[
		'code'        => ErrorCode::INVALID_XML,
		'description' => 'XML строка не может быть прочитана.',
	], [
		'code'        => ErrorCode::CARD_NOT_FOUND,
		'description' => 'Дисконтной карты, сертификата или мобильного кода с таким номером не найдено.',
	], [
		'code'        => ErrorCode::RESTAURANT_NOT_FOUND,
		'description' => 'По такому коду ресторана не найдено.',
	]];
}
