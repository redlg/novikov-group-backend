<?php

namespace App\Models\Abstracts;

use App\Models\DeliveryOrder;
use App\Models\Guest;
use App\Models\LegalEntity;
use App\Models\Operation;
use App\Models\OperationType;
use App\Models\Restaurant;
use App\Models\Transaction;
use App\Traits\AddField;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class Card extends Model
{
	use SoftDeletes;
	use AddField;

	const NUMBER_LENGTH = 9; //длина номера карты
	const MAX_NUMBER = 999999999; //Самый большой номер карты
	const NOT_ACTIVATED = 'not_activated'; //контрагент ресторан еще не продал сертификат гостю и срок действия карты не начался
	const ACTIVATED_IN_OFFICE = 'activated_in_office'; //контрагент физическое лицо или юридическое лицо (партнер или контрагент), карта активируется в момент выпуска
	const ACTIVATED_IN_RESTAURANT = 'activated_in_restaurant'; // активирована в ресторане при прокатке карты
	const BLOCKED_MANUALLY = 'blocked_manually'; // карта заблокирована вручную в CRM
	const BLOCKED_EXPIRED = 'blocked_expired'; // закончился срок действия

	CONST RELATION_CERTIFICATE_ID = 'certificate';
	CONST RELATION_CERTIFICATE_NAME = 'Сертификат';
	CONST RELATION_DISCOUNT_CARD_ID = 'discount_card';
	CONST RELATION_DISCOUNT_CARD_NAME = 'Дисконтная карта';
	CONST RELATIONS = [[
			'id'   => self::RELATION_CERTIFICATE_ID,
			'name' => self::RELATION_CERTIFICATE_NAME,
		], [
			'id'   => self::RELATION_DISCOUNT_CARD_ID,
			'name' => self::RELATION_DISCOUNT_CARD_NAME,
		]];

    public $statuses = [
        Card::NOT_ACTIVATED           => 'Не активирована',
        Card::ACTIVATED_IN_OFFICE     => 'Активирована в офисе',
        Card::ACTIVATED_IN_RESTAURANT => 'Активирована в ресторане',
        Card::BLOCKED_MANUALLY        => 'Заблокирована вручную',
        Card::BLOCKED_EXPIRED         => 'Истекла',
    ];

    protected $dateFormat = 'Y-m-d H:i:sO';

	protected $fillable = [
		'card_type_id',
		'issuer_id',
		'contractor_id',
		'guest_id',
		'restaurant_id',
		'user_id',
		'number',
		'balance',
		'started_at',
		'ended_at',
		'blocked_at',
	];

	protected $hidden = [
		'external_id',
		'deleted_at',
		'updated_at',
	];

	protected $casts = [
		'card_type_id'  => 'int',
		'issuer_id'     => 'int',
		'contractor_id' => 'int',
		'guest_id'      => 'int',
		'restaurant_id' => 'int',
		'user_id'       => 'int',
	];

	protected $dates = [
		'started_at',
		'ended_at',
		'blocked_at',
	];

	protected $appends = ['status_name'];

	protected $with = ['guest', 'type'];

	public function guest()
	{
		return $this->belongsTo(Guest::class);
	}

	public function delivery_orders()
	{
		return $this->hasMany(DeliveryOrder::class);
	}

	public function transactions()
	{
		return $this->morphMany(Transaction::class, 'card');
	}

	public function operations()
	{
		return $this->morphMany(Operation::class, 'card');
	}

	public function contractor()
	{
		return $this->belongsTo(LegalEntity::class, 'contractor_id');
	}

	public function issuer()
	{
		return $this->belongsTo(LegalEntity::class, 'issuer_id');
	}

	public function restaurant()
	{
		return $this->belongsTo(Restaurant::class);
	}

	public function activate()
	{
		//активировать можно только неактивированную карту
		if ($this->status == Card::NOT_ACTIVATED) {
			$operation_type = OperationType::whereName(OperationType::ACTIVATION)->first();
			$operation = new Operation();
			$operation->operation_type_id = $operation_type->id;
			$operation->card_id = $this->id;
			$operation->card_type = get_class($this);
			$operation->user_id = \Auth::id();
			$operation->guest_id = $this->guest_id;
			$operation->save();

			$this->status = Card::ACTIVATED_IN_OFFICE;
			$this->started_at = Carbon::now();
			$this->ended_at = Carbon::now()->addDays($this->type->life_time);
			$this->save();
		}
	}

	public function block($message)
	{
		//заблокировать можно активированную или неактивированную карту
		if (
			$this->status == Card::ACTIVATED_IN_RESTAURANT
			|| $this->status == Card::ACTIVATED_IN_OFFICE
			|| $this->status == Card::NOT_ACTIVATED
		) {
			$operation_type = OperationType::whereName(OperationType::BLOCKING_MANUALLY)->first();
			$operation = new Operation();
			$operation->operation_type_id = $operation_type->id;
			$operation->card_id = $this->id;
			$operation->card_type = get_class($this);
			$operation->user_id = \Auth::id();
			$operation->guest_id = $this->guest_id;
			$operation->comment = $message;
			$operation->save();

			$this->status = Card::BLOCKED_MANUALLY;
			$this->blocked_at = Carbon::now();
			$this->save();
		}
	}

	public function unblock($message)
	{
		//разблокировать можно только заблокированную вручную карту
		if ($this->status == Card::BLOCKED_MANUALLY) {
			$operation_type = OperationType::whereName(OperationType::UNBLOCKING)->first();
			$operation = new Operation();
			$operation->operation_type_id = $operation_type->id;
			$operation->card_id = $this->id;
			$operation->card_type = get_class($this);
			$operation->user_id = \Auth::id();
			$operation->guest_id = $this->guest_id;
			$operation->comment = $message;
			$operation->save();

			$this->status = Card::ACTIVATED_IN_OFFICE;
			$this->blocked_at = null;
			$this->save();
		}
	}

	public function deactivate()
    {
        //деактивировать можно только активированную карту
        if ($this->status == Card::ACTIVATED_IN_RESTAURANT || $this->status == Card::ACTIVATED_IN_OFFICE) {
            $operation_type = OperationType::whereName(OperationType::BLOCKING_EXPIRED)->first();
            $operation = new Operation();
            $operation->operation_type_id = $operation_type->id;
            $operation->card_id = $this->id;
            $operation->card_type = get_class($this);
	        $operation->guest_id = $this->guest_id;
            $operation->save();

            $this->status = Card::BLOCKED_EXPIRED;
            $this->blocked_at = $this->ended_at;
            $this->save();
        }
    }

	public function isBlocked()
	{
		return $this->status == Card::BLOCKED_MANUALLY || $this->status == Card::BLOCKED_EXPIRED;
	}

	public function isActive()
	{
		return $this->status == Card::ACTIVATED_IN_OFFICE || $this->status == Card::ACTIVATED_IN_RESTAURANT;
	}

    public function getStatusNameAttribute()
    {
        return $this->status ? $this->statuses[$this->status] : null;
    }
}
