<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\CardOrder
 *
 * @property int $id
 * @property int $card_type_id
 * @property string $card_type_type
 * @property int $guest_id
 * @property int $product_delivery_method_id
 * @property int $address_id
 * @property string|null $comment
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $card_type
 * @property-read \App\Models\ProductDeliveryMethod $delivery
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CardOrder onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereCardTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereCardTypeType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereProductDeliveryMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\CardOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CardOrder withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CardOrder withoutTrashed()
 * @mixin \Eloquent
 */
class CardOrder extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'card_type_id',
    	'card_type_type',
    	'guest_id',
    	'address_id',
    	'product_delivery_method_id',
    	'comment',
    ];

    protected $casts = [
	    'card_type_id'               => 'int',
	    'guest_id'                   => 'int',
	    'address_id'                 => 'int',
	    'product_delivery_method_id' => 'int',
    ];

    protected $with = ['card_type', 'delivery'];

    public function card_type()
    {
    	return $this->morphTo();
    }

    public function delivery()
    {
    	return $this->belongsTo(ProductDeliveryMethod::class, 'product_delivery_method_id');
    }
}
