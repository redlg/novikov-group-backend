<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\PaymentMethod
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property bool $common
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Restaurant[] $restaurants
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Transaction[] $transactions
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentMethod onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentMethod whereCommon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentMethod whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentMethod whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentMethod whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PaymentMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentMethod withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\PaymentMethod withoutTrashed()
 * @mixin \Eloquent
 */
class PaymentMethod extends Model
{
	use SoftDeletes;

	const EXPENSES = 'Потраты гостя';
	const CASH = 'Наличные';
	const CARD = 'Банковская карта';
	const DISCOUNT_CARD = 'Дисконтная карта';
	const DISCOUNT_CARD_VIA_APP = 'Дисконтная карта через приложение';
	const CERTIFICATE = 'Сертификат';
	const CERTIFICATE_VIA_APP = 'Сертификат через приложение';

    protected $fillable = [
        'name_ru',
        'name_en',
        'common',
    ];

	protected $hidden = [
		'common',
		'created_at',
		'updated_at',
		'deleted_at',
		'pivot',
		'name',
	];
    
    protected $casts = [
        'common' => 'bool',
    ];

    protected $appends = ['name'];

    public function restaurants()
    {
        return $this->belongsToMany(Restaurant::class)->withTimestamps();
    }

    public function delivery_orders()
    {
        return $this->hasMany(DeliveryOrder::class);
    }

	public function transactions()
	{
		return $this->hasMany(Transaction::class);
	}

	public function getNameAttribute()
	{
		$name = 'name_' . request()->get('language');

		return $this->$name;
	}
}
