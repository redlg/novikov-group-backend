<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Chef
 *
 * @property int $id
 * @property int $image_id
 * @property string $first_name_ru
 * @property string $first_name_en
 * @property string $last_name_ru
 * @property string $last_name_en
 * @property string $description_ru
 * @property string|null $description_en
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Image|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Restaurant[] $restaurants
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chef onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereFirstNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereFirstNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereLastNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereLastNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Chef whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chef withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Chef withoutTrashed()
 * @mixin \Eloquent
 */
class Chef extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'image_id',
        'first_name_ru',
        'first_name_en',
        'last_name_ru',
        'last_name_en',
        'description_ru',
        'description_en',
    ];

    protected $casts = [
        'image_id' => 'int',
    ];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

    protected $with = ['image'];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    public function restaurants()
    {
        return $this->hasMany(Restaurant::class);
    }
}
