<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DiscountCode
 *
 * @property int $id
 * @property int $device_id
 * @property int $discount_card_type_id
 * @property int $discount_card_id
 * @property string $code
 * @property \Carbon\Carbon $expiration_at
 * @property-read \App\Models\DiscountCardType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereDeviceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereDiscountCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereDiscountCardTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereExpirationAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DiscountCode whereId($value)
 * @mixin \Eloquent
 */
class DiscountCode extends Model
{
	const TTL = 5; //in minutes
	const CODE_LENGTH = 6; //in symbols

	public $timestamps = false;

    protected $casts = [
	    'device_id'             => 'int',
	    'discount_card_type_id' => 'int',
	    'discount_card_id'      => 'int',
    ];

    protected $dates = ['expiration_at'];

    protected $with = ['type'];

	public function type()
	{
		return $this->belongsTo(DiscountCardType::class, 'discount_card_type_id');
	}
}
