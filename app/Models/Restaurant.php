<?php

namespace App\Models;

use App\Scopes\RestaurantScope;
use App\Traits\AddField;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Restaurant
 *
 * @property int $id
 * @property int $legal_entity_id
 * @property int $logo_id
 * @property int $address_id
 * @property int $chef_id
 * @property string $name_ru
 * @property string $name_en
 * @property string|null $phone
 * @property string|null $phone_string
 * @property string|null $work_time_weekdays_from
 * @property string|null $work_time_weekdays_to
 * @property string|null $work_time_friday_from
 * @property string|null $work_time_friday_to
 * @property string|null $work_time_saturday_from
 * @property string|null $work_time_saturday_to
 * @property string|null $work_time_sunday_from
 * @property string|null $work_time_sunday_to
 * @property string|null $work_time_string
 * @property bool $last_people
 * @property string $description_ru
 * @property string|null $description_en
 * @property string|null $announce_ru
 * @property string|null $announce_en
 * @property bool $parking
 * @property bool $delivery
 * @property string|null $delivery_phone
 * @property int $min_order_check
 * @property int $average_check
 * @property string|null $average_check_string
 * @property int $number_of_seats
 * @property string|null $language_hash
 * @property string|null $external_id
 * @property string|null $resto_id
 * @property string|null $iiko_id
 * @property string|null $rkeeper_id
 * @property string|null $temp_id
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Address $address
 * @property-read \App\Models\Chef|null $chef
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryCategory[] $delivery_categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryEmail[] $delivery_emails
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryCategory[] $delivery_menu
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryOrder[] $delivery_orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\DeliveryDishesCompilation[] $dishes_compilations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read mixed $announce
 * @property-read array $compilation_title_image
 * @property-read mixed $delivery_payment_services
 * @property-read mixed $delivery_services
 * @property-read mixed $description
 * @property-read array $detail_images
 * @property-read mixed $favorite
 * @property-read mixed $kitchen
 * @property-read array $list_title_image
 * @property-read mixed $name
 * @property-read mixed $payment_services
 * @property-read mixed $work_time_from
 * @property-read mixed $work_time_to
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kitchen[] $kitchens
 * @property-read \App\Models\LegalEntity $legal_entity
 * @property-read \App\Models\Image $logo
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuCategory[] $menu
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MenuCategory[] $menu_categories
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Operation[] $operations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PaymentMethod[] $payment_methods
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Reservation[] $reservations
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SocialNetworkLink[] $social_links
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tag[] $tags
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant addField($relation_name, $operator = '=', $type = 'left', $where = false)
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Restaurant onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereAnnounceEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereAnnounceRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereAverageCheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereAverageCheckString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereChefId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereDeliveryPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereDescriptionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereDescriptionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereExternalId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereIikoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereLanguageHash($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereLastPeople($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereLegalEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereLogoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereMinOrderCheck($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereNameEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereNameRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereNumberOfSeats($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereParking($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant wherePhoneString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereRestoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereRkeeperId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereTempId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeFridayFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeFridayTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeSaturdayFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeSaturdayTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeString($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeSundayFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeSundayTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeWeekdaysFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Restaurant whereWorkTimeWeekdaysTo($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Restaurant withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Restaurant withoutTrashed()
 * @mixin \Eloquent
 */
class Restaurant extends Model
{
	use SoftDeletes;
	use AddField;

	const LIST_IMAGES_PATH = 'upload/resized_images/restaurants/list/';
	const COMPILATION_IMAGES_PATH = 'upload/resized_images/restaurants/compilation/';
	const DETAIL_IMAGES_PATH = 'upload/resized_images/restaurants/detail/';

	protected $fillable = [
        'legal_entity_id',
        'logo_id',
        'address_id',
        'chef_id',
        'name_ru',
        'name_en',
        'phone',
	    'phone_string',
        'work_time_string',
		'work_time_weekdays_from',
		'work_time_weekdays_to',
		'work_time_friday_from',
		'work_time_friday_to',
		'work_time_saturday_from',
		'work_time_saturday_to',
		'work_time_sunday_from',
		'work_time_sunday_to',
        'last_people',
        'description_ru',
        'description_en',
		'announce_ru',
		'announce_en',
        'parking',
        'delivery',
        'delivery_phone',
        'delivery_mail',
	    'min_order_check',
        'average_check',
        'average_check_string',
        'number_of_seats',
        'rkeeper_id',
    ];

    protected $hidden = [
	    'name',
	    'description',
	    'announce',
	    'kitchen',
        'work_time_from',
        'work_time_to',
    	'compilation_title_image',
    	'list_title_image',
    	'detail_images',
	    'favorite',
		'payment_methods',
	    'created_at',
	    'updated_at',
	    'deleted_at',
        'external_id',
	    'temp_id',
	    'language_hash',
        'resto_id',
        'iiko_id',
    ];

    protected $casts = [
	    'legal_entity_id'      => 'int',
	    'logo_id'              => 'int',
	    'address_id'           => 'int',
	    'chef_id'              => 'int',
	    'last_people'          => 'bool',
	    'parking'              => 'bool',
	    'delivery'             => 'bool',
	    'number_of_seats'      => 'int',
	    'min_order_check'      => 'int',
	    'average_check_mobile' => 'int',
    ];

    protected $with = [
		'logo', 
		'images', 
		'address',
        'delivery_emails',
		'tags', 
		'kitchens',
		'payment_methods',
	];

    protected $appends = [
    	'name',
	    'description',
	    'announce',
	    'kitchen',
        'work_time_from',
        'work_time_to',
		'favorite', 
		'list_title_image', 
		'compilation_title_image', 
		'detail_images',
	    'delivery_services',
		'payment_services',
	    'delivery_payment_services',
	];

	protected static function boot()
	{
		parent::boot();

		static::addGlobalScope(new RestaurantScope);
	}

    public function legal_entity()
    {
        return $this->belongsTo(LegalEntity::class);
    }

    public function logo()
    {
        return $this->belongsTo(Image::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function delivery_emails()
    {
        return $this->hasMany(DeliveryEmail::class);
    }

    public function chef()
    {
        return $this->belongsTo(Chef::class);
    }

	public function tags()
	{
		return $this->morphToMany(Tag::class, 'taggable')->withTimestamps();
	}

    public function images()
    {
        return $this->belongsToMany(Image::class)->withTimestamps();
    }

    public function social_links()
    {
        return $this->hasMany(SocialNetworkLink::class);
    }

	public function kitchens()
	{
		return $this->belongsToMany(Kitchen::class)->withTimestamps();
	}

    public function menu()
    {
        return $this->hasMany(MenuCategory::class)->whereNull('menu_category_id')->with('categories');
    }

	public function menu_categories()
	{
		return $this->hasMany(MenuCategory::class);
	}

	public function delivery_menu()
	{
		return $this->hasMany(DeliveryCategory::class)->with('dishes');
	}

	public function delivery_categories()
	{
		return $this->hasMany(DeliveryCategory::class);
	}

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function delivery_orders()
    {
        return $this->hasMany(DeliveryOrder::class);
    }

    public function events()
    {
        return $this->belongsToMany(Event::class)->withTimestamps();
    }

    public function operations()
    {
    	return $this->hasMany(Operation::class);
    }

    public function dishes_compilations()
    {
    	return $this->hasMany(DeliveryDishesCompilation::class);
    }

	public function payment_methods()
	{
		return $this->belongsToMany(PaymentMethod::class)->withPivot(['disable'])->withTimestamps();
	}

    public function getNameAttribute()
    {
    	$language = request()->get('language') ?: 'ru';
    	$name = 'name_' . $language;

    	return $this->$name;
    }

	public function getDescriptionAttribute()
	{
		$description = 'description_' . request()->get('language');

		return $this->$description;
	}

	public function getAnnounceAttribute()
	{
		$announce = 'announce_' . request()->get('language');

		return $this->$announce;
	}

	public function getKitchenAttribute()
	{
		$kitchen = $this->kitchens->each(function(Kitchen $kitchen) {
			$kitchen
				->makeVisible([
					'name',
				])
				->makeHidden([
					'name_ru',
					'name_en',
				]);
		});

		return $kitchen;
	}

	public function getDeliveryServicesAttribute()
	{
		return DeliveryMethod::all();
	}

	public function getDeliveryPaymentServicesAttribute()
	{
		return DeliveryPaymentMethod::all();
	}

	public function getPaymentServicesAttribute()
	{
		$common_methods = PaymentMethod::whereCommon(true)->get()
			->mapWithKeys(function($method) {
				return [$method['id'] => $method];
			});

		$restaurant_methods = $this->payment_methods;
		foreach ($restaurant_methods as $method) {
			if ($common_methods->has($method->id) && $method->pivot->disable) {
				$common_methods->forget($method->id);
			} else {
				$common_methods->put($method->id, $method);
			}
		}

		$common_methods->each(function(PaymentMethod $method) {
			$method
				->makeVisible([
					'name',
				])
				->makeHidden([
					'name_ru',
					'name_en',
				]);
		});

		return $common_methods->flatten();
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function getListTitleImageAttribute()
	{
		$images[] = $this->images->first();

		return make_resize_images(collect($images), self::LIST_IMAGES_PATH, 560)[0];
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function getCompilationTitleImageAttribute()
	{
		$images[] = $this->images->first();

		return make_resize_images(collect($images), self::COMPILATION_IMAGES_PATH, 960)[0];
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function getDetailImagesAttribute()
	{
		return make_resize_images($this->images, self::DETAIL_IMAGES_PATH, 960);
	}

    public function getFavoriteAttribute()
    {
    	$ids = request()->get('favorite_restaurant_ids', collect())->toArray();

    	return !is_null($ids) && in_array($this->id, $ids);
    }

    public function getWorkTimeFromAttribute()
    {
        $now = Carbon::now();
        if ($now->isFriday()) {
            return $this->work_time_friday_from;
        } elseif ($now->isSaturday()) {
            return $this->work_time_saturday_from;
        } elseif ($now->isSunday()) {
            return $this->work_time_sunday_from;
        } else {
            return $this->work_time_weekdays_from;
        }
    }

    public function getWorkTimeToAttribute()
    {
        $now = Carbon::now();
        if ($now->isFriday()) {
            return $this->work_time_friday_to;
        } elseif ($now->isSaturday()) {
            return $this->work_time_saturday_to;
        } elseif ($now->isSunday()) {
            return $this->work_time_sunday_to;
        } else {
            return $this->work_time_weekdays_to;
        }
    }
}
