<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\TicketTypeOrder
 *
 * @property int $id
 * @property int $ticket_type_id
 * @property int $product_delivery_method_id
 * @property int $guest_id
 * @property string|null $server_url
 * @property float $cost
 * @property string $customer_name
 * @property string $customer_phone
 * @property string|null $customer_email
 * @property int $order_id
 * @property int $invoice_id
 * @property bool $paid
 * @property string|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\ProductDeliveryMethod $delivery
 * @property-read \App\Models\Guest|null $guest
 * @property-read \App\Models\TicketType $type
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TicketTypeOrder onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereCustomerEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereCustomerName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereCustomerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereGuestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereInvoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereProductDeliveryMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereServerUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereTicketTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TicketTypeOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TicketTypeOrder withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Models\TicketTypeOrder withoutTrashed()
 * @mixin \Eloquent
 */
class TicketTypeOrder extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'ticket_type_id',
	    'product_delivery_method_id',
        'guest_id',
	    'server_url',
        'cost',
        'customer_name',
        'customer_phone',
        'order_id',
        'invoice_id',
	    'paid',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
	    'ticket_type_id'             => 'int',
	    'product_delivery_method_id' => 'int',
	    'guest_id'                   => 'int',
	    'cost'                       => 'float',
	    'paid'                       => 'bool',
    ];

    protected $with = ['type', 'delivery', 'guest'];

    public function type()
    {
        return $this->belongsTo(TicketType::class, 'ticket_type_id');
    }

    public function delivery()
    {
    	return $this->belongsTo(ProductDeliveryMethod::class, 'product_delivery_method_id');
    }

    public function guest()
    {
        return $this->belongsTo(Guest::class);
    }
}
