<?php

/*
|--------------------------------------------------------------------------
| Mobile Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'IndexController@index');
Route::post('device/auth', 'DeviceController@auth');

Route::group(['middleware' => ['mobile.device', 'language']], function () {
	//Device
	Route::get('device/profile', 'DeviceController@getProfile');
	Route::get('device/orders', 'DeviceController@getOrders');
	Route::get('device/cards', 'DeviceController@getCards');
	Route::get('device/discount-card', 'DeviceController@getDiscountCardInfo');
	Route::get('device/purchase-card', 'DeviceController@getPurchaseInformation');
	Route::post('device/profile', 'DeviceController@updateProfile');
	Route::post('device/add-favorite-restaurant', 'DeviceController@addRestaurantToFavorites');
	Route::post('device/delete-favorite-restaurant', 'DeviceController@deleteRestaurantFromFavorites');
	Route::post('device/add-favorite-dish', 'DeviceController@addDishToFavorites');
	Route::post('device/delete-favorite-dish', 'DeviceController@deleteDishFromFavorites');
	Route::post('device/get-code', 'DeviceController@getCode');
	Route::post('device/register', 'DeviceController@register');
	Route::post('device/logout', 'DeviceController@logout');

	//Restaurant
	Route::get('restaurant/bulk', 'RestaurantController@bulk');
	Route::get('restaurant/get-nearest', 'RestaurantController@getNearest');
	Route::get('restaurant/delivery-methods', 'RestaurantController@getDeliveryMethods');
	Route::get('restaurant/delivery-payment-methods', 'RestaurantController@getDeliveryPaymentMethods');
	Route::get('restaurant/{restaurant}/payment-methods', 'RestaurantController@getPaymentMethods');
	Route::apiResource('restaurant', 'RestaurantController', ['only' => ['index', 'show']]);

	//Event
	Route::apiResource('event', 'EventController', ['only' => ['index', 'show']]);

	//Partners
	Route::apiResource('partner', 'PartnerController', ['only' => ['index']]);

	//Delivery Order
	Route::apiResource('delivery-order', 'DeliveryOrderController', ['only' => ['show', 'store']]);
	
	//Delivery Dish
	Route::get('delivery-dish/bulk', 'DeliveryDishController@bulk');
	Route::apiResource('delivery-dish', 'DeliveryDishController', ['only' => ['index', 'show']]);

	//Compilation
	Route::apiResource('compilation/restaurants', 'RestaurantsCompilationController', ['only' => ['index']]);
	Route::apiResource('compilation/dishes', 'DeliveryDishesCompilationController', ['only' => ['index']]);
	Route::get('compilation/delivery', 'CompilationsCompilationController@getCompilationsForDelivery');

	//Discount Code
	Route::apiResource('discount-code', 'DiscountCodeController', ['only' => ['store']]);

	//Search
	Route::apiResource('search', 'SearchController', ['only' => ['index']]);

	//Project
	Route::apiResource('project', 'ProjectController', ['only' => ['index']]);

	//Ticket Type Order
	Route::post('ticket/buy', 'TicketTypeOrderController@buy');

	//Product Delivery Method
	Route::apiResource('product-delivery-method', 'ProductDeliveryMethodController', ['only' => ['index']]);
});