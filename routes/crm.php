<?php

/*
|--------------------------------------------------------------------------
| CRM Routes
|--------------------------------------------------------------------------
*/

use App\Models\Permission;

Route::get('/', 'IndexController@index');
Route::get('ticket/invoice-success', 'TicketTypeOrderController@invoiceSuccess'); //внешний сервис подтверждает покупку билетов на события

Route::post('auth/login', 'AuthController@login');
Route::post('auth/refresh-token', 'AuthController@refreshToken');

Route::group(['middleware' => ['language', 'jwt.auth']], function () {
	//Image
	Route::group(['middleware' => 'permission:' . Permission::IMAGES_EDIT], function() {
		Route::get('image/filters', 'ImageController@getFilters');
		Route::apiResource('image', 'ImageController', ['only' => ['store']]);
	});

	//Address
	Route::group(['middleware' => 'permission:' . Permission::ADDRESSES_EDIT], function() {
		Route::get('address/filters', 'AddressController@getFilters');
		Route::apiResource('address', 'AddressController', ['except' => ['index']]);
	});

	//Chef
	Route::group(['middleware' => 'permission:' . Permission::RESTAURANTS_EDIT], function() {
		Route::apiResource('chef', 'ChefController', ['except' => ['index']]);
	});

	//Tag
	Route::group(['middleware' => 'permission:' . Permission::TAGS_EDIT], function() {
		Route::apiResource('tag', 'TagController');
	});

	//Kitchen
	Route::group(['middleware' => 'permission:' . Permission::RESTAURANTS_EDIT], function() {
		Route::apiResource('kitchen', 'KitchenController');
	});

	//Guest
	Route::group(['middleware' => 'permission:' . Permission::GUESTS_EXPORT], function() {
		Route::get('guest/export', 'GuestController@export');
	});
	Route::group(['middleware' => 'permission:' . Permission::GUESTS_EDIT], function() {
		Route::apiResource('guest', 'GuestController');
	});

	//Legal Entity
	Route::apiResource('legal-entity', 'LegalEntityController', ['only' => ['index']]);
	Route::group(['middleware' => 'permission:' . Permission::LEGAL_ENTITIES_EDIT], function() {
		Route::get('legal-entity/filters', 'LegalEntityController@getFilters');
		Route::apiResource('legal-entity', 'LegalEntityController', ['only' => ['store', 'show', 'update', 'destroy']]);
	});

	//Compilation
	Route::group(['middleware' => 'permission:' . Permission::COMPILATIONS_EDIT], function() {
		Route::get('compilation/filters', 'CompilationsCompilationController@getFilters');
		Route::apiResource('compilation/compilations', 'CompilationsCompilationController', ['only' => ['index', 'show', 'update']]);
		Route::apiResource('compilation/restaurants', 'RestaurantsCompilationController');
		Route::apiResource('compilation/dishes', 'DeliveryDishesCompilationController');
	});

	//Restaurant
	Route::group(['middleware' => 'permission:' . Permission::RESTAURANTS_CREATE], function() {
		Route::apiResource('restaurant', 'RestaurantController', ['only' => ['store', 'delete']]);
	});
	Route::group(['middleware' => 'permission:' . Permission::RESTAURANTS_EDIT], function() {
		Route::get('restaurant/filters', 'RestaurantController@getFilters');
		Route::get('restaurant/values', 'RestaurantController@getValues');
		Route::apiResource('restaurant', 'RestaurantController', ['only' => ['index', 'show', 'update']]);
	});

	//Delivery Email
	Route::group(['middleware' => 'permission:' . Permission::RESTAURANTS_EDIT], function() {
		Route::apiResource('delivery-email', 'DeliveryEmailController', ['only' => ['store', 'update', 'destroy']]);
	});

	//Delivery Order
	Route::group(['middleware' => 'permission:' . Permission::RESTAURANTS_EDIT], function() {
		Route::get('delivery-order/filters', 'DeliveryOrderController@getFilters');
		Route::post('delivery-order/{id}/cancel', 'DeliveryOrderController@cancel');
		Route::post('delivery-order/{id}/complete', 'DeliveryOrderController@complete');
		Route::apiResource('delivery-order', 'DeliveryOrderController');
	});

	//Delivery Dish & Menu Dish
	Route::group(['middleware' => 'permission:' . Permission::RESTAURANTS_EDIT], function() {
		Route::apiResource('delivery-dish', 'DeliveryDishController');
		Route::apiResource('delivery-category', 'DeliveryCategoryController');
		Route::apiResource('menu-dish', 'MenuDishController');
		Route::apiResource('menu-category', 'MenuCategoryController');
	});

	//Events
	Route::group(['middleware' => 'permission:' . Permission::EVENTS_EDIT], function() {
		Route::get('event/filters', 'EventController@getFilters');
		Route::apiResource('event', 'EventController');
		Route::apiResource('event-type', 'EventTypeController');
	});

	//Ticket Type
	Route::group(['middleware' => 'permission:' . Permission::EVENTS_EDIT], function() {
		Route::apiResource('ticket-type', 'TicketTypeController');
	});

	//Ticket Type Order
	Route::group(['middleware' => 'permission:' . Permission::PRODUCTS_EDIT], function() {
		Route::apiResource('ticket', 'TicketTypeOrderController', ['except' => ['store']]);
	});

	//Partner
	Route::group(['middleware' => 'permission:' . Permission::PARTNERS_EDIT], function() {
		Route::apiResource('partner', 'PartnerController');
	});

	//Project
	Route::group(['middleware' => 'permission:' . Permission::PROJECTS_EDIT], function() {
		Route::apiResource('project', 'ProjectController');
	});

	//Discount Card & Certificate
	Route::group(['middleware' => 'permission:' . Permission::CARDS_EXPORT], function() {
		Route::get('certificate/export', 'CertificateController@export');
		Route::get('discount-card/export', 'DiscountCardController@export');
	});
	Route::group(['middleware' => 'permission:' . Permission::CARDS_EDIT], function() {
		Route::post('discount-card/{id}/activate', 'DiscountCardController@activate');
		Route::post('discount-card/{id}/block', 'DiscountCardController@block');
		Route::post('discount-card/{id}/unblock', 'DiscountCardController@unblock');
		Route::post('certificate/{id}/activate', 'CertificateController@activate');
		Route::post('certificate/{id}/block', 'CertificateController@block');
		Route::post('certificate/{id}/unblock', 'CertificateController@unblock');
		Route::post('discount-card/bulk-create', 'DiscountCardController@bulkStore');
		Route::post('discount-card/mass-create', 'DiscountCardController@massStore');
		Route::post('certificate/bulk-create', 'CertificateController@bulkStore');
		Route::post('certificate/mass-create', 'CertificateController@massStore');
		Route::get('discount-card/statuses', 'DiscountCardController@getStatuses');
		Route::apiResource('discount-card', 'DiscountCardController');
		Route::apiResource('discount-card-type', 'DiscountCardTypeController');
        Route::get('certificate/statuses', 'CertificateController@getStatuses');
		Route::apiResource('certificate', 'CertificateController');
		Route::apiResource('certificate-type', 'CertificateTypeController');
	});

	//Card Order
	Route::group(['middleware' => 'permission:' . Permission::PRODUCTS_EDIT], function() {
		Route::apiResource('card-order', 'CardOrderController');
	});

	//Operation & Transaction
	Route::group(['middleware' => 'permission:' . Permission::OPERATIONS_EXPORT], function() {
		Route::get('operation/export', 'OperationController@export');
		Route::get('transaction/export', 'TransactionController@export');
	});
	Route::group(['middleware' => 'permission:' . Permission::OPERATIONS_READ], function() {
		Route::get('operation/filters', 'OperationController@getFilters');
		Route::get('transaction/filters', 'TransactionController@getFilters');
		Route::apiResource('operation', 'OperationController', ['only' => ['index', 'show']]);
		Route::apiResource('transaction', 'TransactionController', ['only' => ['index', 'show', 'store']]);
	});

	//User
	Route::get('user/me', 'UserController@me');
	Route::group(['middleware' => 'permission:' . Permission::USERS_EDIT], function() {
		Route::get('user/roles', 'UserController@getRolesAndPermissions');
		Route::apiResource('user', 'UserController');
	});

	//Reservation
	Route::apiResource('reservation', 'ReservationController');

	//Error Code
	Route::apiResource('error-code', 'ErrorCodeController', ['only' => ['index']]);

	//Export
	Route::get('export', 'ExportController@checkFileExistence');
});