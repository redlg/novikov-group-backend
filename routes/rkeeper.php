<?php

/*
|--------------------------------------------------------------------------
| Rkeeper Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'IndexController@index');

Route::apiResource('cardinfo', 'CardController', ['only' => ['store']]);

Route::apiResource('accumulatereverse', 'TransactionController', ['only' => ['store']]);
